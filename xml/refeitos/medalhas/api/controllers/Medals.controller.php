<?php

class MedalsController
{
    public $paises = [];
    private $countrys;
    private $results;
    private $skill;

    public function __construct()
    {
        $this->countrys = simplexml_load_file("../../xml/WSC-Countrys.xml");
        $this->results = simplexml_load_file("../../xml/WSC-Results.xml");
        $this->skill = simplexml_load_file("../../xml/WSC-Skills.xml");
    }

    private function exibir()
    {

        if ($this->countrys) {
            foreach ($this->countrys as $c) {

                array_push($this->paises, [
                    "iso" => $c->iso,
                    "name" => $c->{'name-en'}
                ]);
            }

            return $this->paises;
        }
    }

    public function returnExibir()
    {
        return $this->exibir();
    }

    private function trazer($el)
    {
        $resultados = array();
        $resultadosSkills = array();

        foreach ($this->results->result as $resultado) {
            $country = $resultado->{'country-iso'};

            if ($el === (string)$country) {
                $year = $resultado->year;
                $medal = $resultado->medal;
                $skill_number = $resultado->{'skill-number'};


                foreach ($this->skill->skill as $s) {
                    $skill = $s->number;

                    if ((string)$skill === (string)$skill_number) {
                        array_push($resultadosSkills, $skill);
                        $modalidade = $s->{'name-en'};

                        if ($medal == "") {
                            $medal = "-";
                        }

                        $resultados["$skill"] = [
                            "name" => $modalidade,
                            "resultados" => [
                                "2007" => $year == '2007' ? "$medal" : ($resultados["$skill"]["resultados"]["2007"] ? $resultados["$skill"]["resultados"]["2007"] : "-"),
                                "2009" => $year == '2009' ? "$medal" : ($resultados["$skill"]["resultados"]["2009"] ? $resultados["$skill"]["resultados"]["2009"] : "-"),
                                "2011" => $year == '2011' ? "$medal" : ($resultados["$skill"]["resultados"]["2011"] ? $resultados["$skill"]["resultados"]["2011"] : "-")
                            ]
                        ];
                    }
                }
            }
        }
        return array($resultados, $resultadosSkills);
    }

    public function returnTrazer($el)
    {
        return $this->trazer($el);
    }

}

//echo "<pre>";
//$el = "CH";
//$let = new MedalsController();
////$let->returnTrazer($el);
//print_r($let->returnTrazer($el));
