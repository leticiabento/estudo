<?php

header("Content-type: application/json");
error_reporting(0);
if ($_SERVER['REQUEST_METHOD'] === "GET") {
    $el = $_GET['el'];

    include_once "../autoload.php";

    $medalsController = new MedalsController();
    echo json_encode(
        array(
            'success' => true,
            'data' => $medalsController->returnTrazer($el)
        )
    );
}