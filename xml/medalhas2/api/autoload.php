<?php

function autoload($class) {
    $class = explode("Controller", $class);
    include_once "controllers/" . $class[0] . ".controller.php";
}

spl_autoload_register('autoload');