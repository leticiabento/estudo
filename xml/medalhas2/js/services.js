function Medals() {
    this.exibir = function () {
        $.ajax({
            url: "api/views/exibir.php",
            type: "GET"
        }).done(function (res) {
            res.map(function (item) {
                $("#paises").append(
                    "<option value='" + item.iso[0] + "'>" + item.iso[0] + " - " + item.name[0] + "</option>"
                );
            })

        })
    }

    this.trazer = function (element) {
        $.ajax({
            url: "api/views/trazer.php?el=" + element,
            type: "GET"
        }).done(function (res) {
            var data, ano2007, ano2009, ano2011;

            $("tbody, tfoot td").empty();
            if (res.success) {
                res.data[1].map(function (item, index) {

                    if (res.data[0][item[0]]) {
                        data = res.data[0][item[0]];
                        ano2007 = data.resultados['2007'] !== "-" ? "ano2007" : "";
                        ano2009 = data.resultados['2009'] !== "-" ? "ano2009" : "";
                        ano2011 = data.resultados['2011'] !== "-" ? "ano2011" : "";

                        $("tbody").append(
                            '<tr>' +
                            '<td>' + data.name[0] + '</td>' +
                            '<td class="' + ano2007 + '">' + data.resultados['2007'] + '</td>' +
                            '<td class="' + ano2009 + '">' + data.resultados['2009'] + '</td>' +
                            '<td class="' + ano2011 + '">' + data.resultados['2011'] + '</td>' +
                            '</tr>'
                        );

                    }
                })
            }

            $(".2007").append($(".ano2007").length);
            $(".2009").append($(".ano2009").length);
            $(".2011").append($(".ano2011").length);


        })
    }
}