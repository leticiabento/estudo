function Equipamento() {
    this.exibir = function () {
        $.ajax({
            url: "api/equipamentos/central_eletronica.php",
            type: "GET"
        }).done(function (res) {
            var data = res.data;
            var info;
            $(".container-comodo").html("");

            $(".dataAtualizacao").text(res.dataAtualizacao[0]);

            for (var i = 0; i < data.length; i++) {
                info = data[i][0];

                $(".container-comodo").append(
                    '<div class="comodo" >' +
                    '        <div class="tituloComodo" onclick="abrirComodo(this)">' +
                    '            <span class="nameComodo padding">' + info.name + '</span>' +
                    '            <span class="seta"></span>' +
                    '        </div>' +
                    '    </div>'
                );

                info.equipamento.map(function (item) {

                    $(".container-comodo .comodo").eq(i).append(
                        '<li class="lista-comodo padding">' +
                        '<span>' + item.name + ' </span>' +
                        '<button onclick="trigger(this, ' + item.id + ')" class="equipamento ' + item.status + ' ' + item['@attributes'].type + '" id="' + item.id + '"><p>' + item.status + '</p></button>' +
                        '</li>'
                    );
                });

                $(".container-comodo .comodo").eq(i).append(
                    '<div class="sensor">' +
                    '<span class="status">' + info.sensor.status + '</span>' +
                    '<span class="scale">' + info.sensor['@attributes'].scale + '</span>' +
                    '</div>'
                );


            }

        })
    }

    this.modificar = function (id) {
        $.ajax({
            url: "api/equipamentos/modificar.php?id=" + id,
            type: "GET"
        }).done(function (res) {

            if (res) {
                equipamento.exibir();
            }
        })
    }


}

