<?php

include_once "Conexao.class.php";

class EquipamentoModel
{

    private $xml;
    private $infos = [];

    public function __construct()
    {
        $this->xml = simplexml_load_file("../xml/estrutura_xml.xml");
    }

    private function exibirEquipamentosModel()
    {
        if ($this->xml) {
            $data = $this->xml->{'dataAtualizacao'};
            $comodos = array();

            foreach ($this->xml->{'comodo'} as $value) {
                array_push($comodos, array($value));
            }

            return array('data' => $comodos, 'dataAtualizacao' => $data);
        }
    }

    public function returnExibirEquipamentosModel()
    {
        return $this->exibirEquipamentosModel();
    }

    private function modificarModel()
    {
        if ($this->xml) {
            return $this->xml;
        }
    }

    public function returnModificarModel()
    {
        return $this->modificarModel();
    }

}