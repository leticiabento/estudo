<?php

    header("Content-type: application/json");
    if($_SERVER['REQUEST_METHOD']=="GET") {

        include_once "../autoload.php";

        $equipamentoController = new EquipamentoController();
        echo json_encode($equipamentoController->returnExibirEquipamentos());
    }