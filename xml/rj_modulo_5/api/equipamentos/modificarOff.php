<?php

header("Content-type: application/json");
if($_SERVER['REQUEST_METHOD']=="GET") {

    $id = $_GET['id'];
    include_once "../autoload.php";

    $equipamentoController = new EquipamentoController();
    echo json_encode($equipamentoController->returnModificarOff($id));
}