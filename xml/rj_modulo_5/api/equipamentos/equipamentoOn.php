<?php

$id = "887212331";

$doc = simplexml_load_file("../xml/estrutura_xml.xml");

$xml = new DOMDocument("1.0", 'utf-8');
$xml->formatOutput = true;

$dataAtual = date("Y-m-d H:i:s");

$domoticasenai = $xml->createElement("domoticasenai");
$xml->appendChild($domoticasenai);

$data = $xml->createElement("dataAtualizacao", $dataAtual);
$domoticasenai->appendChild($data);

foreach ($doc as $value) {


    if ($value->{'name'}) {

        $comodo = $xml->createElement("comodo");
        $domoticasenai->appendChild($comodo);

        $name = $xml->createElement("name", (string)$value->{'name'});
        $comodo->appendChild($name);

        foreach ($value->{'equipamento'} as $value1) {

            $equipamento = $xml->createElement("equipamento");
            $comodo->appendChild($equipamento);
            $equipamento->setAttribute('type', $value1['type']);

            $idEquipamento = $xml->createElement("id", $value1->{'id'});
            $equipamento->appendChild($idEquipamento);

            $name = $xml->createElement("name", $value1->{'name'});
            $equipamento->appendChild($name);


            if ($id === (string)$value1->{'id'}) {
                switch ($value1->{'status'}) {
                    case "on":
                        $status = $xml->createElement("status", "off");
                        break;

                    case "off":
                        $status = $xml->createElement("status", "on");
                        break;

                    case "locked":
                        $status = $xml->createElement("status", "unlocked");
                        break;

                    case "unlocked":
                        $status = $xml->createElement("status", "locked");
                        break;

                    default:
                        $status = $xml->createElement("status", $value1->{'status'});
                        break;
                }

            } else {
                $status = $xml->createElement("status", $value1->{'status'});
            }

            $equipamento->appendChild($status);

        }

    }
}


//header("Content-type: text/xml");
$xml->save("../xml/estrutura_xml.xml");
//print $xml->saveXML();

