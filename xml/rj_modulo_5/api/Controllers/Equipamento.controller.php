<?php

include_once "../Models/Equipamento.class.php";

class EquipamentoController
{

    private $equipamentoModel;

    public function __construct()
    {
        $this->equipamentoModel = new EquipamentoModel();
    }

    private function exibirEquipamentos()
    {
        return $this->equipamentoModel->returnExibirEquipamentosModel();
    }

    public function returnExibirEquipamentos()
    {
        return $this->exibirEquipamentos();
    }

    private function modificar($id)
    {
        $doc = $this->equipamentoModel->returnModificarModel();

        $xml = new DOMDocument("1.0", 'utf-8');
        $xml->formatOutput = true;

        $dataAtual = date("Y-m-d H:i:s");

        $domoticasenai = $xml->createElement("domoticasenai");
        $xml->appendChild($domoticasenai);

        $data = $xml->createElement("dataAtualizacao", $dataAtual);
        $domoticasenai->appendChild($data);

        foreach ($doc as $value) {

            if ($value->{'name'}) {

                $comodo = $xml->createElement("comodo");
                $domoticasenai->appendChild($comodo);

                $name = $xml->createElement("name", (string)$value->{'name'});
                $comodo->appendChild($name);

                foreach ($value->{'equipamento'} as $value1) {

                    $equipamento = $xml->createElement("equipamento");
                    $comodo->appendChild($equipamento);
                    $equipamento->setAttribute('type', $value1['type']);

                    $idEquipamento = $xml->createElement("id", $value1->{'id'});
                    $equipamento->appendChild($idEquipamento);

                    $name = $xml->createElement("name", $value1->{'name'});
                    $equipamento->appendChild($name);


                    if ($id === (string)$value1->{'id'}) {
                        switch ($value1->{'status'}) {
                            case "on":
                                $status = $xml->createElement("status", "off");
                                break;

                            case "off":
                                $status = $xml->createElement("status", "on");
                                break;

                            case "locked":
                                $status = $xml->createElement("status", "unlocked");
                                break;

                            case "unlocked":
                                $status = $xml->createElement("status", "locked");
                                break;

                            default:
                                $status = $xml->createElement("status", $value1->{'status'});
                                break;
                        }

                    } else {
                        $status = $xml->createElement("status", $value1->{'status'});
                    }


                    $equipamento->appendChild($status);


                }

                $sensor = $xml->createElement("sensor");
                $comodo->appendChild($sensor);
                $sensor->setAttribute('type', $doc->{"comodo"}->{"sensor"}['type']);
                $sensor->setAttribute('scale', $doc->{"comodo"}->{"sensor"}['scale']);

                $status = $xml->createElement("status", $doc->{"comodo"}->{"sensor"}->{'status'});
                $sensor->appendChild($status);

            }

        }

        return $xml->save("../xml/estrutura_xml.xml");
    }

    public function returnModificar($id)
    {
        return $this->modificar($id);
    }

}

//echo "<pre>";
//$let = new EquipamentoController();
//$let->returnModificar("8887182777631");