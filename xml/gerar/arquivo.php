<?php

$data = [
    "itinerarios" =>
        [
            "idlinha" => 1,
            "descricao" => "Descricao da linha",
            "local_origem" => [
                "idlocal" => 1,
                "descricao" => "Guara"
            ],
            "local_destino" => [
                "idlocal" => 6,
                "descricao" => "Asa Sul"
            ],
            "horarios" => [
                "horarioinicial" => [
                    "idhorario" => 1,
                    "horainicial" => "05:00:00",
                    "horafinal" => "06:00:00"
                ],
                "horariofinal" => [
                    "idhorario" => 2,
                    "horainicial" => "07:00:00",
                    "horafinal" => "08:00:00"
                ]
            ]
        ],
    [
        "idlinha" => 2,
        "descricao" => "Descricao da linha 2",
        "local_origem" => [
            "idlocal" => 2,
            "descricao" => "Penha"
        ],
        "local_destino" => [
            "idlocal" => 3,
            "descricao" => "Maracana"
        ],
        "horarios" => [
            "horarioinicial" => [
                "idhorario" => 1,
                "horainicial" => "05:00:00",
                "horafinal" => "06:00:00"
            ],
            "horariofinal" => [
                "idhorario" => 2,
                "horainicial" => "07:00:00",
                "horafinal" => "08:00:00"
            ]
        ]
    ]
];


$dom = new DOMDocument("1.0");
$dom->formatOutput = true;


$itinerarios = $dom->createElement("itinerarios");

foreach ($data as $value) {
    $itinerario = $dom->createElement("itinerario");
    $itinerarios->appendChild($itinerario);

    $idlinha = $dom->createElement("idlinha", $value["idlinha"]);
    $itinerario->appendChild($idlinha);

    $descricao = $dom->createElement("descricao", $value["descricao"]);
    $itinerario->appendChild($descricao);

    $local_origem = $dom->createElement("local_origem");
    $itinerario->appendChild($local_origem);

    $idlocal_origem = $dom->createElement("idlocal", $value["local_origem"]['idlocal']);
    $local_origem->appendChild($idlocal_origem);

    $descricao_origem = $dom->createElement("descricao", $value["local_origem"]['descricao']);
    $local_origem->appendChild($descricao_origem);

    $local_destino = $dom->createElement("local_destino");
    $itinerario->appendChild($local_destino);

    $idlocal_destino = $dom->createElement("idlocal", $value["local_destino"]['idlocal']);
    $local_destino->appendChild($idlocal_destino);

    $descricao_destino = $dom->createElement("descricao", $value["local_destino"]['descricao']);
    $local_destino->appendChild($descricao_destino);

    $horarios = $dom->createElement("horarios");
    $itinerario->appendChild($horarios);

    $horarioinicial = $dom->createElement("horarioinicial");
    $horarios->appendChild($horarioinicial);

    $idhorario_inicial = $dom->createElement("idhorario", $value['horarios']['horarioinicial']['idhorario']);
    $horarioinicial->appendChild($idhorario_inicial);

    $horainicial_inicial = $dom->createElement("horainicial", $value['horarios']['horarioinicial']['horainicial']);
    $horarioinicial->appendChild($horainicial_inicial);

    $horafinal_inicial = $dom->createElement("horainicial", $value['horarios']['horarioinicial']['horafinal']);
    $horarioinicial->appendChild($horafinal_inicial);

    $horariofinal = $dom->createElement("horariofinal");
    $horarios->appendChild($horariofinal);

    $idhorario_final = $dom->createElement("idhorario", $value['horarios']['horariofinal']['idhorario']);
    $horariofinal->appendChild($idhorario_final);

    $horainicial_final = $dom->createElement("horainicial", $value['horarios']['horariofinal']['horainicial']);
    $horariofinal->appendChild($horainicial_final);

    $horafinal_final = $dom->createElement("horainicial", $value['horarios']['horariofinal']['horafinal']);
    $horariofinal->appendChild($horafinal_final);
}

$dom->appendChild($itinerarios);

//$dom->save("itinerarios.xml");

header("Content-type: text/xml");
print $dom->saveXML();



