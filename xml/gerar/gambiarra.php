<?php

header("Content-type: text/xml");

$data = [
    "itinerarios" =>
        [
            "idlinha" => 1,
            "descricao" => "Descricao da linha",
            "local_origem" => [
                "idlocal" => 1,
                "descricao" => "Guara"
            ],
            "local_destino" => [
                "idlocal" => 6,
                "descricao" => "Asa Sul"
            ],
            "horarios" => [
                "horarioinicial" => [
                    "idhorario" => 1,
                    "horainicial" => "05:00:00",
                    "horafinal" => "06:00:00"
                ],
                "horariofinal" => [
                    "idhorario" => 2,
                    "horainicial" => "07:00:00",
                    "horafinal" => "08:00:00"
                ]
            ]
        ],
    [
        "idlinha" => 2,
        "descricao" => "Descricao da linha 2",
        "local_origem" => [
            "idlocal" => 2,
            "descricao" => "Penha"
        ],
        "local_destino" => [
            "idlocal" => 3,
            "descricao" => "Maracana"
        ],
        "horarios" => [
            "horarioinicial" => [
                "idhorario" => 1,
                "horainicial" => "05:00:00",
                "horafinal" => "06:00:00"
            ],
            "horariofinal" => [
                "idhorario" => 2,
                "horainicial" => "07:00:00",
                "horafinal" => "08:00:00"
            ]
        ]
    ]
];


echo "<itinerarios>";
foreach ($data as $value) {
    echo "    <itinerario>
                    <idlinha>" . $value['idlinha'] . "</idlinha>
                    <descricao>" . $value['descricao'] . "</descricao>
                    <local_origem>
                        <idlocal>" . $value['local_origem']['idlocal'] . "</idlocal>
                        <descricao>" . $value['local_origem']['descricao'] . "</descricao>
                    </local_origem>
           
                    <local_destino>
                        <descricao>" . $value['local_destino']['descricao'] . "</descricao>
                        <idlocal>" . $value['local_destino']['idlocal'] . "</idlocal>
                    </local_destino>
                    
                    <horarios>
                        <horarioinicial>
                             <idhorario>" . $value['horarios']['horarioinicial']['idhorario'] . "</idhorario>
                             <horainicial>" . $value['horarios']['horarioinicial']['horainicial'] . "</horainicial>
                             <horafinal>" . $value['horarios']['horarioinicial']['horafinal'] . "</horafinal>
                        </horarioinicial>
                        
                        <horariofinal>
                             <idhorario>" . $value['horarios']['horariofinal']['idhorario'] . "</idhorario>
                             <horainicial>" . $value['horarios']['horariofinal']['horainicial'] . "</horainicial>
                             <horafinal>" . $value['horarios']['horariofinal']['horafinal'] . "</horafinal>
                        </horariofinal>
                    
                    
                    </horarios>
              </itinerario>";
}

echo "</itinerarios>";