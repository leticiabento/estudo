function Pais() {
    this.buscar = function () {
        $.ajax({
            url: "api/pais/buscar.php",
            type: "GET"
        }).done(function (res) {
            var iso, name;
            for (var i = 0; i < res.country.length; i++) {
                iso = res.country[i]['iso'];
                name = iso + " - " + res.country[i]['name-en'];
                $("#medalhas").append("<option value='" + iso + "'>" + name + "</option>");
            }
        })
    }

    this.infos = function (element) {

        var data = $(element).serialize();

        $.ajax({
            url: "api/pais/infos.php",
            type: "POST",
            data: data
        }).done(function (res) {
            $("tbody, tfoot").html("");

            $.map(res, function (item) {
                if (item) {
                    var ano2007 = (item['2007'] && item['2007'][0] ? ['ano2007', item['2007'][0]] : ['', "-"]);
                    var ano2009 = (item['2009'] && item['2009'][0] ? ['ano2009', item['2009'][0]] : ['', "-"]);
                    var ano2011 = (item['2011'] && item['2011'][0] ? ['ano2011', item['2011'][0]] : ['', "-"]);

                    $("tbody").append(
                        "<tr>" +
                        "<td class='title'>" + item.nome[0] + "</td>" +
                        "<td class='" + ano2007[0] + "'>" + ano2007[1] + "</td>" +
                        "<td class='" + ano2009[0] + "'>" + ano2009[1] + "</td>" +
                        "<td class='" + ano2011[0] + "'>" + ano2011[1] + "</td>" +
                        "</tr>"
                    );
                }
            });

            $("tfoot").append(
                "<tr>" +
                "<th>Total medals</th>" +
                "<td>" + $(".ano2007").length + "</td>" +
                "<td>" + $(".ano2009").length + "</td>" +
                "<td>" + $(".ano2011").length + "</td>" +
                "</tr>"
            );
        })
    }
}