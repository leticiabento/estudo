<?php

class PaisModel
{
    private $nome;
    private $results;
    private $modalidade;


    public function __construct()
    {
        $this->nome = simplexml_load_file("../xml/WSC-Countrys.xml");
        $this->results = simplexml_load_file("../xml/WSC-Results.xml");
        $this->modalidade = simplexml_load_file("../xml/WSC-Skills.xml");
    }

    private function todasMedalhas()
    {
        if ($this->nome) {
            return $this->nome;
        }
    }

    public function returnTodasMedalhas()
    {
        return $this->todasMedalhas();
    }

    private function todasInfos()
    {
        $data = array('resultados' => $this->results, 'modalidades' => $this->modalidade);
        return $data;
    }

    public function returnTodasInfos()
    {
        return $this->todasInfos();
    }
}