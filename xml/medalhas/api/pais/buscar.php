<?php

header("Content-type: application/json");
if ($_SERVER['REQUEST_METHOD'] == "GET") {

    include_once "../autoload.php";

    $paisController = new PaisController();
    echo json_encode($paisController->returnModalidades());
}