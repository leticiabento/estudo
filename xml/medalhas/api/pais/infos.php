<?php

header("Content-type: application/json");
if ($_SERVER['REQUEST_METHOD'] == "POST") {

    $iso = $_POST['medalhas'];
    include_once "../autoload.php";

    $paisController = new PaisController();
    echo json_encode($paisController->returnInfos($iso));
}