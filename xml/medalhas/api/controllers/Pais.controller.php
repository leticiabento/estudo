<?php

include_once "../models/Pais.class.php";

class PaisController extends PaisModel
{
    private function modalidades()
    {
        return $this->returnTodasMedalhas();
    }

    public function returnModalidades()
    {
        return $this->modalidades();
    }

    private function infos($iso)
    {
        $data = $this->returnTodasInfos();

        $infos = array();
        $resultSkills = array();
        $count = 0;
        $countInfos = 0;

        foreach ($data['resultados'] as $value) {
            if ($value->{'country-iso'} == $iso) {
                array_push($infos, array(
                    "modalidade-" . $value->{'skill-number'} => $value->{'skill-number'},
                    "ano" => $value->{'year'},
                    "medalha" => $value->{'medal'}
                ));

                foreach ($data['modalidades'] as $value_skill) {
                    if (intval($value_skill->{'number'}) === intval($value->{'skill-number'})) {
                        array_push($infos[$count], array('modalidade' => $value_skill->{'name-en'}));
                    }
                }

                $count++;
            }
        }


        foreach ($infos as $value) {
            foreach ($data['modalidades'] as $valor_modalidade) {
                $existeModalidade = isset($value['modalidade-' . $valor_modalidade->{'number'}]);

                if ($existeModalidade) {
                    $numeroModalidade = intval($valor_modalidade->{'number'});
                    $numeroInfo = intval($value['modalidade-' . $valor_modalidade->{'number'}][0]);
                }

                if ($existeModalidade && $numeroModalidade === $numeroInfo) {
                    $resultSkills['modalidade-' . $valor_modalidade->{'number'}]["nome"] = $valor_modalidade->{'name-en'};

                    if ($value['ano'] == "2007") {
                        $resultSkills['modalidade-' . $valor_modalidade->{'number'}]["2007"] = $value['medalha'];
                    } else if ($value['ano'] == "2009") {
                        $resultSkills['modalidade-' . $valor_modalidade->{'number'}]["2009"] = $value['medalha'];
                    } else {
                        $resultSkills['modalidade-' . $valor_modalidade->{'number'}]["2011"] = $value['medalha'];
                    }
                }
            }
            $countInfos++;
        }
        return $resultSkills;
    }

    public function returnInfos($iso)
    {
        return $this->infos($iso);
    }
}