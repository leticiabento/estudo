var caloriasTotais;
var count = 0;
var countCalorias = 0;
var countTodasCalorias = 0;
var valor, valorCalorias, consumir, qntCalorias, somarCalorias;
$("#consumir").html("");

function Alimento() {
    this.exibir = function () {
        $.ajax({
            url: "api/alimento/exibir.php",
            type: "GET"
        }).done(function (res) {
            // console.log(res);
            $.map(res, function (element, index) {
                // console.log(element['alimentos'][0]);
                $("#alimento").append("<option value='" + element['alimentos'][0] + "'>" + element['alimentos'][0] + "</option>");
            })
        })
    }

    this.buscarInfos = function (element, event) {
        event.preventDefault();

        var data = $(element).serialize();

        $.ajax({
            url: "api/alimento/buscar.php",
            type: "POST",
            data: data
        }).done(function (res) {
            caloriasTotais = $("#consumir").text(res['calculo'].toFixed(1) + "k");

            $.each(res.alimentos, function (element, index) {
                // console.log(index);

                $("tbody").append(
                    "<tr class='caloria-" + index.id[0] + "'>" +
                    "<td>" + index.nome[0] + "</td>" +
                    "<td>" + index.quantidade[0] + "</td>" +
                    "<td class='qnt-gramas'>" + index.gramas[0] + "g</td>" +
                    "<td class='qnt-calorias'>" + index.kcal[0] + "k</td>" +
                    "<td><button class='btn-Excluir' onclick='btnExcluir(this)'>Excluir</button></td>" +
                    "</tr>"
                );


                somar();
            })


        })
    }

}

function somar() {

    valor = $(".qnt-gramas").text().split("g");
    valor[valor.length - 1] = 0;
    for (var i = 0; i < valor.length; i++) {
        count = count + parseInt(valor[i]);
    }

    $(".valorGramas").html(count + "g");
    count = 0;

    valorCalorias = $(".qnt-calorias").text().split("k");
    valorCalorias[valorCalorias.length - 1] = 0;
    for (var i = 0; i < valorCalorias.length; i++) {
        countCalorias = countCalorias + parseInt(valorCalorias[i]);
    }

    $(".valorCalorias").html(countCalorias + "g");


    consumir = $("#consumir").text().split("k");


    countTodasCalorias = consumir[0] - countCalorias;
    $("#consumir").text(countTodasCalorias + "k");
    countCalorias = 0;


}

function btnExcluir(element) {
    console.log(element)
    $(".valorGramas").html(" ");
    $(".valorCalorias").html(" ");
    qntCalorias = $(element).parent().parent().find('.qnt-calorias').text().split("k");
    consumir = $("#consumir").text().split("k");
    console.log($(element).parent().parent().remove());
    somarCalorias = parseInt(qntCalorias[0]) + parseInt(consumir[0]);
    somar();

    console.log(somarCalorias, qntCalorias[0], consumir[0]);

    $("#consumir").text(somarCalorias + 'k');
}