<?php

class AlimentoModel
{

    private $xml;
    private $todosAlimentos = [];
    private $infosAlimentos = [];

    public function __construct()
    {
        $this->xml = simplexml_load_file("../../src/xml/calorias.xml");
    }

    private function exibirAlimentosModel()
    {
        if ($this->xml) {

            $grupos = $this->xml->{'grupos'}->{'grupo'};

            foreach ($grupos as $value1) {
                $id_grupo = $value1->{'id'};
                $alimentos = $value1->{'alimentos'};

                foreach ($alimentos as $value2) {
                    $alimento = $value2->{'alimento'};


                    foreach ($alimento as $value3) {
//                        print_r($value3);

                        array_push($this->todosAlimentos, [
                            "id_grupo" => $id_grupo,
                            'alimentos' => $value3->{"nome"},
                            "quantidade" => $value3->{'quantidade'},
                            "gramas" => $value3->{'gramas'},
                            "kcal" => $value3->{'kcal'}

                        ]);
                    }
                }
            }
            return $this->todosAlimentos;
        }
    }

    public function returnExibirAlimentosModel()
    {
        return $this->exibirAlimentosModel();
    }

    private function exibirInfosModel($alimento)
    {
//        print_r($this->xml);
        if ($this->xml) {

            $grupos = $this->xml->{'grupos'}->{'grupo'};


            foreach ($grupos as $value) {

                $alimentos = $value->{'alimentos'}->{'alimento'};

                foreach ($alimentos as $value1) {
                    $id = $value1->{'id'};
                    $nomeAlimento = $value1->{'nome'};
                    $quantidade = $value1->{'quantidade'};
                    $gramas = $value1->{'gramas'};
                    $kcal = $value1->{'kcal'};


                    if ($nomeAlimento == $alimento) {

                        array_push($this->infosAlimentos, [
                            "id" => $id,
                            "nome" => $nomeAlimento,
                            "quantidade" => $quantidade,
                            "gramas" => $gramas,
                            "kcal" => $kcal
                        ]);

                        return $this->infosAlimentos;

                    }


                }

            }
        }

    }


    public function returnExibirInfosModel($alimento)
    {
        return $this->exibirInfosModel($alimento);
    }
}

//echo "<pre>";
//$let = new AlimentoModel();
//$let->returnExibirAlimentosModel();