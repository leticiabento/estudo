<?php

include_once "../Models/Alimento.class.php";

class AlimentoController
{

    private $model;

    public function __construct()
    {
        $this->model = new AlimentoModel();
    }

    private function exibirAlimentos()
    {
        return $this->model->returnExibirAlimentosModel();
    }

    public function returnExibirAlimentos()
    {
        return $this->exibirAlimentos();
    }

    private function exibirInfos($alimento)
    {
//        print_r($alimento);
        return $this->model->returnExibirInfosModel($alimento);
    }

    public function returnExibirInfos($alimento)
    {
        return $this->exibirInfos($alimento);
    }
}