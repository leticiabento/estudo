<?php

    header("Content-type: application/json");
    if($_SERVER['REQUEST_METHOD']=="GET") {
        include_once "../autoload.php";

        $alimentoController = new AlimentoController();
        echo json_encode($alimentoController->returnExibirAlimentos());
    }