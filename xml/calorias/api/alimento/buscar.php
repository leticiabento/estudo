<?php

header("Content-type: application/json");
if($_SERVER['REQUEST_METHOD']=="POST") {

    $peso = $_POST['peso'];
    $altura = $_POST['altura'];
    $sexo = $_POST['sexo'];
    $alimento = $_POST['alimento'];

    if($sexo == "F") {
        $calculo = ($altura*$peso) * 5;
    } else {
        $calculo = ($altura*$peso) * 8;
    }

    include_once "../autoload.php";

    $alimentoController = new AlimentoController();
    echo json_encode(["alimentos" => $alimentoController->returnExibirInfos($alimento), "calculo"=>$calculo]);
}