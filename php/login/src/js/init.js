var formLogin = $("#formLogin");
var pessoa = new Pessoa();


formLogin.submit(function(event) {
    pessoa.login(this, event);
})

$(document).ready(function() {

    if(location.pathname === "/login/" || location.pathname === "/login/login.html" || location.pathname === "/login/index.html") {
        pessoa.consultar();
    }
})

$(".btn-sair").on('click', function() {
    pessoa.sair();
})