function Pessoa() {
    this.login = function (element, event) {
        event.preventDefault();

        var data = $(element).serialize();

        $.ajax({
            url: "api/login/login.php",
            type: "POST",
            data: data
        }).done(function (res) {
            if(res) {
                location.href="index.html";
            } else {
                alert ("Usuario e/ou senha incorretos");
            }
        })
    };

    this.consultar = function() {
        $.ajax({
            url: "api/login/consultar.php",
            type: "GET"
        }).done(function(res) {
            console.log(res);
            console.log(location);
            var login = (location.pathname == "/login/login.html");

            if(res && login) {
                location.href = "index.html";
            } else if (!res && !login) {
                location.href = "login.html";
            }
        })
    }

    this.sair = function() {
        $.ajax({
            url: "api/login/sair.php",
            type: "GET"
        }).done(function(res) {
            if(res) {
                location.href = "login.html";
            }
        })
    }

}