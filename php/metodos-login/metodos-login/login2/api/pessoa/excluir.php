<?php

    header("Content-type: application/json");

    if($_SERVER['REQUEST_METHOD']=="GET") {        
        include_once "../autoload.php";
        $pessoaController = new PessoaController();
        $pessoaController->setId($_GET['id']);

        echo json_encode($pessoaController->returnExcluirUsuarios());
    }