<?php

    include_once "Conexao.class.php";

    class PessoaModel extends Conexao {
        private function buscarDadosDb() {
            $sql = "SELECT id, nome FROM login";
            $stmt = $this->prepare($sql);
            $stmt->execute();
    
            return $stmt->fetchAll(PDO::FETCH_ASSOC);    
        }

        public function returnBuscarDadosDb() {
            return $this->buscarDadosDb();
        }

        private function excluirUsuariosDb($id) {
            $sql = "DELETE FROM login WHERE id=?";
            $stmt = $this->prepare($sql);
            $stmt->bindValue(1, $id);
            $stmt->execute();

            return $stmt->rowCount();
        }

        public function returnExcluirUsuariosDb($id) {
            return $this->excluirUsuariosDb($id);
        }

        private function editarDadosDb($nome, $id) {
            $sql = "UPDATE login set nome=? WHERE id=?";
            $stmt = $this->prepare($sql);
            $stmt->bindValue(1, $nome);
            $stmt->bindValue(2, $id);
            $stmt->execute();
            
            return $stmt->rowCount();
        }

        public function returnEditarDadosDb($nome, $id) {
            return $this->editarDadosDb($nome, $id);
        }

        private function cadastrarUsuariosDb($nome, $senha, $imagem) {
            $sql = "INSERT INTO login
                    (nome, senha, imagem)
                    VALUES (?,?,?)";

            $stmt = $this->prepare($sql);
            $stmt->bindValue(1, $nome);
            $stmt->bindValue(2, md5($senha));
            $stmt->bindValue(3, $imagem);
            $stmt->execute();

            return $stmt->rowCount();

        }

        public function returnCadastrarUsuariosDb($nome, $senha, $imagem) {
            return $this->cadastrarUsuariosDb($nome, $senha, $imagem);
        }
    }