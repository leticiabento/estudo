<?php

    include_once "../models/Pessoa.model.php";
    class PessoaController extends PessoaModel {
        private $id;
        private $nome;

        private function getId() {
            return $this->id;
        }

        public function setId($id) {
            $this->id = $id;
        }

        private function getNome() {
            return $this->nome;
        }

        public function setNome($nome) {
            $this->nome = $nome;
        }

        private function getSenha() {
            return $this->senha;
        }

        public function setSenha($senha) {
            $this->senha = $senha;
        }

        private function getImagem() {
            return $this->imagem;
        }

        public function setImagem($imagem) {
            $this->imagem = $imagem;
        }

        private function buscarUsuarios() {
            return $this->returnBuscarDadosDb();
        }

        public function returnBuscarUsuarios() {
            return $this->buscarUsuarios();
        }

        private function excluirUsuarios() {
            return $this->returnExcluirUsuariosDb($this->getId());
        }

        public function returnExcluirUsuarios() {
            return $this->excluirUsuarios();
        }

        private function editarDados() {
            return $this->returnEditarDadosDb($this->getNome(), $this->getId());
        }

        public function returnEditarDados() {
            return $this->editarDados();
        }

        private function cadastrarUsuario() {
            return $this->returnCadastrarUsuariosDb($this->getNome(), $this->getSenha(), $this->getImagem());
        }        

        public function uploadImagem($imagem) {
            $dir = "../../src/img/upload/";
            $extensao = @end(explode(".", $imagem['name']));
            $novoNome = uniqid(md5($imagem['name'].date("d/m/Y H:i:s"))) . "." . $extensao;
            $move = move_uploaded_file($imagem['tmp_name'], $dir.$novoNome);

            $this->setImagem($novoNome);
            
            if ($move) {
                return $this->cadastrarUsuario() ? json_encode(array('success' => true, 'message' => 'Foiii')) : json_encode(array('success' => false, 'message' => 'Nãoooo cadastrado')); 
                
            } else {
                return json_encode(array('success' => false, 'message' => 'Nãoooo imagem')); 
            }
        }
    }