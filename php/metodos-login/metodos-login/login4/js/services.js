function Pessoa() {
    this.login = function(element, event) {
        event.preventDefault();

        var data = $(element).serialize();

        $.ajax({
            url: "api/login/login.php",
            type: "POST",
            data: data
        }).done(function(res) {
            console.log(res);
        })
    }

    this.consultar = function (element, event) {
        $.ajax({
            url: "api/login/consultar.php",
            type: "GET"
        }).done(function (res) {
            console.log(res);
        })
    }
}