<?php

header("Content-type: application/json");
if ($_SERVER['REQUEST_METHOD'] == "POST") {
    $login = $_POST['login'];
    $senha = $_POST['senha'];

    include_once "../autoload.php";

    $loginController = new LoginController();
    $loginController->setLogin($login);
    $loginController->setSenha($senha);

    echo json_encode($loginController->returnLogar());

}