<?php

    include_once "../models/Login.class.php";

    class LoginController extends LoginModel {
        private $login;
        private $senha;

        private function getLogin() {
            return $this ->login;
        }

        public function setLogin($login) {
            $this->login = $login;
        }

        private function getSenha() {
            return $this ->senha;
        }

        public function setSenha($senha) {
            $this->senha = $senha;
        }


        private function logar() {
            session_start();
            $user = $this->returnLogarDb($this->getLogin(), $this->getSenha());

            if(!empty($user)) {
                $_SESSION['login'] = $user[0]['login'];

                return true;
            }

            return $this->limparLogin();
        }

        public function returnLogar() {
            return $this->logar();
        }

        private function consultar() {
            session_start();
            if(isset($_SESSION['login']) && ($this->returnConsultarDb($_SESSION['login']) > 0) ) {
                return array("success"=>true, "login"=> $_SESSION['login']);

            }

            return $this->limparLogin();

        }

        public function returnConsultar() {
            return $this->consultar();
        }

        public function limparLogin() {
            session_destroy();
            session_unset();

            return false;
        }
    }