var form = $("form");
var pessoa = new Pessoa();

form.submit(function(event) {
    pessoa.login(this, event);
})

$(document).ready(function() {
    init();
})

$(".linkSair").on('click', function(event) {
    pessoa.sair(this, event);
})


function init() {
    pessoa.consultar();

    if(location.pathname == "/login/dashboard.html") {
        pessoa.usuarios();
    }
}