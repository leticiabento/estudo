<?php 

    include_once "../models/Login.model.php";

    class LoginController extends LoginModel {
        private $login;
        private $senha;

        private function getLogin() {
            return $this->login;
        }

        public function setLogin($login) {
            return $this->login = $login;
        }

        private function getSenha() {
            return $this->senha;
        }

        public function setSenha($senha) {
            return $this->senha = $senha;
        }

        private function logar() {
            session_start();
            if ($this->returnLogarDb($this->getLogin(), $this->getSenha()) > 0) {
                $_SESSION['login'] = $this->getLogin();
                return true;
            } 

            return $this->limparLogin();
        }

        public function returnLogar() {
            return $this->logar();
        }

        private function limparLogin() { 
            session_unset();
            session_destroy();

            return false;
        }

        public function returnLimparLogin() {
            session_start(); 
            
            return $this->limparLogin();
        }

        private function consultarLogin() {
            session_start(); 
            return isset($_SESSION['login']) ? $this->returnConsultarDb($_SESSION['login']) : $this->limparLogin();
        }

        public function returnConsultarLogin() {
            return $this->consultarLogin();
            // session_start(); 
            // return $_SESSION['login'];
        }

    }