<?php

    function __autoload($class) {
        $class = explode("Controller", $class);
        include_once "controllers/" . $class . ".controller.php";
    }