function Pessoa() {
    this.login = function(element, event) {
        event.preventDefault();

        var data = $(element).serialize();

        $.ajax({
            url: "api/views/login.php",
            type: "POST",
            data: data
        }).done(function(res) {
            if(res) {
                location.href="dashboard.html"
            } else {
                alert('Usuário e/ou senhas incorretos');
            }
        })

    }

    this.consultar = function(element, event) {
        $.ajax({
            url: "api/views/login.php?action=consultar",
            type: "GET"
        }).done(function(res) {
            var index = (location.pathname == "/login/index.html" || location.pathname == "/login/");
            // console.log(res);
            if(res && index) {
                location.href="dashboard.html"
            } else if(!res && !index) {
                location.href="index.html"
            }
        })
    }

    this.sair = function(element, event) {
        event.preventDefault();
        
        $.ajax({
            url: "api/views/login.php?action=sair",
            type: "GET"
        }).done(function(res) {
            console.log(res);
            if(res) {
                location.href="index.html"
            } 
        })
    }

    
}