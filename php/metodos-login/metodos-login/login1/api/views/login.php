<?php

    header("Content-type: application/json");
    if($_SERVER['REQUEST_METHOD']=="POST") {

        $login = $_POST['login'];
        $senha = md5($_POST['senha']);

        include_once "../autoload.php";

        $loginController = new LoginController();
        $loginController->setLogin($login);
        $loginController->setSenha($senha);

        echo json_encode($loginController->returnLogar());

    } else if($_SERVER['REQUEST_METHOD']=="GET" && $_GET['action'] == "consultar") {
        
        include_once "../autoload.php";        
        $loginController = new LoginController();

        echo json_encode($loginController->returnConsultar());

    } else if($_SERVER['REQUEST_METHOD']=="GET" && $_GET['action'] == "sair") {
        include_once "../autoload.php";        
        $loginController = new LoginController();

        echo json_encode(!$loginController->returnLimparLogin());
    }