<?php 

    header("Content-type: application/json");
    if($_SERVER['PHP_SELF'] == "/login2/api/views/pessoa.php" && $_SERVER['REQUEST_METHOD']=="GET" && $_GET['action'] === 'buscar') {
        include_once "../autoload.php";

        $pessoaController = new PessoaController();
        echo json_encode($pessoaController->returnBuscarUsuarios());
        
    } else if($_SERVER['REQUEST_METHOD']=="GET" && $_GET['action']=="excluir") {

        include_once "../autoload.php";
        $pessoaController = new PessoaController();
        $pessoaController->setId($_GET['id']);

        echo json_encode($pessoaController->returnExcluirUsuarios());
    } else if($_SERVER['REQUEST_METHOD']=="POST" && isset($_POST['id'])) {
        $id = $_POST['id'];
        $nome = $_POST['login'];

        include_once "../autoload.php";
        $pessoaController = new PessoaController();
        $pessoaController->setNome($nome);
        $pessoaController->setId($id);

        echo json_encode($pessoaController->returnEditarDados());
    } else if($_SERVER['REQUEST_METHOD']=="POST" && $_POST['handle']=="cadastrar") {

        $nome = $_POST['nome'];
        $senha = $_POST['senha'];
        $imagem = $_FILES['imagem'];

        $dir = "../../src/img/upload/";
        $extensao = @end(explode(".", $imagem['name']));
        $novoNome = uniqid(md5($imagem['name'].date("d/m/Y H:i:s"))) . "." . $extensao;
        $move = move_uploaded_file($imagem['tmp_name'], $dir.$novoNome);

        if($move) {
            include_once "../autoload.php";
            
            $pessoaController = new PessoaController();
            $pessoaController->setNome($nome);
            $pessoaController->setImagem($novoNome);
            $pessoaController->setSenha($senha);

            if ($pessoaController->returnCadastrarUsuario()) {
                echo json_encode(array("success"=>true, "message"=>"Cadastrado com success"));            
            } else {
                echo json_encode(array("success"=>false, "message"=>"NÃOOO Cadastrado com success"));            
            }
        } else {
            echo json_encode(array("success"=>false, "message"=>"ERRO ao mover a imagem"));
        }


    }
    