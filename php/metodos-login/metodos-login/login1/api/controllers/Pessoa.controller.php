<?php

    include_once "../models/Pessoa.model.php";
    class PessoaController extends PessoaModel {
        private $id;
        private $nome;

        private function getId() {
            return $this->id;
        }

        public function setId($id) {
            $this->id = $id;
        }

        private function getNome() {
            return $this->nome;
        }

        public function setNome($nome) {
            $this->nome = $nome;
        }

        private function getSenha() {
            return $this->senha;
        }

        public function setSenha($senha) {
            $this->senha = $senha;
        }

        private function getImagem() {
            return $this->imagem;
        }

        public function setImagem($imagem) {
            $this->imagem = $imagem;
        }

        private function buscarUsuarios() {
            return $this->returnBuscarDadosDb();
        }

        public function returnBuscarUsuarios() {
            return $this->buscarUsuarios();
        }

        private function excluirUsuarios() {
            return $this->returnExcluirUsuariosDb($this->getId());
        }

        public function returnExcluirUsuarios() {
            return $this->excluirUsuarios();
        }

        private function editarDados() {
            return $this->returnEditarDadosDb($this->getNome(), $this->getId());
        }

        public function returnEditarDados() {
            return $this->editarDados();
        }

        private function cadastrarUsuario() {
            return $this->returnCadastrarUsuariosDb($this->getNome(), $this->getSenha(), $this->getImagem());
        }
        
        public function returnCadastrarUsuario() {
            return $this->cadastrarUsuario();
        }
    }