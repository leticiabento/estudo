function Pessoa() {
    this.login = function(element, event) {

        event.preventDefault();

        var data = $(element).serialize();

        $.ajax({
            url : "api/views/login.php",
            type: "POST",
            data: data
        }).done(function(res) {
            if(res) {
                location.href = "dashboard.html";
            } else {
                alert("Usuário e/ou senha incorretos");
            }
        })

    }

    this.consultar = function(element, event) {
        $.ajax({
            url: "api/views/login.php?action=consultar",
            type: "GET"
        }).done(function(res) {
            // res = true;
            var index = (location.pathname == "/login2/index.html" || location.pathname == "/login2/");

            if(res && index) {
                location.href = "dashboard.html";
            } 
            else if(!res && !index) {
                location.href = "index.html";
            }
        })
    }

    this.sair = function (event) {
        event.preventDefault();
        $.ajax({
            url: "api/views/login.php?action=sair",
            type: "GET"
        }).done(function(res) {
            if(res) {
                location.href = "index.html";
            }
        })
    }

    this.usuarios = function() {
        $.ajax({
            url: "api/views/pessoa.php?action=buscar",
            type: "GET"
        }).done(function(res) {
            var tr;
            var td;
            var table = $(".table-users tbody");
            table.html(" ");
            var button;
            for(var i=0; i<res.length; i++) {
                tr = $("<tr />");
                td = $("<td /><td />");
                var count = 0;
                button = $("<a href='editar.html?id="+res[i].id+"'><button class='btn btn-primary'>Editar</button></a> <a href='?action=excluir&id="+res[i].id+"' id='button' ><button class='btn btn-danger excluir' onclick='btnExcluir(this, event)'>Excluir</button></a>");
                table.append(tr);
                table.find(tr).eq(0).append(td);
                table.find(tr).eq(0).find(td).eq(0).text(res[i].nome);
                table.find(tr).eq(0).find(td).eq(1).append(button);
            }
        })
    }

    this.excluirUsuarios = function(id) {
        // var id = location.search.split("&");
        // console.log(id);

        $.ajax({
            url: "api/views/pessoa.php?action=excluir",
            type: "GET",
            data: id
        }).done(function(res) {
            if(res > 0) {
                pessoa.usuarios();
            }
            console.log(res);
        })
    }

    this.editar = function(element, event) {
        event.preventDefault();

        var data = $(element).serialize();
        var id = location.search.split('?')
        data = data + '&' + id[1]; 

        $.ajax({
            url: "api/views/pessoa.php",
            type: "POST",
            data: data
        }).done(function(res) {
           if(parseInt(res) > 0) {
                alert('alterado');
                location.href = "dashboard.html";
           } else {
                alert('não alterado');
           }
        })
    }

    this.cadastrar = function(element, event) {
        // var data = $(element).serialize();
        event.preventDefault();

        var data = new FormData($(element)[0]);

        $.ajax({
            url: "api/views/pessoa.php",
            type: "POST",
            data: data,
            processData: false,
            contentType: false
        }).done(function(res) {
            alert(res.message);
        })
    }
}