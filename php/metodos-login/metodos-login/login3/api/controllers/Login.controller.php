<?php

    include_once "../models/Login.model.php";

    class LoginController extends LoginModel {
        private $login;
        private $senha;

        private function getLogin() {
            return $this->login;
        }

        public function setLogin($login) {
            $this->login = $login;
        }

        private function getSenha() {
            return $this->senha;
        }

        public function setSenha($senha) {
            $this->senha = $senha;
        }

        private function logar() {
            session_start();
            if($this->returnLogarDb($this->getLogin(), $this->getSenha()) > 0) {
                $_SESSION['login'] = $this->getLogin();
                return true;
            }

            return $this->limparLogin();
        }

        public function returnLogar() {
            return $this->logar();
        }

        private function limparLogin() {
            session_unset();
            session_destroy();

            return false;
        }

        public function returnLimparLogin() {
            session_start();
            return $this->limparLogin();
        }

        private function consultar() {
            session_start();
            
            if(isset($_SESSION['login'])) {
                return $this->returnConsultarDb($_SESSION['login']) > 0;
            } else {
                return $this->limparLogin();
            }
        }

        public function returnConsultar() {
            return $this->consultar();
        }
    }