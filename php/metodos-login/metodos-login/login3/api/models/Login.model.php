<?php 

    include_once "Conexao.class.php";

    class LoginModel extends Conexao {
        private function logarDb($login, $senha) {
            $sql = "SELECT * FROM usuario WHERE nome=? AND senha=?";
            $stmt = $this->prepare($sql);
            $stmt->bindValue(1, $login);
            $stmt->bindValue(2, $senha);
            $stmt->execute();

            return $stmt->rowCount();
        }

        public function returnLogarDb($login, $senha) {
            return $this->logarDb($login, $senha);
        }

        private function consultarDb($login) {
            $sql = "SELECT * FROM usuario WHERE nome=?";
            $stmt = $this->prepare($sql);
            $stmt->bindValue(1, $login);
            $stmt->execute();

            return $stmt->rowCount();
        }

        public function returnConsultarDb($login) {
            return $this->consultarDb($login);
        }
    }