<?php
    class Conexao extends PDO {
        private $dsn = "mysql:host=localhost; dbname=login; port=3306; charset=utf8";
        private $user = 'root';
        private $password = '';
        private $handle = null;

        public function __construct() {
            try {
                if ($this->handle == null) {
                    $dbh = parent::__construct($this->dsn, $this->user, $this->password, array(PDO::ATTR_PERSISTENT => true));
                    $this->handle = $dbh;
                }
            } 
            catch (PDOException $e) {
                die("Error: ".$e->getMessage());
            }
        }

        public function __destruct() {
            $this->handle = null;
        }
    }
