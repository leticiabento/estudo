<?php
    header("Content-type: application/json");
    
    if($_SERVER['REQUEST_METHOD']=="POST" && isset($_POST['id'])) {
        $id = $_POST['id'];
        $nome = $_POST['login'];

        include_once "../autoload.php";
        $pessoaController = new PessoaController();
        $pessoaController->setNome($nome);
        $pessoaController->setId($id);

        echo json_encode($pessoaController->returnEditarDados());
    }