<?php 

    header("Content-type: application/json");
    if($_SERVER['PHP_SELF'] == "/login3/api/pessoa/buscar.php" && $_SERVER['REQUEST_METHOD']=="GET") {
        include_once "../autoload.php";

        $pessoaController = new PessoaController();
        echo json_encode($pessoaController->returnBuscarUsuarios());
        
    }
    