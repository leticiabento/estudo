<?php

    header("Content-type: application/json");
    if($_SERVER['REQUEST_METHOD']=="POST") {
        
        $nome = $_POST['nome'];
        $senha = $_POST['senha'];
        $imagem = $_FILES['imagem'];

        include_once "../autoload.php";
        
        $pessoaController = new PessoaController();
        $pessoaController->setNome($nome);
        $pessoaController->setSenha($senha);

        echo $pessoaController->uploadImagem($imagem);
    }