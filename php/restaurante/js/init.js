var pedido = new Pedido();
var formPedidos = $("#formPedidos");

function btnAdd(id) {
    pedido.abrirMesa(id);
}

formPedidos.submit(function (event) {
    var id = location.search.split("?id=");
    pedido.cadastrar(this, event, id[1]);
});

$(document).ready(function () {
    init();
});

function init() {

    if (location.pathname == "/restaurante/" || location.pathname == "/restaurante/index.html") {
        pedido.listarMesas();
        pedido.exibirPedidos();
    }

    if (location.pathname == "/restaurante/produtos.html") {
        pedido.listarTodasCozinhas();


        $("#cozinha").change(function () {
            console.log($(this).val());

            $("#produto").html('');
            var id = $(this).val();
            pedido.listarProdutos(id);
        })
    }

    if(location.pathname == "/restaurante/cozinhas.html") {

        pedido.listarTodasCozinhas();
        pedido.pedidosDasCozinhas();
    }

}

function fecharConta(id, event) {
    pedido.fecharConta(id, event);
}