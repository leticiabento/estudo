function Pedido() {
    this.listarMesas = function () {
        $.ajax({
            url: "api/mesas/mesa.php",
            type: "GET"
        }).done(function (res) {
            $(".boxes").html(" ");
            for (var i = 0; i < res.length; i++) {
                $(".boxes").append(
                    '<div class="box mesa-' + res[i].id + '">' +
                    '<div class="container">' +
                    '<h1>' + res[i].nome + '</h1>' +

                    '<div class="orders">' +
                    '<h2>Pedidos</h2>' +
                    '<ul></ul>' +
                    '</div>' +

                    '<div class="options"></div> ' +
                    '</div>' +
                    '</div>'
                );

                $(".boxes .mesa-" + res[i].id + " .options").append('<button onclick="btnAdd(' + res[i].id + ')" class="btnAbrir">ABRIR MESA</button>');

                if (res[i].status == 1) {
                    $(".boxes .mesa-" + res[i].id + " .options").append(
                        '<a href="produtos.html?id=' + res[i].id + '"><button>Adicionar Pedido</button></a> ' +
                        '<a href="" onclick="fecharConta(' + res[i].id + ', event)"><button>Fechar Conta</button></a> '
                    );

                    $(".mesa-" + res[i].id + " .btnAbrir").remove();
                }
                pedido.exibirPedidos();
            }
        })
    };

    this.abrirMesa = function (id) {
        $.ajax({
            url: "api/mesas/mesa.php?mesa=" + id,
            type: "GET"
        }).done(function (res) {
            if (res) {
                location.href = "produtos.html?id=" + id;
            }
        })
    };

    this.listarTodasCozinhas = function () {
        $.ajax({
            url: "api/cozinha/cozinha.php",
            type: "GET"
        }).done(function (res) {
            // console.log(res);
            for (var i = 0; i < res.length; i++) {
                $(".boxes-cozinha").append(
                    '<div class="box cozinha-' + res[i].id + '">' +
                    '<div class="container">' +
                    '<h1>' + res[i].tipo + '</h1>' +

                    '<div class="orders">' +
                    '<h2>Pedidos</h2>' +
                    '<ul></ul>' +
                    '</div>' +
                    '</div>'
                );

                pedido.pedidosDasCozinhas(res[i].id);


                $("#cozinha").append(
                    "<option value='" + res[i].id + "'>" + res[i].tipo + "</option>"
                );
            }
        })
    }


    this.pedidosDasCozinhas = function (id) {

        $.ajax({
            url: "api/cozinha/pedidos.php?id=" + id,
            type: "GET"
        }).done(function (res) {
            console.log(res);
            for (var i = 0; i < res.length; i++) {
                $(".cozinha-" + id + " ul").append("<li>" + res[i].nomeMesa + " - " + res[i].nome + " <small>(" + res[i].quantidade + ")</small></li>");
            }
        })
    }

    this.cadastrar = function (element, event, id) {
        event.preventDefault();
        var data = $(element).serialize();

        $.ajax({
            url: "api/pedidos/cadastrar.php?id=" + id,
            type: "POST",
            data: data
        }).done(function (res) {
            console.log(res);
            if (res.success) {
                alert(res.message);
                location.href = "index.html";
            }
        })
    }

    this.listarProdutos = function (id) {
        $.ajax({
            url: "api/produtos/listar.php?id=" + id,
            type: "GET"
        }).done(function (res) {
            console.log(res)
            for (var i = 0; i < res.length; i++) {
                console.log();
                for (var i = 0; i < res.length; i++) {
                    $("#produto").append(
                        "<option value='" + res[i].id + "'>" + res[i].nome + "</option>"
                    );
                }
            }
        })
    }

    this.exibirPedidos = function () {
        var box = $(".box");
        if (box && box.length === 5) {
            for (var i = 0; box.length > i; i++) {
                var id = $(box[i]).attr("class").split("mesa-");

                $.ajax({
                    url: "api/pedidos/exibir.php?id=" + id[1],
                    type: "GET"
                }).done(function (res) {
                    for (var i = 0; i < res.length; i++) {
                        // console.log(res);
                        $(".mesa-" + res[i].idMesa + " .orders ul").append("<li>" + res[i].tipoCozinha + ": " + res[i].quantidade + " - " + res[i].nomeProduto + " (<small class='total-" + res[i].idMesa + " quantidade-" + res[i].quantidade + "'>" + res[i].valor + "</small>)</li>");
                    }
                })
            }
        }

    }

    this.fecharConta = function (id, event) {

        event.preventDefault();

        var idMesas = $('.total-' + id);
        var total = 0;
        var quantidade;

        for (var i = 0; i < idMesas.length; i++) {
            quantidade = $(idMesas[i]).attr('class').split('quantidade-');
            quantidade = parseInt(quantidade[1]);
            total += quantidade > 1 ? quantidade * parseFloat($(idMesas[i]).text()) : parseFloat($(idMesas[i]).text());
        }

        // console.log(total);
        alert("O valor total é: " + total);
        $.ajax({
            url: "api/pedidos/fechar.php?id=" + id,
            type: "GET"
        }).done(function (res) {
            if (res > 0) {
                // location.href = "index.html";
                pedido.fecharMesa(id);
            }
        })


    }

    this.fecharMesa = function (id) {
        $.ajax({
            url: "api/mesas/fechar.php?id=" + id,
            type: "GET"
        }).done(function (res) {
            pedido.listarMesas();

        })
    }

}