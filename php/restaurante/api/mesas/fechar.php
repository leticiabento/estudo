<?php

header("Content-type: application/json");
if ($_SERVER['REQUEST_METHOD'] == "GET") {

    $id = $_GET['id'];
    include_once "../autoload.php";

    $mesaController = new MesaController();
    echo json_encode($mesaController->returnFecharMesa($id));
}
