<?php

header("Content-type: application/json");
if ($_SERVER['REQUEST_METHOD'] == "GET" && !isset($_GET['mesa'])) {

    include_once "../autoload.php";

    $mesaController = new MesaController();
    echo json_encode($mesaController->returnListarMesas());
}

else if ($_SERVER['REQUEST_METHOD'] == "GET" && isset($_GET['mesa'])) {
    $id_mesa = $_GET['mesa'];

    include_once "../autoload.php";

    $mesaController = new MesaController();
    echo json_encode($mesaController->returnMudarStatusMesa($id_mesa));
}