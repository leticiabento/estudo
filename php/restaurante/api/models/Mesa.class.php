<?php
/**
 * Created by PhpStorm.
 * User: OC
 * Date: 26/01/2018
 * Time: 16:04
 */

include_once "Conexao.class.php";

class MesaModel extends Conexao
{
    private function consultarMesasDb()
    {
        $sql = "SELECT * FROM mesa";

        $stmt = $this->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function returnConsultarMesasDb()
    {
        return $this->consultarMesasDb();
    }

    private function mudarStatusMesaDb($id_mesa)
    {
        $sql = "UPDATE mesa SET status=1 WHERE id=?";

        $stmt = $this->prepare($sql);
        $stmt->bindValue(1, $id_mesa);
        $stmt->execute();

        return $stmt->rowCount() > 0 ? $id_mesa : false;
    }

    public function returnMudarStatusMesaDb($id_mesa)
    {
        return $this->mudarStatusMesaDb($id_mesa);
    }

    private function fecharMesaDb($id)
    {
        $sql = "UPDATE mesa SET status=0 WHERE id=?";

        $stmt = $this->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();

        return $stmt->rowCount() > 0;
    }

    public function returnFecharMesaDb($id)
    {
        return $this->fecharMesaDb($id);
    }


}