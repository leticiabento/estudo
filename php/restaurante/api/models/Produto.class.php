<?php

include_once "Conexao.class.php";

class ProdutoModel extends Conexao
{
    private function listarProdutosDb($id)
    {
        $sql = "SELECT id, nome, valor  FROM produto WHERE cozinha_id=?";

        $stmt = $this->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function returnListarProdutosDb($id)
    {
        return $this->listarProdutosDb($id);
    }
}