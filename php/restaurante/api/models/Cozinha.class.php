<?php

include_once "Conexao.class.php";

class CozinhaModel extends Conexao
{
    private function listarCozinhasDb()
    {
        $sql = "SELECT
                cozinha.id,
                cozinha.tipo
                FROM cozinha";

        $stmt = $this->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function returnListarCozinhasDb()
    {
        return $this->listarCozinhasDb();
    }

    private function pedidosCozinhaDb($id)
    {
        $sql = "SELECT quantidade, 
                mesa.nome as nomeMesa, 
                pedido.mesa_id, 
                pedido.cozinha_id,
                produto.nome,
                produto.valor,
                pedido.produto_id
                FROM pedido JOIN produto 
                ON pedido.produto_id = produto.id
                JOIN mesa ON 
                pedido.mesa_id = mesa.id
                WHERE pedido.cozinha_id=?";

        $stmt = $this->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);

    }

    public function returnPedidosCozinhaDb($id)
    {
        return $this->pedidosCozinhaDb($id);
    }
}