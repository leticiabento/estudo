<?php

include_once "Conexao.class.php";

class PedidosModel extends Conexao
{
    private function cadastrarDb($cozinha, $produto, $quantidade, $mesa) {
        $sql = "INSERT INTO pedido
                (mesa_id, produto_id, cozinha_id, quantidade)
                VALUES(?,?,?,?)";

        $stmt = $this->prepare($sql);
        $stmt->bindValue(1, $mesa);
        $stmt->bindValue(2, $produto);
        $stmt->bindValue(3, $cozinha);
        $stmt->bindValue(4, $quantidade);
        $stmt->execute();

        return $stmt->rowCount();
    }

    public function returnCadastrarDb($cozinha, $produto, $quantidade, $mesa) {
        return $this->cadastrarDb($cozinha, $produto, $quantidade, $mesa);
    }

    private function exibirDb($id) {
        $sql = "SELECT pedido.quantidade, 
                mesa.nome as nomeMesa, 
                mesa.id as idMesa, 
                produto.nome as nomeProduto,
                produto.valor as valor,
                cozinha.tipo as tipoCozinha
                FROM pedido JOIN mesa ON pedido.mesa_id = mesa.id 
                JOIN produto ON pedido.produto_id = produto.id 
                JOIN cozinha ON pedido.cozinha_id = cozinha.id
                WHERE mesa.id = ?";

        $stmt = $this->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function returnExibirDb($id) {
        return $this->exibirDb($id);
    }

    private function excluirPedidoDb($id) {
        $sql = "DELETE FROM pedido WHERE mesa_id = ?";

        $stmt = $this->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();

        return $stmt->rowCount();
    }

    public function returnExcluirPedidoDb($id) {
        return $this->excluirPedidoDb($id);
    }
}