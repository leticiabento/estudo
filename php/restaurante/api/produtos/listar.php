<?php
header("Content-type: application/json");
if ($_SERVER['REQUEST_METHOD'] == "GET") {

    $id = $_GET['id'];
    include_once "../autoload.php";

    $produtosController = new ProdutoController();
    echo json_encode($produtosController->returnListarProdutos($id));
}

