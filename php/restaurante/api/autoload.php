<?php
/**
 * Created by PhpStorm.
 * User: OC
 * Date: 26/01/2018
 * Time: 15:32
 */
    function __autoload($class) {
        $class = explode("Controller", $class);
        include_once "controllers/" . $class[0] . ".controller.php";
    }