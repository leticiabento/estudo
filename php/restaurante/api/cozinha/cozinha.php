<?php
header("Content-type: application/json");
if ($_SERVER['REQUEST_METHOD'] == "GET") {
    include_once "../autoload.php";

    $cozinhaController = new CozinhaController();
    echo json_encode($cozinhaController->returnListarCozinhas());
}

