<?php

include_once "../models/Produto.class.php";

class ProdutoController extends ProdutoModel
{
    private function listarProdutos($id) {
        return $this->returnListarProdutosDb($id);
    }

    public function returnListarProdutos($id) {
        return $this->listarProdutos($id);
    }
}