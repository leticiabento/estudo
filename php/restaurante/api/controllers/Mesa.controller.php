<?php

include_once "../models/Mesa.class.php";

class MesaController extends MesaModel
{
    private function listarMesas()
    {
        return $this->returnConsultarMesasDb();
    }

    public function returnListarMesas()
    {
        return $this->listarMesas();
    }

    private function mudarStatusMesa($id_mesa)
    {
        return $this->returnMudarStatusMesaDb($id_mesa);
    }

    public function returnMudarStatusMesa($id_mesa)
    {
        return $this->mudarStatusMesa($id_mesa);
    }

    private function fecharMesa($id)
    {
        return $this->returnFecharMesaDb($id);
    }

    public function returnFecharMesa($id)
    {
        return $this->fecharMesa($id);
    }


}