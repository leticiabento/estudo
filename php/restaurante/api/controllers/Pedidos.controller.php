<?php

include_once "../models/Pedidos.class.php";

class PedidosController extends PedidosModel
{
    private $produto;
    private $cozinha;
    private $quantidade;
    private $mesa;

    /**
     * @return mixed
     */
    public function getProduto()
    {
        return $this->produto;
    }

    /**
     * @param mixed $produto
     */
    public function setProduto($produto)
    {
        $this->produto = $produto;
    }

    /**
     * @return mixed
     */
    public function getCozinha()
    {
        return $this->cozinha;
    }

    /**
     * @param mixed $cozinha
     */
    public function setCozinha($cozinha)
    {
        $this->cozinha = $cozinha;
    }

    /**
     * @return mixed
     */
    public function getQuantidade()
    {
        return $this->quantidade;
    }

    /**
     * @param mixed $quantidade
     */
    public function setQuantidade($quantidade)
    {
        $this->quantidade = $quantidade;
    }

    private function cadastrar($mesa) {
        return $this->returnCadastrarDb($this->getCozinha(), $this->getProduto(), $this->getQuantidade(), $mesa);
    }

    public function returnCadastrar($mesa) {
        return $this->cadastrar($mesa);
    }

    private function exibir($id) {
        return $this->returnExibirDb($id);
    }

    public function returnExibir($id) {
        return $this->exibir($id);
    }

    private function excluirPedido($id) {
        return $this->returnExcluirPedidoDb($id);
    }

    public function returnExcluirPedido($id) {
        return $this->excluirPedido($id);
    }
}