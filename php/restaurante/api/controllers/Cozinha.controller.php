<?php

include_once "../models/Cozinha.class.php";

class CozinhaController
{
    private $model;

    public function __construct()
    {
        $this->model = new CozinhaModel();
    }

    private function listarCozinhas()
    {
        return $this->model->returnListarCozinhasDb();
    }

    public function returnListarCozinhas()
    {
        return $this->listarCozinhas();
    }

    private function pedidosCozinha($id)
    {
        return $this->model->returnPedidosCozinhaDb($id);
    }

    public function returnPedidosCozinha($id)
    {
        return $this->pedidosCozinha($id);
    }
}