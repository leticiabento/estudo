<?php
header("Content-type: application/json");
if ($_SERVER['REQUEST_METHOD'] == "POST") {

    $mesa = explode("=", $_SERVER['QUERY_STRING']);


    $produto = $_POST['produto'];
    $cozinha = $_POST['cozinha'];
    $quantidade = $_POST['quantidade'];

    include_once "../autoload.php";

    $pedidosController = new PedidosController();
    $pedidosController->setProduto($produto);
    $pedidosController->setCozinha($cozinha);
    $pedidosController->setQuantidade($quantidade);


    if($pedidosController->returnCadastrar($mesa[1])) {
        echo json_encode(array("success"=>true, "message"=>"Pedido cadastrado com sucesso"));

    } else {
        echo json_encode(array("success"=>false, "message"=>"Pedido não cadastrado "));
    }
}

