<?php
header("Content-type: application/json");
if ($_SERVER['REQUEST_METHOD'] == "GET") {

    $id = $_GET['id'];
    include_once "../autoload.php";

    $pedidosController = new PedidosController();
    echo json_encode($pedidosController->returnExcluirPedido($id));
}

