-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 31-Jan-2018 às 19:51
-- Versão do servidor: 10.1.28-MariaDB
-- PHP Version: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `restaurante`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `cozinha`
--

CREATE TABLE `cozinha` (
  `id` int(11) NOT NULL,
  `tipo` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `cozinha`
--

INSERT INTO `cozinha` (`id`, `tipo`) VALUES
(6, 'bebidas'),
(7, 'massas'),
(8, 'sobremesas'),
(9, 'sanduíches'),
(10, 'pizzas');

-- --------------------------------------------------------

--
-- Estrutura da tabela `mesa`
--

CREATE TABLE `mesa` (
  `id` int(11) NOT NULL,
  `nome` varchar(45) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `mesa`
--

INSERT INTO `mesa` (`id`, `nome`, `status`) VALUES
(1, 'Mesa Um', 1),
(2, 'Mesa Dois', 0),
(3, 'Mesa Três', 1),
(4, 'Mesa Quatro', 1),
(5, 'Mesa Cinco', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido`
--

CREATE TABLE `pedido` (
  `id` int(11) NOT NULL,
  `mesa_id` int(11) NOT NULL,
  `quantidade` varchar(150) NOT NULL,
  `cozinha_id` int(11) NOT NULL,
  `produto_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `produto`
--

CREATE TABLE `produto` (
  `id` int(11) NOT NULL,
  `nome` varchar(150) NOT NULL,
  `valor` varchar(150) NOT NULL,
  `cozinha_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `produto`
--

INSERT INTO `produto` (`id`, `nome`, `valor`, `cozinha_id`) VALUES
(1, 'Refrigerante', '4.50', 6),
(2, 'Taça de Vinho', '15.00', 6),
(3, 'Copo de suco', '6.00', 6),
(4, 'Água', '2.50', 6),
(5, 'Whisky', '27.00', 6),
(6, 'Penne à parisience', '35.00', 7),
(7, 'Macarronada', '27.00', 7),
(8, 'Lasanha', '38.00', 7),
(9, 'Ravioli de Carne', '39.00', 7),
(10, 'Pudim', '7.50', 8),
(11, 'Sorvete', '5.00', 8),
(12, 'Doce de Mamão', '6.50', 8),
(13, 'Tapioca', '8.50', 8),
(14, 'Misto', '5.00', 9),
(15, 'X-filé', '17.00', 9),
(16, 'X-burguer', '15.00', 9),
(17, 'Minuano', '9.00', 9),
(18, 'Cachorro Quente', '12.00', 9),
(19, 'Portuguesa', '27.00', 10),
(20, 'Mussarela', '17.00', 10),
(21, 'Calabresa', '24.00', 10),
(22, 'Mista', '21.00', 10);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cozinha`
--
ALTER TABLE `cozinha`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mesa`
--
ALTER TABLE `mesa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido`
--
ALTER TABLE `pedido`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_pedido_mesa_idx` (`mesa_id`),
  ADD KEY `fk_pedido_cozinha1_idx` (`cozinha_id`),
  ADD KEY `fk_pedido_produto1_idx` (`produto_id`);

--
-- Indexes for table `produto`
--
ALTER TABLE `produto`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_produto_cozinha1_idx` (`cozinha_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cozinha`
--
ALTER TABLE `cozinha`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `mesa`
--
ALTER TABLE `mesa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pedido`
--
ALTER TABLE `pedido`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `produto`
--
ALTER TABLE `produto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `pedido`
--
ALTER TABLE `pedido`
  ADD CONSTRAINT `fk_pedido_cozinha1` FOREIGN KEY (`cozinha_id`) REFERENCES `produto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pedido_mesa` FOREIGN KEY (`mesa_id`) REFERENCES `mesa` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
