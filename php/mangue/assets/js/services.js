function Pessoa() {

    this.logar = function (element) {

        var data = $(element).serialize();
        $.ajax({
            url: "api/login/logar.php",
            type: "POST",
            data: data
        }).done(function (res) {
            if (!res) {
                alert('Usuário e/ou senha incorretos')
            } else {
                location.href = 'admin/product-add.html'
            }

        })
    };

    this.consultar = function () {
        $.ajax({
            url: "http://localhost/mangue/api/login/consultar.php",
            type: "GET"
        }).done(function (res) {
            var login = (location.pathname === "/mangue/login.html");

            if (res.success && login) {
                location.href = "http://localhost/mangue/admin/product-add.html";
            } else if (!res.success && !login) {
                location.href = "http://localhost/mangue/login.html";
            }


        })
    }

    this.sair = function () {
        $.ajax({
            url: "http://localhost/mangue/api/login/sair.php",
            type: "GET"
        }).done(function (res) {
            if (res) {
                location.href = "http://localhost/mangue/login.html";
            }
        })
    }
}


function Produto() {
    this.gerarCodigo = function (element) {
        $.ajax({
            url: "http://localhost/mangue/api/produto/gerarCodigo.php",
            type: "GET"
        }).done(function (res) {
            if (res > 0) {
                alert('Codigo gerado com sucesso');
                produto.exibirCodigos();

            }
        })
    }

    this.exibirCodigos = function () {
        $.ajax({
            url: "http://localhost/mangue/api/produto/exibirCodigo.php",
            type: "GET"
        }).done(function (res) {
            $(".table tbody").empty();
            var dataUso, diaUso, horaUso, diaLimite;
            res.map(function (item, value) {
                dataUso = item['data_uso'].split(' ');
                diaUso = dataUso[0].split('-');
                horaUso = dataUso[1];
                diaLimite = item['data_limite'].split('-');
                console.log(diaLimite);

                $(".table tbody").append(
                    '<tr>' +
                    '<td>' + item.codigo + '</td>' +
                    '<td>' + item.usado + '</td>' +
                    '<td>' + diaUso[2] + '/' + diaUso[1] + '/' + diaUso[0] + ' ' + horaUso +  '</td>' +
                    '<td>' + diaLimite[2] + '/' + diaLimite[1] + '/' + diaLimite[0] + '</td>' +
                    '<td><button onclick="btnExcluirCodigo(' + item.id + ')">Excluir</button></td>' +
                    '</tr>'
                );
            })
        })
    }

    this.excluirCodigo = function (id) {
        $.ajax({
            url: "http://localhost/mangue/api/produto/excluirCodigo.php?id=" + id,
            type: "GET"
        }).done(function (res) {
            if (res > 0) {
                alert('excluido com sucesso!');
                produto.exibirCodigos();
            }
        })
    }

    this.exibirProdutos = function () {
        $.ajax({
            url: "http://localhost/mangue/api/produto/exibirProd.php",
            type: "GET"
        }).done(function (res) {
            console.log(res);
            $(".table tbody").empty();
            res.map(function (item, value) {
                $(".table tbody").append(
                    '<tr>' +
                    '<td>' + item.descricao + '</td>' +
                    '<td>R$ ' + item.preco + '</td>' +
                    '<td>' + item.quantidade + '</td>' +
                    '<td><a href="product-edit.html?id=' + item.id + '"">Editar</button></td>' +
                    '<td><button onclick="btnExcluirProduto(' + item.id + ')">Excluir</button></td>' +
                    '</tr>'
                );
            })
        })
    }

    this.cadastrarProd = function (element) {
        // var data = $(element).serialize();
        var data = new FormData($(element)[0]);

        $.ajax({
            url: "http://localhost/mangue/api/produto/cadastroProd.php",
            type: "POST",
            data: data,
            processData: false,
            contentType: false
        }).done(function (res) {
            if (res) {
                alert('produto cadastrado com sucesso');
                $("input:not([type='submit'])").val('');
                produto.exibirProdutos();
                $("#add").addClass('none');
                $("#list").removeClass('none');

            } else {
                alert('produto não cadastrado. Tente novamente!');
            }
        })

    }

    this.excluirProduto = function (id) {
        $.ajax({
            url: "http://localhost/mangue/api/produto/excluirProduto.php?id=" + id,
            type: "GET"
        }).done(function (res) {
            if (res > 0) {
                alert('excluido com sucesso');
                produto.exibirProdutos();
            }
        })
    }

    this.editarProdutosEditar = function (id) {
        $.ajax({
            url: "http://localhost/mangue/api/produto/editarProdutosEditar.php?id=" + id,
            type: "GET"
        }).done(function (res) {
            if (res) {
                $("#formEditar").append("<input type='hidden' name='id' value=" + res.id + " />");

                $("#formEditar #descEditar").val(res.descricao);
                $("#formEditar #precoEditar").val(res.preco);
                $("#formEditar #quantEditar").val(res.quantidade);
                $("#formEditar #catEditar").val(res.categoria);

            }
        })
    }

    this.update = function (element) {

        var data = new FormData($(element)[0]);

        $.ajax({
            url: "http://localhost/mangue/api/produto/editarProduto.php",
            type: "POST",
            data: data,
            contentType: false,
            processData: false
        }).done(function (res) {
            if (res) {
                alert('editado com sucesso!');
                location.href = "product-add.html";

            }
        })
    }

    this.ultimasNovidades = function () {

        $.ajax({
            url: "http://localhost/mangue/api/produto/exibirUltimasNovidades.php",
            type: "GET"
        }).done(function (res) {
            $(".novidades").empty();
            res.map(function (item, value) {
                console.log(item);

                if(item.quantidade > 1) {
                    $(".novidades").append(
                        '<li>' +
                        '<div class="product-image">' +
                        '<img src="assets/uploads/products/full/'+item.imagem+'" alt="' + item.descricao + '">' +
                        '<span class="product-price"><small>R$</small>' + item.preco + '</span>' +
                        '</div>' +
                        '<a href="product.html?id=' + item.id + '" class="product-link">' + item.descricao + '</a>' +

                        '<button type="submit" onclick="btnInserirVenda(' + item.id + ')" class="btn btn-success"><i class="icon icon-ok"></i> Comprar</button>' +
                        '</li>'
                    );
                }
            })
        })

    }

    this.infosProduto = function (id) {
        $.ajax({
            url: "http://localhost/mangue/api/produto/exibirInfosProduto.php?id=" + id,
            type: "GET"
        }).done(function (res) {
            // console.log(res[0]);
            $("#product-profile, .interesse").empty();

            res[0].map(function (item, value) {

                $("#product-profile").append(
                    '<img src="assets/uploads/products/full/'+item.imagem+'" alt="">' +
                    '<div class="product-info">' +
                    '<h1>' + item.descricao + '</h1>' +
                    '<span class="product-price"><small>R$</small>' + item.preco + '</span>' +
                    '<button type="submit" onclick="btnInserirVenda(' + item.id + ')" class="btn btn-success"><i class="icon icon-ok"></i> Comprar</button>'
                );
            })

            res[1].map(function (item, value) {
                console.log(item);

                $(".interesse").append(
                    '<li>' +
                    '<div class="product-image">' +
                    '<img src="assets/uploads/products/full/'+item.imagem+'" alt="Bola adidas Brazuca Glider Copa Do Mundo da FIFA 2014 - Tam. 5">' +
                    '<span class="product-price"><small>R$</small>' + item.preco + '</span>' +
                    '</div>' +
                    '<a href="product.html?id=' + item.id + '" class="product-link">' + item.descricao + '</a>' +
                    '<button type="submit" onclick="btnInserirVenda(' + item.id + ')" class="btn btn-success"><i class="icon icon-ok"></i> Comprar</button>' +
                    '</li>'
                );
            })
        })
    }

    this.exibirCategorias = function () {
        $.ajax({
            url: "http://localhost/mangue/api/produto/exibirCategoria.php",
            type: "GET"
        }).done(function (res) {
            $("#categoria, #catEditar").empty();

            $("#categoria, #catEditar").append('<option value="">Selecione</option>');
            res.map(function (item, value) {
                $("#categoria, #catEditar").append(
                    '<option value="' + item.id + '">' + item.nome + '</option>'
                );
            })

        })
    }

    this.iniciarVenda = function (id) {

        $.ajax({
            url: "http://localhost/mangue/api/produto/iniciarVenda.php",
            type: "POST",
            data: 'id='+id
        }).done(function (res) {
            console.log(res);
            if(res > 0) {
                location.href = "checkout.html";
            } else {
                alert('produto indisponivel!');

            }
        })
    }


    this.finalizarCompra = function() {
        $.ajax({
            url: "http://localhost/mangue/api/produto/checkout.php",
            type: "GET"
        }).done(function(res) {
            $(".checkout tbody").empty();
            var total = 0;

            res.map(function(item, value) {
                // console.log(item);
                total += parseInt(item.preco);

                $(".checkout tbody").append(
                    '<tr>' +
                    '<td class="text-left"><a href="product.html?id='+item.id+'">'+item.descricao+'</a></td>' +
                    '<td class="text-right">R$ '+item.precoUnitario+'</td>' +
                    '<td class="text-right">'+item.quantidade+'</td>' +
                    '<td class="text-right">R$ '+item.preco+'</td>' +
                    '</tr>'
                );

            });

            $(".checkout tbody").append(
                '<tr>' +
                '<td colspan="3" class="title">Total da compra</td>' +
                '<td class="text-right valorTotal">R$ 319,60</td>' +
                '</tr>'
            );

            $(".valorTotal").text('R$ ' + total + '.00');

        })
    }

    this.listarQntItens = function() {
        $.ajax({
            url: "http://localhost/mangue/api/produto/listarQntItens.php",
            type: "GET"
        }).done(function (res) {
            $(".kart-n-itens").empty();
            $(".kart-n-itens").append(res[0] + ' itens');
        })
    }
};