var formLogin = $("#formLogin");
var formGerar = $(".formGerar");
var formCadProd = $("#formCadastrarProduto");
var formEditar = $("#formEditar");
var pessoa = new Pessoa();
var produto = new Produto();

$(document).ready(function () {
    init();
});

formLogin.submit(function (event) {
    event.preventDefault();
    pessoa.logar(this);
});

formGerar.submit(function (event) {
    event.preventDefault();
    produto.gerarCodigo(this);
});

formCadProd.submit(function (event) {
    event.preventDefault();
    produto.cadastrarProd(this);
});

formEditar.submit(function (event) {
    event.preventDefault();
    produto.update(this);
})

formEditar.on('keyup', function () {
    $(".btn").prop('disabled', false);
});

$(".btnSair").click(function () {
    pessoa.sair();
});


function init() {
    if (location.pathname !== "/mangue/" && location.pathname !== "/mangue/product.html" && location.pathname !== "/mangue/checkout.html") {
        pessoa.consultar();
    }

    if (location.pathname === "/mangue/" || location.pathname === "/mangue/index.html") {
        produto.ultimasNovidades();
    }

    if (location.pathname === "/mangue/admin/promos.html") {
        produto.exibirCodigos();
    }

    if (location.pathname === "/mangue/admin/product-add.html") {
        produto.exibirProdutos();
        produto.exibirCategorias();
    }

    if (location.pathname === "/mangue/admin/product-edit.html" && location.search !== -1) {
        var id = location.search.split('?id=');
        produto.editarProdutosEditar(id[1]);
        produto.exibirCategorias();
    }

    if (location.pathname === "/mangue/checkout.html") {
        produto.finalizarCompra();
    }

    if (location.pathname === "/mangue/checkout.html" || location.pathname === "/mangue/" || location.pathname === "/mangue/index.html" || (location.pathname === "/mangue/product.html" && location.search !== -1)) {
        produto.listarQntItens();
    }

    if (location.pathname === "/mangue/product.html" && location.search !== -1) {
        var id = location.search.split('?id=');
        produto.infosProduto(id[1]);
    }

    // if(location.hash == "#list") {
    //     produto.exibirProdutos();
    //
    //     $("#add").addClass('none');
    //     $("#list").removeClass('none');
    // }
}

function btnExcluirCodigo(id) {
    produto.excluirCodigo(id);
}

function btnExcluirProduto(id) {
    produto.excluirProduto(id);
}

function btnInserirVenda(id) {
    produto.iniciarVenda(id);
}

$(".btnGerar, .btnAddProd").click(function () {
    // $("#list").removeClass('none');
    $("#list").addClass('none');
    $("#add").removeClass('none');
});

$(".btnListarCodigo, .btnListProd").click(function () {
    // $("#add").removeClass('none');
    $("#add").addClass('none');
    $("#list").removeClass('none');

});


