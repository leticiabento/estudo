<?php

include_once "Conexao.class.php";

class LoginModel extends Conexao
{
    private function logarDb($email, $senha)
    {
        $sql = "SELECT * FROM login WHERE email=? AND password=?";
        $stmt = $this->prepare($sql);
        $stmt->bindValue(1, $email);
        $stmt->bindValue(2, $senha);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function returnLogarDb($email, $senha)
    {
        return $this->logarDb($email, $senha);
    }

    private function consultarDb($email)
    {
        $sql = "SELECT * FROM login WHERE email=?";
        $stmt = $this->prepare($sql);
        $stmt->bindValue(1, $email);
        $stmt->execute();

        return $stmt->rowCount();
    }

    public function returnConsultarDb($email)
    {
        return $this->consultarDb($email);
    }
}
