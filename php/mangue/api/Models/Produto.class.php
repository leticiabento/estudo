<?php

include_once "Conexao.class.php";

class ProdutoModel extends Conexao
{
    private function inserirCodigoDb($data, $dataLimite)
    {
        $sql = "INSERT INTO codigos
                (codigo, data_limite) VALUES (?, ?)";

        $stmt = $this->prepare($sql);
        $stmt->bindValue(1, $data);
        $stmt->bindValue(2, $dataLimite);
        $stmt->execute();

        return $stmt->rowCount();
    }

    public function returnInserirCodigoDb($data, $dataLimite)
    {
        return $this->inserirCodigoDb($data, $dataLimite);
    }

    private function exibirCodigosDb()
    {
        $sql = "SELECT * from codigos";

        $stmt = $this->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function returnExibirCodigosDb()
    {
        return $this->exibirCodigosDb();
    }

    private function excluirCodigoDb($id)
    {
        $sql = "DELETE FROM codigos WHERE id = ?";

        $stmt = $this->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();

        return $stmt->rowCount();
    }

    public function returnExcluirCodigoDb($id)
    {
        return $this->excluirCodigoDb($id);
    }

    private function cadastrarProdutoDb($descricao, $preco, $quantidade, $categoria, $imagem)
    {
        $row2 = 0;
        $sql = "INSERT INTO produtos
                (preco, categorias_id, descricao, imagem)
                VALUES (?,?,?,?);";

        $stmt = $this->prepare($sql);
        $stmt->bindValue(1, $preco);
        $stmt->bindValue(2, $categoria);
        $stmt->bindValue(3, $descricao);
        $stmt->bindValue(4, $imagem);
        $stmt->execute();


        $row = $stmt->rowCount();
        $id = $this->lastInsertId($row);

        if (isset($id)) {
            $sql2 = "INSERT INTO estoque
                (produtos_id, quantidade)
                VALUES (?,?);";

            $stmt2 = $this->prepare($sql2);
            $stmt2->bindValue(1, $id);
            $stmt2->bindValue(2, $quantidade);
            $stmt2->execute();

            $row2 = $stmt2->rowCount();

        }

        return $row > 0 || $row2 > 0;


    }

    public function returnCadastrarProdutoDb($descricao, $preco, $quantidade, $categoria, $imagem)
    {
        return $this->cadastrarProdutoDb($descricao, $preco, $quantidade, $categoria, $imagem);
    }

    private function exibirProdutosDb()
    {
        $sql = "SELECT produtos.id,
                    produtos.preco, 
                    produtos.categorias_id,
                    produtos.descricao,
                    produtos.imagem,
                    estoque.quantidade
                    from produtos JOIN estoque
                    ON produtos.id = estoque.produtos_id";

        $stmt = $this->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function returnExibirProdutosDb()
    {
        return $this->exibirProdutosDb();
    }

    private function excluirProdutoDb($id)
    {
        $sql = "DELETE FROM estoque WHERE produtos_id = ?";

        $stmt = $this->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $sql2 = "DELETE FROM produtos WHERE id = ?";

            $stmt2 = $this->prepare($sql2);
            $stmt2->bindValue(1, $id);
            $stmt2->execute();

        }

        return $stmt->rowCount() > 0 && $stmt2->rowCount() > 0;
    }

    public function returnExcluirProdutoDb($id)
    {
        return $this->excluirProdutoDb($id);
    }

    private function editarProdutoDb($data)
    {


        if (isset($data['imagem'])) {
            $sql = "UPDATE produtos SET preco='" . $data['preco'] . "', categorias_id='" . $data['categoria'] . "', descricao='" . $data['descricao'] . "', imagem='" . $data['imagem'] . "' WHERE id='" . $data['id'] . "'";
            $stmt = $this->prepare($sql);

        } else {
            $sql = "UPDATE produtos SET preco='" . $data['preco'] . "', categorias_id='" . $data['categoria'] . "', descricao='" . $data['descricao'] . "' WHERE id='" . $data['id'] . "'";
            $stmt = $this->prepare($sql);
        }

        $stmt->execute();
        $row = $stmt->rowCount();

//        QUANTIDADE
        $sql2 = "UPDATE estoque SET quantidade=? WHERE produtos_id=?";
        $stmt2 = $this->prepare($sql2);
        $stmt2->bindValue(1, $data['quantidade']);
        $stmt2->bindValue(2, $data['id']);
        $stmt2->execute();

        $row2 = $stmt2->rowCount();

        return $row > 0 || $row2 > 0;
    }

    public function returnEditarProdutoDb($data)
    {
        return $this->editarProdutoDb($data);
    }

    private function exibirProdutosEditarDb($id)
    {
        $sql = "SELECT produtos.id,
                    produtos.preco, 
                    produtos.categorias_id,
                    produtos.descricao,
                    estoque.quantidade
                    from produtos JOIN estoque
                    ON produtos.id = estoque.produtos_id WHERE produtos.id = '$id'";

        $stmt = $this->prepare($sql);
        $stmt->execute();


        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    public function returnExibirProdutosEditarDb($id)
    {
        return $this->exibirProdutosEditarDb($id);
    }

    private function exibirUltimasNovidadesDb()
    {
        $sql = "SELECT produtos.preco, produtos.descricao, produtos.imagem, produtos.id, estoque.quantidade from produtos 
                JOIN estoque ON estoque.produtos_id = produtos.id
                order by produtos.id DESC limit 4";

        $stmt = $this->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function returnExibirUltimasNovidadesDb()
    {
        return $this->exibirUltimasNovidadesDb();
    }

    private function infosProdutoDb($id)
    {
        $sql = "SELECT * FROM produtos WHERE id = '$id'";

        $stmt = $this->prepare($sql);

        if ($stmt->execute()) {
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $categoria = $result[0]['categorias_id'];

            $sql2 = "SELECT * FROM produtos WHERE categorias_id = '$categoria' limit 4";
            $stmt2 = $this->prepare($sql2);
            $stmt2->execute();

            $result2 = $stmt2->fetchAll(PDO::FETCH_ASSOC);
            return array($result, $result2);
        }

    }

    public function returnInfosProdutoDb($id)
    {
        return $this->infosProdutoDb($id);
    }

    private function exibirCategoriasDb()
    {
        $sql = "SELECT * from categorias";

        $stmt = $this->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);

    }

    public function returnExibirCategoriasDb()
    {
        return $this->exibirCategoriasDb();
    }

    private function iniciarVendaDb()
    {
        $sql = "INSERT INTO venda 
                (total) VALUES (?)";

        $stmt = $this->prepare($sql);
        $stmt->bindValue(1, '0');
//        $this->beginTransaction();
        $stmt->execute();
        $row = $stmt->rowCount();

        if ($row > 0) {
            return $this->lastInsertId($row);
            //pego a id de venda e retorno
        }
    }

    public function returnIniciarVendaDb()
    {
        return $this->iniciarVendaDb();
    }

    private function inserirVendaItens($id)
    {
        $sql = "INSERT INTO itens (venda_id) VALUES (?)";

        $stmt = $this->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();

        return ($stmt->rowCount() > 0) ? $this->lastInsertId() : false;
        // pego a id de itens e retorno
    }

    public function returnInserirVendaItens($id)
    {
        return $this->inserirVendaItens($id);
    }

    private function produtoReferente($idProduto, $idVenda)
    {

        $sql = "INSERT INTO itens
                  (produtos_id, venda_id) VALUES(?,?)";

        $stmt = $this->prepare($sql);
        $stmt->bindValue(1, $idProduto);
        $stmt->bindValue(2, $idVenda);
        $stmt->execute();

        return $stmt->rowCount();


    }

    public function buscarProdutoReferente($idProduto, $idVenda)
    {
        return $this->produtoReferente($idProduto, $idVenda);
    }

    private function exibirProdutosCheckout($id)
    {
        $sql = "SELECT  
				produtos.id,
				produtos.preco as precoUnitario,
                sum(produtos.preco) as preco, 
                produtos.id as idProd,
                produtos.descricao,
                sum(itens.quantidade) as quantidade
                from produtos JOIN itens
                ON produtos.id = itens.produtos_id
                where venda_id = ? GROUP BY produtos.id";

        $stmt = $this->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();


        return $stmt->fetchAll();
    }

    public function returnExibirProdutosCheckout($id)
    {
        return $this->exibirProdutosCheckout($id);
    }

    private function verificarQntItensDb($idProd, $venda_id)
    {

        $sql = "SELECT sum(itens.quantidade) as qntItem, venda_id, produtos.descricao, produtos.preco, 
                    estoque.quantidade as qntEstoque 
                    FROM mangue.itens JOIN produtos
                    ON produtos.id = itens.produtos_id
                    JOIN estoque
                    ON estoque.produtos_id = produtos.id
                    WHERE venda_id = ? AND itens.produtos_id = ?  group by produtos.id";

        $stmt = $this->prepare($sql);
        $stmt->bindValue(1, $venda_id);
        $stmt->bindValue(2, $idProd);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function returnVerificarQntItensDb($idProd, $venda_id)
    {
        return $this->verificarQntItensDb($idProd, $venda_id);
    }

    private function listarQntItensDb($id)
    {
        $sql = "SELECT sum(itens.quantidade) as qnt from itens where venda_id = ?";

        $stmt = $this->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();

        return $stmt->fetch();

    }

    public function returnListarQntItensDb($id)
    {
        return $this->listarQntItensDb($id);
    }
}
