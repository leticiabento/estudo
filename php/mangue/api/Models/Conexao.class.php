<?php

class Conexao extends PDO {

    private $dsn = "mysql:host=localhost; dbname=mangue; charset=utf8; port=3306";
    private $user = "root";
    private $password = "";
    private $handle = null;

    public function __construct()
    {
        if($this->handle === null) {
           try {
               $dbh = parent::__construct($this->dsn, $this->user, $this->password, array(PDO::ATTR_PERSISTENT => true));
               $this->handle = $dbh;
           } catch(PDOException $e) {
               die("Error: ". $e->getMessage());
           }
        }
    }

    public function __destruct()
    {
        $this->handle = null;
    }
}