<?php

header("Content-type: application/json");

if($_SERVER['REQUEST_METHOD'] == "GET") {

    include_once "../autoload.php";

    $produtoController = new ProdutoController();
    echo json_encode($produtoController->returnExibirCodigos());
}