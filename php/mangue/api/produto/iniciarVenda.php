<?php

header("Content-type: application/json");

if ($_SERVER['REQUEST_METHOD'] == "POST") {

    $id = $_POST['id'];

    include_once "../autoload.php";

    $produtoController = new ProdutoController();
    $produtoController->setId($id);
    echo json_encode($produtoController->returnIniciarVenda());
}