<?php

header("Content-type: application/json");

if($_SERVER['REQUEST_METHOD'] == "GET") {

    session_start();
    if(isset($_SESSION['venda_id'])) {
        include_once "../autoload.php";

        $produtoController = new ProdutoController();
        echo json_encode($produtoController->returnCheckoutProduto($_SESSION['venda_id']));
    }

}