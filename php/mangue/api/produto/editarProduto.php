<?php

header("Content-type: application/json");

if ($_SERVER['REQUEST_METHOD'] == "POST") {

    $id = $_POST['id'];
    $descricao = $_POST['descricao'];
    $preco = $_POST['preco'];
    $quantidade = $_POST['quantidade'];
    $categoria = $_POST['categoria'];
    $imagem = $_FILES['imagem'];


    include_once "../autoload.php";

    $produtoController = new ProdutoController();
    $produtoController->setId($id);
    $produtoController->setDescricao($descricao);
    $produtoController->setPreco($preco);
    $produtoController->setQuantidade($quantidade);
    $produtoController->setCategoria($categoria);

    if (!empty($imagem['name'])) {
        $extensao = @end(explode(".", $imagem['name']));
        $size = $imagem['size'] / 1024;

        if ($extensao === "jpg") {
            $novaImagem = uniqid(md5($imagem['tmp_name'])) . "." . $extensao;
            $produtoController->setImagem($novaImagem);
            $dir = "./../../assets/uploads/products/full/";
            $move = move_uploaded_file($imagem['tmp_name'], $dir . $novaImagem);

            if ($move) {
                echo json_encode($produtoController->returnEditarProduto());
            }
        }
    } else {
        echo json_encode($produtoController->returnEditarProduto());
    }

}