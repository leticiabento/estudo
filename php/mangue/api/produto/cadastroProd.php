<?php

header("Content-type: application/json");

if($_SERVER['REQUEST_METHOD'] == "POST") {

    $descricao = $_POST['descricao'];
    $preco = $_POST['preco'];
    $quantidade = $_POST['quantidade'];
    $categoria = $_POST['categoria'];
    $imagem = $_FILES['imagem'];

    $dir = "./../../assets/uploads/products/full/";
    $extensao = @end(explode(".", $imagem['name']));

    $novaImagem = uniqid(md5($imagem['tmp_name'])) . "." . $extensao;
    $size = $imagem['size'] / 1024;


    if($extensao === "jpg") {
        $move = move_uploaded_file($imagem['tmp_name'], $dir.$novaImagem);

        include_once "../autoload.php";

        $produtoController = new ProdutoController();
        $produtoController->setDescricao($descricao);
        $produtoController->setPreco($preco);
        $produtoController->setQuantidade($quantidade);
        $produtoController->setCategoria($categoria);
        $produtoController->setImagem($novaImagem);

        echo json_encode($produtoController->returnCadastrarProduto());
    }



}