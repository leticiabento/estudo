<?php

header("Content-type: application/json");

if($_SERVER['REQUEST_METHOD'] == "GET") {
    include_once "../autoload.php";

    $loginController = new LoginController();
    echo json_encode(!$loginController->returnLimparLogin());
}