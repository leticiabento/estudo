<?php

include_once "../Models/Produto.class.php";

class ProdutoController
{
    private $id;
    private $descricao;
    private $preco;
    private $quantidade;
    private $categoria;
    private $imagem;
    private $prodModel;

    public function __construct()
    {
        $this->prodModel = new ProdutoModel();
    }

    public function getImagem()
    {
        return $this->imagem;
    }

    public function setImagem($imagem)
    {
        $this->imagem = $imagem;
    }

    public function getDescricao()
    {
        return $this->descricao;
    }

    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }

    /**
     * @return mixed
     */
    public function getPreco()
    {
        return $this->preco;
    }

    /**
     * @param mixed $preco
     */
    public function setPreco($preco)
    {
        $this->preco = $preco;
    }

    /**
     * @return mixed
     */
    public function getQuantidade()
    {
        return $this->quantidade;
    }

    /**
     * @param mixed $quantidade
     */
    public function setQuantidade($quantidade)
    {
        $this->quantidade = $quantidade;
    }

    /**
     * @return mixed
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * @param mixed $categoria
     */
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }


    private function gerarCodigo()
    {
        $date = date("Y-m-d H:i:s");
        $data = md5($date);

        $datalimite = explode("-", $date);
        $datalimiteNew = $datalimite[1] + 5;
        $dataLimiteNova = $datalimite[0] . '-' . $datalimiteNew . '-' . $datalimite[2];

        return $this->prodModel->returnInserirCodigoDb($data, $dataLimiteNova);
    }

    public function returnGerarCodigo()
    {
        return $this->gerarCodigo();
    }

    private function exibirCodigos()
    {
        return $this->prodModel->returnExibirCodigosDb();
    }

    public function returnExibirCodigos()
    {
        return $this->exibirCodigos();
    }

    private function excluirCodigo($id)
    {
        return $this->prodModel->returnExcluirCodigoDb($id);
    }

    public function returnExcluirCodigo($id)
    {
        return $this->excluirCodigo($id);
    }

    private function cadastrarProduto()
    {
        return $this->prodModel->returnCadastrarProdutoDb($this->getDescricao(), $this->getPreco(), $this->getQuantidade(), $this->getCategoria(), $this->getImagem());
    }

    public function returnCadastrarProduto()
    {
        return $this->cadastrarProduto();
    }


    private function exibirProdutos()
    {
        return $this->prodModel->returnExibirProdutosDb();
    }

    public function returnExibirProdutos()
    {
        return $this->exibirProdutos();
    }

    private function excluirProduto($id)
    {
        return $this->prodModel->returnExcluirProdutoDb($id);
    }

    public function returnExcluirProduto($id)
    {
        return $this->excluirProduto($id);
    }

    private function editarProduto()
    {
        $data = array(
            'descricao' => $this->getDescricao(),
            'preco' => $this->getPreco(),
            'quantidade' => $this->getQuantidade(),
            'categoria' => $this->getCategoria(),
            'id' => $this->getId(),
            'imagem' => $this->getImagem()
        );

        return $this->prodModel->returnEditarProdutoDb($data);
    }

    public function returnEditarProduto()
    {
        return $this->editarProduto();
    }

    private function exibirProdutosEditar($id)
    {
        return $this->prodModel->returnExibirProdutosEditarDb($id);
    }

    public function returnExibirProdutosEditar($id)
    {
        return $this->exibirProdutosEditar($id);
    }

    private function exibirUltimasNovidades()
    {
        return $this->prodModel->returnExibirUltimasNovidadesDb();
    }

    public function returnExibirUltimasNovidades()
    {
        return $this->exibirUltimasNovidades();
    }

    private function infosProduto($id)
    {
        return $this->prodModel->returnInfosProdutoDb($id);
    }

    public function returnInfosProduto($id)
    {
        return $this->infosProduto($id);
    }

    private function exibirCategorias()
    {
        return $this->prodModel->returnExibirCategoriasDb();
    }

    public function returnExibirCategorias()
    {
        return $this->exibirCategorias();
    }

    private function iniciarVenda()
    {
        if (!isset($_SESSION['venda_id'])) {
            $_SESSION['venda_id'] = $this->prodModel->returnIniciarVendaDb($this->getId());
        }

        return $this->returnVerificarQntItens($this->getId(), $_SESSION['venda_id']);
    }

    public function returnIniciarVenda()
    {
        session_start();
        return $this->iniciarVenda();
    }

    private function checkoutProduto($id)
    {
        return $this->prodModel->returnExibirProdutosCheckout($id);
    }

    public function returnCheckoutProduto($id)
    {
        return $this->checkoutProduto($id);
    }

    private function verificarQntItens()
    {
        $result = $this->prodModel->returnVerificarQntItensDb($this->getId(), $_SESSION['venda_id']);

        if (empty($result) || (is_array($result) && (int)$result[0]['qntEstoque'] > (int)$result[0]['qntItem'])) {
            return $this->prodModel->buscarProdutoReferente($this->getId(), $_SESSION['venda_id']);
        } else {
            return false;
        }


    }

    public function returnVerificarQntItens()
    {
        return $this->verificarQntItens();
    }

    private function listarQntItens()
    {
        return $this->prodModel->returnListarQntItensDb($_SESSION['venda_id']);
    }

    public function returnListarQntItens()
    {
        session_start();
        return $this->listarQntItens();
    }

}