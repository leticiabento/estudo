<?php

include_once "../Models/Login.class.php";

class LoginController extends LoginModel
{

    private $email;
    private $senha;

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getSenha()
    {
        return $this->senha;
    }

    /**
     * @param mixed $senha
     */
    public function setSenha($senha)
    {
        $this->senha = $senha;
    }


    private function logar()
    {
        session_start();
        $user = $this->returnLogarDb($this->getEmail(), $this->getSenha());

        if (!empty($user)) {
            $_SESSION['email'] = $this->getEmail();

            return true;
        }

        return $this->limparLogin();

    }

    public function returnLogar()
    {
        return $this->logar();
    }

    private function consultar()
    {
        session_start();
        if (isset($_SESSION['email']) && ($this->returnConsultarDb($_SESSION['email']) > 0)) {
            return array("success" => true, "email" => $_SESSION['email']);
        }

        return $this->limparLogin();
    }

    public function returnConsultar()
    {
        return $this->consultar();
    }

    private function limparLogin()
    {
        session_destroy();
        session_unset();

        return false;
    }

    public function returnLimparLogin()
    {
        session_start();
        return $this->limparLogin();
    }
}