// FUN��ES GERAIS
jQuery.exists = function(selector) { return ($(selector).length > 0); }
//rel�gio sess�o
var sa;

function validacaoObrigatorios(idformulario){
	$("#"+idformulario+" .required").each(function(index,l) {
		if($(this).attr('type')=='radio' || $(this).attr('type')=='checkbox'){
			if($('input[name="'+$(this).attr('name')+'"]:checked').length == 0){
				var campo = $(this).prev().text();
				msgErro += "<br /> - Selecione uma das op��es no campo "+campo.substr(0,campo.length - 2).toUpperCase() +" para efetivar a opera��o !!!";
				erro = true; destacarErro(document.getElementById($(this).attr('id'))); 
			} else {
				desmarcarErro(document.getElementById($(this).attr('id')));
			}
		}else{
			if($(this).val()==''){
				var campo = $("label[for='"+$(this).attr('id')+"']").text();
				msgErro += "<br /> - Preencha o campo "+campo.substr(0,campo.length - 2).toUpperCase() +" para efetivar a opera��o !!!";
				erro = true; destacarErro(document.getElementById($(this).attr('id'))); 
			} else {
				//erro = false;
				desmarcarErro(document.getElementById($(this).attr('id')));
			}
		}
	});
}

function destacarErro(obj) {$("#" + obj.id).css("background", "#fceab5"); }

function desmarcarErro(obj) {$("#" + obj.id).css("background", "#F9F9F9"); }

function quemEhQuem(cod) {
	$("form#frmquemehquem input#cod_usuario").val(cod);
	$("form#frmquemehquem").submit();
}

function voltar(value) { history.go(-1); }
//converter caracteres para HTML Entities (& > &amp;)
function htmlEncode(value) {
	if (value) {
		return $("<div />").text(value).html();
	} else { return "";	}
}
//converter HTML Entities para caracteres (&amp; > &)�
function htmlDecode(value) {
	if (value) {
		return $("<div />").html(value).text();
	} else { return ""; }
}

var newwindow = '';
function abrir(Pag,nome,width,height,resizable){
	if (Pag != "") {
		var newwindow = '';
		var winl = (screen.width - width) / 2;
		var wint = (screen.height - height) / 2;
		if(resizable!=0) { var linky = 'resizable=yes,'; } else { var linky = ''; }

		if (!newwindow.closed && newwindow.location) {
			newwindow.location.href = Pag;
		} else {
			newwindow = window.open(Pag+'',nome,'menubar=yes,'+linky+'scrollbars=yes,status=no,toolbar=no,width='+width+',height='+height+',top='+wint+',left='+winl+'');
			if (!newwindow.opener) { newwindow.opener = self; }
		}
		if (window.focus) { newwindow.focus() }
		return false;
	}
}

function reverse(s) { return s.split("").reverse().join(""); } //reverter string

/* AJAX CRUD */

/* ENVIO PADR�O DE SUBMITS VIA AJAX
	id_formulario = "frmcadastro", "aluno" ...
	rotnornos(lista separada por v�gula das vari�veis de retorno desejadas) = "titulo,ativo", "texto,comentario,arquivo"
*/
function submitAjax(id_formulario,lista_retornos,formulario_de_acao) {
	$("#botao-salvar").attr("disabled", true);
	if (formulario_de_acao == undefined) { formulario_de_acao = false; }
	var retorno = null;
	if ($(".xinha_iframe").length > 0) {
		$(".xinha_iframe").each(function () {
			$("#" + $(this).attr("id").replace("XinhaIFrame_","")).val(eval("xinha_editors." + $(this).attr("id").replace("XinhaIFrame_","") + ".getHTML()"));
		});
	}
	var tdata = $("#" + id_formulario).serializeArray();
	var data = "";
	var regex_espaco = /\s/g;
	jQuery.each(tdata, function(i, field){
		field.value = encodeURIComponent(field.value);
		field.value = field.value.replace(regex_espaco, "%20");
		data += field.name + "=" + field.value + "x&";
    });
	$.ajax({
		type: "POST",
		contentType: 'application/x-www-form-urlencoded; charset=iso-8859-1;',
		async: false, /*aguardar execu��o da chamada*/
		url: $("#" + id_formulario).attr("action"),
		data: data+"lajx=x",
		beforeSend: function() {  },
		success: function (data, textStatus, jqXHR) {
			var sucesso = true;
			var error = "";
			try{
	   			data = jQuery.parseJSON(data);
			}catch(err){
				sucesso = false;
				if (data.indexOf("superErro")!=-1){
					error = data.substring(data.indexOf("superErro")+14,data.indexOf("</span>"));
				} else {
					error = err;
				}
	   			exibirFlashMsg("Ocorreu um erro! N�o foi poss�vel executar a opera��o solicitada.", error, "erro");
			}	
			//if (data.erro != null) {
			if (!sucesso) {
				if (retorno == null) { retorno = data; }
				exibirFlashMsg("Ocorreu um erro! N�o foi poss�vel executar a opera��o solicitada.", data.erro, "erro");
			} else {
				if (retorno == null) { retorno = data; }
				exibirFlashMsg("Opera��o realizada com sucesso!", data.mensagem, "ok");
				if ($.exists("#cod")) {
					$("#cod").val(data.cod);
					alterarTituloAcao();
					if ($.exists("#opt")) $("#opt").val("EDITAR");
				}
			}
		},
		error: function (xhr, textStatus, errorThrown) {
			exibirFlashMsg("Ocorreu um erro! N�o foi poss�vel executar a opera��o solicitada.",
				"Caso o erro persista, favor entrar em contato com a Administra��o do Sistema.",
				"erro");
		}
	});
	$("#botao-salvar").attr("disabled", false);
	return retorno;
}

function alterarTituloAcao() {
	var ehNovo = false;
	var regex_operacao = /\[ incluir \]/g;
	var tHtml = "";
	if ($.exists("title")) {
		tHtml = $("title").html();
		tHtml = tHtml.replace(regex_operacao, " [ editar ]");
		$("title").innerHtml = tHtml;
	}
	$("h2").each(function() {
		if ($(this).html().indexOf("[ incluir ]") > -1) {
			$(this).html($(this).html().replace(regex_operacao, " [ editar ]"));
		}
	});
	var regex_operacao = /\[ novo \]/g;
	if ($.exists("title")) {
		tHtml = $("title").html();
		tHtml = tHtml.replace(regex_operacao, " [ editar ]");
		$("title").innerHtml = tHtml;
	}
	$("h2").each(function() {
		if ($(this).html().indexOf("[ incluir ]") > -1) {
			$(this).html($(this).html().replace(regex_operacao, " [ editar ]"));
		}
	});
	if (ehNovo) { exibirBotaoNovo(); }
}

function exibirBotaoNovo() {
	if ($.exists("#ctrl_novoregistro")) {
		$("#ctrl_novoregistro").removeClass("invisivel");
	}
}

function confirmarEnvio(id_formulario, campos_a_serem_preenchidos, valores_de_preenchimento) {
	/*preenchimento de campos de configura��o
		- os campos a serem preenchidos devem ser declarados na mesma ordem
			que a ordem os valores do valores_de_preenchimento*/
	if (campos_a_serem_preenchidos != undefined) {
		campos_a_serem_preenchidos = removerEspacosEmBranco(campos_a_serem_preenchidos);
		campos_a_serem_preenchidos = campos_a_serem_preenchidos.split(",");
		var i = 0;
		for(valor in valores_de_preenchimento) { $("#" + campos_a_serem_preenchidos[i]).val(valores_de_preenchimento[valor]); i++; }
	}
	/*resetar formul�rio com valores padr�o para os campos, de acordo com os valores atual*/
	$("#" + id_formulario).children("input:text, input:hidden, input:password, textarea").each(function() { $(this).prop("defaultValue", $(this).val()); });
	$("#" + id_formulario).children("input:checkbox, input:radio, fieldset input:checkbox, fieldset input:radio").each(function() { if ($(this).prop("checked")) { $(this).prop("defaultChecked", true); } });
	$("#" + id_formulario).children("select, fieldset select").each(function() {
		var select_pai = $(this);
		$(this).children("option").each(function() {
			if (select_pai.val() == $(this).val()) {
				$(this).prop("defaultSelected", true);
				$(this).prop("selected", true);
			} else { $(this).prop("defaultSelected", false); }
		});
	});
	$("#" + id_formulario).children("fieldset").each(function() {
		$(this).children("input:text, input:hidden, input:password, textarea").each(function() { $(this).prop("defaultValue", $(this).val());	});
		$(this).children("input:checkbox, input:radio, input:checkbox, input:radio").each(function() { if ($(this).prop("checked")) { $(this).prop("defaultChecked", true); } });
		$(this).children("select").each(function() {
			var select_pai = $(this);
			$(this).children("option").each(function() {
				if (select_pai.val() == $(this).val()) {
					$(this).prop("defaultSelected", true);
					$(this).prop("selected", true);
				} else { $(this).prop("defaultSelected", false); }
			});
		});
	});
}

function paginar(form){
	var field = form.numpaginas;
	var erro = false;

	if (!field.value || field.value <= 0) {
		alert('ERRO: Informe um n�mero v�lido para a quantidade de REGISTROS POR P�GINA !!!');
		erro = true; destacarErro(field);
		field.focus();
	}

	if (!erro) { return true; } else { return false; }
}

function irParaPagina(pagina) { document.paginacao.currpage.value = pagina; document.paginacao.submit(); }

function incluirRegistro() { document.frmincluir.submit(); }

function novoRegistro(botao, destino) {
	$("<form id='formnovoregistro' action='" + destino + "' method='POST'>" +
		"<input type='hidden' name='opt' value='incluir' />" +
		"<input type='hidden' name='cod' value='' />" +
	"</form>").insertBefore("#" + botao.id);
	$("#formnovoregistro").submit();
}

function editarRegistro(codigo) {
	var form = document.listaregistros;
	var campoCod = document.getElementById('editar-cod');
	var valor = null;
	
	if (codigo == null || codigo == '') {
		if (form.chaveregistro.length) {
			for (var i = 0; i < form.chaveregistro.length; i++)
				if (form.chaveregistro[i].checked) { valor = form.chaveregistro[i].value; }
		} else { if (form.chaveregistro.checked) { valor = form.chaveregistro.value; } }
	} else { valor = codigo; }

	if (valor) {
		campoCod.value = valor;
		document.frmeditar.submit();
	} else { alert('ERRO: � necess�rio selecionar um REGISTRO na listagem principal para efetivar esta opera��o !!!'); }
}

function excluirRegistro(form) {
	var form = document.listaregistros;
	var campoCod = document.getElementById('excluir-cod');
	var valor = null;
	if (form.chaveregistro.length) {
		for (var i = 0; i < form.chaveregistro.length; i++)
			if (form.chaveregistro[i].checked) valor = form.chaveregistro[i].value;
	} else { if (form.chaveregistro.checked) { valor = form.chaveregistro.value; } }
	if (valor) {
		var registro = document.getElementById('registro_' + valor) ? document.getElementById('registro_' + valor).innerHTML : 'NULL';
		var mensagem = 'Voc� realmente deseja excluir o registro \'' + registro + '\' do sistema? \n\n A exclus�o deste registro poder� afetar outros registros existentes no sistema.';
		if (confirm(mensagem)) {
			campoCod.value = valor;
			//document.excluir.submit();
			if (resultado = submitAjax("frmexcluir", "")) {
				if (resultado.erro == null) {
					$("<input type='hidden' name='fm-tiporesposta' value='ok' />" +
						"<input type='hidden' name='fm-msgresposta' value='Registro(s) exclu�do(s) com sucesso.' />").appendTo("form#paginacao");
					document.paginacao.submit();
				} else {
					exibirFlashMsg("Ocorreu um erro! N�o foi poss�vel executar a opera��o solicitada.", resultado.erro, "erro");
				}
			} else { exibirFlashMsg("Ocorreu um erro! N�o foi poss�vel executar a opera��o solicitada.", "", "erro"); }
		}
	} else { alert('ERRO: � necess�rio selecionar um REGISTRO na listagem principal para efetivar esta opera��o !!!'); }
}

function printWindow() { bV = parseInt(navigator.appVersion); if (bV >= 4) window.print(); }

var linhaselecionada = '';
function limpa(form,campo){
	for (var i=0; i<document.forms[form][campo].length; i++) {
		if (document.forms[form][campo][i].checked) {
			var linhaselecionada = document.forms[form][campo][i].value;	
			if (linhaselecionada.substring(0, linhaselecionada.indexOf("_"))){
			selecionalinha(linhaselecionada.substring(linhaselecionada.indexOf("_")+1,linhaselecionada.length));
			} else { selecionalinha(document.forms[form][campo][i].value); }
			return;
		}
	}
	if (document.forms[form][campo].checked) { 
		var linhaselecionada = document.forms[form][campo].value;	
		selecionalinha(document.forms[form][campo].value);
		return;
	}
}

function selecionalinha(valor){
	if(!valor) return;
	if(valor != linhaselecionada){
		if (linhaselecionada){
			linha = 'row_'+linhaselecionada;
			var oTD = document.getElementById(linha);
			oTD.bgColor='';
		}		
		linhaselecionada = valor;
		linha = 'row_'+valor;
		var oTD = document.getElementById(linha);
		oTD.bgColor='#FFFFCC';	
	}
}

function controlalinha(id,estado) { $("#" + id).css("display", estado); }

function emailCheck (emailStr) {
	if (!emailStr) {
		alert("Indique um endere�o de e-mail v�lido !!!");
		return false;
	}
	/* The following variable tells the rest of the function whether or not
	to verify that the address ends in a two-letter country or well-known
	TLD.  1 means check it, 0 means don't. */
	var checkTLD=1;
	/* The following is the list of known TLDs that an e-mail address must end with. */
	var knownDomsPat=/^(com|net|org|edu|int|mil|gov|arpa|biz|aero|name|coop|info|pro|museum)$/;
	/* The following pattern is used to check if the entered e-mail address
	fits the user@domain format.  It also is used to separate the username
	from the domain. */
	var emailPat=/^(.+)@(.+)$/;
	/* The following string represents the pattern for matching all special
	characters.  We don't want to allow special characters in the address. 
	These characters include ( ) < > @ , ; : \ " . [ ] */
	var specialChars="\\(\\)><@,;:\\\\\\\"\\.\\[\\]";
	/* The following string represents the range of characters allowed in a 
	username or domainname.  It really states which chars aren't allowed.*/
	var validChars="\[^\\s" + specialChars + "\]";
	/* The following pattern applies if the "user" is a quoted string (in
	which case, there are no rules about which characters are allowed
	and which aren't; anything goes).  E.g. "jiminy cricket"@disney.com
	is a legal e-mail address. */
	var quotedUser="(\"[^\"]*\")";
	/* The following pattern applies for domains that are IP addresses,
	rather than symbolic names.  E.g. joe@[123.124.233.4] is a legal
	e-mail address. NOTE: The square brackets are required. */
	var ipDomainPat=/^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/;
	/* The following string represents an atom (basically a series of non-special characters.) */
	var atom=validChars + '+';
	/* The following string represents one word in the typical username.
	For example, in john.doe@somewhere.com, john and doe are words.
	Basically, a word is either an atom or quoted string. */
	var word="(" + atom + "|" + quotedUser + ")";
	// The following pattern describes the structure of the user
	var userPat=new RegExp("^" + word + "(\\." + word + ")*$");
	/* The following pattern describes the structure of a normal symbolic
	domain, as opposed to ipDomainPat, shown above. */
	var domainPat=new RegExp("^" + atom + "(\\." + atom +")*$");
	/* Finally, let's start trying to figure out if the supplied address is valid. */
	/* Begin with the coarse pattern to simply break up user@domain into
	different pieces that are easy to analyze. */
	var matchArray=emailStr.match(emailPat);
	if (matchArray==null) {
		/* Too many/few @'s or something; basically, this address doesn't
		even fit the general mould of a valid e-mail address. */
		alert("Endere�o de E-mail inv�lido !!!");
		return false;
	}
	var user=matchArray[1];
	var domain=matchArray[2];
	// Start by checking that only basic ASCII characters are in the strings (0-127).
	for (i=0; i<user.length; i++) {
		if (user.charCodeAt(i)>127) {
			alert("O endere�o de e-mail cont�m caracteres inv�lidos !!!");
			return false;
		}
	}
	for (i=0; i<domain.length; i++) {
		if (domain.charCodeAt(i)>127) {
			alert("O endere�o de e-mail cont�m caracteres inv�lidos !!!");
			return false;
		}
	}
	// See if "user" is valid 
	if (user.match(userPat)==null) {	// user is not valid
		alert("Endere�o de e-mail inv�lido !!!");
		return false;
	}
	/* if the e-mail address is at an IP address (as opposed to a symbolic
	host name) make sure the IP address is valid. */
	var IPArray=domain.match(ipDomainPat);
	if (IPArray!=null) { // this is an IP address
		for (var i=1;i<=4;i++) { 
			if (IPArray[i]>255) {
				alert("Endere�o IP inv�lido !!!");
				return false;
			}
		}
		return true;
	}
	// Domain is symbolic name.  Check if it's valid.	 
	var atomPat=new RegExp("^" + atom + "$");
	var domArr=domain.split(".");
	var len=domArr.length;
	for (i=0;i<len;i++) {
		if (domArr[i].search(atomPat)==-1) {
		alert("Endere�o de e-mail inv�lido !!!");
		return false;
		 }
	}
	/* domain name seems valid, but now make sure that it ends in a
	known top-level domain (like com, edu, gov) or a two-letter word,
	representing country (uk, nl), and that there's a hostname preceding 
	the domain or country. */
	if (checkTLD && domArr[domArr.length-1].length!=2 && 
		domArr[domArr.length-1].search(knownDomsPat)==-1) {
		alert("Endere�o de e-mail inv�lido !!!");
		return false;
	}
	// Make sure there's a host name preceding the domain.
	if (len<2) {
		alert("Endere�o de e-mail inv�lido !!!");
		return false;
	}
	return true; // If we've gotten this far, everything's valid!
}

function descobreBrowser() {
	var versao;
	if (navigator.userAgent.indexOf('MSIE')>0) { //BROWSERS IE
		versao=navigator.userAgent.substring((navigator.userAgent.indexOf('MSIE')+4), navigator.userAgent.length);
		versao=versao.substring(0, versao.indexOf('.'));
		return versao;
	} else {
		return null;
	}
}

/*OPERA��ES PARA CAMPO SELECT*/
function addConteudo(campoID, objConteudo) {
	try { document.getElementById(campoID).add(objConteudo, null); }
	catch(ex) { document.getElementById(campoID).add(objConteudo); }
}
function appendChildConteudo(campoID, objConteudo) {
	try { document.getElementById(campoID).appendChild(objConteudo, null); }
	catch(ex) { document.getElementById(campoID).appendChild(objConteudo); }
}
/*ORDENA��O DE LISTAS*/
function pcima(id,limite) {
	var obj = document.getElementById(id);
	var colecao = obj.parentNode.getElementsByTagName('LI');
	var proximo = null;

	var parar = false;
	for (i = 0; i < colecao.length; i++) {
		if (colecao[i].id == id) {
			for (j = (i-1); j >= 0; j--) {
				if (colecao[i].parentNode == colecao[j].parentNode) {
					proximo = colecao[j];
					$("#" + obj.id).animate({ 
						opacity: 0.2
					}, 800, function (){
						cor_obj = obj.style.backgroundColor;
						cor_proximo = proximo.style.backgroundColor
						if (i == limite) {
							obj.style.backgroundColor = cor_proximo;
							proximo.style.backgroundColor = cor_obj;
						}
						obj.parentNode.insertBefore(obj, proximo);
					});
					$("#" + obj.id).animate({ 
						opacity: 1
					}, 2000 );
					parar = true;
					break;
				}
			}
			if (parar) break;
		}
	}
}
function pbaixo(id,limite) {
	if (!limite) var limite = null;
	var obj = document.getElementById(id);
	var colecao = obj.parentNode.getElementsByTagName('LI');
	var anterior = null;

	var parar = false;
	for (i = 0; i < colecao.length; i++) {
		if (colecao[i].id == id) {
			for (j = (i+1); j < colecao.length; j++) {
				if (colecao[i].parentNode == colecao[j].parentNode) {
					anterior = colecao[j];$("#" + obj.id).animate({ 
						opacity: 0.2
					}, 200, function (){
						cor_obj = obj.style.backgroundColor;
						cor_anterior = anterior.style.backgroundColor
						if (i == (limite-1)) {
							obj.style.backgroundColor = cor_anterior;
							anterior.style.backgroundColor = cor_obj;
						}
						obj.parentNode.insertBefore(anterior, obj);
					});
					$("#" + obj.id).animate({ 
						opacity: 1
					}, 2000 );
					parar = true;
					break;
				}
			}
			if (parar) break;
		}
	}
}
function nomeRealMidia(titulo) {
	titulo = titulo.substring((0, titulo.indexOf('_')+1));
	return titulo;
}
//function destacarErro(obj) { $("#" + obj.id).css("background", "#fceab5"); }

// POPUP - na mesma janela; IFrame
//  - chamada <a href="#abrirpopup" onclick="javascript:abrirPopup('usuarios_lista.asp', 400, 200);">abrir popup</a>
function posicionarPopup(src, width, height, show) {
	if (width == undefined) width = "640";
	width += "px";
	if (height == undefined) height = "480";
	height += "px";
	if (show == undefined) show = true;

	$("body").css("height","99.5%");
	$("html").css("height","99.5%");
	$("#popup-container").attr("src", src);
	$("#popup-container").css("width",width); $("#popup-container").css("height",height);
	
	alturaPai = Number($("#bg-preto").css("height").replace("px",""));
	alturaFilho = ($("#popup-container").css("height").replace("px",""));
	larguraPai = Number($("#bg-preto").css("width").replace("px",""));
	larguraFilho = ($("#popup-container").css("width").replace("px",""));
	
	$("#popup-container").css("top",((alturaPai-alturaFilho)/2) + "px");
	$("#popup-container").css("left",((larguraPai-larguraFilho)/2) + "px");

	alturaBody = Number($("body").css("height").replace("px",""));
	larguraBody = Number($("body").css("width").replace("px",""));

	$("#fechar-popup").css("top",(((alturaPai-alturaFilho)/2)) + "px");
	$("#fechar-popup").css("right",(((larguraPai-larguraFilho)/2)-35) + "px");

	if ($.browser.msie && $.browser.version=="6.0") $("select").fadeOut("fast");

	if (show) $("#bg-preto").fadeIn("slow");
}
function abrirPopup(src, width, height, show) {
	if (src == undefined) {
		alert('ATEN��O! � necess�rio informar o SRC que dever� ser aberto pelo Pop-up!');
	} else {
		posicionarPopup(src, width, height, show);
	}
}
function fecharPopup() { $("#bg-preto").fadeOut("slow", function() { if ($.browser.msie && $.browser.version=="6.0") $("select").fadeIn("fast"); }); }

// FLASH MSG - exibi��o da mensagem do sistema ap�s a execu��o de alguma a��o de registro - tipo_mensagem: 'ok', 'erro'
function exibirFlashMsg(titulo, mensagem, tipo_mensagem) {
	document.location.hash = "#topo";
	$("#flash-msg").attr("class", "msg" + tipo_mensagem);
	$("#flash-msg .titulo").html(titulo);
	$("#flash-msg .mensagem").html(mensagem);
	$("#flash-msg").fadeIn(400);
	$("#flash-msg").delay(7000).fadeOut(400);
}

function removerEspacosEmBranco(texto) {	if (texto != null && texto != undefined) { return texto.replace(/ /g,""); } else { return ""; } }

// COMPONENTE UPLOAD
// *requer jQuery 1.7.1+
function cancelarUpload(id, tipo_erro, msg) {
	$("iframe#iupload").remove(); //remover objetos temporarios do upload
	$("#load" + id).remove();
	$("#" + id).attr("disabled", false);
	
	switch(tipo_erro) {
		case "tamanho_arquivo":
			alert("ERRO: O tamanho do arquivo atual excede o limite permitido!!!" + msg);
			break;
		case "extensao_arquivo":
			alert("ERRO: N�o � poss�vel enviar o arquivo escolhido!!!" + msg);
			break;
		case "duplicata_arquivo":
			alert("ERRO: J� existe um arquivo cadastrado no sitema com o mesmo nome!!! Renomei o seu arquivo antes de enviar-lo.");
			break;
		case "dimensao_arquivo":
			alert("ERRO: As dimens�es da imagem enviada extrapolam o limite estabelecido!!!" + msg);
			break;
		default:
	}
}

function confirmarUpload(id, arquivo) {
	$("iframe#iupload").remove(); //remover objetos temporarios do upload
	$("#" + id).replaceWith(reconstruirCampoUpload(id)); //resetar o formulario temporario, limpando o input[type=file]

	if (arquivo != "" && arquivo != undefined) {
		// montar thumbnail, caso o arquivo em quest�o seja uma imagem
		var upthumbnail = "";
		var extensoes_permitidas = new Array("jpg","gif","png","bmp");
		var extensao_arquivo = reverse(arquivo);
		extensao_arquivo = reverse((extensao_arquivo.substring(0,extensao_arquivo.indexOf("."))).toLowerCase());
		//verificar se a arquivo proposto se encaixa na regra das extensoes
		for(i = 0; i < extensoes_permitidas.length; i++) {
			if (extensoes_permitidas[i] == extensao_arquivo) {
				var upaleatorio = Math.floor(Math.random()*1000);
				upthumbnail = "<img id='imagemupload" + upaleatorio + "' src='midias/" + arquivo + "' style='display:none;' /><br />";
				var myImage = new Image();
				myImage.src = "midias/" + arquivo;
				myImage.onload = function() {
					var upredimensionamento = "";
					if (myImage.width > 155 && upredimensionamento == "") {
						upredimensionamento = new Array("height", "155");
					}
					if (myImage.heigth > 196 && upredimensionamento == "") {
						upredimensionamento = new Array("height", "196");
					}
					$("#imagemupload" + upaleatorio).css(upredimensionamento[0], upredimensionamento[1]);
					$("#imagemupload" + upaleatorio).css("display", "block");
				}
			}
		}

		//adicionar item na lista de arquivos upados (UL#lstupload)
		$("ul#lstupload" + id).append("<li title=\"" + arquivo + "\">" + upthumbnail + 
															arquivo + " <a href=\"#\" class=\"removerarquivoupload\">(remover)</a></li>");
		$(".removerarquivoupload").click(function() { removerArquivoUpload($(this)); });

		//adicionar item na lista de controle dos arquivos a serem salvos
		//if ($("ul#lstupload" + id).attr("class") == "unico") { //upload �nico
		if ($("ul#lstupload" + id).hasClass("unico")) { //upload �nico
			//verificar a existencia de algum arquivo tempor�rio j� enviado ao servidor
			$("#lstupadd option").each(function() {
				if ($(this).val().indexOf(id) >= 0) {
					var arquivo_a_ser_movido = $(this).val().replace(id + "=","");
					//mover item do SELECT lstupadd para lstupdel
					$(this).appendTo("#lstupdel");
					//remover item da UL lstupload[nomedocampo]
					$("#lstupload" + id + " li").each(function() { if ($(this).attr("title") == arquivo_a_ser_movido) { $(this).remove(); } });
				}
			});
			$("#lstupadd").append("<option value=\"" + id + "=" + arquivo + "\">" + id + "=" + arquivo + "</option>");
		} else {
			//upload m�ltiplo
			$("#lstupadd").append("<option value=\"" + id + "=" + arquivo + "\">" + id + "=" + arquivo + "</option>");
		}
	}
	//selecionar todas os options dos selects de controle
	$("#lstupadd option").each(function () { $(this).prop("selected", true); });
	$("#lstupdel option").each(function () { $(this).prop("selected", true); });

	$("#load" + id).remove();
	$("#" + id).attr("disabled", false);
}

function removerArquivoUpload(obj) {
	obj = obj.parent("li");
	arquivo  = obj.attr("title");
	//mover item do SELECT lstupadd para lstupdel, caso ele exista
	var movido = false;
	$("#lstupadd option").each(function() { if ($(this).val().indexOf(arquivo) >= 0) { $(this).appendTo("#lstupdel"); movido = true;  } });
	//remover item da UL lstupload[nomedocampo]
	obj.remove();
	//selecionar todas as options dos selects de controle
	$("#lstupadd option").each(function () { $(this).prop("selected", true); });
	$("#lstupdel option").each(function () { $(this).prop("selected", true); });
}

function enviarUpload(id) {
	if (verificarExtensoesUpload(id)) {
		//verificar se n�o existe outra requisi��o em aberto
		if (!$.exists("iframe#iupload")) { 
			$("#" + id).attr("disabled", false);
			$("body").append("<iframe id=\"iupload\" name=\"iupload\" width=\"400\" height=\"200\" style=\"display:none\"></iframe>");
			$("#" + id).change(function() { return false; });
			$("#" + id).after("<img id=\"load" + id + "\" src=\"imagens/ajaxloaderv2.gif\" />");

			var id_formulario;
			var id_formulario = $("#" + id).parent("form").attr("id");
			var tmpObj = $("#" + id).parent();
			while (id_formulario == undefined)  {
				if (tmpObj.prop("tagName") == "FORM") { id_formulario = tmpObj.attr("id"); }
				tmpObj = tmpObj.parent();
			}
			var action_original = $("#" + id_formulario).attr("action");
			var target_original = $("#" + id_formulario).attr("target");
			$("#" + id_formulario).attr("action", "uploader_temporario.asp");
			$("#" + id_formulario).attr("target", "iupload");
			$("#" + id_formulario).attr("enctype", "multipart\/form-data");

			$("#" + id_formulario).append("<input type=\"hidden\" name=\"optupload\" value=\"temporario\" />");
			$("#" + id_formulario).append("<input type=\"hidden\" name=\"nomedocampoupload\" value=\"" + id + "\" />");
			$("#" + id_formulario).append("<input type=\"hidden\" name=\"arquivomodulo\" value=\"" + action_original + "\" />");

			//$("#" + id_formulario).submit(); /* algum problema?!?! */
			eval("document." + $("#" + id_formulario).attr("name") + ".submit()");

			$("#" + id_formulario).attr("action", action_original);
			$("#" + id_formulario).attr("target", target_original);
			$("#" + id_formulario).attr("enctype", "application\/x-www-form-urlencoded");
      
			$("#" + id_formulario + " input[name=optupload]").remove();
			$("#" + id_formulario + " input[name=nomedocampoupload]").remove();
			$("#" + id_formulario + " input[name=arquivomodulo]").remove();
			$("#" + id).attr("disabled", true);
		} else {
			setTimeout("enviarUpload(\"" + id + "\")",5000);
			$("#" + id).attr("disabled", true);
		}
	}
}

function reconstruirCampoUpload(id) {
	$("#" + id).replaceWith("<input type=\"file\" id=\"" + id + "\" name=\"" + id + "\" class=\"" + $("#" + id).attr("class") + "\" />");
	$("#" + id).change(function (e) { enviarUpload(id); });
}

//verificar observacao com id = ext[nomedocampo]
function verificarExtensoesUpload(id) {
	var retorno = false;
	if ($.exists("#ext" + id)) {
		//capturar extensoes permitidas
		var extensoes_permitidas = $("#ext" + id).html().replace(/\s/g,'');
		extensoes_permitidas = extensoes_permitidas.split(",");
		//capturar extensao do arquivo proposto
		var extensao_arquivo = reverse($("#" + id).val());
		extensao_arquivo = reverse((extensao_arquivo.substring(0,extensao_arquivo.indexOf("."))).toLowerCase());
		//verificar se a arquivo proposto se encaixa na regra das extensoes
		for(i = 0; i < extensoes_permitidas.length; i++) {
			if (extensoes_permitidas[i] == extensao_arquivo) { retorno = true; }
		}
	}
	if (!retorno) {
		alert("ERRO: O arquivo proposto n�o obedece �s extens�es permitidas para este campo!!!");
		reconstruirCampoUpload(id); //resetar o formulario temporario, limpando o input[type=file]
	}
	return retorno;
}

//Inicializar objetos de automacao do componente de upload.
//
//Cada campo file deve ser identificado utilizando o mesmo id e name,
//sendo precedido de um label com id = lbl[nomedocampo] e sucedido por uma
//observacao de preenchimento contendo apenas as extensoes permitidas para
//o upload atraves do mesmo, separadas por virgulas com id = ext[nomedocampo],
//assim como, uma lista de arquivo(s) upado(s) ul#lstupload[nomedocampo]
//
//O tipo de upload [�nico/m�ltiplo] � definido pela classe da lista ul do
//componente
function instalarUpload(idformulario) {
	var erro = false;
	//verificar integridade dos campos input[type=file] do upload
	if ($.exists("input[type=file]")) {
		$("input[type=file]").each(function() {
			if (!$.exists("#ext" + $(this).attr("id")) || !$.exists("#lstupload" + $(this).attr("id"))) { erro = true; }
		});
	} else { erro = true; }
	//instalar componente de upload
	if (!erro) {
		$("#" + idformulario).append("<select id=\"lstupadd\" name=\"lstupadd\" multiple=\"multiple\" style=\"display:none;\"></select>");
		$("#" + idformulario).append("<select id=\"lstupdel\" name=\"lstupdel\" multiple=\"multiple\" style=\"display:none;\"></select>");

		if ($.exists(".removerarquivoupload")) {
			//caso existam m�dias j� relacionadas ao registro atual
			$(".removerarquivoupload").each(function() {				
				//atualiza��o da lista lstupadd com a m�dia j� relacionada
				var arquivo = $(this).parent("li").parent("ul").attr("id").replace("lstupload","") + "=" + $(this).parent("li").attr("title");
				$("#lstupadd").append("<option value=\"" + arquivo + "\">" + arquivo + "</option>");

				//trigger de click do link (remover)
				$(this).click(function() {removerArquivoUpload($(this)); });
			});
		}

		$("input[type=file]").change(function (e) { enviarUpload($(this).attr("id")); }); //trigger para acao do upload
	} else {
		alert("N�o � possivel instalar o componente de upload corretamente.");
		$("body").append("<h2 style=\"color:red;\">Componente de upload n�o instalado corretamente!</h2>");
	}
}

function atualizarArquivos(valores, msg) {
	var valor;
	if (valores != "") {
		$("#lstupadd").children().remove();
		$("#lstupdel").children().remove();
		$("input[type=file]").each(function() {
			if ($.exists("#lstupload" + $(this).attr("name"))) {
				$("#lstupload" + $(this).attr("name")).children().remove();
			}
		});
		for (tuaa = 0; tuaa < valores.length; tuaa++) {
			valor = valores[tuaa].split("|");
			confirmarUpload(valor[0], valor[2]);
		}
	}
}
// FIM - COMPONENTE UPLOAD


// FORMATA��O DE DADOS
// mascaras e valida��es para campos de formul�rios
//
// substituicao dos caracteres indesejados nas mensagens
function caracteresIndesejados(texto){
	texto = texto.replace("�","-");
	texto = texto.replace("�",String.fromCharCode(34));
	texto = texto.replace("�",String.fromCharCode(34));
	return texto;
}

function formataDataEUA(data){
	dataFinal = data.split("/");
	dia = dataFinal[0];
	mes = dataFinal[1];
	ano = dataFinal[2];

	return mes + '/' + dia + '/' + ano;
}

// Inicio ---- Valida��o e cria��o do CPF ----
function ArredondaNumero(numero,casas) {
	return Math.round(numero*Math.pow(10,casas))/Math.pow(10,casas);
}
/*
	M�SCARAS para campos retirada de:
	http://elcio.com.br/ajax/mascara/
	Autor: Elcio Ferreira
*/
function mascara(o,f) {
    v_obj=o; v_fun=f;
    setTimeout("execmascara()",1);
}
function execmascara() { v_obj.value=v_fun(v_obj.value); }
function soNumeros(v) { return v.replace(/\D/g,""); }
function leech(v) {
    v=v.replace(/o/gi,"0");
    v=v.replace(/i/gi,"1");
    v=v.replace(/z/gi,"2");
    v=v.replace(/e/gi,"3");
    v=v.replace(/a/gi,"4");
    v=v.replace(/s/gi,"5");
    v=v.replace(/t/gi,"7");
    return v;
}
function mmoeda(v) {
	v=v.replace(/\D/g,"");
	if (v.length > 3) {
		v=parseInt(v, 10);
		v=v.toString();
	}
	if (v.length > 2)
		v=v.replace(eval('/^(\\d{' + (v.length-2) + '})(\\d)/'),"$1,$2");

	if (v.length > 6) {
		tmpDig = v.substring(0, v.indexOf(','));
		while (tmpDig.length > 0) {
			tmpDig = tmpDig.substring(0, (tmpDig.length - 3));
			if (tmpDig.length > 0)
				v=v.replace(eval('/^(\\d{' + tmpDig.length + '})(\\d)/'),"$1.$2");
		}
	}
	return v;
}
function mmilhar(v){
	v=v.replace(/\D/g,"");
	if (v.length > 3) {
		v=parseInt(v, 10);
		v=v.toString();
	}

	if (v.length > 3) {
		tmpDig = v;
		while (tmpDig.length > 0) {
			tmpDig = tmpDig.substring(0, (tmpDig.length - 3));
			if (tmpDig.length > 0)
				v=v.replace(eval('/^(\\d{' + tmpDig.length + '})(\\d)/'),"$1.$2");
		}
	}
	return v;
}
function mtelefone(v) {
	v=v.replace(/\D/g,"");
	v=v.replace(/^(\d\d)(\d)/g,"($1) $2");
	v=v.replace(/(\d{4})(\d)/,"$1-$2");
	return v;
}
function mtelefonesemddd(v) {
	v=v.replace(/\D/g,"");
	v=v.replace(/(\d{4})(\d)/,"$1-$2");
	return v;
}
function mcpf(v) {
	v=v.replace(/\D/g,"");
	v=v.replace(/(\d{3})(\d)/,"$1.$2");
	v=v.replace(/(\d{3})(\d)/,"$1.$2");
	v=v.replace(/(\d{3})(\d{1,2})$/,"$1-$2");
	return v;
}
function mcep(v) {
	v=v.replace(/D/g,"");
	v=v.replace(/^(\d{5})(\d)/,"$1-$2");
	return v;
}
function mhora(v) {
	v=v.replace(/D/g,"");
	v=v.replace(/^(\d{2})(\d)/,"$1:$2");
	return v;
}
function mdata(v){
	v=v.replace(/D/g,"");
	v=v.replace(/^(\d{2})(\d)/,"$1/$2");
	v=v.replace(/^(\d{2})\/(\d{2})(\d)/,"$1/$2/$3");
	return v;
}
function mcnpj(v){
	v=v.replace(/\D/g,"");
	v=v.replace(/^(\d{2})(\d)/,"$1.$2");
	v=v.replace(/^(\d{2})\.(\d{3})(\d)/,"$1.$2.$3");
	v=v.replace(/\.(\d{3})(\d)/,".$1/$2");
	v=v.replace(/(\d{4})(\d)/,"$1-$2");
	return v;
}
function murl(v){
	v=v.replace(/^http:\/\/?/,"");
	dominio=v;
	caminho="";
	if(v.indexOf("/")>-1);
			dominio=v.split("/")[0];
			caminho=v.replace(/[^\/]*/,"");
	dominio=dominio.replace(/[^\w\.\+-:@]/g,"");
	caminho=caminho.replace(/[^\w\d\+-@:\?&=%\(\)\.]/g,"");
	caminho=caminho.replace(/([\?&])=/,"$1");
	if(caminho!="")dominio=dominio.replace(/\.+$/,"");
	v="http://"+dominio+caminho;
	return v;
}
/* XHTML DE EXEMPLO DAS FUN�OES DE M�SCARA
<form id="formteste" name="formteste">
	<label for="cpf">CPF: </label> <input type="text" id="cpf" name="cpf" maxlength="14" onkeypress="mascara(this,mcpf)" onblur="validarDocumento(this)" /><br />
	<label for="cnpj">CNPJ: </label> <input type="text" id="cnpj" name="cnpj" onkeypress="mascara(this,mcnpj)" maxlength="18" onblur="validarDocumento(this)" /><br />
	<label for="data">Data: </label> <input type="text" id="data" name="data" onkeypress="mascara(this,mdata)" maxlength="10" onblur="validarData(this)" /><br />
	<label for="telefone">Telefone: </label> <input type="text" id="telefone" name="telefone" onkeypress="mascara(this,mtelefone)" size="12" maxlength="14" onblur="validarTelefone(this)" /><br />
	<label for="telefonesemddd">Telefone sem ddd: </label> <input type="text" id="telefonesemddd" name="telefone" onkeypress="mascara(this,mtelefonesemddd)" size="10" maxlength="9" onblur="validarTelefone(this)" /><br />
	<label for="cep">CEP: </label> <input type="text" id="cep" name="cep" onkeypress="mascara(this,mcep)" maxlength="9" /><br />
	<label for="url">URL: </label> <input type="text" id="url" name="url" onkeypress="mascara(this,murl)" maxlength="150"  /><br />
	<label for="email">E-mail: </label> <input type="text" id="email" name="email" onblur="validarEmail(this)" /><br />
	<label for="moeda">Moeda: </label> <input type="text" id="moeda" name="moeda" onkeypress="mascara(this,mmoeda)" maxlength="150"  /><br />
  <label for="inumeros">Somente n�meros:</label><input id="inumeros" onkeypress="mascara(this,soNumeros)" />
</form>
	Editado e Adaptado por: N�cleo de Tecnologia M�ltimidia (NTM) do SENAI Alagoas
*/

/*FUNC�OES DE VALIDA�AO*/
function validarDocumento(obj) {
	if (obj.value != '') {
		if (obj.value.length == obj.maxLength && eval('check'+obj.id.toUpperCase()+'(obj)')) {
			return true;
		} else {
			alert('ERRO: ' + obj.id.toUpperCase() +' inv�lido!!! (Utilize apenas n�meros para completar todo o campo)');
			document.getElementById(obj.id).value = '';
			document.getElementById(obj.id).focus();
			return false;
		}
	}
}
function validarHora(obj) {
	if (obj.value.length == 5) {
		if ((parseInt(obj.value.substr(0, obj.value.indexOf(':'))) >= 0 && parseInt(obj.value.substr(0, obj.value.indexOf(':'))) <= 23) && (parseInt(obj.value.substr((obj.value.indexOf(':')+1), obj.value.length)) >= 0 && parseInt(obj.value.substr((obj.value.indexOf(':')+1), obj.value.length)) <= 59)) {
			return true;
		} else {
			alert('ERRO: HOR�RIO inv�lido!!! (Formato para hor�rios: hh:mm)');
			document.getElementById(obj.id).value = '';
			document.getElementById(obj.id).focus();
			return false;
		}
	}
}
function validarEmail(obj) {
	if (obj.value.length > 0) {
		if (obj.value.indexOf('@') >= 0 && obj.value.substr(obj.value.indexOf('@'), obj.value.length).indexOf('.') >= 0) {
			return true;
		} else {
			alert('ERRO: Endere�o de E-mail inv�lido');
			document.getElementById(obj.id).value = '';
			document.getElementById(obj.id).focus();
			return false;
		}
	} else {
		return true;
	}
}
function validarData(obj) {
	if (obj.value != '') {
		if (obj.value.length == obj.maxLength && checkData(obj)) {
			return true;
		} else {
			alert('ERRO: DATA inv�lida!!! (Formato para datas: dd/mm/aaaa)');
			document.getElementById(obj.id).value = '';
			document.getElementById(obj.id).focus();
			return false;
		}
	}
}
function validarTelefone(obj) {
	if (obj.value != '') {
		if (obj.value.length == obj.maxLength || obj.value.length == (obj.maxLength - 1)) {
			return true;
		} else {
			alert('ERRO: TELEFONE inv�lido!!! (Utilize apenas n�meros para completar todo o campo)');
			document.getElementById(obj.id).value = '';
			document.getElementById(obj.id).focus();
			return false;
		}
	}
}
function checkCPF(obj) {
	var numerocpf = obj.value;
	numerocpf = soNumeros(numerocpf);
	if ((numerocpf == "00000000000") || (numerocpf == "11111111111") || (numerocpf == "22222222222") || (numerocpf == "33333333333") || (numerocpf == "44444444444") || (numerocpf == "55555555555") || (numerocpf == "66666666666") || (numerocpf == "77777777777")|| (numerocpf == "88888888888") || (numerocpf == "99999999999")) {
		v = true;
		return false;
	}
	s = numerocpf;

	var c = s.substr(0,9);
	var dv = s.substr(9,2);
	var d1 = 0;
	var v = false;
	for (i = 0; i < 9; i++)
		d1 += c.charAt(i)*(10-i);

	if (d1 == 0) {
		v = true;
		return false;
	}

	d1 = 11 - (d1 % 11);
	if (d1 > 9) d1 = 0;
	if (dv.charAt(0) != d1) {
		v = true;
		return false;
	}

	d1 *= 2;
	for (i = 0; i < 9; i++)
		d1 += c.charAt(i)*(11-i);

	d1 = 11 - (d1 % 11);
	if (d1 > 9) d1 = 0;
	if (dv.charAt(1) != d1) {
		v = true;
		return false;
	}
	if (!v) return true;
}
function checkCNPJ(obj){
	 var numero; var situacao = '';
	 pri = obj.value.substring(0,2);
	 seg = obj.value.substring(3,6);
	 ter = obj.value.substring(7,10);
	 qua = obj.value.substring(11,15);
	 qui = obj.value.substring(16,18);

	 numero = (pri+seg+ter+qua+qui);
 	 s = numero;

	 c = s.substr(0,12);
	 var dv = s.substr(12,2);
	 var d1 = 0;
	 var resultado = true;

	 for (i = 0; i < 12; i++){
			d1 += c.charAt(11-i)*(2+(i % 8));
	 }

	 if (d1 == 0){
			resultado = false;
	 }
	 d1 = 11 - (d1 % 11);

	 if (d1 > 9) d1 = 0;

		if (dv.charAt(0) != d1){
			 resultado = false;
		}

	 d1 *= 2;
	 for (i = 0; i < 12; i++){
			d1 += c.charAt(11-i)*(2+((i+1) % 8));
	 }

	 d1 = 11 - (d1 % 11);
	 if (d1 > 9) d1 = 0;

			if (dv.charAt(1) != d1){
				 resultado = resultado;
			}

	 return resultado;
}
function checkData(obj) {
  var expReg = /^((0[1-9]|[12]\d)\/(0[1-9]|1[0-2])|30\/(0[13-9]|1[0-2])|31\/(0[13578]|1[02]))\/(19|20)?\d{2}$/;
  var aRet = true;
  if ((obj) && (obj.value.match(expReg)) && (obj.value != '')) {
        var dia = new Number(obj.value.substring(0,2));
        var mes = new Number(obj.value.substring(3,5));
        var ano = new Number(obj.value.substring(6,10));
        if ((mes == 4 || mes == 6 || mes == 9 || mes == 11) && (dia > 30))
          aRet = false;
        else
          if ((ano % 4) != 0 && mes == 2 && dia > 28)
                aRet = false;
          else
                if ((ano%4) == 0 && mes == 2 && dia > 29)
                  aRet = false;
  }  else 
        aRet = false;  
  return aRet;
}

//rel�gio sess�o
function controleDeSessao() {
	this.lb = $("#limitebd").html(); //lb = limite do banco de dados
	this.la = $("#limiteapp").html(); //la = limite da aplica��o
	this.hc = $("#horariocarregamento").html(); //hc = horario de carregamento da pagina
	this.temporizador;

	this.lb = eval("new Date(" + this.lb + ")");
	this.la = eval("new Date(" + this.la + ")");
	this.hc = eval("new Date(" + this.hc + ")");

	this.cr = 0; //cr = contador regressivo

	if (this.lb < this.la) {
		this.cr = ((this.lb - this.hc) / 1000);
	} else {
		this.cr = ((this.la - this.hc) / 1000);
	}
	//alert((this.la - this.hc)/1000);
	//alert((this.lb - this.hc)/1000);
	//alert(this.lb + "\n\n" + this.la + "\n\n" + this.hc + "\n\n" + this.cr);
	
	this.encerrarSessao = function (tdata) {
		clearTimeout(this.temporizador);
	}
	this.renovarSessao = function () {
		$.ajax({
			url: "relogiosessao_renovar.asp",
			success:function(data) {
				$("#relogiosessao").html(data);
				reiniciarRelogioSessao();
			}
		});
	}

	this.atualizarRelogioSessao = function () {
		if ($.exists("#horacompleta")) {
			var temp = this;
			temp.temporizador = setTimeout(function () {
				temp.cr -= 1;
				tdata = new Date(1900,1,1,0,0,0,0);
				tdata.setSeconds(temp.cr);

				if (tdata.getHours() > 0) {
					complemento_horas = "";
					if (tdata.getHours() < 10) { complemento_horas = "0"; }
					complemento_minutos = "";
					if (tdata.getMinutes() < 10) { complemento_minutos = "0"; }
					complemento_segundos = "";
					if (tdata.getSeconds() < 10) { complemento_segundos = "0"; }
					$("#horacompleta").html(complemento_horas + tdata.getHours() + ":" + complemento_minutos + tdata.getMinutes() + ":" + complemento_segundos + tdata.getSeconds());
				} else {
					complemento_minutos = "";
					if (tdata.getMinutes() < 10) { complemento_minutos = "0"; }
					complemento_segundos = "";
					if (tdata.getSeconds() < 10) { complemento_segundos = "0"; }
					$("#horacompleta").html(complemento_minutos + tdata.getMinutes() + ":"  + complemento_segundos + tdata.getSeconds());
				}
				temp.atualizarRelogioSessao();

				if (temp.cr == 125) { $("#horacompleta").after("<span id=\"renovarsessao\"><br /><a href=\"#\" onclick=\"javascript:renovarSessao();\">Renovar sess�o</a></span>"); }
				if (temp.cr == 1) { $("#horacompleta").remove(); $("#horacompleta").html("Sua sess�o expirou!"); }
			},1000);
		}
	}
}
function renovarSessao() {
	sa.encerrarSessao();
	sa.renovarSessao();
}
function reiniciarRelogioSessao() {
	if ($.exists("#relogiosessao")) { sa = new controleDeSessao(); sa.atualizarRelogioSessao(); }
}

//MODAL  - jQuery UI
function Modal(form, op) {
	if(op == "abrir") {
		$(form).dialog({
			modal: true,
			resizable: false,
			closeOnEscape: true,
			width: 400,
			height: 440
		});
	}

	if(op == "fechar") {
		$(form).dialog("close");
	}
}


//atribui��es de fun��o dos bot�es da barra de navega��o
$("document").ready(function() {
	//aplicar hover de destaque para os botoes de a��o
	//$("button.botao-acao").hover(function () { $(this).css("background-image","url(imagens/bg_bt_acao_hover.jpg)"); }, function() { $(this).css("background-image",""); });
	$("button.botao-acao").click(function() { return false; }); //remover a��o padr�o dos bot�es de a��o
	//aplicando opera��es padr�o para os bot�es de incluir, editar e excluir
	$("button.botao-acao").each(function() {
		objId = $(this).attr("id");
		objOperacao = objId.substring(objId.indexOf("botao-")+6,objId.length);
		objOperacao = objOperacao.substring(0,objOperacao.indexOf("-"));

		if (objOperacao == "incluir" || objOperacao == "editar" || objOperacao == "excluir" || objOperacao == "novo") { $(this).bind("click", function() {
				objId = $(this).attr("id");
				objOperacao = objId.substring(objId.indexOf("botao-")+6,objId.length);
				objOperacao = objOperacao.substring(0,objOperacao.indexOf("-"));
				jQuery.globalEval(objOperacao + "Registro()");
			});
		}
	});
});