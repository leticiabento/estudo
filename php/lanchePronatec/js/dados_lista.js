$("document").ready(function () { //ON LOAD
	instalarUpload("frmcadastro");
});

var erro = false; var msgErro = "";
function valid(idformulario) {
	erro = false; 
	msgErro = "";
	validacaoObrigatorios(idformulario);

    if ($("#senha").val() || $("#novasenha").val() || $("#repetenovasenha").val()){
		if (!$("#senha").val()) {
			msgErro += "<br /> - ERRO: Para alterar a senha � necess�rio preencher o campo SENHA ATUAL para efetivar a opera��o !!!";
			erro = true; destacarErro('senha'); 
		}
		if (!erro){
			if (!$("#novasenha").val()) {
				msgErro += "<br /> - ERRO: Para alterar a senha � necess�rio preencher o campo NOVA SENHA para efetivar a opera��o !!!";
				erro = true; destacarErro('novasenha'); 
			}
		}	
		if (!erro){
			if (!$("#repetenovasenha").val()) {
				msgErro += "<br /> - ERRO: Para alterar a senha � necess�rio preencher o campo CONFIRME A NOVA ATUAL para efetivar a opera��o !!!";
				erro = true; destacarErro('repetenovasenha'); 
			}
		}	
		if (!erro){			
			if ($("#novasenha").val() != $("#repetenovasenha").val()){
				msgErro += "<br /> - ERRO: Os campos NOVA SENHA e CONFIRME NOVA SENHA devem ser iguais para efetivar a opera��o !!!";
				erro = true; destacarErro('novasenha');destacarErro('repetenovasenha'); 
			} 
		}	
	}	
	
	if(!erro) {
			if (resultado = submitAjax(idformulario, "")) {
				confirmarEnvio(idformulario);
				atualizarArquivos(resultado["nomearquivo"]);
				exibirBotaoNovo();
			} else {
				exibirFlashMsg("OPERA��O CANCELADA", "Ocorreu um erro inesperado. Por favor, tome nota do ocorrido e entre em contato com os administradores do sistema. <a href=\"contato_lista.asp\">Contato</a>", "erro");
			}
	} else {
			exibirFlashMsg("OPERA��O CANCELADA", "<p class=\"erro-cr\">Foi(ram) encontrado(s) o(s) seguinte(s) erro(s):" + msgErro + "</p>", "erro");
	}
}