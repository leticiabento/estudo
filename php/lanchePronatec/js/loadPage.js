/* A��ES GLOBAIS */
$("document").ready(function() {
	if ($.exists("form #cod")) {
		if ($("form #cod").prop("defaultValue") != "") {
			alterarTituloAcao();
		}
	}
	alert("Carreguei");

	/*BLOQUEAR ESPACOS EM BRANCO NOS CAMPOS DE SENHA*/
	$("input#senha,input#novasenha,input#repetenovasenha").on({
	  keydown: function(e) {
		if (e.which === 32)
		  return false;
	  },
	  change: function() {
		this.value = this.value.replace(/\s/g, "");
	  }
	});	

	$(".cadastro").one("submit", function () {
		alert("aqui");
		return false;
		valid(document.cadastro);
	});
	/* marca��o de campos ao receber/perder o foco */
	$(".cadastro input, .login input, .cadastro select, .cadastro textarea").focus(function() {
		$(this).attr("onfocus");
		$(this).css("background-color", "#e8f2f4");
	});
	$(".cadastro input, .login input, .cadastro select, .cadastro textarea").blur(function() {
		$(this).attr("onblur");
		$(this).css("background-color", "");
	});

	/* destacar linhas das tabelas de listagem */
	$("#listaregistros .padrao tr").hover(function() {
		$(this).css("background", "#f2f2f2");
	}, function() {
		$(this).css("background", "");
	});

	/* checks de valida��o antes de abandonar a p�gina */
	if ($.exists(".cadastro")) {
		$(".menu-h a, .info_usuario a, #operacoes_pagina img.voltar").click(function() {
			var erro = false;
			var resposta = true;
			$(".cadastro input:text, .cadastro textarea, .cadastro fieldset input:text, .cadastro fieldset input:password, .cadastro fieldset textarea").each(function() { if ($(this).val() != $(this).prop("defaultValue")) { erro = true; } });
			var itens_repetidos = new Array();
			$(".cadastro input:checkbox, .cadastro input:radio, .cadastro fieldset input:checkbox, .cadastro fieldset input:radio").each(function() {
				if (itens_repetidos[$(this).attr("name")] == undefined && $("[name=\"" + $(this).attr("name") + "\"]").length > 1) {//verificar campos de conjunto
					itens_repetidos[$(this).attr("name")] = 0;
					var valor_selecionado_conjunto;
					$("[name=\"" + $(this).attr("name") + "\"]").each(function() {
						if ($(this).prop("defaultChecked")) {
							if (valor_selecionado_conjunto == undefined)  { valor_selecionado_conjunto = $(this).val(); } else { valor_selecionado_conjunto += "," + $(this).val(); }
						}
					});
					$("[name=\"" + $(this).attr("name") + "\"]").each(function() {
						if ((valor_selecionado_conjunto == undefined) && $(this).prop("checked")) { erro = true; }
						if (!erro) { if ($(this).prop("defaultChecked") && !$(this).prop("checked")) { erro = true; } }
					});
				} else if ($("[name=\"" + $(this).attr("name") + "\"]").length == 1) { //verificar campos �nicos
					if ($(this).prop("defaultChecked") != $(this).prop("checked")) { erro = true; }
				}
			});
			if (resposta) {
				$(".cadastro select, .cadastro fieldset select").each(function() {
					if ($(this).css("display") != "none") {
						var valor_selecionado;
						$(this).children("option").each(function() {
							if ($(this).prop("defaultSelected")) {
								if (valor_selecionado == undefined)  { valor_selecionado = $(this).val(); } else { valor_selecionado += "," + $(this).val(); }
							}
						});
						$(this).children("option").each(function(i, opt) {
							if ((valor_selecionado == undefined) && opt.selected) { erro = true; }
							if (!erro) { if ($(this).prop("defaultSelected") && !$(this).prop("selected")) { erro = true; } }
						});
					}
				});
			}
			if (erro) { resposta = confirm("Aten��o! Os dados do cadastro foram alterados.\n\nVoc� deseja realmente deixar a p�gina sem salvar os dados?"); }
			return resposta;
		});
	}

	/* limpar arquivos tempor�rios dos campos de upload */
	$(window).bind('beforeunload', function() {
		if (!(window.fUpload === undefined)) {
			if (!fUpload.manterArquivos) {
				cancelarUpload();
			}
		}
	});

	/* DEVLOG - acompanhamento de execu��es SQL */
	var contador = 0;
	var altura_anterior = 0;
	$(".devlog").each(function () {
		if (contador == 0) {
			$(this).css("top",altura_anterior+ "px");
		} else {
			$(this).css("top",altura_anterior+ "px");
		}
		altura_anterior += 3 + $(this).innerHeight();
	});
	$(".devlog").dblclick(function() {
		$(this).fadeOut("slow");
	});
	
	/* POPUP DO SISTEMA - Atualizar posi��o do conainter do popup */
	if ($.exists("#bg-preto")) { //adicionar funcionalidade somente perante a existencia do objeto
		$(window).resize(function() {
			alturaPai = Number($("#bg-preto").css("height").replace("px",""));
			alturaFilho = ($("#popup-container").css("height").replace("px",""));
			larguraPai = Number($("#bg-preto").css("width").replace("px",""));
			larguraFilho = ($("#popup-container").css("width").replace("px",""));
			
			$("#popup-container").css("top",((alturaPai-alturaFilho)/2) + "px");
			$("#popup-container").css("left",((larguraPai-larguraFilho)/2) + "px");

			alturaBody = Number($("body").css("height").replace("px",""));
			larguraBody = Number($("body").css("width").replace("px",""));

			$("#fechar-popup").css("top",(((alturaPai-alturaFilho)/2)) + "px");
			$("#fechar-popup").css("right",(((larguraPai-larguraFilho)/2)-35) + "px");
		});
	}
	/* FLASH-MSG */
	if ($.exists("#flash-msg")) { //adicionar funcionalidades do componente
		$("#flash-msg .fechar").click(function () { $("#flash-msg").hide(); });
		if ($.exists("#flash-msg-msgresposta")) {
			var titulo = "";
			if ($("#flash-msg-tiporesposta").val() == "ok") {
				titulo = "Opera��o realizada com sucesso!";
			} else {
				titulo = "Ocorreu um erro! N�o foi poss�vel executar a opera��o solicitada.";
			}
			exibirFlashMsg(titulo, $("#flash-msg-resposta").val(), $("#flash-msg-tiporesposta").val());
			$("#flash-msg-tiporesposta").val(""); $("#flash-msg-resposta").val("");
		}
	}
	/* DRAG & DROP PARA LISTAS DE ORDENA��O
		- fun��o asp = EscreverLista(...)
		- classe da lista para ordena��o = lista_ordenavel
	*/
	if ($.exists(".lista_ordenavel")) { //adicionar funcionalidade somente perante a existencia do objeto
		$(".lista_ordenavel li").each(function() {
			if ($(this).children("ul").length > 0) {
				$(this).children("ul:first").sortable({
					update: function(event) {
						var params = "";
						var item = "";
						var current_action = $(this);
						$(".lista_ordenavel li").each(function () {
							item = $(this).attr("id").replace("item-raiz-","");
							item = item.replace("item-","");
							params += "itens=" + item + "&";
						});
						$.ajax({
							type:"POST",
							url:$(".lista_ordenavel:first").attr("id") + "ordenar_salvar.asp?opt=editar",
							data:params,
							success:function() {
								//atualizar o menu do sistema
								if ($(".lista_ordenavel").attr("id") == "menu") $("div#nav").load("menu_adm2.asp");
							},
							error:function(event, ui) {
								//lan�ar mensagem de erro e cancelar a a��o sortable
								alert("N�o foi poss�vel ordenar este n�vel da lista. A ordena��o ser� cancelada.\n\n\nAssim que poss�vel, informe o ocorrido ao(s) administrador(es) do sistema.");
								current_action.sortable('cancel');
							}
						});
					}
				});
			}
		});
		
		$(".lista_ordenavel").sortable({
			update: function() {
				var params = "";
				var item = "";
				$(".lista_ordenavel li").each(function () {
					item = $(this).attr("id").replace("item-raiz-","");
					item = item.replace("item-","");
					params += "itens=" + item + "&";
				});
				$.ajax({
					type:"POST",
					url:$(".lista_ordenavel:first").attr("id") + "ordenar_salvar.asp?opt=editar",
					data:params,
					success:function() {
						//atualizar o menu do sistema
						if ($(".lista_ordenavel").attr("id") == "menu") $("div#nav").load("menu_adm2.asp");
					},
					error:function(event, ui) {
						//lan�ar mensagem de erro e cancelar a a��o sortable
						alert("N�o foi poss�vel ordenar este n�vel da lista. A ordena��o ser� cancelada.\n\n\nAssim que poss�vel, informe o ocorrido ao(s) administrador(es) do sistema.");
						current_action.sortable('cancel');
					}
				});
			}
		});
	}
	//adicionar a��es de click
	$(".lista_ordenavel ul, .lista_nao_ordenavel ul").attr("class", $(".lista_ordenavel ul, .lista_nao_ordenavel ul").attr("class") + " invisivel")
	$(".lista_ordenavel li, .lista_nao_ordenavel li").click(function(event) {
		event.stopPropagation();
		if ($(this).children("ul").length > 0) $(this).children("ul:first").toggleClass("invisivel");
	});

	//adicionar trigger de submit para formularios	
	$("input:not(.nosubmit), select:not(.nosubmit)").keypress(function(event) {
		if (event.which == 13) {
			var id_formulario = null;
			$(this).closest("form").each(function() {
				if ($(this).get(0).tagName == "FORM") {
					id_formulario = $(this).attr("id");
					return false;
				}
			});

			if(typeof valid == 'function') {
				$("#" + id_formulario).submit(function () {
				return false; });
				valid(eval("document." + id_formulario));
			} else {
				$("#" + id_formulario).submit();
			}
		}
	});

	//rel�gio sess�o
	if ($.exists("#relogiosessao")) { sa = new controleDeSessao(); sa.atualizarRelogioSessao(); }


	$("a.popup").click(function() {
		var class_list = $(this).attr("class").split(/\s+/);
		var obj_link = $(this);
		$.each(class_list, function(index, value) {
			if (value.indexOf("size_") > -1) {
				value = value.replace("size_", "");
				var width, height = 0;
				width = value.substring(0,value.indexOf("x"));
				height = value.substring((value.indexOf("x") + 1),value.length);
				window.open(obj_link.attr("href"), obj_link.attr("title"), "width=" + width + ",height=" + height + ",status=0,toolbar=0");
				return false;
			}
		});
		return false;
	});
	
	/* DATAPICKER PARA OS CAPOS DE DATA COM CLASSE DATAPICKER */	
	$(".datapicker").datepicker({
		dateFormat: 'dd/mm/yy',
		dayNames: ["domingo","segunda","terl�a","quarta","quinta","sexta","s�bado","domingo"],
		dayNamesMin: ["D","S","T","Q","Q","S","S","D"],
		dayNamesShort: ["Dom","Seg","Ter","Qua","Qui","Sex","S�b","Dom"],
		monthNames: ["Janeiro","Fevereiro","Mar�o","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"],
		monthNamesShort: ["Jan","Fev","Mar","Abr","Mai","Jun","Jul","Ago","Set","Out","Nov","Dez"],
		nextText: "Pr�ximo",
		prevText: "Anterior"
	});
	
});

(function($) {
	    $.fn.extend( {
	        limiter: function(limit, elem) {
	            $(this).on("keyup focus", function() {
	                setCount(this, elem);
	            });
	            function setCount(src, elem) {
	                var chars = src.value.length;
	                if (chars > limit) {
	                    src.value = src.value.substr(0, limit);
	                    chars = limit;
	                }
	                elem.html( limit - chars );
	            }
	            setCount($(this)[0], elem);
	        }
	    });
})(jQuery);