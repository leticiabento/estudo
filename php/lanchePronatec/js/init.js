var formLogin = $("#formLogin");
var formRegistro = $("#formRegistro");
var pessoa = new Pessoa();

formRegistro.submit(function(event) {
    event.preventDefault();
    pessoa.registro(this);
})

formLogin.submit(function(event) {
    event.preventDefault();
    pessoa.logar(this);
})

$(document).ready(function() {
    if(location.pathname !== "/lanchePronatec/registro.html") {
        pessoa.consultar();
    }
})

$(".btn-sair").click(function() {
    pessoa.sair();
})