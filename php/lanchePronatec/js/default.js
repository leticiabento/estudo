$("document").ready(function() {
	$("#login").focus();
});

function valid(form) {
	var erro = false;

	field = form.login;
	if (field.value.length == 0) {
		erro = true; destacarErro(field);
		alert ('O campo USU�RIO deve ser preenchido.');
		field.focus();
	} else {
 	    desmarcarErro(field);
		if (!erro) { 
			field = form.senha;
			if (field.value.length == 0) {
				erro = true; destacarErro(field);
				alert('O campo SENHA deve ser preenchido.');
				field.focus();
			} else {
				desmarcarErro(field);
			}
		}
	}
	if (!erro) 
		form.submit();
}