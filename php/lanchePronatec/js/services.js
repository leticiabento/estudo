function Pessoa() {
    this.registro = function (element) {
        var data = new FormData($(element)[0]);

        $.ajax({
            url: "api/login/registrar.php",
            type: "POST",
            data: data,
            contentType: false,
            processData: false
        }).done(function (res) {
            console.log(res);
        })
    }

    this.logar = function (element) {
        var data = $(element).serialize();

        $.ajax({
            url: "api/login/login.php",
            type: "POST",
            data: data
        }).done(function (res) {
            if (res) {
                location.href = "estudantes.html";
            } else {
                alert('usuario e/ou senha incorretos');
            }
        })
    }

    this.consultar = function () {
        $.ajax({
            url: "api/login/consultar.php",
            type: "GET"
        }).done(function (res) {
            var index = (location.pathname === "/lanchePronatec/" || location.pathname === "/lanchePronatec/index.html")

            if (res && index) {
                location.href = "estudantes.html";


            } else if (!res && !index) {
                location.href = "index.html";
            } else if(res) {
                $(".profile").append(
                    '<div class="profile-image">' +
                    '<img src="assets/uploads/Images/' + res.imagem + '" alt="John Doe">' +
                    '</div>' +
                    ' <div class="profile-data">' +
                    '  <div class="profile-data-name">' + res.nome + '</div>' +
                    '<div class="profile-data-title">Estudantes</div>' +
                    ' <div class="profile-data-title">' + res.email + '</div>' +
                    '</div>' +
                    ' <div class="profile-controls">' +
                    '<a href="#" class="profile-control-left"><span class="fa fa-info"></span></a>' +
                    ' <a href="#" class="profile-control-right"><span class="fa fa-envelope"></span></a>' +
                    '</div>'
                );
            }


        })
    }

    this.sair = function () {
        $.ajax({
            url: "api/login/sair.php",
            type: "GET"
        }).done(function (res) {
            if (res) {
                location.href = "index.html";
            }
        })
    }
}