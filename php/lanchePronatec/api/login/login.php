<?php

header("Content-type: application/json");

if ($_SERVER['REQUEST_METHOD'] === "POST") {


    $email = $_POST['email'];
    $password = md5($_POST['password']);


    include_once "../autoload.php";

    $loginController = new LoginController();

    $loginController->setEmail($email);
    $loginController->setSenha($password);

    echo json_encode($loginController->returnLogar());

}