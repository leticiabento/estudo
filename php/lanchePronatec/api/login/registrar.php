<?php

header("Content-type: application/json");

if($_SERVER['REQUEST_METHOD'] === "POST") {

    $nome = $_POST['nome'];
    $nascimento = $_POST['datanascimento'];
    $email = $_POST['email'];
    $password = md5($_POST['senha']);
    $confirmasenha = md5($_POST['confirmasenha']);
    $imagem = $_FILES['foto'];

    $dir = "./";
    $extensao = @end(explode('.', $imagem['name']));
    $novaImagem = uniqid(md5($imagem['tmp_name'])). "." . $extensao;

    if($password == $confirmasenha) {
        include_once "../autoload.php";

        $loginController = new LoginController();
        $loginController->setNome($nome);
        $loginController->setNascimento($nascimento);
        $loginController->setEmail($email);
        $loginController->setSenha($password);
        $loginController->setImagem($novaImagem);

        echo json_encode($loginController->returnCadastrar());
    }
}