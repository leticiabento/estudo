<?php

function __autoload($class) {
    $class = explode("Controller", $class);
    include_once "Controllers/". $class[0] . ".controller.php";
}