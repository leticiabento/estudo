<?php

include_once "Conexao.class.php";

class LoginModel extends Conexao
{

    private function cadastrarDb($senha, $email, $nascimento, $nome, $imagem)
    {
        $sql = "INSERT INTO users (nome, senha, nascimento, email) VALUES (?,?,?,?)";
        $stmt = $this->prepare($sql);

        $stmt->bindValue(1, $nome);
        $stmt->bindValue(2, $senha);
        $stmt->bindValue(3, $nascimento);
        $stmt->bindValue(4, $email);
        $stmt->execute();

        $row = $stmt->rowCount();

        if ($row > 0) {

            $sql2 = "INSERT INTO imagem (user_id, imagem) VALUES (?,?)";
            $stmt2 = $this->prepare($sql2);

            $stmt2->bindValue(1, $this->lastInsertId($row));
            $stmt2->bindValue(2, $imagem);
            $stmt2->execute();
            $row2 = $stmt2->rowCount();

            return $row2;
        }

    }

    public function returnCadastrarDb($senha, $email, $nascimento, $nome, $imagem)
    {
        return $this->cadastrarDb($senha, $email, $nascimento, $nome, $imagem);
    }

    private function logarDb($senha, $email)
    {

        $sql = "SELECT * FROM users JOIN imagem
                on users.id = imagem.user_id
                WHERE email = '$email' AND senha = '$senha'";

        $stmt = $this->prepare($sql);
        $stmt->execute();

        return $stmt->fetch(PDO::FETCH_ASSOC);

    }

    public function returnLogarDb($senha, $email)
    {
        return $this->logarDb($senha, $email);
    }

    private function consultarDb($email)
    {
        $sql = "SELECT * FROM users JOIN imagem
                on users.id = imagem.user_id
                WHERE email = '$email'";

        $stmt = $this->prepare($sql);
        $stmt->execute();

        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    public function returnConsultarDb($email)
    {
        return $this->consultarDb($email);
    }

}