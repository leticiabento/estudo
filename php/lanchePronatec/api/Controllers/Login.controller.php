<?php

include_once "../Models/Login.class.php";

class LoginController extends LoginModel
{

    private $nome;
    private $nascimento;
    private $email;
    private $senha;
    private $imagem;

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getNascimento()
    {
        return $this->nascimento;
    }

    /**
     * @param mixed $nascimento
     */
    public function setNascimento($nascimento)
    {
        $this->nascimento = $nascimento;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getSenha()
    {
        return $this->senha;
    }

    /**
     * @param mixed $senha
     */
    public function setSenha($senha)
    {
        $this->senha = $senha;
    }

    /**
     * @return mixed
     */
    public function getImagem()
    {
        return $this->imagem;
    }

    /**
     * @param mixed $imagem
     */
    public function setImagem($imagem)
    {
        $this->imagem = $imagem;
    }


    private function cadastrar()
    {
        return $this->returnCadastrarDb($this->getSenha(), $this->getEmail(), $this->getNascimento(), $this->getNome(), $this->getImagem());
    }


    public function returnCadastrar()
    {
        return $this->cadastrar();
    }

    private function logar()
    {
        $user = $this->returnLogarDb($this->getSenha(), $this->getEmail());

        if (!empty($user)) {
            $_SESSION['email'] = $user['email'];
            $_SESSION['nome'] = $user['nome'];
            $_SESSION['imagem'] = $user['imagem'];

            return true;
        }

        return $this->removerLogin();
    }

    public function returnLogar()
    {
        session_start();
        return $this->logar();
    }

    private function removerLogin()
    {
        session_unset();
        session_destroy();

        return false;
    }

    public function returnRemoverLogin()
    {
        session_start();
        return $this->removerLogin();
    }

    private function consultar()
    {
        if(isset($_SESSION['email']) && $this->returnConsultarDb($_SESSION['email']) > 0) {
            return array("success" => true, "nome" => $_SESSION['nome'], "email" => $_SESSION['email'], "imagem" => $_SESSION['imagem']);
        }
    }

    public function returnConsultar()
    {
        session_start();
        return $this->consultar();
    }

}