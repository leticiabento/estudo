<?php

namespace FluidGallery\Http\Controllers;

use FluidGallery\Upload;
use Illuminate\Http\Request;

class UploadsController extends Controller
{

    private $up;
    public function __construct(Upload $up)
    {
        $this->up = $up;
    }

    public function home()
    {
        $files = $this->up->all();
        return view("FluidGallery.index", compact("files"));
    }

    public function create()
    {
        return view("FluidGallery.create");
    }

    public function store(Request $request)
    {
        $infos = $request->all();
        $myImage = $request->image;
        $hashImage = $myImage->hashName();

        $infos['file'] = $hashImage;

        if($request->hasFile('image')) {
            $upload = $myImage->store('images');

            if($upload) {
                $this->up->create($infos);

                return redirect()->route("gallery.home");
            }

            return redirect()->route("gallery.create");

        }

    }


}
