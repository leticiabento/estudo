<?php

namespace FluidGallery;

use Illuminate\Database\Eloquent\Model;

class Upload extends Model
{
    protected $table = "uploads";
    protected $fillable = ["author", "title", "description", "file"];
}
