@extends("app")

@section("content")
    <section class="container-imagens">

        @foreach($files as $file)
        <div class="imagem">
            <div class="texto">
                <h3>{{$file->title}}</h3>
                <small>por: {{$file->author}}</small>

                <p>{{$file->description}}</p>
            </div>
            <img src="images/images/{{$file->file}}" alt="">
        </div>
        @endforeach

    </section>
@endsection