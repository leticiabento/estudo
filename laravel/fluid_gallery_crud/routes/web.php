<?php


Route::get('/', function () {
    return view('FluidGallery.index');
});


Route::get('home', "UploadsController@home")->name('gallery.home');
Route::get('create', "UploadsController@create")->name('gallery.create');
Route::post('upload', "UploadsController@store")->name('gallery.store');