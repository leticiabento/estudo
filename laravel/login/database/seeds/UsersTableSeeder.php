<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $data = [
            'name' => "Letícia Bento",
            'email' => "let@let.com",
            'password' => bcrypt(123456), // secret
            'remember_token' => str_random(10),
        ];

        DB::table('users')->insert($data);
    }
}
