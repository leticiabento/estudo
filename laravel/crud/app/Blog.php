<?php

namespace Crud;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $table = "posts";
    protected $fillable = ["title", "file", "description"];
}
