<?php

namespace Crud\Http\Controllers;

use Crud\Blog;
use Illuminate\Http\Request;

class BlogsController extends Controller
{
    private $blog;
    public function __construct(Blog $blog)
    {
        $this->blog = $blog;
    }

    public function index()
    {
        $files = $this->blog->all();
        return view("blog.index", compact("files"));
    }

    public function create()
    {
        return view("blog.create");
    }

    public function store(Request $request)
    {
        $info = $request->all();
        $myImage = $request->image;
        $hashImage = $myImage->hashName();

        $info['file'] = $hashImage;

        if($request->hasFile('image')) {
            $upload = $myImage->store('images');

            if($upload) {
                $this->blog->create($info);

                return redirect()->route("blog.home");
            }

            return redirect()->route("blog.create");
        }

    }

    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = $this->blog->findOrFail($id);
        return view("blog.edit", compact("post"));

    }

    public function update(Request $request, $id)
    {

        $infos = $request->all();
        $image = $request->image;
        $hashImage = $image->hashName();
        $infos['file'] = $hashImage;

        if($request->hasFile("image")) {
            $upload = $image->store("images");

            if($upload) {
                $post = $this->blog->find($id)->fill($infos);
                $post->update();

                if($post) {
                    return redirect()->route("blog.home");
                }

                return redirect()->route("blog.home");
            }
        }


    }

    public function destroy($id)
    {
        $blog = $this->blog->find($id)->delete();

        if($blog) {
            return redirect()->route("blog.home");
        }

        return redirect()->route("blog.home");
    }
}
