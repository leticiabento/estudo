<?php


Route::get('/', "BlogsController@index")->name("blog.home");

Route::get("home", "BlogsController@index")->name("blog.home");
Route::get("create", "BlogsController@create")->name("blog.create");
Route::post("store", "BlogsController@store")->name("blog.store");
Route::get("blog/{id}/destroy", "BlogsController@destroy")->name("blog.destroy");
Route::get("blog/{id}/edit", "BlogsController@edit")->name("blog.edit");
Route::post("blog/{id}/update", "BlogsController@update")->name("blog.update");

