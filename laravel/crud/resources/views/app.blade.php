<html>
    <head>
        <meta charset="UTF-8">
        <title>Blog</title>
        <link rel="stylesheet" href="{{ asset("css/app.css") }}">
        <link rel="stylesheet" href="{{ asset("css/style.css") }}">
    </head>

    <body>
        <ul class="menu">
            <li><a href="{{route("blog.home")}}">HOME</a></li>
            <li><a href="{{route("blog.create")}}">CADASTRO</a></li>
        </ul>

        @yield("content")

    </body>
</html>