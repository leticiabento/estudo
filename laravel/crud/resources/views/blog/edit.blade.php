@extends("app")

@section("content")

    <section class="container-contato">
        <form action="{{route("blog.update", $post)}}" method="post" enctype="multipart/form-data">

            {{ csrf_field() }}

            <div>
                <label for="title">Titulo</label>
                <input type="text" name="title" value="{{$post->title}}" />
            </div>

            <div>
                <label for="description">Descrição</label>
                <textarea name="description" id="description">{{$post->description}}</textarea>
            </div>

            <div>
                <label for="image">Imagem</label>
                <input type="file" name="image" id="image" value="{{$post->image}}">
            </div>

            <button type="submit">ATUALIZAR</button>
        </form>
    </section>

@endsection