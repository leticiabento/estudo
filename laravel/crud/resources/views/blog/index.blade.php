@extends("app")
@section("content")

    <section class="container-imagens">

        @foreach($files as $file)

            <div class="imagem">
                <div class="botoes">
                    <a href="{{route("blog.edit", $file->id)}}" class="btn btn-warning">Editar</a>
                    <a href="{{route("blog.destroy", $file->id)}}" class="btn btn-danger">Deletar</a>
                </div>

                <div class="texto">
                    <h3>{{$file->title}}</h3>
                    <p>{{$file->description}}</p>
                </div>
                <img src="app/images/{{$file->file}}" alt="">
            </div>
        @endforeach

    </section>

@endsection