@extends("app")

@section("content")

    <section class="container-contato">
        <form action="{{route("blog.store")}}" method="post" enctype="multipart/form-data">

            {{ csrf_field() }}

            <div>
                <label for="title">Titulo</label>
                <input type="text" name="title" />
            </div>

            <div>
                <label for="description">Descrição</label>
                <textarea name="description" id="description"></textarea>
            </div>

            <div>
                <label for="image">Imagem</label>
                <input type="file" name="image" id="image">
            </div>

            <button type="submit">CADASTRAR</button>
        </form>
    </section>

@endsection