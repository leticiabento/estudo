<?php

namespace Blog\Http\Controllers;

use Blog\Post;
use Illuminate\Http\Request;


class PostsController extends Controller
{

    private $post;

    public function __construct(Post $post) //injeção de dependencia. Injeto o post(model)
    {
        $this->post = $post; //mesma coisa que fazer $this->post = new Post();
    }

    public function index()
    {
//        return Post::paginate(1); //isso aumenta o nivel de acoplamento
//        return Post::all();

        $posts = $this->post->paginate(5);
        return view('posts.index', compact('posts'));
    }

    public function create()
    {
        return view('posts/create');
    }


    public function store(Request $request)
    {
//        return $this->post->create($request->all());
        //create -> passo os dados que recuperei

//        return $request->all()

//        dd($request->all()); var_dump + die()

        $post = $this->post->create($request->all());

        if (!$post) {
            return redirect("posts/create")->with("feedback_error", "Não cadastrado. Tente novamente!");
        }
        return redirect("posts/create")->with("feedback_ok", "Cadastrado com sucesso!");


    }

    public function show($id)
    {
        $post = $this->post->with('comments')->find($id);

//        return $post->commets;

        return view('posts.show', compact('post'));
    }

    public function edit($id)
    {
        $post = $this->post->findOrFail($id);
        return view('posts.edit', compact('post')); //compact tem q ser o msm nome da viariavel de cima
    }

    public function update(Request $request, $id)
    {
//        $post = $request->all();
//        return $this->find($id)->fill($post); //fill->preencher
        $post = $this->post->find($id)->fill($request->all());
        $post->update();

        if (!$post) {
            return redirect("posts/". $id . "/edit")->with("feedback_error", "Não atualizado. Tente novamente!");
        }
        return redirect("posts/". $id . "/edit")->with("feedback_ok", "Atualizado com sucesso!");

    }

    public function destroy($id)
    {
        $post = $this->post->find($id)->delete();

//        dd($post);

        if (!$post) {
            return redirect("posts")->with("feedback_error", "Não deletado. Tente novamente!");
        }
        return redirect("posts")->with("feedback_ok", "Deletado com sucesso!");
    }
}
