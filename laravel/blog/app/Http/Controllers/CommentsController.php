<?php

namespace Blog\Http\Controllers;

Use Blog\Comment;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    private $comment;

    public function __construct(Comment $comment) //injeção de dependencia. Injeto o post(model)
    {
        $this->comment = $comment; //mesma coisa que fazer $this->post = new Post();
    }

    public function index()
    {

    }

    public function create()
    {

    }

    public function store(Request $request)
    {
        $comment = $this->comment->create($request->all());

        if (!$comment) {
            return redirect("posts/" . $request->post_id)->with("feedback_error", "Não cadastrado. Tente novamente!");
        }
        return redirect("posts/" . $request->post_id)->with("feedback_ok", "Comentario cadastrado com sucesso!");
    }


    public function show($id)
    {

    }

    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
