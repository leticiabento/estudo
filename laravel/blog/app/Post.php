<?php

namespace Blog;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = "posts";
    protected $fillable = ["title", "description"]; //se n passar os campos, o laravel n consegue interpretar

    //smp protected

    public function comments()
    {
        return $this->hasMany("Blog\Comment"); //um relacionamento de muitos para um
    }

}
