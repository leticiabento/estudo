<?php

namespace Blog;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = "coments";
    protected $fillable = ["user", "comment", "post_id"];

    public function post()
    {
        return $this->belongsTo('Blog\Post'); //um relacionamento de um para um
    }
}
