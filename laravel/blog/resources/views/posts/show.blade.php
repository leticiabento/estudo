@extends('app')

@section('content')
    <div class="row">
        <div>
            <h1>{{$post->title}}</h1>
            <p>{{$post->description}}</p>
        </div>
    </div>

    <br>

    <div class="row">
        <h3>Comentários</h3>
        <hr>

        <div class="row">
            @foreach($post->comments as $comment)

                <div class="col-sm-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{$comment->user}}</h3>
                        </div>
                        <div class="panel-body">
                            <p>{{$comment->comment}}</p>
                        </div>
                    </div>
                </div>

            @endforeach
        </div>

        <form action="{{ route('comments.store') }}" method="POST">

            {{ csrf_field() }}

            <input type="hidden" name="post_id" value="{{ $post->id }}">

            <div class="form-group">
                <label for="nome">Nome</label>
                <input type="text" class="form-control" id="nome" name="user" placeholder="Nome">
            </div>

            <div class="form-group">
                <label for="comentario">Comentario</label>
                <textarea name="comment" id="comentario" class="form-control"></textarea>
            </div>

            <button type="submit">Comentar</button>
        </form>
    </div>
@endsection


{{--rota p recurso q vou precisar--}}
{{--definir action do form--}}
{{--cria o metodo q cadastra--}}