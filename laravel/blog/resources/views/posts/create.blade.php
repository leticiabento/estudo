@extends('app')

@section('content')

    <div class="row">

        @include("posts._feedback")

        <form action="{{route('posts.store')}}" method="POST">
            {{ csrf_field() }}

            @include("posts._form")

            <button type="submit" class="btn btn-default">Cadastrar</button>
        </form>
    </div>

@endsection