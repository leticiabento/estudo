<div class="form-group">
    <label for="title">Título</label>
    <input type="text" class="form-control" id="title" name="title" placeholder="Título" value="{{ @$post->title }}">
</div>

<div class="form-group">
    <label for="descricao">Descrição</label>
    <textarea name="description" id="descricao" class="form-control">{{ @$post->description }}</textarea>
</div>