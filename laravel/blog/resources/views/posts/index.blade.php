@extends('app')

@section('content')
    <div class="row">

        @include("posts._feedback")

        @foreach($posts as $value)

            <div class="col-sm-12 col-md-4">
                <div class="panel panel-default">
                    <div class="panel-body">
                        {{ $value->title }}
                    </div>

                    <div class="panel-footer">
                        {{ $value->description }}
                        <hr>

                        <a class='btn btn-success' href="{{ route('posts.show', $value->id) }}">Leia Mais</a>
                        <a class='btn btn-danger' href="{{ route('posts.destroy', $value->id) }}">Deletar</a>
                        <a class='btn btn-warning' href="{{ route('posts.edit', $value->id) }}">Editar</a>
                    </div>

                </div>
            </div>

        @endforeach
    </div>
@endsection