@extends('app')

@section('content')

    <div class="row">

        @include("posts._feedback")

        <form action="{{route('posts.update', $post)}}" method="post">
            {{ csrf_field() }}

            @include("posts._form")

            {{--<div class="form-group">--}}
                {{--<label for="title">Título</label>--}}
                {{--<input type="text" class="form-control" id="title" name="title" placeholder="Título" value="{{ $post->title }}">--}}
            {{--</div>--}}

            {{--<div class="form-group">--}}
                {{--<label for="descricao">Descrição</label>--}}
                {{--<textarea name="description" id="descricao" class="form-control">{{ $post->description }}</textarea>--}}
            {{--</div>--}}

            <button type="submit" class="btn btn-default">Atualizar</button>
        </form>
    </div>

@endsection