<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComentsTable extends Migration
{

    public function up()
    {
        Schema::create('coments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user');
            $table->longText('comment');

            $table->integer('post_id')->unsigned(); //so recebe valores positivos. Nao esquecer p criar o relacionamento
            $table->foreign('post_id')->references('id')->on('posts');
            //chave estrageira (integer), que vira chave estrangeira referencio a minha id, na tabela posts

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('coments');
    }
}
