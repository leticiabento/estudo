<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('app');
});

//protocolos: get, post, put, patch, delete
//Route::get('teste', function() {
//    return "página de teste";
//});

//Route::get("new", 'NewController@index')->name('new.index');

//Route::get("post/{id}", 'PostsController@show')->name("posts.show");
Route::post("post/{id}", 'PostsController@update')->name("posts.update");
Route::get("post/{id}/destroy", 'PostsController@destroy')->name("posts.destroy");



Route::resource('posts', 'PostsController', [
    "except" => ["update", "destroy"]
]);

Route::resource('comments', 'CommentsController');

//Route::resource('posts', 'PostsController', [
//    "only" => ['store']
//    "except" => ['index']
//]);

