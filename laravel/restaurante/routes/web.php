<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect("desks"); //nome da pasta + nome do arquivo
});

Route::get("order/desk/{id}", "OrdersController@create")->name("orders.desks.index");

Route::resource("desks", "DesksController");
Route::resource("orders", "OrdersController");
Route::resource("kitchens", "KitchensController");
