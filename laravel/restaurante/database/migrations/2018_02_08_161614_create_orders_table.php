<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("desk_id")->unsigned();
            $table->integer("product_id")->unsigned();
            $table->integer("kitchen_id")->unsigned();

            $table->foreign("desk_id")->references("id")->on("desks");
            $table->foreign("product_id")->references("id")->on("products");
            $table->foreign("kitchen_id")->references("id")->on("kitchens");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
