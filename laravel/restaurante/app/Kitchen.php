<?php

namespace Restaurant;

use Illuminate\Database\Eloquent\Model;

class Kitchen extends Model
{
    protected $table = 'kitchens';
    protected $fillable = ['name'];

    public function orders() {
        return $this->hasMany("Restaurant\Order");
    }
}
