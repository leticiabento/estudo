<?php

namespace Restaurant;

use Illuminate\Database\Eloquent\Model;

class Desk extends Model
{
    protected $table = "desks";
    protected $fillable = ['name', 'status'];

    public function orders() {
        return $this->hasMany("Restaurant\Order");
    }
}
