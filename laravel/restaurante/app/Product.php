<?php

namespace Restaurant;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = "products";
    protected $fillable = ["name", "price"];

    public function orders() {
        return $this->hasMany("Restaurant\Order");
    }
}
