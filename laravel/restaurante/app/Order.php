<?php

namespace Restaurant;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = "orders";
    protected $fillable = ['desks_id', 'products_id', 'kitchens_id', 'quantidade'];

    public function product()
    {
        return $this->belongsTo("Restaurant\Product");
    }

    public function kitchen()
    {
        return $this->belongsTo("Restaurant\Kitchen");
    }

    public function desk()
    {
        return $this->belongsTo("Restaurant\Desk");
    }
}
