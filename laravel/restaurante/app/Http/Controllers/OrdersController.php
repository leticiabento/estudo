<?php

namespace Restaurant\Http\Controllers;

use Illuminate\Http\Request;
use Restaurant\Kitchen;
use Restaurant\Order;
use Restaurant\Product;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private $order;
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
//        return $id;
        $kitchens = Kitchen::all();
        $products = Product::all();
        return view("layouts.pedidos", compact("kitchens", "products", "id"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        return $request->all();
        $order = $this->order->create($request->all());

        if(!$order) {
            return redirect("desks")->with("feedback_error", "Não cadastrado!");
        } else {
            return redirect("desks")->with("feedback_ok", "Cadastrado com success!");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
