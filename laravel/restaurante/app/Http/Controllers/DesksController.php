<?php

namespace Restaurant\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Restaurant\Desk;


class DesksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $desks = DB::table("orders")
//            ->join("desks", "orders.desks_id", "=", "desks.id")
//            ->join("kitchens", "orders.kitchens_id", "=", "kitchens.id")
//            ->join("products", "orders.products_id", "=", "products.id")
//            ->select("kitchens.name as cozinha", "orders.quantidade", "products.name as produto", "products.price", "desks.name as mesas", "desks.id as idMesa")
////            ->groupBy('desks.id')
//            ->get();

        $desks = Desk::with('orders')->find(2);

        return $desks;
        return view("layouts.index", compact("desks"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
