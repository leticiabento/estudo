@extends("app")

@section("content")
    <div class="boxes">
        <div class="form">
            <div class="container">
                <h1>Faça seu pedido</h1>

                <form action="{{ route("orders.store") }}" method="post">

                    {{ csrf_field() }}

                    <input type="hidden" name="desks_id" value="{{ $id }}" >

                    <div class="element">
                        <label>Cozinha:</label>
                        <select name="kitchens_id" id="cozinha" class="field">

                            @foreach($kitchens as $kitchen)
                                <option value="{{ $kitchen->id }}">{{ $kitchen->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="element">
                        <label>Produto:</label>
                        <select name="products_id" id="produto" class="field">
                            @foreach($products as $product)
                                <option value="{{$product->id}}">{{$product->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="element">
                        <label>Quantidade:</label>
                        <input type="number" name="quantidade" id="quantidade">
                    </div>

                    <div class="options">
                        <input type="submit" class="button" value="Cadastrar"/>
                        <input type="submit" class="button" value="Limpar"/>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection