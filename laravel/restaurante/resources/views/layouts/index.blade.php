@extends("app")

@section("content")

    @include("layouts._feedback")

    @foreach($desks as $desk)
        <div class="box">
            <div class="container">
                <h1> {{$desk->mesas}} </h1>

                <div class="orders">
                    <h2>Pedidos</h2>
                    <ul>
                        <li>{{$desk->cozinha}}: {{$desk->quantidade}} - {{$desk->produto}} <small> {{ $desk->price }}</small> </li>
                    </ul>
                </div>

                <div class="options">
                    <a href=" {{ route("orders.desks.index", $desk->idMesa) }}">
                        <button>Adicionar Pedido</button>
                    </a>
                    <a href="">
                        <button>Fechar Conta</button>
                    </a>
                </div>
            </div>
        </div>
    @endforeach

@endsection