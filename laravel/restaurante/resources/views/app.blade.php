<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Gerenciador de Restaurantes - Fernando Monteiro</title>
    {{--<link href=" {{ asset("css/app.css") }}" rel="stylesheet" type="text/css" media="all"/>--}}
    <link href=" {{ asset("css/style.css") }}" rel="stylesheet" type="text/css" media="all"/>
</head>
<body>
<div class="wrapper">
    <div id="header">
        <div id="logo">
            <a href="#"><img src=" {{ asset("images/logo_fernando_monteiro.png") }}" alt="Logo - Fernando Monteiro"/></a>
        </div>
        <div id="nav">
            <ul>
                <li><a href=" {{ route("desks.index") }}">Home</a></li>
                <li><a href="{{ route("desks.index") }}">Mesas</a></li>
                <li><a href="cozinhas.html">Cozinhas</a></li>
            </ul>
        </div>
    </div>
    <div id="content">

        @yield("content")

    </div>
</div>

</body>
</html>