@extends("app")
@section("content")

<div class="container">
    <h2>Booking</h2>
    <div class="row">
        <div class="col-md-8">
            <form role="form" action="{{route('hotel.payment')}}" method="post">
                {{csrf_field()}}

                <input type="hidden" name="hotel_id" value="{{session("hotel")[0]->id}}">
                {{--<input type="hidden" name="users_id" value="{{$user_id}}">--}}
                <input type="hidden" name="count" value="{{session("users")->count()}}">

                <section>
                    <h3>Contact Information</h3>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="form-group row">
                                <div class="col-sm-2">
                                    <label class="control-label">Title:</label>
                                    <select class="form-control" name="title">
                                        <option value="mr">Mr.</option>
                                        <option value="mrs">Mrs.</option>
                                    </select>
                                </div>

                                <div class="col-sm-5">
                                    <label class="control-label">
                                        First Name:
                                    </label>
                                    <input type="text" class="form-control" name="first_name">
                                </div>
                                <div class="col-sm-5">
                                    <label class="control-label">
                                        Last Name:
                                    </label>
                                    <input type="text" class="form-control" name="last_name">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <label class="control-label">
                                        Contact's Mobile phone number
                                    </label>
                                    <input type="tel" class="form-control" name="telephone">
                                </div>
                                <div class="col-sm-6">
                                    <label class="control-label">
                                        Contact's email address
                                    </label>
                                    <input type="email" class="form-control" name="email">
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section>
                    <h3>Price Details</h3>
                    <div>
                        <h5>
                            <strong>{{session("hotel")[0]->name}} - {{session("hotel")[0]->city}}</strong>
                        </h5>
                        <ul class="list-group">
                            <li class="list-group-item">
                                <div class="pull-left">
                                    <strong>{{session("users")->count()}} people</strong>
                                </div>
                                <div class="pull-right">
                                    <strong>{{session("hotel")[0]->moeda}} {{session("soma")[0]->total}}</strong>
                                </div>
                                <div class="clearfix"></div>
                            </li>
                            <li class="list-group-item list-group-item-info">
                                <div class="pull-left">
                                    <strong>You Pay</strong>
                                </div>
                                <div class="pull-right">
                                    <strong>{{session("hotel")[0]->moeda}} {{session("soma")[0]->total}}</strong>
                                </div>
                                <div class="clearfix"></div>
                            </li>
                        </ul>
                    </div>
                </section>
                <section>
                    <h3>Payment</h3>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="control-label">
                                    Payment Method:
                                </label>
                                <select class="form-control" name="method">
                                    <option value="transfer">Transfer</option>
                                    <option value="credit_card">Credit Card</option>
                                    <option value="paypal">Paypal</option>
                                </select>
                            </div>
                            <h4>Credit Card</h4>
                            <div class="form-group">
                                <label class="control-label">Card Number</label>
                                <input type="number" class="form-control" placeholder="16 digit card number" name="card_number">
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-10">
                                    <label class="control-label">Name on card:</label>
                                    <input type="text" class="form-control" name="card_name">
                                </div>
                                <div class="col-sm-2">
                                    <label class="control-label">CCV Code:</label>
                                    <input type="number" maxlength="3" class="form-control" placeholder="3 digit CCV" name="card_ccv">
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section>
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">
                            Finish Payment
                        </button>
                    </div>
                </section>
            </form>
        </div>
        <div class="col-md-4">
            <h3>Booking Details</h3>
            <aside>
                <article>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h4><strong>{{session("hotel")[0]->name}}</strong></h4>
                                    <div class="location">
                                        <small>
                                            <i class="glyphicon glyphicon-map-marker"></i> {{session("hotel")[0]->city}}
                                        </small>
                                    </div>
                                </div>
                            </div>
                            <br>

                            <div class="row">
                                <div class="col-sm-12">
                                    <ul class="list-group" style="margin-bottom: 0;">
                                        <li class="list-group-item">
                                            <div class="pull-left">No. of guests</div>
                                            <div class="pull-right">{{session("hotel")[0]->guests}}  Guests</div>
                                            <div class="clearfix"></div>
                                        </li>
                                        <li class="list-group-item">
                                            <div class="pull-left">Price per guest</div>
                                            <div class="pull-right">{{session("hotel")[0]->moeda}} {{session("hotel")[0]->price}}</div>
                                            <div class="clearfix"></div>
                                        </li>
                                        </li>
                                        <li class="list-group-item">
                                            <div class="pull-left">Total cost</div>
                                            <div class="pull-right">{{session("hotel")[0]->moeda}} {{session("soma")[0]->total}}</div>
                                            <div class="clearfix"></div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </aside>
        </div>
    </div>
</div>

@endsection