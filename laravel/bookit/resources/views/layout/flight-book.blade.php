@extends("app")
@section("content")

    <div class="container">
        <h2>Booking</h2>

        @include('layout._feedback')

        <div class="row">
            <div class="col-md-8">
                <form role="form" action="{{route('storePassanger')}}" method="POST">

                    {{csrf_field()}}
                    <input type="hidden" name="flight_id" value={{$id}}>
                    <h3 class="clearfix">Passenger(s) Details
                        <button type="button" class="btn btn-primary pull-right btnAddPassenger"><i
                                    class="glyphicon glyphicon-plus"></i> Add passenger
                        </button>
                    </h3>

                    <section class="passengers">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <h4>Passenger #1
                                    <button class="btn btn-danger pull-right" type="button"><i
                                                class="glyphicon glyphicon-remove"></i></button>
                                </h4>
                                <div class="form-group row">
                                    <div class="col-sm-3">
                                        <label class="control-label">Title:</label>
                                        <select class="form-control" name="title[0]">
                                            <option value="mr">Mr.</option>
                                            <option value="mrs">Mrs.</option>
                                        </select>
                                    </div>

                                    <div class="col-sm-3">
                                        <label class="control-label">Category:</label>
                                        <select class="form-control" name="category[0]">
                                            @foreach($categorias as $cat)
                                                <option value="{{$cat->name}}">{{$cat->name}}</option>
                                            @endforeach

                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <label class="control-label">First Name:</label>
                                        <input type="text" class="form-control" name="first_name[0]">
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="control-label">Last Name:</label>
                                        <input type="text" class="form-control" name="last_name[0]">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>


                    <section>
                        <div class="text-right">
                            <button type="submit" class="btn btn-primary">
                                Continue for Payment
                            </button>
                        </div>
                    </section>
                </form>
            </div>
            <div class="col-md-4">
                <h3>Flight</h3>
                <aside>
                    <article>
                        <div>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <h5>
                                        <strong class="airline">{{$voo[0]['company']}} {{$voo[0]['code']}}</strong>
                                    </h5>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div><big class="time">18:45</big></div>
                                                    <div>
                                                        <small class="date">{{$voo[0]['date_start']}}</small>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div><span class="place">{{$voo[0]['city_start']}}</span></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 text-center">
                                            <i class="glyphicon glyphicon-arrow-down"></i>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div><big class="time">23:20</big></div>
                                                    <div>
                                                        <small class="date">{{$voo[0]['date_end']}}</small>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div><span class="place">{{$voo[0]['city_end']}}</span></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </article>
                </aside>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script>
        var count = 0;
        var countPassenger = 1;
        $(".btnAddPassenger").click(function () {
            count++;
            countPassenger++;

            $(".passengers").append(
                '<div class="panel panel-default">' +
                '                        <div class="panel-body">' +
                '                            <h4>Passenger #' + countPassenger + ' <button class="btn btn-danger pull-right" type="button"><i class="glyphicon glyphicon-remove"></i></button></h4>' +
                '                            <div class="form-group row">' +
                '                                <div class="col-sm-3">' +
                '                                    <label class="control-label">Title:</label>' +
                '                                    <select class="form-control" name="title[' + count + ']">' +
                '                                        <option value="mr">Mr.</option>' +
                '                                        <option value="mrs">Mrs.</option>' +
                '                                    </select>' +
                '                                </div>' +
                '' +
                '                                <div class="col-sm-3">' +
                '                                    <label class="control-label">Category:</label>' +
                '                                    <select class="form-control" name="category[' + count + ']">' +
                '                                        @foreach($categorias as $cat)' +
                '                                            <option value="{{$cat->name}}">{{$cat->name}}</option>' +
                '                                        @endforeach' +
                '                                        ' +
                '                                    </select>' +
                '                                </div>' +
                '                            </div>' +
                '                            <div class="form-group row">' +
                '                                <div class="col-sm-6">' +
                '                                    <label class="control-label">First Name:</label>' +
                '                                    <input type="text" class="form-control" name="first_name[' + count + ']">' +
                '                                </div>' +
                '                                <div class="col-sm-6">' +
                '                                    <label class="control-label">Last Name:</label>' +
                '                                    <input type="text" class="form-control" name="last_name[' + count + ']">' +
                '                                </div>' +
                '                            </div>' +
                '                        </div>' +
                '                    </div>'
            );

        })
    </script>
@endsection