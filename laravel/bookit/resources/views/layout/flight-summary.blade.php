@extends("app")
@section("content")
<div class="container">
    <h2>Flight Summary</h2>

    <div class="row">
        <div class="col-md-12">
            <form role="form" action="#">
                <section>
                    <h4>Contact Information</h4>
                    <ul class="list-group">
                        <li class="list-group-item">
                            <div class="pull-left">
                                <span>Full name</span>
                            </div>
                            <div class="pull-right">
                                <span>{{$payment[0]->first_name}} {{$payment[0]->last_name}}</span>
                            </div>
                            <div class="clearfix"></div>
                        </li>
                        <li class="list-group-item">
                            <div class="pull-left">
                                <span>Contact's Mobile phone number</span>
                            </div>
                            <div class="pull-right">
                                <span>{{$payment[0]->telephone}}</span>
                            </div>
                            <div class="clearfix"></div>
                        </li>
                        <li class="list-group-item">
                            <div class="pull-left">
                                <span>Contact's Email</span>
                            </div>
                            <div class="pull-right">
                                <span>{{$payment[0]->email}}</span>
                            </div>
                            <div class="clearfix"></div>
                        </li>
                    </ul>

                    <h4>Passengers</h4>
                    <!-- PASSENGER -->
                    <ul class="list-group">
                        <li class="list-group-item">
                            <div class="pull-left">
                                <strong>Passenger #01</strong>
                            </div>
                            <div class="clearfix"></div>
                        </li>
                        <li class="list-group-item">
                            <div class="pull-left">
                                <span>Full name</span>
                            </div>
                            <div class="pull-right">
                                <span>{{$user[0]['first_name']}} {{$user[0]['last_name']}}</span>
                            </div>
                            <div class="clearfix"></div>
                        </li>
                        <li class="list-group-item">
                            <div class="pull-left">
                                <span>Category</span>
                            </div>
                            <div class="pull-right">
                                <span>{{$infos[0]->category}}</span>
                            </div>
                            <div class="clearfix"></div>
                        </li>
                    </ul>
                    <!-- /PASSENGER -->


                    <h4>Flight Information</h4>
                    <ul class="list-group">
                        <li class="list-group-item">
                            <div class="pull-left">
                                <span>Flight Code</span>
                            </div>
                            <div class="pull-right">
                                <span>{{$infos[0]->flight['code']}}</span>
                            </div>
                            <div class="clearfix"></div>
                        </li>
                        <li class="list-group-item">
                            <div class="pull-left">
                                <span>Airline</span>
                            </div>
                            <div class="pull-right">
                                <span>{{$infos[0]->flight['company']}}</span>
                            </div>
                            <div class="clearfix"></div>
                        </li>
                        <li class="list-group-item">
                            <div class="pull-left">
                                <span>Departure</span>
                            </div>
                            <div class="pull-right">
                                <span>{{$infos[0]->flight['city_start']}} - {{$infos[0]->flight['date_start']}}</span>
                            </div>
                            <div class="clearfix"></div>
                        </li>
                        <li class="list-group-item">
                            <div class="pull-left">
                                <span>Destination</span>
                            </div>
                            <div class="pull-right">
                                <span>{{$infos[0]->flight['city_end']}} - {{$infos[0]->flight['date_end']}}</span>
                            </div>
                            <div class="clearfix"></div>
                        </li>
                    </ul>

                    <h4>Payment</h4>
                    <ul class="list-group">
                        <li class="list-group-item">
                            <div class="pull-left">
                                <strong>Passengers Fare</strong>
                            </div>
                            <div class="pull-right">
                                <strong>{{$price[0]['moeda']}} {{$price[0]['price']}}</strong>
                            </div>
                            <div class="clearfix"></div>
                        </li>
                        <li class="list-group-item list-group-item-info">
                            <div class="pull-left">
                                <strong>You Pay</strong>
                            </div>
                            <div class="pull-right">
                                <strong>{{$price[0]['moeda']}} {{$price[0]['price']}}</strong>
                            </div>
                            <div class="clearfix"></div>
                        </li>
                    </ul>

                    <button class="btn btn-primary pull-right">Print</button>
                </section>
            </form>
        </div>
    </div>
</div>

@endsection