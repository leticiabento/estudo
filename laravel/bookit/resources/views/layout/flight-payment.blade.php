@extends("app")
@section("content")

<div class="container">
    <h2>Payment</h2>
    <div class="row">
        <div class="col-md-8">
            <form role="form" action="{{route('storePayment')}}" method="POST">
                {{csrf_field()}}
                <input type="hidden" name="users_id" value="{{$id}}">

                <section>
                    <h3>Contact Information</h3>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="form-group row">
                                <div class="col-sm-2">
                                    <label class="control-label">Title:</label>
                                    <select class="form-control" name="title">
                                        <option value="mr">Mr.</option>
                                        <option value="mrs">Mrs.</option>
                                    </select>
                                </div>

                                <div class="col-sm-5">
                                    <label class="control-label">
                                        First Name:
                                    </label>
                                    <input type="text" class="form-control" name="first_name">
                                </div>
                                <div class="col-sm-5">
                                    <label class="control-label">
                                        Last Name:
                                    </label>
                                    <input type="text" class="form-control" name="last_name">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <label class="control-label">
                                        Contact's Mobile phone number
                                    </label>
                                    <input type="tel" class="form-control" name="telephone">
                                </div>
                                <div class="col-sm-6">
                                    <label class="control-label">
                                        Contact's email address
                                    </label>
                                    <input type="email" class="form-control" name="email">
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section>
                    <h3>Price Details</h3>
                    <div>
                        <h5>
                            <strong class="airline">{{$infosVoo[0]->flight->company}} {{$infosVoo[0]->flight->code}}</strong>
                        </h5>
                        <ul class="list-group">
                            <li class="list-group-item">
                                <div class="pull-left">
                                    <strong>Passengers Fare</strong>
                                </div>
                                <div class="pull-right">
                                    <strong>R$ {{$valorVoo[0]['soma']}}</strong>
                                </div>
                                <div class="clearfix"></div>
                            </li>
                            <li class="list-group-item list-group-item-info">
                                <div class="pull-left">
                                    <strong>You Pay</strong>
                                </div>
                                <div class="pull-right">
                                    <strong>R$ {{$valorVoo[0]['soma']}}</strong>
                                </div>
                                <div class="clearfix"></div>
                            </li>
                        </ul>
                    </div>
                </section>
                <section>
                    <h3>Payment</h3>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="control-label">
                                    Payment Method:
                                </label>
                                <select class="form-control" name="method">
                                    <option value="transfer">Transfer</option>
                                    <option value="credit_card">Credit Card</option>
                                    <option value="paypal">Paypal</option>
                                </select>
                            </div>
                            <h4>Credit Card</h4>
                            <div class="form-group">
                                <label class="control-label">Card Number</label>
                                <input type="text" class="form-control" name="card_number" placeholder="16 digit card number">
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-10">
                                    <label class="control-label">Name on card:</label>
                                    <input type="text" class="form-control" name="card_name">
                                </div>
                                <div class="col-sm-2">
                                    <label class="control-label">CCV Code:</label>
                                    <input type="text" maxlength="3" name="card_ccv" class="form-control" placeholder="3 digit CCV">
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section>
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">
                            Finish Payment
                        </button>
                    </div>
                </section>
            </form>
        </div>
        <div class="col-md-4">
            <h3>Flights</h3>
            <aside>
                <article>

                    <div>
                        <ul class="list-group">
                            <li class="list-group-item">
                                <h5>
                                    <strong class="airline">{{$infosVoo[0]->flight->company}} {{$infosVoo[0]->flight->code}}</strong>
                                </h5>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div><big class="time">{{$formatTimeStart}}</big></div>
                                                <div><small class="date">{{$formatDataStart}}</small></div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div><span class="place">{{$infosVoo[0]->flight->city_start}}</span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 text-center">
                                        <i class="glyphicon glyphicon-arrow-down"></i>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div><big class="time">{{$formatTimeEnd}}</big></div>
                                                <div><small class="date">{{$formatDataEnd}}</small></div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div><span class="place">{{$infosVoo[0]->flight->city_end}}</span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </article>
            </aside>
        </div>
    </div>
</div>
@endsection