@extends("app")
@section("content")

    <div class="container">
        <section>
            <h3>Flight Booking</h3>
            <div class="panel panel-default">
                <div class="panel-body">
                    <form role="form" method="post" action="{{route('flight.search')}}">
                        {{csrf_field()}}

                        <div class="row">
                            <div class="col-sm-4">
                                <h4 class="form-heading">1. Flight Origin</h4>
                                <div class="form-group">
                                    <label class="control-label">Departure city name: </label>
                                    <select class="form-control" name="city_start">
                                        <option value="">Select a city</option>
                                        @foreach($cities as $city)
                                            <option value="{{$city->name}}">{{$city->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <h4 class="form-heading">2. Flight Destination</h4>
                                <div class="form-group">
                                    <label class="control-label">Destination city name: </label>
                                    <select class="form-control" name="city_end">
                                        <option value>Select a city</option>
                                        @foreach($cities as $city)
                                            <option value="{{$city->name}}">{{$city->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <h4 class="form-heading">3. Date of Flight</h4>
                                <div class="form-group">
                                    <label class="control-label">Departure date: </label>
                                    <input type="date" name="date_start" class="form-control"
                                           placeholder="Choose Departure Date">
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-block">Search Flights</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>

@endsection