@extends("app")
@section("content")

    <div class="container">
        <h2>Booking</h2>

        @include('layout._feedback')

        <div class="row">
            <div class="col-md-8">
                <form role="form" action="{{route('hotel.passanger')}}" method="post">
                    {{csrf_field()}}

                    <input type="hidden" name="hotel_id" value="{{$id}}">

                    <section class="guests">
                        <h3 class="clearfix">Guest(s) Details
                            <button type="button" class="btn btn-primary pull-right btnAddGuest"><i
                                        class="glyphicon glyphicon-plus"></i> Add guest
                            </button>
                        </h3>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <h4>Guest #1</h4>
                                <div class="form-group row">
                                    <div class="col-sm-3">
                                        <label class="control-label">Title:</label>
                                        <select class="form-control" name="title[0]">
                                            <option value="mr">Mr.</option>
                                            <option value="mrs">Mrs.</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6" name="first_name">
                                        <label class="control-label">First Name:</label>
                                        <input type="text" class="form-control" name="first_name[0]">
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="control-label">Last Name:</label>
                                        <input type="text" class="form-control" name="last_name[0]">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section>
                        <div class="text-right">
                            <button type="submit" class="btn btn-primary">
                                Continue for Payment
                            </button>
                        </div>
                    </section>
                </form>
            </div>
            <div class="col-md-4">
                <h3>Booking Details</h3>
                <aside>
                    <article>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <h4><strong>{{$hotel[0]->name}}</strong></h4>
                                        <div class="location">
                                            <small>
                                                <i class="glyphicon glyphicon-map-marker"></i> {{$hotel[0]->city}}
                                            </small>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <ul class="list-group" style="margin-bottom: 0;">
                                            <li class="list-group-item">
                                                <div class="pull-left">No. of guests</div>
                                                <div class="pull-right">{{$hotel[0]->guests}} Guests</div>
                                                <div class="clearfix"></div>
                                            </li>
                                            <li class="list-group-item">
                                                <div class="pull-left">Price per guest</div>
                                                <div class="pull-right">{{$hotel[0]->moeda}}{{$hotel[0]->price}}</div>
                                                <div class="clearfix"></div>
                                            </li>
                                            </li>
                                            <li class="list-group-item">
                                                <div class="pull-left">Total cost</div>
                                                <div class="pull-right">{{$hotel[0]->moeda}} {{$soma[0]->total}}</div>
                                                <div class="clearfix"></div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                </aside>
            </div>
        </div>
    </div>


@endsection

@section('js')
    <script>
        var limite = '{!! $hotel[0]->guests !!}';
        var count = 1;

        $(".btnAddGuest").click(function () {
            if (count < limite) {
                $(".guests").append(
                    '<div class="panel panel-default">' +
                    '<div class="panel-body">' +
                    '<h4>Guest #' + (count + 1) + '</h4>' +
                    '<div class="form-group row">' +
                    '<div class="col-sm-3">' +
                    '<label class="control-label">Title:</label>' +
                    '<select class="form-control" name="title['+count+']">' +
                    '<option value="mr">Mr.</option>' +
                    '<option value="mrs">Mrs.</option>' +
                    '</select>' +
                    '</div>' +
                    '</div>' +
                    '<div class="form-group row">' +
                    '<div class="col-sm-6">' +
                    '<label class="control-label">First Name:</label>' +
                    '<input class="form-control" type="text" name="first_name['+count+']">' +
                    '</div>' +
                    '<div class="col-sm-6">' +
                    '<label class="control-label">Last Name:</label>' +
                    '<input class="form-control" type="text" name="last_name['+count+']">' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>'
                );

                count += 1;
            }
        })


    </script>
@endsection