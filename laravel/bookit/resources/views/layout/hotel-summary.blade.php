@extends("app")
@section("content")


    <div class="container">
        <h2>Hotel Summary</h2>
        <div class="row">
            <div class="col-md-12">
                <form role="form" action="#">
                    <section>
                        <h4>Contact Information</h4>
                        <ul class="list-group">
                            <li class="list-group-item">
                                <div class="pull-left">
                                    <span>Full name</span>
                                </div>
                                <div class="pull-right">
                                    <span>{{$data['first_name']}} {{$data['last_name']}}</span>
                                </div>
                                <div class="clearfix"></div>
                            </li>
                            <li class="list-group-item">
                                <div class="pull-left">
                                    <span>Contact's Mobile phone number</span>
                                </div>
                                <div class="pull-right">
                                    <span>{{$data['telephone']}}</span>
                                </div>
                                <div class="clearfix"></div>
                            </li>
                            <li class="list-group-item">
                                <div class="pull-left">
                                    <span>Contact's Email</span>
                                </div>
                                <div class="pull-right">
                                    <span>{{$data['email']}}</span>
                                </div>
                                <div class="clearfix"></div>
                            </li>
                        </ul>

                        <h4>Guests</h4>

                    @php
                        $a = 1;
                    @endphp

                    @foreach($lastUsers as $users)
                        <!-- GUEST -->
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <div class="pull-left">
                                        <strong>Guest #{{$a}}</strong>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <li class="list-group-item">
                                    <div class="pull-left">
                                        <span>Full name</span>
                                    </div>
                                    <div class="pull-right">
                                        <span>{{$users->first_name}} {{$users->last_name}}</span>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                            </ul>


                        @php
                            $a++;
                        @endphp

                        <!-- /GUEST -->
                        @endforeach


                        <h4>Hotel Information</h4>
                        <ul class="list-group">
                            <li class="list-group-item">
                                <div class="pull-left">
                                    <span>Name</span>
                                </div>
                                <div class="pull-right">
                                    <span>{{$myHotel[0]['name']}}</span>
                                </div>
                                <div class="clearfix"></div>
                            </li>
                            <li class="list-group-item">
                                <div class="pull-left">
                                    <span>City</span>
                                </div>
                                <div class="pull-right">
                                    <span>{{$myHotel[0]['city']}}</span>
                                </div>
                                <div class="clearfix"></div>
                            </li>
                        </ul>

                        <h4>Payment</h4>
                        <ul class="list-group">
                            <li class="list-group-item">
                                <div class="pull-left">
                                    <strong>Guests price</strong>
                                </div>
                                <div class="pull-right">
                                    <strong>{{$myHotel[0]['moeda']}} {{$myHotel[0]['price']}}</strong>
                                </div>
                                <div class="clearfix"></div>
                            </li>
                            <li class="list-group-item list-group-item-info">
                                <div class="pull-left">
                                    <strong>You Pay</strong>
                                </div>
                                <div class="pull-right">
                                    <strong>{{$myHotel[0]['moeda']}} {{$soma[0]['total']}}</strong>
                                </div>
                                <div class="clearfix"></div>
                            </li>
                        </ul>

                        <button class="btn btn-primary pull-right">Print</button>
                    </section>
                </form>
            </div>
        </div>
    </div>

@endsection