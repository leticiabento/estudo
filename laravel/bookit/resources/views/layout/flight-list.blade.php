@extends("app")
@section("content")

<div class="container">
    <section>
        <h2>{{$city_start}} <i class="glyphicon glyphicon-arrow-right"></i> {{$city_end}}</h2>

        <!-- FLIGHT  -->
        @foreach($voos as $voo)

        <article>
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h4><strong>{{$voo->company}}</strong></h4>
                            <div class="row">
                                <div class="col-sm-3">
                                    <img class="img-responsive airline-logo" src="assets/img/emirates.png" alt="Emirates logo">
                                </div>

                                <div class="col-sm-3">
                                    <div class="list-box">
                                        <label class="control-label">From:</label>
                                        <div><big class="time">{{$formatDataStart}} {{$voo->time_start}}</big></div>
                                        <div><span class="place">{{$voo->city_start}}</span></div>
                                    </div>

                                    <div class="list-box">
                                        <label class="control-label">To:</label>
                                        <div><big class="time">{{$formatDataEnd}} {{$voo->time_end}}</big></div>
                                        <div><span class="place">{{$voo->city_end}}</span></div>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <div class="list-box">
                                        <label class="control-label">Duration:</label>
                                        <div><big class="time">{{$diffInHours}}h</big></div>
                                    </div>

                                    <div class="list-box">
                                        <label class="control-label">Flight Code:</label>
                                        <div><span class="place">{{$voo->code}}</span></div>
                                    </div>
                                </div>
                                
                                <div class="col-sm-3 text-right">
                                    @foreach($voo->class as $class)
                                        <h3 class="price text-danger">{{$class->name}} <strong>{{$class->moeda}} {{$class->price}}</strong></h3>
                                    @endforeach
                                   
                                    <div>
                                        <a href="{{route('book', $voo->id)}}" class="btn btn-primary">Booking</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </article>
        @endforeach
        <!-- /FLIGHT  -->

    
        <div class="text-center">
            {{$voos->links()}}
        </div>
    </section>
</div>

@endsection