@extends("app")
@section("content")

    <div class="container">
        <h2>Your search results in {{$infos[0]->city}} for {{$infos[0]->guests}} guests</h2>
        <section>
            <!-- HOTEL -->
            @foreach($infos as $info)
                <article>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-sm-8">
                                            <h4><strong>{{$info->name}}</strong></h4>
                                            <div class="location">
                                                <small>
                                                    <i class="glyphicon glyphicon-map-marker"></i> {{$info->city}}
                                                </small>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 text-right">
                                            <h3 class="price text-danger">
                                                Price for guest
                                                <strong>{{$info->moeda}} {{$info->price}}</strong>
                                            </h3>
                                            <div>
                                                <button class="btn btn-default" href="#" class="btn btn-link"><i
                                                            class="glyphicon glyphicon-remove"></i> Close Details
                                                </button>
                                                <a href="{{route('hotel.book', $info->id)}}" class="btn btn-primary">Booking</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <section class="rooms">
                                <h3>Description</h3>
                                <p>{{$info->description}}</p>
                                <h3>Rooms Description</h3>

                                <ul class="list-group">
                                    @foreach($rooms as $room)
                                        <li class="list-group-item hotel-list">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <label class="control-label">Type Name:</label>
                                                    <h4>{{$room->name}}, {{$room->beds}}</h4>
                                                </div>
                                                <div class="col-sm-6 text-right">
                                                    <label class="control-label">Capacity:</label>
                                                    <h4 class="price text-danger"><strong>{{$room->capacity}}</strong></h4>
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach

                                </ul>
                            </section>
                        </div>
                    </div>
                </article>
                <!-- /HOTEL -->
            @endforeach

        </section>
    </div>
@endsection

<div class="text-center">
    {{$infos->links()}}
</div>