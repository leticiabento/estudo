@extends("app")
@section("content")

    <div class="container">
        <br>
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#flight-tab">Flight Booking</a></li>
            <li><a data-toggle="tab" href="#hotel-tab">Hotel Booking</a></li>
        </ul>
        <div class="tab-content">
            <div id="flight-tab" class="tab-pane fade in active">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form role="form" method="post" action="{{route('flight.search')}}">
                            {{csrf_field()}}

                            <div class="row">
                                <div class="col-sm-4">
                                    <h4 class="form-heading">1. Flight Origin</h4>
                                    <div class="form-group">
                                        <label class="control-label">Departure city name: </label>
                                        <select class="form-control" name="city_start">
                                            <option value="">Select a city</option>
                                            @foreach($cities as $city)
                                                <option value="{{$city->name}}">{{$city->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <h4 class="form-heading">2. Flight Destination</h4>
                                    <div class="form-group">
                                        <label class="control-label">Destination city name: </label>
                                        <select class="form-control" name="city_end">
                                            <option value>Select a city</option>
                                            @foreach($cities as $city)
                                                <option value="{{$city->name}}">{{$city->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <h4 class="form-heading">3. Date of Flight</h4>
                                    <div class="form-group">
                                        <label class="control-label">Departure date: </label>
                                        <input type="date" name="date_start" class="form-control"
                                               placeholder="Choose Departure Date">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-block">Search Flights</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div id="hotel-tab" class="tab-pane fade">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form role="form" action="{{route('hotel.search')}}" method="post">
                            {{csrf_field()}}
                            <div class="row">
                                <div class="col-sm-8">
                                    <div>
                                        <h4 class="form-heading">1. City Name</h4>
                                        <div class="form-group">
                                            <input type="search" name="city" placeholder="City" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <h4 class="form-heading">2. Total Guests</h4>
                                                <select class="form-control" name="guests">
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-block">Search Hotels</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection