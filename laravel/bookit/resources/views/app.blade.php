<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Flights - Worldskills Travel</title>
    <link rel="stylesheet" type="text/css" href="{{asset("assets/bootstrap/css/bootstrap.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("assets/style.css")}}">
</head>
<body>
<div class="wrapper">
    <header>
        <nav class="navbar-inverse navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main-navbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="{{route("home")}}" class="navbar-brand"><img class="logo" src="{{asset("assets/img/logo.png")}}" alt="Logo"></a>
                </div>
                <div class="collapse navbar-collapse" id="main-navbar">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="{{route("flight")}}">Flights</a></li>
                        <li><a href="{{route("hotel")}}">Hotels</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    <main>
        @yield("content")
        <br>
    </main>
    <footer>
        <div class="container">
            <p class="text-center">
                Copyright &copy; 2017 | All Right Reserved
            </p>
        </div>
    </footer>
</div>
<!--scripts-->
<script type="text/javascript" src="{{asset("assets/jquery-3.2.1.min.js")}}"></script>
<script type="text/javascript" src="{{asset("assets/bootstrap/js/bootstrap.min.js")}}"></script>

@stack('js')
@yield('js')

</body>
</html>