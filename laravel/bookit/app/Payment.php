<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = "payment";
    protected $fillable = ['first_name', 'last_name', 'email', 'telephone', 'method', 'card_number', 'card_name', 'card_ccv', 'users_id', 'title'];
}
