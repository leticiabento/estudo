<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FlightClass extends Model
{
    protected $table = "class";
    protected $fillable = ["name", "price", "flight_id"];

    public function flights() 
    {
        return $this->belongsTo("App\Flight");
    }
}
