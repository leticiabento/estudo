<?php

namespace App\Http\Controllers;

use App\City;
use App\Flight;
use App\User;
use App\UserFlight;
use App\FlightClass;
use App\Payment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FlightsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cities = City::all();
        return view("layout.flight", compact("cities"));
    }

    public function book($id)
    {
        $categorias = FlightClass::where('flight_id', $id)->get();
        $voo = Flight::where('id', $id)->get();

        return view("layout.flight-book", compact("id", "voo", "categorias"));
    }

    public function search(Request $request)
    {
        $data = $request->date_start;
        $city_start = $request->city_start;
        $city_end = $request->city_end;

        if ($city_start !== $city_end) {
            $voos = Flight::with("class")
                ->where('city_start', $city_start)
                ->where('city_end', $city_end)
                ->where('date_start', $data)
                ->paginate(3);


            $formatDataStart = date('d/m/Y', strtotime($voos[0]->date_start));
            $formatDataEnd = date('d/m/Y', strtotime($voos[0]->date_end));

            $to = Carbon::createFromFormat('H:s:i', $voos[0]->time_start);
            $from = Carbon::createFromFormat('H:s:i', $voos[0]->time_end);

            $diffInHours = $to->diffInHours($from);

            return view('layout.flight-list', compact("voos", "city_start", "city_end", "formatDataEnd", "formatDataStart", "diffInHours"));
        }

    }

    public function list()
    {
        return view("layout.flight-list");
    }

    public function payment()
    {
        return view("layout.flight-payment");
    }

    public function summary($id)
    {
        $infos = UserFlight::with('flight', 'user')
            ->where('users_id', $id)
            ->get();

        $user = User::where('id', $infos[0]->users_id)->get();
        $price = FlightClass::where('flight_id', $infos[0]->flight_id)->get();
        $payment = Payment::where('users_id', $id)->get();

        return view("layout.flight-summary", compact('infos', 'payment', 'price', 'user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function storePassanger(Request $request)
    {
        $infos = [];
        $dados = $request->all();
        $flight_id = $request->flight_id;
        unset($dados['_token']);

        foreach ($dados['title'] as $key => $value) {
            $data = [
                "title" => $value,
                "category" => $dados['category'][$key],
                "first_name" => $dados['first_name'][$key],
                "last_name" => $dados['last_name'][$key]
            ];

            array_push($infos, $data);
        }


        foreach ($infos as $informacao) {

            $inf = [
                'first_name' => $informacao['first_name'],
                'last_name' => $informacao['last_name'],
                'title' => $informacao['title'],
                'remember_token' => str_random(10)
            ];

            $user = User::create($inf);

            if (!$user) {
                return redirect('flight-book')->with("feedback_error", 'Erro ao efetuar cadastro de passageiro');
            }

            $relationship = [
                'users_id' => $user->id,
                'flight_id' => $flight_id,
                'category' => $informacao['category']
            ];


            $createRelationship = UserFlight::create($relationship);

            if (!$createRelationship) {
                return redirect('flight-book')->with("feedback_error", 'Erro ao efetuar cadastro de passageiro');
            }
        }

        $id = $user->id;
        $infosVoo = UserFlight::with('flight')
            ->where('users_id', $id)->get();


        $valorVoo = FlightClass::where('flight_id', $infosVoo[0]->flight_id)
            ->where('name', $informacao['category'])
            ->selectRaw('sum(price) as soma')
            ->get();

        $formatDataStart = date('d/m/Y', strtotime($infosVoo[0]['flight']->date_start));
        $formatDataEnd = date('d/m/Y', strtotime($infosVoo[0]['flight']->date_end));

        $formatTimeStart = explode(':', $infosVoo[0]['flight']->time_start);
        $formatTimeStart = $formatTimeStart[0] . ':' . $formatTimeStart[1];

        $formatTimeEnd = explode(':', $infosVoo[0]['flight']->time_end);
        $formatTimeEnd = $formatTimeEnd[0] . ':' . $formatTimeEnd[1];

        return view('layout.flight-payment', compact("id", "infosVoo", "valorVoo", "formatDataStart",
            "formatDataEnd", "formatTimeStart", "formatTimeEnd"));
    }

    public function storePayment(Request $request)
    {
        $infos = [
            "first_name" => $request->first_name,
            "last_name" => $request->last_name,
            "telephone" => $request->telephone,
            "email" => $request->email,
            "method" => $request->{'method'},
            "card_number" => $request->card_number,
            "card_name" => $request->card_name,
            "card_ccv" => $request->card_ccv,
            "title" => $request->title,
            "users_id" => $request->users_id
        ];

        $user = Payment::create($infos);

        if (!$user) {
            return redirect("flight-book")->with("feedback_error", "Nao foi possivel efetuar o pagamento");
        }

        $id = $request->users_id;
        return redirect("flight-summary/$id");
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
