<?php

namespace App\Http\Controllers;

use App\Hotel;
use App\Room;
use App\User;
use App\UserHotel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class HotelsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("layout.hotel");
    }

    public function book($id)
    {

        $hotel = Hotel::where('id', $id)->get();
        $soma = Hotel::where('id', $id)
            ->selectRaw('sum(price*guests) as total')
            ->get();


        return view("layout.hotel-book", compact("hotel", "soma", "id"));
    }

    public function list()
    {
        return view("layout.hotel-list");
    }

    public function payment($id)
    {
        return view("layout.hotel-payment", compact("id"));
    }

    public function summary()
    {
        return view("layout.hotel-summary");
    }

    public function search(Request $request)
    {
        $infos = Hotel::where('city', 'like', "%$request->city%")
            ->where('guests', 'like', "$request->guests")
            ->paginate(3);

        $rooms = Room::where('hotel_id', $infos[0]->id)->get();
        return view('layout.hotel-list', compact("infos", "rooms"));
    }

    public function storePassanger(Request $request)
    {
        $dados = $request->all();
        $infos = [];
        $hotel_id = $request->hotel_id;
        unset($dados['_token'], $dados['hotel_id']);

        foreach ($dados['title'] as $key => $value) {
            array_push($infos, [
                'title' => $value,
                'first_name' => $dados['first_name'][$key],
                'last_name' => $dados['last_name'][$key]
            ]);
        }

        foreach ($infos as $info) {

            $data = [
                'title' => $info['title'],
                'first_name' => $info['first_name'],
                'last_name' => $info['last_name'],
                'remember_token' => str_random(10)
            ];

            $createInfos = User::create($data);

            if (!$createInfos) {
                return redirect('hotel-book')->with('feedback_error', 'Erro ao prosseguir');
            }

            $relationship = [
                "hotel_id" => (int)$hotel_id,
                "users_id" => $createInfos->id
            ];

            $createRelationship = UserHotel::create($relationship);

            if (!$createRelationship) {
                return redirect('hotel-book')->with('feedback_error', 'Erro ao prosseguir');
            }

        }

        $user_id = $createInfos->id;

        $hotel = UserHotel::where('hotel_id', $hotel_id)
            ->where('users_id', $user_id)
            ->get();

        $myHotel = Hotel::where('id', $hotel[0]->hotel_id)->get();
        $soma = Hotel::where('id', $hotel[0]->hotel_id)
            ->selectRaw('sum(price*guests) as total')
            ->get();

        $count = count($info);

        $lastUsers = DB::table('users')
            ->orderBy('id', 'DESC')
            ->limit("$count")
            ->get();


        return redirect("hotel-payment/$user_id")
            ->with('info', $hotel)
            ->with('hotel', $myHotel)
            ->with('soma', $soma)
            ->with('users', $lastUsers);

    }

    public function storePayment(Request $request)
    {

        $data = [
            "title" => $request->title,
            "first_name" => $request->first_name,
            "last_name" => $request->last_name,
            "telephone" => $request->telephone,
            "email" => $request->email,
            "method" => $request->{'method'},
            "card_number" => $request->card_number,
            "card_name" => $request->card_name,
            "card_ccv" => $request->card_ccv
        ];

        $user = User::create($data);

        if (!$user) {
            return view('hotel-book')->with('feedback_error', 'Erro ao prosseguir');
        }

        $count = $request->count;
        $lastUsers = DB::table('users')
            ->orderBy('id', 'DESC')
            ->limit("$count")
            ->get();


        $myHotel = Hotel::where('id', $request->hotel_id)->get();
        $soma = Hotel::where('id', $request->hotel_id)
            ->selectRaw('sum(price*guests) as total')
            ->get();


        return view('layout.hotel-summary', compact("data", "lastUsers", "myHotel", "soma"));
    }

    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        //
    }


    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
