<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserFlight extends Model
{
    protected $table = "users_has_flight";
    protected $fillable = ["users_id", "flight_id", "category"];

    public function flight()
    {
        return $this->belongsTo('App\Flight');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
