<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = "users";
    protected $fillable = [
        'first_name', 'last_name', 'title', 'remember_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function flights()
    {
        return $this->belongsToMany("App\Flight", "users_has_flight", "users_id", "flight_id");
    }

    public function hotels()
    {
        return $this->belongsToMany('App\Hotel', 'hotel_has_users', 'users_id', 'hotel_id');
    }
}
