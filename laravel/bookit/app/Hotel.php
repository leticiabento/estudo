<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    protected $table = "hotel";
    protected $fillable = ["city", "guests", "description", "price", "name", "moeda"];

    public function users()
    {
        return $this->belongsToMany('App\User', 'hotel_has_users', 'hotel_id', 'users_id');
    }
}
