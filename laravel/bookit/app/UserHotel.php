<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserHotel extends Model
{
    protected $table = "hotel_has_users";
    protected $fillable = ["hotel_id", "users_id"];

    public function hotel()
    {
        return $this->belongsTo('App\Hotel');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
