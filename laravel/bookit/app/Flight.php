<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Flight extends Model
{
    protected $table = "flight";
    protected $fillable = ["city_start", "city_end", "date_start", "date_end", "description", "code", "image"];

    public function class()
    {
        return $this->hasMany('App\FlightClass', 'flight_id');
    }

    public function users() {
        return $this->belongsToMany("App\User", "users_has_flight", "flight_id", "users_id");
    }
}
