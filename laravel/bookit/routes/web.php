<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('app/layout.index');
// });

Route::get('/', "HomeController@index")->name("home");

Route::get('/hotel', "HotelsController@index")->name("hotel");
Route::get('/hotel-book/{id}', "HotelsController@book")->name("hotel.book");
Route::get('/hotel-list', "HotelsController@list")->name("list");
Route::get('/hotel-payment/{id}', "HotelsController@payment")->name("payment");
Route::get('/hotel-summary', "HotelsController@summary")->name("summary");
Route::post('/hotel-search', "HotelsController@search")->name('hotel.search');
Route::post('/hotel-passanger', "HotelsController@storePassanger")->name('hotel.passanger');
Route::post('/hotel-payment', "HotelsController@storePayment")->name('hotel.payment');

Route::get('/flight', "FlightsController@index")->name("flight");
Route::get('/flight-book/{id}', "FlightsController@book")->name("book");
Route::get('/flight-list', "FlightsController@list")->name("list");
Route::get('/flight-payment/{id}', "FlightsController@payment")->name("payment");
Route::get('/flight-summary/{id}', "FlightsController@summary")->name("summary");
Route::post('/flight-search', "FlightsController@search")->name("flight.search");
Route::post('/flight-book', 'FlightsController@storePassanger')->name("storePassanger");
Route::post('/flight-payment', "FlightsController@storePayment")->name("storePayment");
