<?php

namespace App\Http\Controllers;

use App\School;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UsersController extends Controller
{
    public function store(Request $request)
    {
        $img = $request->photo_url;
        $hashImage = $img->hashName();
        $extensao = $request->photo_url->getClientOriginalExtension();
        $extensoes = ["jpg", "jpeg", "png"];
        $size = ($request->photo_url->getClientSize()) / 1024;

        if ($request->hasFile('photo_url')) {

            if (!in_array($extensao, $extensoes)) {
                return redirect('register')->with("feedback_error", "Extensao invalida! ");
            }

            if ($size > 300) {
                return redirect('register')->with("feedback_error", "Tamanho invalido! ");
            }

            $up = $request->photo_url->store('imagens');

            if (!$up) {
                return redirect('register')->with("feedback_error", "Erro ao enviar a imagem ! ");
            }

            $user = [
                "first_name" => $request->first_name,
                "last_name" => $request->last_name,
                "nickname" => $request->nickname,
                "birthday" => $request->birthday,
                "email" => $request->email,
                "password" => bcrypt($request->password),
                "photo_url" => $hashImage,
                "short_bio" => $request->short_bio,
                "school_id" => $request->school_id,
                "school_start" => $request->school_start,
                "school_year" => $request->school_year,
                "remember_token" => str_random(10)
            ];

            $users = User::create($user);
            if ($users) {
                return redirect('/');
            }

        }
    }

    public function search(Request $request)
    {

        if ($request->search === "classmate") {
            return $this->findByClassmate($request->input);
        }

        if ($request->search === "school") {
            return $this->findBySchool($request->input);
        }

        if ($request->search === "all") {
            return $this->findByAll($request->input);
        }
    }

    public function findBySchool($input)
    {

        $user = School::with('users')
            ->where('name', 'like', "%$input%")
            ->get();

        return view('layout.searchSchool', compact("user"));
    }

    public function findByAll($input)
    {

        $school = School::where('name', 'like', "%$input%")->get();

        foreach ($school as $item) {

            $user = User::with('school')->get();

            foreach($user as $u) {

//                if($item->id === $u->school->id)
                return  array($item->id, $u->school->id);
            }
        }

        return view('layout.searchAll', compact("user"));
    }

//    public function findByClassmate($input)
//    {
//        $user = User::with("school")
//            ->where("first_name", "like", "%$input%")
//            ->get();
//
//
//
//        return $user;
//        return view('layout.searchClassmate', compact("user"));
//    }


}
