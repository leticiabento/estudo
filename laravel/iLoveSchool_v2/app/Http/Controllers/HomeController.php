<?php

namespace App\Http\Controllers;

use App\School;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $user =  Auth::user();
        $myschool =  School::where('id', $user->school_id)->get();

        $classmates = User::with('school')->where('school_id', $user->school_id)->where('school_start', $user->school_start)->where('school_year', $user->school_year)->get();

        return view('layout.index', compact("user", "myschool", "classmates"));
    }

    public function profile($id)
    {
        $user =  User::with('school')->where('id', $id)->get();
        return view('layout.profile', compact('user'));
    }

    public function search()
    {
        return view('layout.search');
    }
}
