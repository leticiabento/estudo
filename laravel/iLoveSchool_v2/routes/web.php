<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/profile/{id}', 'HomeController@profile')->name('profile');
Route::get('/search', 'HomeController@search')->name('search');

Route::post('/cadastro', 'UsersController@store')->name('cadastro');
Route::post('/search', 'UsersController@search')->name('search.store');
