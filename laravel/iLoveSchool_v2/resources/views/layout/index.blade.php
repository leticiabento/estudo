@extends("app")
@section("content")
    <div class="container">
        <div class="row">
            <div class="col-sm-4">

                <div class="panel">
                    <div class="panel-body text-center">
                        <img src="{{asset('app/imagens/')}}/{{$user->photo_url}}" alt=""
                             class="img-responsive img-circle">
                        <h2>{{$user->first_name}} {{$user->last_name}}
                            <small>{{$user->nickname}}</small>
                        </h2>
                        <a href="mailto:{{$user->email}}">{{$user->email}}</a>
                        <br/>
                        <span>23 years old.</span>
                        <br/>
                        <br/>
                        <p>{{$user->short_bio}}</p>
                        <br/>
                        <strong>School</strong>
                        <br/>
                        <span>{{$myschool[0]['name']}}</span>
                        <br/>
                        <span>{{$user->school_start}} - {{$user->school_year}}</span>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <h2>Classmates</h2>
                <div class="row">
                    @foreach($classmates as $class)
                        <div class="col-sm-4">
                            <div class="panel-body text-center">
                                <a href="{{route('profile', $class->id)}}">
                                    <img src="{{asset('app/imagens/')}}/{{$class->photo_url}}" alt="" class="img-responsive img-circle">
                                    <h5>{{$class->first_name}} {{$class->last_name}}</h5>
                                </a>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>
@endsection