@extends("app")
@section("content")

    <div class="container">
        <div class="panel">
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-2">
                        <img src="{{asset('app/imagens')}}/{{$user[0]['photo_url']}}" alt="" class="img-responsive img-circle" />
                    </div>
                    <div class="col-sm-10">
                        <h2>{{$user[0]['first_name']}} {{$user[0]['last_name']}} <small>{{$user[0]['nickname']}}</small></h2>
                        <span>23 years old.</span>
                        <br />
                        <a href="mailto:{{$user[0]['email']}}">{{$user[0]['email']}}</a>
                        <br />
                        <br />
                        <strong>School</strong>
                        <br />
                        <span>{{$user[0]['school']['name']}}</span>
                        <br />
                        <span>{{$user[0]['school_start']}} - {{$user[0]['school_year']}}</span>
                    </div>
                </div>
                <hr />

                <div class="row">
                    <div class="col-sm-12">
                        <p>"{{$user[0]['short_bio']}}"</p>
                    </div>
                </div>

            </div>
        </div>

    </div>

@endsection