@extends("app")
@section("content")
    <div class="container">
        <div class="row">
            <div class="col-sm-12">

                @foreach($user as $u)



                    <div class="row">
                        <h2>{{$u->name}}</h2>
                        @foreach($u->users as $user)

                            <div class="col-sm-2">
                                <div class="panel-body text-center">
                                    <a href="{{route('profile', $user->id)}}">
                                        <img src="{{asset('app/imagens')}}/{{$user->photo_url}}" alt="" class="img-responsive img-circle">
                                        <h5>{{$user->first_name}} {{$user->last_name}}</h5>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endforeach


            </div>
            <!--col-sm-12-->
        </div>
        <!--row-->
    </div>

@endsection