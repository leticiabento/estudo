@extends('app')

@section('content')
    <div class="container">
        <div class="page-header">
            <h1>Sign up</h1>
        </div>

        @include("layout._feedback")

        <h2>
            <small>Personal Info</small>
        </h2>

        <form method="post" action="{{route('cadastro')}}" enctype="multipart/form-data">
            {{csrf_field()}}

            <div class="form-group">
                <input type="text" class="form-control" name="first_name" size="30" id="first_name"
                       placeholder="Fist name">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="last_name" size="30" id="last_name"
                       placeholder="Last name">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="nickname" size="30" id="nickname"
                       placeholder="Nickname (optional)">
                <p class="help-block">How you used to go by at school (e.g. a nickname)</p>
            </div>
            <div class="form-group">
                <input type="date" name="birthday" class="form-control" size="30" id="dob" placeholder="Date of Birth">
            </div>
            <div class="form-group">
                <input type="email" name="email" class="form-control" id="email" placeholder="Email address">
            </div>
            <div class="form-group">
                <input type="password" name="password" class="form-control" id="password" placeholder="Password">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" id="password" placeholder="Repeat Password">
            </div>
            <div class="form-group">
                <label for="exampleInputFile">Profile Picture</label>
                <input type="file" name="photo_url" id="exampleInputFile">
                <p class="help-block">Upload your profile photo. JPG, JPEG, or PNG smaller than 300KB.</p>
            </div>
            <div class="form-group">
                <textarea name="short_bio" placeholder="Short Bio (optional)" class="form-control" rows="3"></textarea>
            </div>
            <h2>
                <small>School</small>
            </h2>
            <div class="form-group">
                <select id="school1" name="school_id">
                    <option value="">Select a school</option>
                    @foreach($schools as $school)
                        <option value="{{$school->id}}">{{$school->name}}</option>
                    @endforeach
                </select>
                From
                <select id="school1" name="school_start">
                    <option value="">Select year</option>
                    <option value="2000">2000</option>
                    <option value="2001">2001</option>
                    <option value="2002">2002</option>
                </select>
                To
                <select id="school1" name="school_year">
                    <option value="">Select year</option>
                    <option value="2005">2005</option>
                    <option value="2006">2006</option>
                    <option value="2007">2007</option>
                </select>
            </div>
            <button type="submit" class="btn btn-primary btn-lg">Create account</button>
        </form>
    </div>

    {{--<div class="container">--}}
    {{--<div class="row">--}}
    {{--<div class="col-md-8 col-md-offset-2">--}}
    {{--<div class="panel panel-default">--}}
    {{--<div class="panel-heading">Register</div>--}}

    {{--<div class="panel-body">--}}
    {{--<form class="form-horizontal" method="POST" action="{{ route('register') }}">--}}
    {{--{{ csrf_field() }}--}}

    {{--<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">--}}
    {{--<label for="name" class="col-md-4 control-label">Name</label>--}}

    {{--<div class="col-md-6">--}}
    {{--<input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>--}}

    {{--@if ($errors->has('name'))--}}
    {{--<span class="help-block">--}}
    {{--<strong>{{ $errors->first('name') }}</strong>--}}
    {{--</span>--}}
    {{--@endif--}}
    {{--</div>--}}
    {{--</div>--}}

    {{--<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">--}}
    {{--<label for="email" class="col-md-4 control-label">E-Mail Address</label>--}}

    {{--<div class="col-md-6">--}}
    {{--<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>--}}

    {{--@if ($errors->has('email'))--}}
    {{--<span class="help-block">--}}
    {{--<strong>{{ $errors->first('email') }}</strong>--}}
    {{--</span>--}}
    {{--@endif--}}
    {{--</div>--}}
    {{--</div>--}}

    {{--<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">--}}
    {{--<label for="password" class="col-md-4 control-label">Password</label>--}}

    {{--<div class="col-md-6">--}}
    {{--<input id="password" type="password" class="form-control" name="password" required>--}}

    {{--@if ($errors->has('password'))--}}
    {{--<span class="help-block">--}}
    {{--<strong>{{ $errors->first('password') }}</strong>--}}
    {{--</span>--}}
    {{--@endif--}}
    {{--</div>--}}
    {{--</div>--}}

    {{--<div class="form-group">--}}
    {{--<label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>--}}

    {{--<div class="col-md-6">--}}
    {{--<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>--}}
    {{--</div>--}}
    {{--</div>--}}

    {{--<div class="form-group">--}}
    {{--<div class="col-md-6 col-md-offset-4">--}}
    {{--<button type="submit" class="btn btn-primary">--}}
    {{--Register--}}
    {{--</button>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</form>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
@endsection
