@extends('app')

@section('content')

    <div class="container text-center">
        <div class="page-header">
            <h1>First, log in.</h1>
        </div>

        <form method="post" action="{{route('login')}}">

            {{csrf_field()}}

            <div class="form-group">
                <input type="email" name="email" class="form-control" id="email" placeholder="Email address">
            </div>
            <div class="form-group">
                <input type="password" name="password" class="form-control" id="password" placeholder="Password">
            </div>
            <br/>
            <br/>
            <button type="submit" class="btn btn-primary btn-lg">Sign in</button>
            &nbsp;
            <a href="{{route('register')}}">Don't have an account?</a>
        </form>
    </div>

@endsection
