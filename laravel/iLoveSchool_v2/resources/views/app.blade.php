<!doctype html>
<html class="no-js">

<head>
    <meta charset="utf-8">
    <title>I
        <3 School</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="{{asset("lib/css/bootstrap.min.css")}}">
    {{--    <link rel="stylesheet" href="{{asset("styles/main.css"}}">--}}
</head>

<body>

<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{route('home')}}">I <i class="glyphicon glyphicon-heart"></i> School</a>
        </div>


        <form class="navbar-form navbar-left" role="search" method="post" action="{{route('search.store')}}">
            {{csrf_field()}}

            <select name="search" class="form-control">
                <option value="all">All</option>
                <option value="school">School</option>
                <option value="classmate">Classmate</option>
            </select>

            &nbsp;

            <div class="form-group">
                <input type="text" name="input" class="form-control" size="65" placeholder="Search">
            </div>

            <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
        </form>

        <div class="nav navbar-nav navbar-right">


            @guest
                <li><a href="{{ route('login') }}">Login</a></li>
                <li><a href="{{ route('register') }}">Register</a></li>
            @else
                <li>
                    <a href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        Logout
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            @endguest
        </div>
    </div>
</nav>

@yield("content")


<script src="{{asset("lib/js/jquery-1.11.2.min.js")}}"></script>
<script src="{{asset("lib/js/bootstrap.min.js")}}"></script>
<script src="{{asset("scripts/main.js")}}"></script>
</body>

</html>