<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    protected $table = "areas";
    public $timestamps = false; //smp publico

    //n preciso de fillable pq n tenho uma gerencia(controller)

    public function users()
    {
        return $this->belongsToMany("App\User", "interests", "area_id", "user_id");
    }

    public function opportunities()
    {
        return $this->belongsToMany("App\Opportunity", "areas_opportunities", "area_id", "opportunities_id");
    }

    public function interests()
    {
        return $this->hasMany('App\Interests');
    }

}
