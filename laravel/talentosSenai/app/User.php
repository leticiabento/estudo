<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = "users";
    protected $fillable = [
        'name', 'email', 'login', 'password', 'role', 'remember_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function areas()
    {
        return $this->belongsToMany("App\Area", "interests", "user_id", "area_id");
        //model q eu quero me relacionar, a tabela "pivo" - do relacionamento, a chave estrangeira do model q eu to, e a chave estrangeira do model q eu quero me relacionar
    }

    public function courses()
    {
        return $this->hasMany("App\Course", "users_id");
    }

    public function opportunities()
    {
        return $this->hasMany("App\Opportunity");
    }

    public function interests()
    {
        return $this->hasMany('App\Interest');
    }
}
