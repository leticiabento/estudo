<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Opportunity extends Model
{
    protected $table = "opportunities";
    protected $fillable = ['title', 'description', 'type', 'quantity', 'start', 'finish', 'users_id'];

    public function areas()
    {
        return $this->belongsToMany("App\Area", "areas_opportunities", "opportunities_id", "area_id");
    }

    public function user()
    {
        return $this->belongsTo("App\User", "users_id"); //mostra quem é a chave estrangeira
        //se n colocar, dá que o usuario é undefined na funcao findByTitle
    }

    public function opportunities()
    {
        return $this->belongsToMany('App\AreaOpportunity', "areas_opportunities", "opportunities_id", "area_id");
    }


}
