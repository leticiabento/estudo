<?php

namespace App\Http\Controllers;

use App\Opportunity;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $professionals = User::where('role', "profissional")->count(); //where smp antes do count (o count finaliza)
        $opportunites = Opportunity::count();
        return view('layouts.index', compact("professionals", "opportunites"));
    }

    public function login()
    {

        return 'oi';

        return view("layouts.login");
    }

}
