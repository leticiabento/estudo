<?php

namespace App\Http\Controllers;

use App\Area;
use App\AreaOpportunity;
use App\Opportunity;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OportunitesController extends Controller
{
    private $opportunity;

    public function __construct(Opportunity $opportunity)
    {
        $this->opportunity = $opportunity;
    }

    public function index(Request $request)
    {

        $dateNow = date("Y-m-d");
        $opportunities = [];

        if ($request->title) {
            return $this->findByTitle($request->title, $dateNow);
        }

        if ($request->type) {
            return $this->findByType($request->type, $dateNow);
        }

        if ($request->area) {
            $areas = explode(",", $request->area); //ele vem virgulas

            if (count($areas) == 1) {
                return $this->findByArea($request->area, $dateNow);
            }

            return $this->findByAreas($areas, $dateNow);

        }


//        return $request->all();
//        $title = $request->title;
//        $type = $request->type;
//        $areas = $request->area;


//        $id = $this->opportunity->find(3);
//        return $id->areas;

//        return DB::table("opportunities")
//            ->select("opportunities.title", "opportunities.description", "opportunities.type", "opportunities.quantity", "opportunities.start", "opportunities.finish", "opportunities.created_at", "users.name", "areas_opportunities.area_id", "areas.area")
//            ->join("users", "users.id", "opportunities.users_id")
//            ->join("areas_opportunities", "areas_opportunities.opportunities_id", "opportunities.id")
//            ->join("areas", "areas.id", "areas_opportunities.area_id")
//            ->where("start", "<=", $dateNow)
//            ->where("finish", ">=", $dateNow)
//            ->get();
    }

    private function findByTitle($title, $dateNow)
    {
        return $this->opportunity->with("areas", "user")//meus relacionamentos no model Opportunity
        ->where("start", "<=", $dateNow)
            ->where("finish", ">=", $dateNow)
            ->where("title", "like", "$title%")//tudo do inicio
            ->orderBy("created_at", "desc")
            ->get();
    }

    private function findByType($type, $dateNow)
    {
        return $this->opportunity->with("areas", "user")
            ->where("start", "<=", $dateNow)
            ->where("finish", ">=", $dateNow)
            ->where("type", $type)
            ->get();
    }

    private function findByArea($area, $dateNow)
    {
        $index = 0;
        $data = [];

         $opportunities = Area::with("opportunities")
            ->where("id", $area)
            ->get(); //trago uma vaga somente, e dentro da area eu trago as oportunidades relacionadas a vaga

        foreach ($opportunities[0]->opportunities as $row) {

            if ($row->start <= $dateNow && $row->finish >= $dateNow) {

                $user = DB::table('users')
                    ->select('name', 'email', 'login')
                    ->where('id', $row->users_id)
                    ->get();

                //faço um array push em user, no cara q to passando no momento
                $row['user'] = $user[0];
                array_push($data, $row); //dou um array em data, da linha q ta interando no momento
            }

            //montou o usuario, mas n montou a area de atuacao
            $data[$index]->areas = [
                0 => (object)['areas' => $opportunities[0]->area] //isso ta vindo da pesquisa ali de cima
            ];

            $index++;
        }

        return $data;
    }

    private function findByAreas($areas, $dateNow)
    {
        $index = 0;
        $data = [];

        $opportunities = AreaOpportunity::with("opportunities", "areas")
            ->distinct()
            ->whereIn("id", $areas)
            ->get();


        foreach ($opportunities as $row) {

            $opportunity = $row->opportunities;
            $areas = [
                ["areas" => $row->areas->area]
            ];

            array_push($data, $opportunity);
            $data[0]["areas"] = $areas;

            if ($row->opportunities->start <= $dateNow && $row->opportunities->finish >= $dateNow) {
                $user = DB::table('users')
                    ->select('users.name', 'users.email', 'users.login')
                    ->where('id', $row->opportunities->users_id)
                    ->get();

                $data[0]["user"] = $user[0];
            }

            $index++;
        }
        return $data;


    }

    public function search()
    {

        $areas1 = DB::table('areas')
            ->offset(0)
            ->limit(7)
            ->orderBy('id')
            ->get();

        $areas2 = DB::table('areas')
            ->offset(7)
            ->limit(7)
            ->orderBy('id')
            ->get();

        $areas3 = DB::table('areas')
            ->offset(14)
            ->limit(7)
            ->orderBy('id')
            ->get();

        $areas4 = DB::table('areas')
            ->offset(21)
            ->limit(7)
            ->orderBy('id')
            ->get();

        return view("layouts.oportunidade_pesquisa", compact("areas1", "areas2", "areas3", "areas4"));
    }

    public function create()
    {

        $areas1 = DB::table('areas')
            ->offset(0)
            ->limit(7)
            ->orderBy('id')
            ->get();

        $areas2 = DB::table('areas')
            ->offset(7)
            ->limit(7)
            ->orderBy('id')
            ->get();

        $areas3 = DB::table('areas')
            ->offset(14)
            ->limit(7)
            ->orderBy('id')
            ->get();

        $areas4 = DB::table('areas')
            ->offset(21)
            ->limit(7)
            ->orderBy('id')
            ->get();

        return view("layouts.oportunidade_cadastro", compact("areas1", "areas2", "areas3", "areas4"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $user = [
            "role" => $request->role,
            "name" => $request->name,
            "email" => $request->email,
            "login" => $request->login,
            "password" => bcrypt($request->password),
            "remember_token" => str_random(10)
        ];

        $createUser = User::updateOrCreate(
            ["email" => $request->email], $user
        );

        $users_id = $createUser->id;
        //user só é gerado depois do updateOrCreate

        $requestStart = str_replace("/", "-", $request->start);
        $requestFinish = str_replace("/", "-", $request->finish);

        $dateStart = new \DateTime($requestStart);
        $start = $dateStart->format("Y-m-d");

        $dateFinish = new \DateTime($requestFinish);
        $finish = $dateFinish->format("Y-m-d");

        $opportunity = [
            "title" => $request->title,
            "description" => $request->description,
            "type" => $request->type,
            "quantity" => $request->quantity,
            "start" => $start,
            "finish" => $finish,
            "users_id" => $users_id
        ];

        $createOpportunity = Opportunity::updateOrCreate(
            ["title" => $request->title], $opportunity
        );


        $area = $request->area;

        $createArea = $createOpportunity->opportunities()->sync($area);

        if ($createUser && $createOpportunity && $createArea) {
            return redirect("oportunidade/cadastro")->with("feedback_ok", "Oportunidade cadastrada com sucesso");
        }

        return redirect("oportunidade/cadastro")->with("feedback_error", "Oportunidade não cadastrada. Tente novamente");


    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function restrita()
    {
        return view('layouts.restrita_empresa');
    }
}
