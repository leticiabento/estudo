<?php

namespace App\Http\Controllers;

use App\Area;
use App\Course;
use App\Interest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TalentsController extends Controller
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function index(Request $request)
    {

        if ($request->nome) {
            return $this->findByName($request->nome, $request->order);
        }

//        if ($request->order) {
//            return $this->findByOrder($request->order);
//        }

        if ($request->area) {
            $areas = explode(',', $request->area);

            if (count($areas) == 1) {
                return $this->findByArea($request->area);
            }

            return $this->findByAreas($request->area);
        }
    }

    private function findByName($nome, $order)
    {
        $data = [];

        if ($order == 'name') {
            $ordenacao = "ASC";
        } else if ($order == 'created_at') {
            $ordenacao = "DESC";
        } else {
            $order = 'id';
            $ordenacao = "ASC";
        }


        $user = $this->user->with("areas")
            ->where('role', "profissional")
            ->where("name", "like", "%$nome%")
            ->orderBy($order, "$ordenacao")
            ->get();


        foreach ($user as $value) {
            $courses = Course::where("users_id", $value->id)->get();

            foreach ($courses as $value1) {
                $value1['user'] = $value;
                array_push($data, $value1);
            }

        }

        return $data;
    }

//    private function findByOrder($order)
//    {
//        $data = [];
//        $user = $this->user->with("areas")
//            ->where('role', 'profissional')
//            ->orderBy($order, 'ASC')
//            ->get();
//
//        foreach ($user as $value) {
//            $courses = Course::where('users_id', $value->id)->get();
//
//            foreach ($courses as $value1) {
//                $value1['user'] = $value;
//                array_push($data, $value1);
//
//            }
//        }
//
//        return $data;
//    }

    private function findByArea($area)
    {
        $index = 0;
        $data = [];

        $interests = DB::table('interests')
            ->select('users.id as userID', 'users.name', 'users.email', 'users.login', 'users.created_at as createdUser', 'areas.id as areaID', 'areas.area', 'courses.id as courseID', 'courses.course', 'courses.finisher', 'courses.country', 'courses.users_id as users_id')
            ->join('users', 'users.id', 'interests.user_id')
            ->join('areas', 'areas.id', 'interests.area_id')
            ->join('courses', 'courses.users_id', 'users.id')
            ->where('area_id', $area)
            ->get();


        foreach ($interests as $interest) {
            $int = [
                'id' => $interest->{'courseID'},
                'course' => $interest->{'course'},
                'finisher' => $interest->{'finisher'},
                'country' => $interest->{'country'},
                'users_id' => $interest->{'users_id'},
                'user' => (object)[
                    'id' => $interest->{'userID'},
                    'name' => $interest->{'name'},
                    'email' => $interest->{'email'},
                    'login' => $interest->{'login'},
                    'created_at' => $interest->{'createdUser'},
                    'areas' => (object)[
                        "$index" => [
                            'id' => $interest->{'areaID'},
                            'area' => $interest->{'area'}
                        ]
                    ]
                ]

            ];

            array_push($data, $int);
            $index++;
        }

        return $data;
    }

    private function findByAreas($areas)
    {

        $index = 0;
        $data = [];
        $areas = explode(",", $areas);

        $minhasAreas = Interest::with("areas", "users")
            ->whereIn('area_id', $areas)
            ->get();

        foreach ($minhasAreas as $area) {
            $courses = Course::where('users_id', $area->users->id)->get();

            foreach ($courses as $course) {

                $int = [
                    'id' => $course->{'id'},
                    'course' => $course->{'course'},
                    'finisher' => $course->{'finisher'},
                    'country' => $course->{'country'},
                    'users_id' => $course->{'users_id'},
                    'user' => (object)[
                        'id' => $area->users->id,
                        'name' => $area->users->{'name'},
                        'email' => $area->users->{'email'},
                        'login' => $area->users->{'login'},
                        'created_at' => $area->users->{'createdUser'},
                        'areas' => (object)[
                            "$index" => [
                                'id' => $area->areas{'id'},
                                'area' => $area->areas{'area'}
                            ]
                        ]
                    ]

                ];
                $index++;
                array_push($data, $int);
            }

        }

        return $data;

    }

    public function search()
    {
        $areas1 = DB::table('areas')
            ->offset(0)
            ->limit(7)
            ->orderBy('id')
            ->get();

        $areas2 = DB::table('areas')
            ->offset(7)
            ->limit(7)
            ->orderBy('id')
            ->get();

        $areas3 = DB::table('areas')
            ->offset(14)
            ->limit(7)
            ->orderBy('id')
            ->get();

        $areas4 = DB::table('areas')
            ->offset(21)
            ->limit(7)
            ->orderBy('id')
            ->get();

        return view("layouts.talento_pesquisa", compact("countries", "areas1", "areas2", "areas3", "areas4"));
    }

    public function create()
    {
//        $countries = Country::all();
        $countries = DB::table("countries")->orderBy('name', "ASC")->get();
        $areas1 = DB::table('areas')
            ->offset(0)
            ->limit(7)
            ->orderBy('id')
            ->get();

        $areas2 = DB::table('areas')
            ->offset(7)
            ->limit(7)
            ->orderBy('id')
            ->get();

        $areas3 = DB::table('areas')
            ->offset(14)
            ->limit(7)
            ->orderBy('id')
            ->get();

        $areas4 = DB::table('areas')
            ->offset(21)
            ->limit(7)
            ->orderBy('id')
            ->get();


        return view("layouts.talento_cadastro", compact("countries", "areas1", "areas2", "areas3", "areas4"));


    }

    public function store(Request $request)
    {

//        $user = $request->all(); //user vira um array. Cuidado!!!

        $user = [
            "name" => $request->name,
            "email" => $request->email,
            "login" => $request->login,
            "password" => bcrypt($request->password),
            "role" => $request->role,
            "remember_token" => str_random(10)
        ];

        $createUser = $this->user->updateOrCreate(
            ['email' => $request->email], $user
        ); //primeiro paramento, envia o que vc quer dar comparar se existe, se existir, update, se nao, create

        $user_id = $createUser->id;

//        $findUser = DB::table('users')
//            ->where('email', $user['email'])
//            ->get();
//
//        $id = $findUser[0]->id;
//
//        if(count($findUser) == 0) {
//            $createUser = $this->user->create($user);
//        } else {
//            $createUser = $this->user->find($id)
//                ->fill($user);
//            $createUser->update();
//        }
        $course = [
            "course" => $request->course,
            "finisher" => $request->finisher,
            "country" => $request->country,
            "users_id" => $user_id
        ];

        $createCourse = Course::updateOrCreate(
            ['users_id' => $user_id, 'course' => $request->course], $course
        );

        $interests = $request->area;
        $createInterest = $createUser->areas()->sync($interests);
        //pego minha instancia do objeto, o metodo responsavel no Model, e depois gravo o relacionamento c attach


        if ($createUser && $createCourse && $createInterest) {
            return redirect('talento/cadastro')->with('feedback_ok', "Cadastrado com sucesso");
        }

        return redirect('talento/cadastro')->with('feedback_error', "Não cadastrado. Tente novamente");

        //limpo o array
//        unset($user['area'], $user['course'], $user['finisher'], $user['country_id'], $user['user_id'], $user['_token']);

//        $user['password'] = bcrypt($user['password']);
//        $user['remember_token'] = str_random(10);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }

    public function restrita()
    {
        return view('layouts.restrita_profissional');
    }
}
