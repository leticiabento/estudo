<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $table = "courses";
    protected $fillable = ['course', 'finisher', 'users_id', 'country'];

//    public function country()
//    {
//        return $this->belongsTo("App\Country");
//        // 1 p 1
//    }

    public function user()
    {
        return $this->belongsTo("App\User");
    }
}
