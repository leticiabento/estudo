<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = "countries";
    public $timestamps = false;

    public function courses()
    {
        return $this->hasMany("App\Course");
        //1 p muitos
    }
}
