<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Interest extends Model
{
    protected $table = "interests";
    public $timestamps = false;

    public function areas()
    {
        return $this->belongsTo('App\Area', 'area_id');
    }

    public function users()
    {
        return $this->belongsTo('App\User', 'user_id');
    }


}
