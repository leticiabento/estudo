<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AreaOpportunity extends Model
{
    protected $table = "areas_opportunities";
    public $timestamps = false;

    public function opportunities()
    {
        return $this->belongsTo("App\Opportunity", "opportunities_id");
    }

    public function areas()
    {
        return $this->belongsTo("App\Area", "area_id");
    }
}
