<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => "Letícia Bento",
                'email' => "let@let.com",
                'login' => 'let',
                'password' => bcrypt(123456),
                'role' => "profissional",
                'remember_token' => str_random(10)
            ],
            [
                'name' => "Hospital Badin",
                'email' => "sat@badin.com.br",
                'login' => 'badin',
                'password' => bcrypt(123456),
                'role' => "empresa",
                'remember_token' => str_random(10)
            ],
            [
                'name' => "Super Prix",
                'email' => "sac@superprix.com.br",
                'login' => 'superprix',
                'password' => bcrypt(123456),
                'role' => "empresa",
                'remember_token' => str_random(10)
            ]
        ];

        DB::table('users')->insert($data);
    }
}
