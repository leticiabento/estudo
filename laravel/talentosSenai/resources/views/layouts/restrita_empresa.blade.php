@extends("layouts.app")
@section('content')
    <div class="tm-section topoInternaEmpresa">
        <div class="uk-container uk-container-center uk-text-center">
            <h1 class="tm-margin-large-bottom"><i class="uk-icon-bank uk-icon-medium"></i> Minhas
                <strong>Oportunidades</strong></h1>
        </div>
    </div>

    <div class="tm-section opcoesInterno">
        <div class="uk-container uk-container-center">

            <a name="resultadoBusca"></a>

            <div class="uk-form">

                <fieldset class="uk-margin">
                    <legend>Oportunidades Registradas</legend>
                    <div class="novaOportunidade"><a href="oportunidade_cadastro.html"
                                                     title="Registrar Nova Oportunidade"><i
                                    class="uk-icon-plus-square-o uk-icon-small uk-icon-button"></i></a></div>
                </fieldset>

                <!-- NAO ENCONTRA RESULTADOS -->
                <div class="uk-grid">
                    <div class="uk-width-medium-1-1">
                        <div class="uk-panel oportunidade uk-text-center">
                            <p>Não foram encontradas oportunidades registradas. <a href="oportunidade_cadastro.html">Registre
                                    uma oportunidade</a>.</p>
                        </div>
                    </div>
                </div>
                <!-- NAO ENCONTRA RESULTADOS FIM -->

                <!--ENCONTRA RESULTADOS-->
                <div class="uk-grid">
                    <div class="uk-width-medium-1-1">
                        <div class="uk-panel oportunidade">
                            <div class="uk-panel-badge uk-badge">Registrado em 22/07/2016 19:00</div>
                            <h3 class="uk-panel_title tituloOportunidade uk-text-bold">REPOSITOR DE ESTOQUE</h3>
                            Empresa: Plinicar distribuidora de alimentos LTDA.
                            <br/>
                            E-mail da Empresa: <a
                                    href="mailto:recutamento@plinicar.com.br">recutamento@plinicar.com.br</a>
                            <br/>
                            Usuário: plinicar_BR
                            <br/>
                            Descrição da Oportunidade: Trabalhar em ambiente cordial com atitudes para trabalho em
                            equipe. Necessário estar cursando ensino médio.
                            <br/>
                            <div class="uk-grid">
                                <div class="uk-width-medium-1-2">
                                    Tipo da Oportunidade: Emprego
                                </div>
                                <div class="uk-width-medium-1-2">
                                    Quantidade de Vagas: 05
                                </div>
                            </div>
                            <div class="uk-grid">
                                <div class="uk-width-medium-1-2">
                                    Data de Início: 20/07/2016
                                </div>
                                <div class="uk-width-medium-1-2">
                                    Data de Término: 30/07/2016
                                </div>
                            </div>
                            Áreas de Atuação: Logística, Gestão

                            <div class="uk-form">

                                <fieldset class="uk-margin">
                                    <legend>Profissionais Interessados</legend>
                                    <div class="novaOportunidade">
                                        <a href="#" title="Ocultar Lista"><i
                                                    class="uk-icon-minus-square-o uk-icon-small"></i></a>
                                    </div>

                                    <div class="uk-panel profissional">
                                        <div class="uk-panel-badge uk-badge">Registrado em 22/07/2016 19:00</div>
                                        <h3 class="uk-panel_title nomeTalento">ALISSON DE OLIVEIRA SILVA SANTOS</h3>
                                        E-mail: <a
                                                href="mailto:alissonosilva@panik.com.br">alissonosilva@panik.com.br</a>
                                        <br/>
                                        Usuário: nelson
                                        <br/>
                                        Curso Realizado: Tortas e confeitaria básica
                                        <br/>
                                        <div class="uk-grid">
                                            <div class="uk-width-medium-1-2">
                                                Estado: Amapá
                                            </div>
                                            <div class="uk-width-medium-1-2">
                                                Data de Conclusão: Não Informado
                                            </div>
                                        </div>
                                        Áreas de Atuação: Alimentos e bebidas, Automotiva, Polímeros, Química
                                    </div>

                                    <div class="uk-panel profissional">
                                        <div class="uk-panel-badge uk-badge">Registrado em 22/07/2016 20:05</div>
                                        <h3 class="uk-panel_title nomeTalento">ALISSON DE OLIVEIRA SILVA SANTOS</h3>
                                        E-mail: <a
                                                href="mailto:alissonosilva@panik.com.br">alissonosilva@panik.com.br</a>
                                        <br/>
                                        Usuário: nelson
                                        <br/>
                                        Curso Realizado: Tortas e confeitaria básica
                                        <br/>
                                        <div class="uk-grid">
                                            <div class="uk-width-medium-1-2">
                                                Estado: Amapá
                                            </div>
                                            <div class="uk-width-medium-1-2">
                                                Data de Conclusão: Não Informado
                                            </div>
                                        </div>
                                        Áreas de Atuação: Alimentos e bebidas, Automotiva, Polímeros, Química
                                    </div>
                                </fieldset>
                            </div>

                            <div class="uk-text-center">
                                <button class="uk-button uk-button-large uk-button-danger interesse bt-cadastrar">
                                    Excluir esta Oportunidade
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="uk-grid">
                    <div class="uk-width-medium-1-1">
                        <div class="uk-panel oportunidade">
                            <div class="uk-panel-badge uk-badge">Registrado em 22/07/2016 10:00</div>
                            <h3 class="uk-panel_title tituloOportunidade  uk-text-bold">ATENDENTE DE LOJA</h3>
                            Empresa: Plinicar distribuidora de alimentos LTDA.
                            <br/>
                            E-mail da Empresa: <a
                                    href="mailto:recutamento@plinicar.com.br">recutamento@plinicar.com.br</a>
                            <br/>
                            Usuário: plinicar_BR
                            <br/>
                            Descrição da Oportunidade: Atendimento ao cliente.
                            <br/>
                            <div class="uk-grid">
                                <div class="uk-width-medium-1-2">
                                    Tipo da Oportunidade: Emprego
                                </div>
                                <div class="uk-width-medium-1-2">
                                    Quantidade de Vagas: 03
                                </div>
                            </div>
                            <div class="uk-grid">
                                <div class="uk-width-medium-1-2">
                                    Data de Início: 22/07/2016
                                </div>
                                <div class="uk-width-medium-1-2">
                                    Data de Término: 15/08/2016
                                </div>
                            </div>
                            Áreas de Atuação: Tecnologia da informação, Gestão
                            <div class="uk-text-center">
                                <button class="uk-button uk-button-large uk-button-danger interesse bt-cadastrar">
                                    Excluir esta Oportunidade
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="uk-grid">
                    <div class="uk-width-medium-1-1">
                        <div class="uk-panel oportunidade">
                            <div class="uk-panel-badge uk-badge">Registrado em 21/07/2016 15:00</div>
                            <h3 class="uk-panel_title tituloOportunidade uk-text-bold">EMPACOTADOR</h3>
                            Empresa: Plinicar distribuidora de alimentos LTDA.
                            <br/>
                            E-mail da Empresa: <a
                                    href="mailto:recutamento@plinicar.com.br">recutamento@plinicar.com.br</a>
                            <br/>
                            Usuário: plinicar_BR
                            <br/>
                            Descrição da Oportunidade: Trabalhar como empacotador de produtos em período integral.
                            <br/>
                            <div class="uk-grid">
                                <div class="uk-width-medium-1-2">
                                    Tipo da Oportunidade: Emprego
                                </div>
                                <div class="uk-width-medium-1-2">
                                    Quantidade de Vagas: 20
                                </div>
                            </div>
                            <div class="uk-grid">
                                <div class="uk-width-medium-1-2">
                                    Data de Início: 20/07/2016
                                </div>
                                <div class="uk-width-medium-1-2">
                                    Data de Término: 30/08/2016
                                </div>
                            </div>
                            Áreas de Atuação: Tecnologia da Informação

                            <div class="uk-form">

                                <fieldset class="uk-margin">
                                    <legend>Profissionais Interessados</legend>
                                    <div class="novaOportunidade">
                                        <a href="#" title="Ocultar Lista"><i
                                                    class="uk-icon-minus-square-o uk-icon-small"></i></a>
                                    </div>

                                    <div class="uk-panel profissional">
                                        <div class="uk-panel-badge uk-badge">Registrado em 22/07/2016 19:00</div>
                                        <h3 class="uk-panel_title nomeTalento">ALISSON DE OLIVEIRA SILVA SANTOS</h3>
                                        E-mail: <a
                                                href="mailto:alissonosilva@panik.com.br">alissonosilva@panik.com.br</a>
                                        <br/>
                                        Usuário: nelson
                                        <br/>
                                        Curso Realizado: Tortas e confeitaria básica
                                        <br/>
                                        <div class="uk-grid">
                                            <div class="uk-width-medium-1-2">
                                                Estado: Amapá
                                            </div>
                                            <div class="uk-width-medium-1-2">
                                                Data de Conclusão: Não Informado
                                            </div>
                                        </div>
                                        Áreas de Atuação: Alimentos e bebidas, Automotiva, Polímeros, Química
                                    </div>

                                    <div class="uk-panel profissional">
                                        <div class="uk-panel-badge uk-badge">Registrado em 22/07/2016 20:05</div>
                                        <h3 class="uk-panel_title nomeTalento">ALISSON DE OLIVEIRA SILVA SANTOS</h3>
                                        E-mail: <a
                                                href="mailto:alissonosilva@panik.com.br">alissonosilva@panik.com.br</a>
                                        <br/>
                                        Usuário: nelson
                                        <br/>
                                        Curso Realizado: Tortas e confeitaria básica
                                        <br/>
                                        <div class="uk-grid">
                                            <div class="uk-width-medium-1-2">
                                                Estado: Amapá
                                            </div>
                                            <div class="uk-width-medium-1-2">
                                                Data de Conclusão: Não Informado
                                            </div>
                                        </div>
                                        Áreas de Atuação: Alimentos e bebidas, Automotiva, Polímeros, Química
                                    </div>
                                </fieldset>
                            </div>

                            <div class="uk-text-center">
                                <button class="uk-button uk-button-large uk-button-danger interesse bt-cadastrar">
                                    Excluir esta Oportunidade
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <!--ENCONTRA RESULTADOS FIM-->

            </div>

        </div>
    </div>
@endsection