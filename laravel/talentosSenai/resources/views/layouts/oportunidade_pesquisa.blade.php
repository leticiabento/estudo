@extends("layouts.app")

@section('css')

    <style>
        #feedback-pesquisa,
        #resultados-pesquisa,
        #oportunidades-pesquisa,
        #botao-imprimir {
            display: none;
        }
    </style>

@endsection

@section("content")

    @include("layouts._menu")

    <div class="tm-section topoInternaEmpresa">
        <div class="uk-container uk-container-center uk-text-center">
            <h1 class="tm-margin-large-bottom"><i class="uk-icon-search-plus uk-icon-medium"></i> Encontre uma <strong>Oportunidade</strong>
            </h1>
        </div>
    </div>

    <div class="tm-section opcoesInterno">
        <div class="uk-container uk-container-center">
            <p>Encontre um oportunidade pesquisando em nosso Banco de Talentos, utilize os campos abaixo para encontrar
                o que procura.</p>
            <form class="uk-form uk-form-horizontal" id="form-pesquisa">
                <fieldset class="uk-margin">
                    <legend>Pesquisa</legend>
                    <div class="uk-form-row">
                        <label for="titulo" class="uk-form-label">Título da Oportunidade:</label>
                        <div class="uk-form-controls uk-text-left">
                            <input type="text" name="title" placeholder="" id="titulo" class="uk-form-width-large">
                        </div>
                    </div>

                    <div class="uk-form-row">
                        <label for="tipo" class="uk-form-label">Tipo da Oportunidade:</label>
                        <div class="uk-form-controls uk-text-left">
                            <select id="tipo" name="type" class="uk-form-width-medium">
                                <option value="">Selecione</option>
                                <option value="Emprego">Emprego</option>
                                <option value="Estágio">Estágio</option>
                                <option value="Free Lancer">Free Lancer</option>
                                <option value="Curso">Curso</option>
                            </select>
                        </div>
                    </div>

                    <div class="uk-form-row">
                        <label class="uk-form-label">Áreas de Atuação:</label>
                    </div>
                    <div class="uk-grid uk-form-row">
                        <div class="uk-width-medium-1-4">
                            <?php $a = 1; ?>
                            @foreach($areas1 as $area1)
                                <div class="uk-form-row">
                                    <input type="checkbox" value="{{$area1->id}}" name="area[]"
                                           id="areaInteresse{{$a}}" class="interesse">
                                    <label for="areaInteresse{{$a}}" class="uk-form-label">{{$area1->area}}</label>
                                </div>
                                <?php $a++; ?>
                            @endforeach
                        </div>

                        <div class="uk-width-medium-1-4">
                            <?php $a = 1; ?>
                            @foreach($areas2 as $area2)
                                <div class="uk-form-row">
                                    <input type="checkbox" value="{{$area2->id}}" name="area[]"
                                           id="areaInteresse{{$a}}" class="interesse">
                                    <label for="areaInteresse{{$a}}" class="uk-form-label">{{$area2->area}}</label>
                                </div>
                                <?php $a++; ?>
                            @endforeach

                        </div>

                        <div class="uk-width-medium-1-4">
                            <?php $a = 1; ?>
                            @foreach($areas3 as $area3)
                                <div class="uk-form-row">
                                    <input type="checkbox" value="{{$area3->id}}" name="area[]"
                                           id="areaInteresse{{$a}}" class="interesse">
                                    <label for="areaInteresse{{$a}}" class="uk-form-label">{{$area3->area}}</label>
                                </div>
                                <?php $a++; ?>
                            @endforeach

                        </div>

                        <div class="uk-width-medium-1-4">
                            <?php $a = 1; ?>
                            @foreach($areas4 as $area4)
                                <div class="uk-form-row">
                                    <input type="checkbox" value="{{$area4->id}}" name="area[]"
                                           id="areaInteresse{{$a}}" class="interesse">
                                    <label for="areaInteresse{{$a}}" class="uk-form-label">{{$area4->area}}</label>
                                </div>
                                <?php $a++; ?>
                            @endforeach

                        </div>


                    </div>

                    <div class="uk-text-center uk-margin">
                        <button type="submit" class="uk-button uk-button-large uk-button-primary bt-cadastrar">
                            Pesquisar
                        </button>
                    </div>

                </fieldset>
            </form>
        </div>
    </div>

    <div class="tm-section opcoesInterno">
        <div class="uk-container uk-container-center">

            <a name="resultadoBusca"></a>

            <div class="uk-form">

                <fieldset class="uk-margin" id="oportunidades-pesquisa">
                    <legend>Oportunidades Encontradas</legend>
                </fieldset>
                <!-- NAO ENCONTRA RESULTADOS -->
                <div class="uk-grid" id="feedback-pesquisa">
                    <div class="uk-width-medium-1-1">
                        <div class="uk-panel oportunidade uk-text-center">
                            <p>Não foram encontradas oportunidades com as opções selecionadas.</p>
                        </div>
                    </div>
                </div>
                <!-- NAO ENCONTRA RESULTADOS FIM -->

                <!--ENCONTRA RESULTADOS-->
                <div id="resultados-pesquisa">
                    <div class="uk-width-medium-1-1">
                        <div class="uk-panel oportunidade">
                            <div class="uk-panel-badge uk-badge">Registrado em 22/07/2016 19:00</div>
                            <h3 class="uk-panel_title tituloOportunidade">REPOSITOR DE ESTOQUE</h3>
                            Empresa: Plinicar distribuidora de alimentos LTDA.
                            <br/>
                            E-mail da Empresa: <a
                                    href="mailto:recutamento@plinicar.com.br">recutamento@plinicar.com.br</a>
                            <br/>
                            Usuário: plinicar_BR
                            <br/>
                            Descrição da Oportunidade: Trabalhar em ambiente cordial com atitudes para trabalho em
                            equipe. Necessário estar cursando ensino médio.
                            <br/>
                            <div class="uk-grid">
                                <div class="uk-width-medium-1-2">
                                    Tipo da Oportunidade: Emprego
                                </div>
                                <div class="uk-width-medium-1-2">
                                    Quantidade de Vagas: 05
                                </div>
                            </div>
                            <div class="uk-grid">
                                <div class="uk-width-medium-1-2">
                                    Data de Início: 20/07/2016
                                </div>
                                <div class="uk-width-medium-1-2">
                                    Data de Término: 30/07/2016
                                </div>
                            </div>
                            Áreas de Atuação: Logística, Gestão
                            <div class="uk-text-center">
                                <button class="uk-button uk-button-large uk-button-primary interesse">Tenho Interesse
                                    nesta Oportunidade
                                </button>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="uk-text-center uk-margin" id="botao-imprimir">
                    <button class="uk-button uk-button-large uk-button-primary bt-cadastrar" onclick="window.print()">
                        Imprimir Lista de Oportunidades
                    </button>
                </div>
                <!--ENCONTRA RESULTADOS FIM-->

            </div>

        </div>
    </div>

    @include("layouts._footer")

@endsection

@section('js')

    <script>
        $(function () {

            $("#form-pesquisa").submit(function (e) {
                e.preventDefault();

                $.ajax({
                    "url": "{!! route('oportunite.index') !!}",
                    "type": "POST",
                    "data": {
                        "_token": "{!! csrf_token() !!}",
                        "title": $("#titulo").val(),
                        "type": $("#tipo").val(),
                        "area": $(".interesse:checked").get().map(el => el.value).join(",")
                    }
                }).success(function(response) {
                    if(response) {
                        $("#resultados-pesquisa, #oportunidades-pesquisa").css('display', "block");
                        resultados(response);
                    }
                }).error(function(response) {

                })


            });

            function resultados(response) {
                // console.log(response);
                $("#resultados-pesquisa").empty();
                $.each(response, function(index, value) {
                    console.log(value);

                    var areas = "";
                    $.each(value.areas, function(index, value) {
                        areas += value.areas + ", "
                    });

                    areas = areas.substring(0, (areas.length - 2)) //0 é p começar de traz p frente
                    console.log(areas);

                    registro = formatDate(value.created_at);
                    start = formatDate(value.start);
                    finish = formatDate(value.finish);


                    $("#resultados-pesquisa").append(
                        '<div class="uk-grid">' +
                            '<div class="uk-width-medium-1-1">' +
                            '<div class="uk-panel oportunidade">' +
                            '<div class="uk-panel-badge uk-badge">Registrado em '+registro+'</div>' +
                            '<h3 class="uk-panel_title tituloOportunidade">'+value.title+'</h3>' +
                            'Empresa: '+value.user.name+'' +
                            '<br/>' +
                            'E-mail da Empresa: <a href="mailto:'+value.user.email+'">'+value.user.email+'</a>' +
                            '<br/>' +
                            'Usuário: '+value.user.login+' <br/>' +
                            'Descrição da Oportunidade: '+value.description+' <br/>' +

                            '<div class="uk-grid">' +
                            '<div class="uk-width-medium-1-2">Tipo da Oportunidade: '+value.type+'</div>' +
                            '<div class="uk-width-medium-1-2">Quantidade de Vagas: '+value.quantity+'</div>' +
                            '</div>	' +
                            '<div class="uk-grid">' +
                            '<div class="uk-width-medium-1-2"> Data de Início: '+start+'</div>' +
                            '<div class="uk-width-medium-1-2">Data de Término: '+finish+'</div>' +
                            '</div>	' +
                            'Áreas de Atuação: '+areas+'' +
                            '<div class="uk-text-center">' +
                            '<button class="uk-button uk-button-large uk-button-primary interesse">Tenho Interesse nesta Oportunidade</button>' +
                            '</div>	' +
                            '</div>' +
                            '</div>' +
                        '</div>'
                    );
                });

            }


            function formatDate(date) {
                data = new Date(date);

                dia=data.getDate();
                mes=data.getMonth();
                ano=data.getFullYear();

                if(dia < 10) {
                    dia = "0"+dia;
                }

                if(mes < 10) {
                    mes = "0"+mes;
                }

                return dia + "/" + (mes++) + "/" + ano; //janeiro p javascript é 0, por isso o ++
            }

        })
    </script>

@endsection