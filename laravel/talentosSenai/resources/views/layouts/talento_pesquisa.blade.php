@extends("layouts.app")
@section("content")

    @include("layouts._menu")

    <div class="tm-section topoInternaEmpresa">
        <div class="uk-container uk-container-center uk-text-center">
            <h1 class="tm-margin-large-bottom"><i class="uk-icon-group uk-icon-medium"></i> Encontre um
                <strong>Talento</strong></h1>
        </div>
    </div>

    <div class="tm-section opcoesInterno">
        <div class="uk-container uk-container-center">
            <p>Encontre um profissional em nosso Banco de Talentos, utilize os campos abaixo para localizar o que
                procura.</p>

            <form class="uk-form uk-form-horizontal" id="talento-pesquisa">

                <fieldset class="uk-margin">
                    <legend>Pesquisa</legend>
                    <div class="uk-form-row">
                        <label for="nome" class="uk-form-label">Nome:</label>
                        <div class="uk-form-controls uk-text-left">
                            <input type="text" name="name" placeholder="" id="nome" class="uk-form-width-large">
                        </div>
                    </div>

                    <div class="uk-form-row">
                        <label class="uk-form-label">Áreas de Atuação:</label>
                    </div>

                    <div class="uk-grid uk-form-row">
                        <div class="uk-width-medium-1-4">

                            <?php $a = 1; ?>
                            @foreach($areas1 as $area1)
                                <div class="uk-form-row">
                                    <input type="checkbox" name="areas[]" value="{{ $area1->id }}"
                                           id="areaInteresse{{$a}}" class="interesse">
                                    <label for="areaInteresse{{$a}}" class="uk-form-label">{{ $area1->area }}</label>
                                </div>

                                <?php $a++; ?>
                            @endforeach
                        </div>

                        <div class="uk-width-medium-1-4">

                            @foreach($areas2 as $area2)
                                <div class="uk-form-row">
                                    <input type="checkbox" name="areas[]" value="{{ $area2->id }}"
                                           id="areaInteresse{{$a}}" class="interesse">
                                    <label for="areaInteresse{{$a}}" class="uk-form-label">{{ $area2->area }}</label>
                                </div>

                                <?php $a++; ?>
                            @endforeach
                        </div>

                        <div class="uk-width-medium-1-4">

                            @foreach($areas3 as $area3)
                                <div class="uk-form-row">
                                    <input type="checkbox" name="areas[]" value="{{ $area3->id }}"
                                           id="areaInteresse{{$a}}" class="interesse">
                                    <label for="areaInteresse{{$a}}" class="uk-form-label">{{ $area3->area }}</label>
                                </div>

                                <?php $a++; ?>
                            @endforeach
                        </div>

                        <div class="uk-width-medium-1-4">

                            @foreach($areas4 as $area4)
                                <div class="uk-form-row">
                                    <input type="checkbox" name="areas[]" value="{{ $area4->id }}"
                                           id="areaInteresse{{$a}}" class="interesse">
                                    <label for="areaInteresse{{$a}}" class="uk-form-label">{{ $area4->area }}</label>
                                </div>

                                <?php $a++; ?>
                            @endforeach
                        </div>
                    </div>

                    <div class="uk-form-row">
                        <label for="tipo" class="uk-form-label">Ordenar Resultados por:</label>
                        <div class="uk-form-controls uk-text-left">
                            <select id="tipo" name="order" class="uk-form-width-medium">
                                <option value="" selected="selected">Selecione</option>
                                <option value="name">Nome</option>
                                <option value="created_at">Data de Registro</option>
                            </select>
                        </div>
                    </div>

                    <div class="uk-text-center uk-margin">
                        <button class="uk-button uk-button-large uk-button-primary bt-cadastrar">Pesquisar</button>
                    </div>

                </fieldset>
            </form>
        </div>
    </div>

    <div class="tm-section opcoesInterno">
        <div class="uk-container uk-container-center">

            <a name="resultadoBusca"></a>

            <div class="uk-form">

                <fieldset class="uk-margin titulo">
                    <legend>Talentos Encontrados</legend>
                </fieldset>
                <!-- NAO ENCONTRA RESULTADOS -->
                <div class="uk-grid">
                    <div class="uk-width-medium-1-1">
                        <div class="uk-panel oportunidade uk-text-center msg">
                            <p>Não foram encontradas talentos com as opções selecionadas.</p>
                        </div>
                    </div>
                </div>
                <!-- NAO ENCONTRA RESULTADOS FIM -->

                <!--ENCONTRA RESULTADOS-->
                <div class="resultados">

                </div>


                <div class="uk-text-center uk-margin">
                    <button class="uk-button uk-button-large uk-button-primary bt-cadastrar btn-imprimir oculto">Imprimir Lista de
                        Talentos
                    </button>
                </div>
                <!--ENCONTRA RESULTADOS FIM-->

            </div>

        </div>
    </div>

    @include("layouts._footer")

@endsection

@section('js')
    <script>
        $(function() {
            $('#talento-pesquisa').submit(function(e) {
                e.preventDefault();

                $(".msg").addClass('oculto');

                $.ajax({
                    url: "{{route('talent.index')}}",
                    type: "POST",
                    data: {
                        "_token": "{{csrf_token()}}",
                        "nome": $("#nome").val(),
                        "order": $("#tipo").val(),
                        "area": $(".interesse:checked").get().map(el => el.value).join(",")
                    }
                }).success(function(response) {
                    resultados(response);
                }).error(function(response) {

                })
            })

            function resultados(res) {
                $(".resultados").empty();
                $.each(res, function(index, value) {
                    console.log(value);

                    var areas = "";
                    $.each(value.user.areas, function(index, value) {
                        areas += value.area + ", "
                    });

                    areas = areas.substring(0, (areas.length - 2))

                    dataConclusao = formatDate(value.finisher);
                    dataRegistro = formatDateRegistro(value.created_at);

                    $(".resultados").append(
                        '<div class="uk-grid">' +
                        ' <div class="uk-width-medium-1-1">' +
                        '   <div class="uk-panel oportunidade">' +
                        '   <div class="uk-panel-badge uk-badge">Registrado em '+dataRegistro+'</div>' +
                        '<h3 class="uk-panel_title nomeTalento">'+value.user.name+'' +
                        ' </h3>' +
                        ' E-mail: <a href="mailto:'+value.user.email+'">'+value.user.email+'</a>' +
                        '<br/>' +
                        'Usuário: '+value.user.login+'' +
                        '<br/>' +
                        'Curso Realizado: '+value.course+'' +
                        '<br/>' +
                        '<div class="uk-grid">' +
                        ' <div class="uk-width-medium-1-2">' +
                        '  Estado: '+value.country+'' +
                        '</div>' +
                        '<div class="uk-width-medium-1-2">' +
                        '   Data de Conclusão: '+dataConclusao+'' +
                        '</div>' +
                        '</div>' +
                        'Áreas de Atuação: '+areas+'' +
                        '</div>' +
                        '</div>' +
                        '</div>'
                    );
                });
            }

            function formatDate(date) {
                data = new Date(date);

                dia=data.getDate();
                mes=data.getMonth();
                ano=data.getFullYear();



                return dia + "/" + (mes++) + "/" + ano; //janeiro p javascript é 0, por isso o ++
            }

            function formatDateRegistro(date) {
                data = new Date(date);

                dia=data.getDate();
                mes=data.getMonth();
                ano=data.getFullYear();
                hora =data.getHours();
                minuto = data.getMinutes();

                return dia + "/" + (mes++) + "/" + ano + " " + hora + ":" + minuto;
            }
        })
    </script>
@endsection