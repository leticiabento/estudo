@extends("layouts.app")
@section('content')
    <div class="tm-section topoInternaEstudante">
        <div class="uk-container uk-container-center uk-text-center">
            <h1 class="tm-margin-large-bottom"><i class="uk-icon-bank uk-icon-medium"></i> Minhas <strong>Oportunidades</strong></h1>
        </div>
    </div>

    <div class="tm-section opcoesInterno">
        <div class="uk-container uk-container-center">

            <a name="resultadoBusca"></a>

            <div class="uk-form">

                <fieldset class="uk-margin">
                    <legend>Oportunidades Interessadas</legend>
                </fieldset>
                <!-- NAO ENCONTRA RESULTADOS -->
                <div class="uk-grid">
                    <div class="uk-width-medium-1-1">
                        <div class="uk-panel oportunidade uk-text-center">
                            <p>Não foram encontradas oportunidades com seu interesse. <a href="oportunidade_pesquisa.html">Encontre uma oportunidade</a>.</p>
                        </div>
                    </div>
                </div>
                <!-- NAO ENCONTRA RESULTADOS FIM -->

                <!--ENCONTRA RESULTADOS-->
                <div class="uk-grid">
                    <div class="uk-width-medium-1-1">
                        <div class="uk-panel oportunidade">
                            <div class="uk-panel-badge uk-badge">22/07/2016 19:00</div>
                            <h3 class="uk-panel_title tituloOportunidade">REPOSITOR DE ESTOQUE</h3>
                            Empresa: Plinicar distribuidora de alimentos LTDA.
                            <br/>
                            E-mail da Empresa: <a href="mailto:recutamento@plinicar.com.br">recutamento@plinicar.com.br</a>
                            <br/>
                            Usuário: plinicar_BR
                            <br/>
                            Descrição da Oportunidade: Trabalhar em ambiente cordial com atitudes para trabalho em equipe. Necessário estar cursando ensino médio.
                            <br/>
                            <div class="uk-grid">
                                <div class="uk-width-medium-1-2">
                                    Tipo da Oportunidade: Emprego
                                </div>
                                <div class="uk-width-medium-1-2">
                                    Quantidade de Vagas: 05
                                </div>
                            </div>
                            <div class="uk-grid">
                                <div class="uk-width-medium-1-2">
                                    Data de Início: 20/07/2016
                                </div>
                                <div class="uk-width-medium-1-2">
                                    Data de Término: 30/07/2016
                                </div>
                            </div>
                            Áreas de Atuação: Logística, Gestão
                            <div class="uk-text-center">
                                <button class="uk-button uk-button-large uk-button-danger interesse">Excluir Interesse nesta Oportunidade</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="uk-grid">
                    <div class="uk-width-medium-1-1">
                        <div class="uk-panel oportunidade">
                            <div class="uk-panel-badge uk-badge">22/07/2016 10:00</div>
                            <h3 class="uk-panel_title tituloOportunidade">ESTAGIÁRIO DE INFORMÁTICA</h3>
                            Empresa: Preston Sistemas.
                            <br/>
                            E-mail da Empresa: <a href="mailto:rh@preston.com.br">rh@preston.com.br</a>
                            <br/>
                            Usuário: preston
                            <br/>
                            Descrição da Oportunidade: Manutenção e configuração de equipamentos de informática. Atendimento ao usuário. Oportunidade para interessados em continuar trabalhando após o término do estágio.
                            <br/>
                            <div class="uk-grid">
                                <div class="uk-width-medium-1-2">
                                    Tipo da Oportunidade: Estágio
                                </div>
                                <div class="uk-width-medium-1-2">
                                    Quantidade de Vagas: 02
                                </div>
                            </div>
                            <div class="uk-grid">
                                <div class="uk-width-medium-1-2">
                                    Data de Início: 22/07/2016
                                </div>
                                <div class="uk-width-medium-1-2">
                                    Data de Término: 15/08/2016
                                </div>
                            </div>
                            Áreas de Atuação: Tecnologia da informação, Gestão
                            <div class="uk-text-center">
                                <button class="uk-button uk-button-large uk-button-danger interesse">Excluir Interesse nesta Oportunidade</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="uk-grid">
                    <div class="uk-width-medium-1-1">
                        <div class="uk-panel oportunidade">
                            <div class="uk-panel-badge uk-badge">21/07/2016 15:00</div>
                            <h3 class="uk-panel_title tituloOportunidade">PROGRAMADOR PHP</h3>
                            Empresa: Softwarehouse
                            <br/>
                            E-mail da Empresa: <a href="mailto:contato@softwarehouse.com.br">contato@softwarehouse.com.br</a>
                            <br/>
                            Usuário: shouse
                            <br/>
                            Descrição da Oportunidade: Desenvolvimento back-end em PHP. Noções de Banco de Dados MySQL. Desejável conhecimento de desenvolvimento front-end (HTML / CSS / Javascript). A empresa oferece salário compatível, vale alimentação, vale transporte e seguro de saúde. Interessados devem candidatar-se para entrevista e realização de prova de conhecimentos.
                            <br/>
                            <div class="uk-grid">
                                <div class="uk-width-medium-1-2">
                                    Tipo da Oportunidade: Emprego
                                </div>
                                <div class="uk-width-medium-1-2">
                                    Quantidade de Vagas: 01
                                </div>
                            </div>
                            <div class="uk-grid">
                                <div class="uk-width-medium-1-2">
                                    Data de Início: 20/07/2016
                                </div>
                                <div class="uk-width-medium-1-2">
                                    Data de Término: 30/08/2016
                                </div>
                            </div>
                            Áreas de Atuação: Tecnologia da Informação
                            <div class="uk-text-center">
                                <button class="uk-button uk-button-large uk-button-danger interesse">Excluir Interesse nesta Oportunidade</button>
                            </div>
                        </div>
                    </div>
                </div>

                <!--ENCONTRA RESULTADOS FIM-->

            </div>

        </div>
    </div>

@endsection