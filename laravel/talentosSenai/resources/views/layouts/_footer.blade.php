<div class="rodape">
    <div class="uk-container uk-container-center uk-text-center">
        <ul class="uk-navbar-nav uk-hidden-small">
            <li><a href="#"><i class="uk-icon-home uk-icon-small uk-icon-button"></i> Portal da Indústria</a></li>
            <li><a href="#" class="ic ic-home-cni"></a></li>
            <li><a href="#" class="ic ic-home-sesi"></a></li>
            <li><a href="#" class="ic ic-home-senai"></a></li>
            <li><a href="#" class="ic ic-home-iel"></a></li>
            <li><a href="#" class="politica">Política de Privacidade</a></li>
            <li><a href="#"><i class="uk-icon-twitter uk-icon-small uk-icon-button"></i></a></li>
            <li><a href="#"><i class="uk-icon-facebook uk-icon-small uk-icon-button"></i></a></li>
            <li><a href="#"><i class="uk-icon-youtube uk-icon-small uk-icon-button"></i></a></li>
        </ul>
    </div>
</div>