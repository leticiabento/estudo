@extends("layouts.app")
@section("content")

    @include("layouts._menu")

    <div class="tm-section topoInternaEmpresa">
        <div class="uk-container uk-container-center uk-text-center">
            <h1 class="tm-margin-large-bottom"><i class="uk-icon-bank uk-icon-medium"></i> Cadastre uma <strong>Oportunidade</strong>
            </h1>
        </div>
    </div>

    <div class="tm-section opcoesInterno">
        <div class="uk-container uk-container-center">

            <p>Preencha o formuário abaixo e ofereça uma vaga aos profissionais mais bem capacitados do mercado.</p>

            @include ("layouts._feedback")

            <form class="uk-form uk-form-horizontal" action="{{ route("oportunite.store") }}" method="post">

                {{csrf_field()}}

                <input type="hidden" name="role" value="empresa">

                <fieldset class="uk-margin">
                    <legend>Dados da Empresa</legend>
                    <div class="uk-form-row">
                        <label for="nome" class="uk-form-label">Nome da Empresa:</label>
                        <div class="uk-form-controls uk-text-left">
                            <input type="text" name="name" placeholder="Nome da Empresa" id="nome"
                                   class="uk-form-width-large">
                            <span class="uk-form-help-inline uk-text-danger uk-text-small oculto">Preencha o campo obrigatório Nome da Empresa.</span>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label for="email" class="uk-form-label">E-mail:</label>
                        <div class="uk-form-controls uk-text-left">
                            <input type="text" name="email" placeholder="Endereço de E-mail" id="email"
                                   class="uk-form-width-large">
                            <span class="uk-form-help-inline uk-text-danger uk-text-small oculto">Preencha o campo obrigatório E-mail.</span>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label for="login" class="uk-form-label">Login:</label>
                        <div class="uk-form-controls uk-text-left">
                            <input type="text" name="login" placeholder="" id="login" class="uk-form-width-medium">
                            <span class="uk-form-help-inline uk-text-danger uk-text-small oculto">Preencha o campo obrigatório Login.</span>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label for="senha" class="uk-form-label">Senha:</label>
                        <div class="uk-form-controls uk-text-left">
                            <input type="password" name="password" placeholder="" id="senha"
                                   class="uk-form-width-medium">
                            <span class="uk-form-help-inline uk-text-danger uk-text-small oculto">Preencha o campo obrigatório Senha.</span>
                        </div>
                    </div>
                </fieldset>
                <fieldset class="uk-margin">
                    <legend>Dados da Oportunidade</legend>
                    <div class="uk-form-row">
                        <label for="titulo" class="uk-form-label">Título da Oportunidade:</label>
                        <div class="uk-form-controls uk-text-left">
                            <input type="text" name="title" placeholder="Título da Oportunidade" id="titulo"
                                   class="uk-form-width-large">
                            <span class="uk-form-help-inline uk-text-danger uk-text-small oculto">Preencha o campo obrigatório Título da Oportunidade.</span>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label for="descricao" class="uk-form-label">Descrição da Oportunidade:</label>
                        <div class="uk-form-controls uk-text-left">
                            <textarea id="descricao" name="description" class="uk-form-width-large"></textarea>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label for="tipo" class="uk-form-label">Tipo da Oportunidade:</label>
                        <div class="uk-form-controls uk-text-left">
                            <select id="tipo" name="type" class="uk-form-width-medium">
                                <option value="">Selecione</option>
                                <option value="Emprego">Emprego</option>
                                <option value="Estágio">Estágio</option>
                                <option value="Free Lancer">Free Lancer</option>
                                <option value="Curso">Curso</option>
                            </select>
                            <span class="uk-form-help-inline uk-text-danger uk-text-small oculto">Selecione uma opção no campo obrigatório Tipo da Oportunidade.</span>
                        </div>
                    </div>

                    <div class="uk-form-row">
                        <label for="quantidade" class="uk-form-label">Quantidade de Vagas:</label>
                        <div class="uk-form-controls uk-text-left">
                            <input type="number" name="quantity" id="quantidade" class="uk-form-width-medium">
                            <span class="uk-form-help-inline uk-text-danger uk-text-small oculto">Preencha o campo obrigatório Quantidade de Vagas.</span>
                        </div>
                    </div>

                    <div class="uk-form-row">
                        <label for="inicio" class="uk-form-label">Data de Início da Oportunidade:</label>
                        <div class="uk-form-controls uk-text-left">
                            <div class="uk-form-icon">
                                <i class="uk-icon-calendar"></i>
                                <input type="text" name="start" placeholder="dd/mm/aaaa" id="inicio" class="uk-form-width-medium dateStart">
                                <span class="uk-form-help-inline uk-text-danger uk-text-small oculto">Preencha o obrigatório Data de Início da Oportunidade.</span>
                            </div>
                        </div>
                    </div>

                    <div class="uk-form-row">
                        <label for="termino" class="uk-form-label">Data de Término da Oportunidade:</label>
                        <div class="uk-form-controls uk-text-left">
                            <div class="uk-form-icon">
                                <i class="uk-icon-calendar"></i>
                                <input type="text" name="finish" placeholder="dd/mm/aaaa" id="termino" class="uk-form-width-medium dateEnd">
                                <span class="uk-form-help-inline uk-text-danger uk-text-small oculto">Preencha o obrigatório Data de Término da Oportunidade.</span>
                            </div>
                        </div>
                    </div>

                </fieldset>
                <fieldset class="uk-margin">
                    <legend>Áreas de Atuação:</legend>
                    <div class="uk-grid">
                        <?php $a = 1; ?>
                        <div class="uk-width-medium-1-4">
                            @foreach($areas1 as $area1)

                                <div class="uk-form-row">
                                    <input name="area[]" value="{{$area1->id}}" type="checkbox"
                                           id="areaInteresse{{$a}}">
                                    <label for="areaInteresse{{$a}}"
                                           class="uk-form-label">{{$area1->area}}</label>
                                </div>
                            @endforeach
                            <?php $a++; ?>
                        </div>

                        <div class="uk-width-medium-1-4">
                            @foreach($areas2 as $area2)

                                <div class="uk-form-row">
                                    <input name="area[]" value="{{$area2->id}}" type="checkbox"
                                           id="areaInteresse{{$a}}">
                                    <label for="areaInteresse{{$a}}"
                                           class="uk-form-label">{{$area2->area}}</label>
                                </div>
                            @endforeach
                            <?php $a++; ?>
                        </div>

                        <div class="uk-width-medium-1-4">
                            @foreach($areas3 as $area3)

                                <div class="uk-form-row">
                                    <input name="area[]" value="{{$area3->id}}" type="checkbox"
                                           id="areaInteresse{{$a}}">
                                    <label for="areaInteresse{{$a}}"
                                           class="uk-form-label">{{$area3->area}}</label>
                                </div>
                            @endforeach
                            <?php $a++; ?>
                        </div>

                        <div class="uk-width-medium-1-4">
                            @foreach($areas4 as $area4)

                                <div class="uk-form-row">
                                    <input name="area[]" value="{{$area4->id}}" type="checkbox"
                                           id="areaInteresse{{$a}}">
                                    <label for="areaInteresse{{$a}}"
                                           class="uk-form-label">{{$area4->area}}</label>
                                </div>
                            @endforeach
                            <?php $a++; ?>
                        </div>


                    </div>
                    <span class="uk-form-help-inline uk-text-danger uk-text-small oculto">Selecione ao menos uma opção nas Áreas de Atuação.</span>
                </fieldset>
                <div class="uk-text-center uk-margin">
                    <button type="submit" class="uk-button uk-button-large uk-button-primary bt-cadastrar">Cadastrar</button>
                </div>
            </form>
        </div>
    </div>

    @include("layouts._footer")

@endsection

@section('js')
    <script src="{{asset("js/components/datepicker.js")}}"></script>
    <script>
        $(function() {
            UIkit.datepicker(".dateStart", {format:'DD/MM/YYYY'});
            UIkit.datepicker(".dateEnd", {format:'DD/MM/YYYY'});

            $("form").submit(function() {
                // e.preventDefault();

                if($("#nome").val() === "") {
                    $("#nome").addClass("uk-form-danger");
                    $("#nome").parent().children().eq(1).removeClass("oculto");
                } else {
                    $("#nome").removeClass("uk-form-danger");
                    $("#nome").parent().children().eq(1).addClass("oculto");
                }

                if($("#email").val() === "") {
                    $("#email").addClass("uk-form-danger");
                    $("#email").parent().children().eq(1).removeClass("oculto");
                } else {
                    $("#email").removeClass("uk-form-danger");
                    $("#email").parent().children().eq(1).addClass("oculto");
                }

                if($("#login").val() === "") {
                    $("#login").addClass("uk-form-danger");
                    $("#login").parent().children().eq(1).removeClass("oculto");
                } else {
                    $("#login").removeClass("uk-form-danger");
                    $("#login").parent().children().eq(1).addClass("oculto");
                }

                if($("#senha").val() === "") {
                    $("#senha").addClass("uk-form-danger");
                    $("#senha").parent().children().eq(1).removeClass("oculto");
                } else {
                    $("#senha").removeClass("uk-form-danger");
                    $("#senha").parent().children().eq(1).addClass("oculto");
                }

                if($("#titulo").val() === "") {
                    $("#titulo").addClass("uk-form-danger");
                    $("#titulo").parent().children().eq(1).removeClass("oculto");
                } else {
                    $("#titulo").removeClass("uk-form-danger");
                    $("#titulo").parent().children().eq(1).addClass("oculto");
                }

                if($("#tipo").val() === "") {
                    $("#tipo").addClass("uk-form-danger");
                    $("#tipo").parent().children().eq(1).removeClass("oculto");
                } else {
                    $("#tipo").removeClass("uk-form-danger");
                    $("#tipo").parent().children().eq(1).addClass("oculto");
                }

                if($("#quantidade").val() === "") {
                    $("#quantidade").addClass("uk-form-danger");
                    $("#quantidade").parent().children().eq(1).removeClass("oculto");
                } else {
                    $("#quantidade").removeClass("uk-form-danger");
                    $("#quantidade").parent().children().eq(1).addClass("oculto");
                }

                if($("#inicio").val() === "") {
                    $("#inicio").addClass("uk-form-danger");
                    $("#inicio").parent().children().eq(2).removeClass("oculto");
                } else {
                    $("#inicio").removeClass("uk-form-danger");
                    $("#inicio").parent().children().eq(2).addClass("oculto");
                }

                if($("#termino").val() === "") {
                    $("#termino").addClass("uk-form-danger");
                    $("#termino").parent().children().eq(2).removeClass("oculto");
                } else {
                    $("#termino").removeClass("uk-form-danger");
                    $("#termino").parent().children().eq(2).addClass("oculto");
                }

               
            })
        })


    </script>
@endsection