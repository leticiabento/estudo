@extends("layouts.app")
@section("content")

    @include("layouts._menu")
    <body>
        <div class="tm-section topoAbertura">
            <div class="uk-container uk-container-center uk-text-center">

                <img class="tm-logo" src="imagens/logo.png" title="Banco de Talentos SENAI" alt="Banco de Talentos SENAI">

                <h2 class="tm-margin-large-bottom"><strong>Fa&ccedil;a parte</strong> do maior banco de Talentos do Brasil
                </h2>

                <p>O mundo do trabalho exige profissionais qualificados e que estejam em sintonia com as inovações
                    tecnológicas das diversas ocupações profissionais. A procura por egressos com este perfil cresce na
                    medida em que crescem as empresas. Profissionais qualificados com a qualidade SENAI estão cada vez mais
                    requisitados, e, pensando nisso, o SENAI oferece gratuitamente o currículos de seus egressos através do
                    Banco de Talentos SENAI. </p>

                <h1><strong>Banco de Talentos SENAI</strong></h1>

                <img class="tm-logo logoBranco" src="imagens/senai_mono.png" title="SENAI" alt="SENAI">
            </div>
        </div>

        <div class="tm-section tm-section-color-white opcoes">
            <div class="uk-container uk-container-center uk-text-center">
                <div class="uk-grid tm-grid-margin-large" data-uk-grid-match="{target:'.uk-panel'}" data-uk-grid-margin>

                    <div class="uk-width-medium-1-4">
                        <a class="uk-panel tm-panel-link paravoce" href="{{route("talent.create")}}">
                            <div class="tm-icon"><i class="uk-icon-user-plus uk-icon-large"></i></div>
                            <h3>Cadastre seu Talento</h3>
                            <p>Registre-se e seja visto por milhares de empresas, seu talento será reconhecido.</p>
                        </a>
                    </div>

                    <div class="uk-width-medium-1-4">
                        <a class="uk-panel tm-panel-link paravoce" href="{{route("oportunite.search")}}">
                            <div class="tm-icon"><i class="uk-icon-search-plus uk-icon-large"></i></div>
                            <h3>Encontre uma Oportunidade</h3>
                            <p>Encontre e candidate-se as melhores oportunidades do mercado.</p>
                        </a>
                    </div>

                    <div class="uk-width-medium-1-4">
                        <a class="uk-panel tm-panel-link paraempresa" href="{{route("oportunite.create")}}">
                            <div class="tm-icon"><i class="uk-icon-bank uk-icon-large"></i></div>
                            <h3>Cadastre uma Oportunidade</h3>
                            <p>Disponibilize sua oferta e receba o interesse dos profissionais com melhor formação no
                                Brasil.</p>
                        </a>
                    </div>

                    <div class="uk-width-medium-1-4">
                        <a class="uk-panel tm-panel-link paraempresa" href="{{route("talent.search")}}">
                            <div class="tm-icon"><i class="uk-icon-group uk-icon-large"></i></div>
                            <h3>Encontre um Talento</h3>
                            <p>Encontre o profissional diferenciado que tanto procura para fazer a difereça na sua
                                empresa.</p>
                        </a>
                    </div>

                </div>

            </div>
        </div>

        <div class="tm-section indicadores">
            <div class="uk-container uk-container-center uk-text-center">
                <h2><strong>NOSSOS NÚMEROS</strong></h2>
                <div class="uk-grid tm-grid-margin-large">
                    <div class="uk-width-medium-1-4 uk-push-1-4">
                        <div class="uk-panel indicador">
                            <div class="uk-panel_title uk-text-large">Oportunidades Disponíveis</div>
                            <div class="uk-text-large uk-text-bold destaque">{{$opportunites}}</div>
                        </div>
                    </div>
                    <div class="uk-width-medium-1-4 uk-push-1-4">
                        <div class="uk-panel indicador">
                            <div class="uk-panel_title uk-text-large">Talentos<br/>Registrados</div>
                            <div class="uk-text-large uk-text-bold destaque">{{ $professionals }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @include("layouts._footer")

    </body>
@endsection