@extends("layouts.app")
@section("content")

    @include("layouts._menu")

    <div class="tm-section topoInternaEstudante">
        <div class="uk-container uk-container-center uk-text-center">
            <h1 class="tm-margin-large-bottom"><i class="uk-icon-user-plus uk-icon-medium"></i> Cadastre seu <strong>Talento</strong>
            </h1>
        </div>
    </div>

    <div class="tm-section opcoesInterno">
        <div class="uk-container uk-container-center">

            <p>Preencha o formuário abaixo e faça parte do nosso Banco de Talentos.</p>

            @include('layouts._feedback')

            <form class="uk-form uk-form-horizontal" action="{{ route("talent.store") }}" method="post">

                {{ csrf_field() }}

                <input type="hidden" name="role" value="profissional">
                <fieldset class="uk-margin">
                    <legend>Dados Pessoais</legend>
                    <div class="uk-form-row">
                        <label for="nome" class="uk-form-label">Nome:</label>
                        <div class="uk-form-controls uk-text-left">
                            <input type="text" name="name" placeholder="Nome Completo" id="nome"
                                   class="uk-form-width-large">
                            <span class="uk-form-help-inline uk-text-danger uk-text-small oculto">Preencha o campo obrigatório Nome.</span>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label for="email" class="uk-form-label">E-mail:</label>
                        <div class="uk-form-controls uk-text-left">
                            <input type="text" name="email" placeholder="Endereço de E-mail" id="email"
                                   class="uk-form-width-large">
                            <span class="uk-form-help-inline uk-text-danger uk-text-small oculto">Preencha o campo obrigatório E-mail.</span>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label for="login" class="uk-form-label">Login:</label>
                        <div class="uk-form-controls uk-text-left">
                            <input type="text" name="login" placeholder="" id="login" class="uk-form-width-medium">
                            <span class="uk-form-help-inline uk-text-danger uk-text-small oculto">Preencha o campo obrigatório Login.</span>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label for="senha" class="uk-form-label">Senha:</label>
                        <div class="uk-form-controls uk-text-left">
                            <input type="password" name="password" placeholder="" id="senha"
                                   class="uk-form-width-medium">
                            <span class="uk-form-help-inline uk-text-danger uk-text-small oculto">Preencha o campo obrigatório Senha.</span>
                        </div>
                    </div>
                </fieldset>
                <fieldset class="uk-margin">
                    <legend>Dados do Curso no SENAI</legend>
                    <div class="uk-form-row">
                        <label for="curso" class="uk-form-label">Curso Realizado:</label>
                        <div class="uk-form-controls uk-text-left">
                            <input type="text" name="course" placeholder="Nome do Curso realizado no SENAI" id="curso"
                                   class="uk-form-width-large">
                            <span class="uk-form-help-inline uk-text-danger uk-text-small oculto">Preencha o campo obrigatório Curso Realizado.</span>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label for="estado" class="uk-form-label">Estado:</label>
                        <div class="uk-form-controls uk-text-left">
                            {{--{{$countries}}--}}

                            <select name="country" id="estado" class="uk-form-width-medium">
                                <option value="">Selecione</option>
                                @foreach($countries as $country)
                                    <option value="{{$country->name}}">{{$country->name}}</option>
                                @endforeach
                            </select>
                            <span class="uk-form-help-inline uk-text-danger uk-text-small oculto">Selecione uma opção no campo obrigatório Estado.</span>
                        </div>
                    </div>

                    <div class="uk-form-row">
                        <label for="conclusao" class="uk-form-label">Data de Conclusão:</label>
                        <div class="uk-form-controls uk-text-left">
                            <div class="uk-form-icon">
                                <i class="uk-icon-calendar"></i>
                                <input type="text" name="finisher" placeholder="dd/mm/aaaa" id="conclusao"
                                       class="uk-form-width-medium" >
                            </div>
                        </div>
                    </div>
                </fieldset>
                <fieldset class="uk-margin">
                    <legend>Áreas de Interesse:</legend>
                    <div class="uk-grid">
                        <div class="uk-width-medium-1-4">
                            <?php $a = 1 ?>

                            @foreach($areas1 as $area1)
                                <div class="uk-form-row">
                                    <input name="area[]" value="{{$area1->id}}" type="checkbox"
                                           id="areaInteresse{{$a}}">
                                    <label for="areaInteresse{{$a}}"
                                           class="uk-form-label">{{$area1->area}}</label>
                                </div>
                            @endforeach
                            <?php $a++; ?>
                        </div>

                        <div class="uk-width-medium-1-4">

                            @foreach($areas2 as $area2)
                                <div class="uk-form-row">
                                    <input name="area[]" value="{{$area2->id}}" type="checkbox"
                                           id="areaInteresse{{$a}}">
                                    <label for="areaInteresse{{$a}}"
                                           class="uk-form-label">{{$area2->area}}</label>
                                </div>
                            @endforeach
                            <?php $a++; ?>
                        </div>

                        <div class="uk-width-medium-1-4">
                            @foreach($areas3 as $area3)
                                <div class="uk-form-row">
                                    <input name="area[]" value="{{$area3->id}}" type="checkbox"
                                           id="areaInteresse{{$a}}">
                                    <label for="areaInteresse{{$a}}"
                                           class="uk-form-label">{{$area3->area}}</label>
                                </div>
                            @endforeach
                            <?php $a++; ?>
                        </div>

                        <div class="uk-width-medium-1-4">
                            @foreach($areas4 as $area4)
                                <div class="uk-form-row">
                                    <input name="area[]" value="{{$area4->id}}" type="checkbox"
                                           id="areaInteresse{{$a}}">
                                    <label for="areaInteresse{{$a}}"
                                           class="uk-form-label">{{$area4->area}}</label>
                                </div>
                            @endforeach
                            <?php $a++; ?>
                        </div>


                    </div>
                </fieldset>
                <div class="uk-text-center uk-margin">
                    <button class="uk-button uk-button-large uk-button-primary bt-cadastrar">Cadastrar</button>
                </div>
            </form>
        </div>
    </div>

    @include("layouts._footer")

@endsection

@section('js')
    <script src="{{asset("js/components/datepicker.js")}}"></script>
    <script>
        $(function() {
            var datepicker = UIkit.datepicker("#conclusao", { /* options */ });

            $("form").submit(function() {

                if($("#nome").val() === "") {
                    $("#nome").addClass("uk-form-danger");
                    $("#nome").parent().children().eq(1).removeClass("oculto");
                } else {
                    $("#nome").removeClass("uk-form-danger");
                    $("#nome").parent().children().eq(1).addClass("oculto");
                }

                if($("#email").val() === "") {
                    $("#email").addClass("uk-form-danger");
                    $("#email").parent().children().eq(1).removeClass("oculto");
                } else {
                    $("#email").removeClass("uk-form-danger");
                    $("#email").parent().children().eq(1).addClass("oculto");
                }

                if($("#login").val() === "") {
                    $("#login").addClass("uk-form-danger");
                    $("#login").parent().children().eq(1).removeClass("oculto");
                } else {
                    $("#login").removeClass("uk-form-danger");
                    $("#login").parent().children().eq(1).addClass("oculto");
                }

                if($("#senha").val() === "") {
                    $("#senha").addClass("uk-form-danger");
                    $("#senha").parent().children().eq(1).removeClass("oculto");
                } else {
                    $("#senha").removeClass("uk-form-danger");
                    $("#senha").parent().children().eq(1).addClass("oculto");
                }

                if($("#curso").val() === "") {
                    $("#curso").addClass("uk-form-danger");
                    $("#curso").parent().children().eq(1).removeClass("oculto");
                } else {
                    $("#curso").removeClass("uk-form-danger");
                    $("#curso").parent().children().eq(1).addClass("oculto");
                }

                if($("#estado").val() === "") {
                    $("#estado").addClass("uk-form-danger");
                    $("#estado").parent().children().eq(1).removeClass("oculto");
                } else {
                    $("#estado").removeClass("uk-form-danger");
                    $("#estado").parent().children().eq(1).addClass("oculto");
                }

                
            })
        })
    </script>
@endsection