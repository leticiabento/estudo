<nav class="tm-navbar uk-navbar uk-navbar-attached menu">
    <div class="uk-container uk-container-center ">

        <a class="uk-navbar-brand uk-hidden-small" href="{{ route("talentos.index") }}">
            <img class="uk-margin uk-margin-remove" src="{{asset("imagens/logo.png")}}" width="32" height="32"
                 title="Banco de Talentos SENAI" alt="Banco de Talentos SENAI">
        </a>

        <ul class="uk-navbar-nav uk-hidden-small">
            <li><a href="#">Egressos</a></li>
            <li><a href="#">Empresas</a></li>
            <li><a href="#">Inclusão</a></li>
            <li><a href="#">SENAI</a></li>

            @if(!Auth::check())
                <li><a href="/login">Login</a></li>
            @else
                <li><a onclick="event.preventDefault(); document.getElementById('form-login').submit()">Logout</a></li>

                <form action="/logout" method="post" id="form-login" style="display: none;">
                    {{csrf_field()}}
                </form>

            @endif

        </ul>
    </div>
</nav>