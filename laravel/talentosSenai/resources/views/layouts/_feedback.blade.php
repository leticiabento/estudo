@if(session('feedback_ok'))
    <div class="uk-alert uk-alert-success">
        <a href="" class="uk-alert-close uk-close"></a>
        <p> {{session('feedback_ok')}}</p>
    </div>
@endif

@if(session('feedback_error'))
    <div class="uk-alert uk-alert-danger">
        <a href="" class="uk-alert-close uk-close"></a>
        <p>{{session('feedback_error')}}</p>
    </div>
@endif