<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Banco de Talentos SENAI</title>
    <link rel="shortcut icon" href="{{ asset("imagens/icone.png") }}" type="image/x-icon">
    <link rel="stylesheet" href="{{ asset("css/uikit.min.css") }}">
    <link rel="stylesheet" href="{{ asset("css/bancotalentos.css") }}">

    @stack('css')
    @yield('css')

    <script src="{{ asset("js/jquery-2.2.0.min.js") }}"></script>
    <script src="{{ asset("js/uikit.min.js") }}"></script>

</head>


<body>


    @yield('content')

    {{--ponto de referencia--}}
    @stack('js')
    @yield('js')

</body>
</html>
