<!DOCTYPE html>
<html class="uk-height-1-1">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Banco de Talentos SENAI</title>
    <link rel="shortcut icon" href="{{ asset("imagens/icone.png") }}" type="image/x-icon">
    <link rel="stylesheet" href="{{ asset("css/uikit.min.css") }}">
    <link rel="stylesheet" href="{{ asset("css/bancotalentos.css") }}">
    <script src="{{ asset("js/jquery-2.2.0.min.js") }}"></script>
    <script src="{{ asset("js/uikit.min.js") }}"></script>
</head>

<body class="uk-height-1-1">

<div class="uk-vertical-align uk-text-center uk-height-1-1">
    <div class="uk-vertical-align-middle" style="width: 250px;">

        <a href="index.html" title="Página Inicial">
            <img class="uk-margin-bottom" width="120" height="120" src="imagens/logo.png" alt="Banco de Talentos SENAI">
        </a>

        {{--@if(session('feedback_error'))--}}
        {{--<div class="uk-alert-danger" uk-alert>--}}
        {{--<a class="uk-alert-close" uk-close></a>--}}
        {{--<p>{{session('feedback_error')}}</p>--}}
        {{--</div>--}}
        {{--@endif--}}

        <form class="uk-panel uk-panel-box uk-form" action="/login" method="post">

            {{csrf_field()}}

            <div class="uk-form-row">
                <input class="uk-width-1-1 uk-form-large" id="login" name="email" type="text" placeholder="Usuário">
            </div>
            <div class="uk-form-row">
                <input class="uk-width-1-1 uk-form-large" id='password' name="password" type="password" placeholder="Senha">
            </div>
            <div class="uk-form-row">
                <button type="submit" class="uk-width-1-1 uk-button uk-button-primary uk-button-large">Login</button>
            </div>
            <div class="uk-form-row uk-text-small">
                <label class="uk-float-left"><input type="checkbox"> Lembre-se de mim</label>
                <a class="uk-float-right uk-link uk-link-muted" href="#">Esqueceu a Senha?</a>
            </div>
        </form>

    </div>
</div>

{{--<script>--}}
{{--$(function () {--}}
{{--$("#form-login").submit(function() {--}}
{{--$.ajax({--}}
{{--url: "{{route('talentos.logar')}}",--}}
{{--type: 'POST',--}}
{{--data: {--}}
{{--"_token": "{{csrf_token()}}",--}}
{{--"login" : $("#login").val(),--}}
{{--"password" : $("#password").val()--}}
{{--}--}}
{{--}).success(function(res) {--}}
{{--// console.log(res[0]);--}}
{{--//--}}
{{--// if(res[0]['role'] === )--}}
{{--}).error(function(res) {--}}

{{--})--}}
{{--})--}}
{{--})--}}
{{--</script>--}}


</body>

</html>
