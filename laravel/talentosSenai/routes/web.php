<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "HomeController@index")->name("talentos.index");
//Route::get('login', "HomeController@login")->name("talentos.login");
//Route::post('logar', "HomeController@logar")->name("talentos.logar");


Route::get("talento/cadastro", "TalentsController@create")->name("talent.create");
Route::get("talento/pesquisa", "TalentsController@search")->name("talent.search");
Route::post("talento/pesquisa", "TalentsController@index")->name("talent.index");
Route::post("talento/store", "TalentsController@store")->name("talent.store");

Route::get("auth", function () {
    if (Auth::user()->role == "profissional") {
        return redirect('admin/talento/restrita');
    } else {
        return redirect('admin/oportunidade/restrita');
    }
});

Route::group(['middleware' => 'auth', 'prefix'=>'admin'], function () {
    Route::get("talento/restrita", "TalentsController@restrita")->name("talent.restrita");
    Route::get("oportunidade/restrita", "OportunitesController@restrita")->name("oportunidade.restrita");
});



Route::get("oportunidade/cadastro", "OportunitesController@create")->name("oportunite.create");
Route::get("oportunidade/pesquisa", "OportunitesController@search")->name("oportunite.search");
Route::post("oportunidade/pesquisa", "OportunitesController@index")->name("oportunite.index");
Route::post("oportunidade/store", "OportunitesController@store")->name("oportunite.store");


Auth::routes();