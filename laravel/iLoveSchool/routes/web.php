<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('app');
});

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'HomeController@home')->name('home');
Route::get('/profile/{id}', 'HomeController@profile')->name('profile');
Route::get('/search', 'HomeController@search')->name('search');
Route::post('/store', 'UsersController@store')->name('user.store');
Route::post('/procurar', 'UsersController@procurar')->name('procurar');