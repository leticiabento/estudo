<?php

namespace App\Http\Controllers;

use App\School;
use App\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $hashImage = $request->photo_url->hashName();


        if ($request->hasFile('photo_url')) {
            $size = $request->photo_url->getClientSize() / 1024;
            $extension = $request->photo_url->getClientOriginalExtension();
            $extensions = ['jpg', 'jpeg', 'png'];
            $up = $request->photo_url->store('images');

            if ($size > 300) {
                return redirect('register')->with('feedback_error', 'O tamanho do arquivo não é compativel');
            }

            if (!in_array($extension, $extensions)) {
                return redirect('register')->with('feedback_error', 'A extensão do arquivo não é compativel');
            }

            if (!$up) {
                return redirect('register')->with('feedback_error', 'Erro ao enviar o arquivo');
            }

            $data = [
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'nickname' => $request->nickname,
                'birthday' => $request->birthday,
                'email' => $request->email,
                'password' => bcrypt($request->password),
                'short_bio' => $request->short_bio,
                'school_id' => $request->school_id,
                'school_start' => $request->school_start,
                'school_year' => $request->school_year,
                'photo_url' => $hashImage,
                'remember_token' => str_random(10)
            ];

            $user = User::create($data);

            if (!$user) {
                unlink(public_path('app/images') . $hashImage);
                return redirect('register')->with('feedback_error', 'Erro ao enviar o arquivo');
            }

            return redirect('register')->with('feedback_ok', 'Cadastrado com sucesso!');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function procurar(Request $request)
    {

        if ($request->select == "all") {
            return $this->findByAll();
        }

        if ($request->select == "school") {
            return $this->findBySchool($request->search);
        }

        if ($request->select == "classmate") {
            return $this->findByClassMate($request->search);
        }
    }

    public function findBySchool($search)
    {

        $users = School::with('user')
            ->where('name', 'like', "%$search%")
            ->get();

        return view('layouts.searchSchool', compact('users'));
    }

    public function findByClassMate($search)
    {

        return $users = User::join('school', 'school.id', 'user.school_id')
            ->where('user.first_name', 'like', "%$search%")
            ->groupBy('user.school_id')
            ->get();

//        foreach ($users as $user) {
//
//             $colegas = User::join('school', 'school.id', 'user.school_id')
////                ->where('id', $user->id)
//                ->get();
//
//             foreach ($colegas as $col) {
//                 return (string) $user->id === (string) $col->id;
//             }
//
//        }



        return view('layouts.searchClassmate');
    }


}
