<?php

namespace App\Http\Controllers;

use App\School;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function home()
    {
        $user = Auth::user();
        $school_id = $user->school_id;
        $school_start = $user->school_start;
        $school_year = $user->school_year;
        $school_user = DB::table('school')
            ->where('id', $user->school_id)
            ->get();

        $classmates = DB::table('user')
            ->where('school_id', $school_id)
            ->where('school_start', $school_start)
            ->where('school_year', $school_year)
            ->get();

        return view('layouts.index', compact('user', 'classmates', 'school_user'));
    }

    public function profile($id)
    {
        $classmate = User::with("schools")
            ->where('id', $id)
            ->get();


        return view('layouts.profile', compact('classmate'));
    }

    public function search()
    {
        return view('layouts.search');
    }

}
