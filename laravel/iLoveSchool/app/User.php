<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = "user";
    protected $fillable = [
        'nickname', 'first_name', 'last_name', 'email', 'password', 'photo_url', 'birthday', 'short_bio', 'school_id', 'school_start', 'school_year', 'remember_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function schools()
    {
        return $this->belongsTo('App\School', 'school_id');
    }
}
