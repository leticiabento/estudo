@extends('app')

@section('content')
    <div class="container">
        <div class="panel">
            <div class="panel-body">

                <div class="row">
                    <div class="col-sm-2">
                        <img src="{{asset('app/images')}}/{{$classmate[0]->photo_url}}" alt="" class="img-responsive img-circle" />
                    </div>
                    <div class="col-sm-10">
                        <h2>{{$classmate[0]->first_name}} {{$classmate[0]->last_name}} <small>{{$classmate[0]->nickname}}</small></h2>
                        <span>23 years old.</span>
                        <br />
                        <a href="mailto:{{$classmate[0]->email}}">{{$classmate[0]->email}}</a>
                        <br />
                        <br />
                        <strong>School</strong>
                        <br />
                        <span>{{$classmate[0]->schools->name}}</span>
                        <br />
                        <span>{{$classmate[0]->school_start}} - {{$classmate[0]->school_year}}</span>
                    </div>
                </div>
                <hr />

                <div class="row">
                    <div class="col-sm-12">
                        <p>"{{$classmate[0]->short_bio}}"</p>
                    </div>
                </div>

            </div>
        </div>

    </div>
@endsection
