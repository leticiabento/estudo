@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12">

                    <div class="row">
                        <h2>{{$users[0]->name}}</h2>
                        @foreach($users[0]->user as $user)
                        <div class="col-sm-2">
                            <div class="panel-body text-center">
                                <a href="{{route('profile', $user->id)}}">
                                    <img src="{{asset('app/images')}}/{{$user->photo_url}}" alt="" class="img-responsive img-circle">
                                    <h5>{{$user->first_name}} {{$user->last_name}}</h5>
                                </a>
                            </div>
                        </div>
                        @endforeach
                    </div>


            </div>
            <!--col-sm-12-->
        </div>
        <!--row-->
    </div>
@endsection
