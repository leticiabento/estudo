@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                @foreach($users as $user)
                    <div class="row">
                        <h2>School A</h2>
                        <div class="col-sm-2">
                            <div class="panel-body text-center">
                                <a href="profile.html">
                                    <img src="images/school_apple.jpg" alt="" class="img-responsive img-circle">
                                    <h5>John Doe</h5>
                                </a>
                            </div>
                        </div>

                    </div>
                @endforeach

            </div>
            <!--col-sm-12-->
        </div>
        <!--row-->
    </div>
@endsection
