<!doctype html>
<html class="no-js">

<head>
    <meta charset="utf-8">
    <title>I
        <3 School</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="{{asset("lib/css/bootstrap.min.css")}}">
    <link rel="stylesheet" href="{{asset("styles/main.css")}}">
</head>

<body>

<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="index.html">I <i class="glyphicon glyphicon-heart"></i> School</a>
        </div>


        <form class="navbar-form navbar-left" role="search" method="post" action="{{route('procurar')}}">

            {{csrf_field()}}
            <select name="select" class="form-control">
                <option value="all">All</option>
                <option value="school">School</option>
                <option value="classmate">Classmate</option>
            </select>

            &nbsp;

            <div class="form-group">
                <input type="text" name="search" class="form-control" size="65" placeholder="Search">
            </div>
            <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i> </button>
        </form>

        <div class="nav navbar-nav navbar-right">


            @guest
                <a href="{{route('register')}}">Sign up</a> &nbsp;
                <a href="{{route('login')}}" class="btn btn-primary navbar-btn">Log in</a>
            @else
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                        {{ Auth::user()->first_name }} <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu">
                        <li>
                            <a href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </li>
            @endguest
        </div>
    </div>
</nav>

@yield("content")


<script src="{{asset("lib/js/jquery-1.11.2.min.js")}}"></script>
<script src="{{asset("lib/js/bootstrap.min.js")}}"></script>
<script src="{{asset("scripts/main.js")}}"></script>
</body>

</html>