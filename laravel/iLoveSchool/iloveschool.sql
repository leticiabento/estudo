-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 14-Mar-2015 às 21:33
-- Versão do servidor: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `iloveschool`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `school`
--

CREATE TABLE IF NOT EXISTS `school` (
`id` int(11) NOT NULL,
  `name` varchar(155) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `school`
--

INSERT INTO `school` (`id`, `name`) VALUES
(1, 'Abraham Lincoln High School'),
(2, 'Academy of Arts & Sciences'),
(3, 'Balboa High School'),
(4, 'Downtown High School'),
(5, 'Galileo Academy of Science and Technology'),
(6, 'George Washington High School'),
(7, 'Ida B. Wells Continuation High School'),
(8, 'Independence High School'),
(9, 'International Studies Academy'),
(10, 'June Jordan School for Equity'),
(11, 'John A. O''Connell High School'),
(12, 'Lowell High School'),
(13, 'Mission High School'),
(14, 'Phillip & Sala Burton High School'),
(15, 'Raoul Wallenberg Traditional High School'),
(16, 'San Francisco Flex Academy'),
(17, 'San Francisco International High School'),
(18, 'School of the Arts (SOTA)'),
(19, 'Thurgood Marshall Academic High School');

-- --------------------------------------------------------

--
-- Estrutura da tabela `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`id` int(11) NOT NULL,
  `nickname` varchar(155) DEFAULT NULL,
  `first_name` varchar(155) NOT NULL,
  `last_name` varchar(155) NOT NULL,
  `email` varchar(155) NOT NULL,
  `password` varchar(255) NOT NULL,
  `photo_url` varchar(155) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `short_bio` text,
  `school_id` int(11) NOT NULL,
  `school_start` date NOT NULL,
  `school_year` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `school`
--
ALTER TABLE `school`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id`), ADD KEY `school_id` (`school_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `school`
--
ALTER TABLE `school`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `user`
--
ALTER TABLE `user`
ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`school_id`) REFERENCES `school` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
