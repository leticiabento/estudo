<?php

namespace FluidGallery;

use Illuminate\Database\Eloquent\Model;

class Create extends Model
{
    protected $table = "uploads";
    protected $fillable = ["title", "description", "image"];
}
