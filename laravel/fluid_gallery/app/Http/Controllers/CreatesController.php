<?php

namespace FluidGallery\Http\Controllers;

use FluidGallery\Create;
use Illuminate\Http\Request;

class CreatesController extends Controller
{
    private $create;

    public function __construct(Create $create)
    {
        $this->create = $create;
    }

    public function home()
    {
        $images = $this->create::all();
        return view("FluidGallery.index", compact("images"));
    }

    public function create()
    {
        return view("FluidGallery.create");
    }

    public function store(Request $request)
    {

//        dd($request);
        $infos = $request->all();
        $myImage = $request->image;
        $imagem = $request->image->hashName();
        $infos['image'] = $imagem;

        //image->nome do arquivo = verifica se existe um arquivo no submit do formulario
        if($request->hasFile('image')) {

            //images->nome da pasta
            $upload = $myImage->store('images');

            if($upload) {
                $this->create->create($infos);

                return redirect()->route("gallery.index");
            }

            return redirect()->route("gallery.index");



        }

        return redirect()->route("gallery.index");

    }


}
