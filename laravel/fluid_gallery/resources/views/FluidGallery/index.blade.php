@extends("app")

@section("content")
    <section class="container-imagens">

        @foreach($images as $image)
        <div class="imagem">
            <div class="texto">
                <h3>{{ $image->title }}</h3>
                <p>{{ $image->description }}</p>
            </div>
            <img src="{{ asset("images/images/")}}/{{$image->image}}" alt="">
        </div>
        @endforeach

    </section>
@endsection