<!DOCTYPE html>
<html>
<head>
    <title>FLUID GALLERY</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href=" {{ asset("css/app.css") }}">
    <link rel="stylesheet" href=" {{ asset("css/style.css") }}">
</head>

<body>

<ul class="menu">
    <li><a href="{{ route("gallery.index") }}">HOME</a></li>
    <li><a href="{{ route("gallery.create") }}">CADASTRO</a></li>
</ul>


    @yield("content")


</body>
</html>