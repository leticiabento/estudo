<?php


Route::get('/', function () {
    return view('FluidGallery.index');
});

Route::get("home", "CreatesController@home")->name("gallery.index");
Route::get("create", "CreatesController@create")->name("gallery.create");

Route::post("formCreate", "CreatesController@store")->name("gallery.form");