<?php

Route::get('/', 'ClassicController@home')->name('classic.index');
Route::get('about', 'ClassicController@about')->name('classic.about');
Route::get('blog', 'ClassicController@blog')->name('classic.blog');
Route::get('contact', 'ClassicController@contact')->name('classic.contact');