<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ClassicController extends Controller
{
    public function home()
    {
        return view("classic.index");
    }

    public function about()
    {
        return view("classic.about");
    }

    public function blog()
    {
        return view("classic.blog");
    }

    public function contact()
    {
        return view("classic.contact");
    }
}
