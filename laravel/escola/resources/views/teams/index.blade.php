@extends("app")

@section("content")

    @include("teams._feedback")

    <h1 class="page-header">Turmas</h1>

    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th>Turma</th>
            </tr>
            </thead>

            <tbody>

            @foreach($teams as $value)
                <tr>
                    <td>{{$value->id}}</td>
                    <td>{{$value->name}}</td>
                    <td>
                        <a href="{{ route('teams.destroy', $value->id) }}" class="btn btn-danger">Excluir</a>
                        <a href="{{ route('teams.edit', $value->id) }}" class="btn btn-warning">Editar</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection