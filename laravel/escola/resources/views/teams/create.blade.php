@extends("app")

@section("content")

    <form action="{{ route('teams.store') }}" method="post">

        {{ csrf_field() }}

        <div class="form-group">
            <label for="nome">Nome da turma</label>
            <input type="text" name="name" class="form-control" id="nome" placeholder="Nome da turma">
        </div>

        <button type="submit" class="btn btn-default">Cadastrar</button>
    </form>

@endsection