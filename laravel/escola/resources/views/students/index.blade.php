@extends("app")

@section("content")

    @include("students._feedback")

    <h1 class="page-header">Estudantes</h1>

    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th>Aluno</th>
                <th>Turma</th>
                <th>Ações</th>
            </tr>
            </thead>

            <tbody>

            @foreach($students as $value)
                <tr>
                    <td>{{$value->id}}</td>
                    <td>{{$value->estudante}}</td>
                    <td>{{$value->name}}</td>

                    <td>
                        <a href="{{ route('students.destroy', $value->id) }} " class="btn btn-danger">Excluir</a>
                        <a href=" {{ route('students.edit', $value->id ) }}" class="btn btn-warning">Editar</a>
                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>
    </div>
@endsection