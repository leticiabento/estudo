@extends("app")

@section("content")

    <form action="{{ route('students.store') }}" method="post">

        {{ csrf_field() }}

        <div class="form-group">
            <label for="nome">Nome do aluno</label>
            <input type="text" name="name" class="form-control" id="nome" placeholder="Seu nome, candango">
        </div>

        <div class="form-group">
            <label for="classroom">Turma</label>
            <select name="classroom_id" id="classroom" class="form-control">

                @foreach($teams  as $team)
                    <option value="{{ $team->id }}">{{ $team->name }}</option>
                @endforeach
            </select>
        </div>

        <button type="submit" class="btn btn-default">Cadastrar</button>
    </form>

@endsection