@extends("app")

@section("content")

    <form action="{{ route('students.update', $student->id) }}" method="post">

        {{ csrf_field() }}

        <div class="form-group">
            <label for="nome">Nome do aluno</label>
            <input type="text" name="name" class="form-control" id="nome" placeholder="Seu nome, candango">
        </div>

        <div class="form-group">
            <label for="classroom">Turma</label>
            <select name="classroom_id" id="classroom" class="form-control">
                <option value="{{ $student->idTurma }}">{{ $student->name }}</option>
            </select>
        </div>

        <button type="submit" class="btn btn-default">Atualizar</button>
    </form>

@endsection