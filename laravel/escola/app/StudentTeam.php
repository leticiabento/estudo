<?php

namespace School;

use Illuminate\Database\Eloquent\Model;

class StudentTeam extends Model
{
    protected $table = "studentsxteams";
    protected $fillable = ["students_id", "teams_id"];
}
