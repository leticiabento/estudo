<?php

namespace School\Http\Controllers;

use Illuminate\Http\Request;
use School\Team;

class TeamsController extends Controller
{
    private $team;
    public function __construct(Team $team)
    {
        $this->team = $team;
    }

    public function index()
    {
        $teams = $this->team->all();
        return view("teams.index", compact('teams'));
    }


    public function create()
    {
        return view("teams.create");
    }


    public function store(Request $request)
    {
        $team = $this->team->create($request->all());

        if(!$team) {
            return redirect("teams")->with("feedback_error", "Erro ao cadastrar uma thûrma");
        } else {
            return redirect("teams")->with("feedback_ok", "Sucesso ao cadastrar uma thûrma");
        }
    }


    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        $team = $this->team->find($id)->delete();

        if(!$team) {
            return redirect("teams")->with("feedback_error", "Erro ao apagar a thûrma");
        } else {
            return redirect("teams")->with("feedback_ok", "Sucesso ao apagar a thurma");
        }
    }
}
