<?php

namespace School\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use School\Student;
use School\StudentTeam;
use School\Team;

class StudentsController extends Controller
{

    private $student;

    public function __construct(Student $student)
    {
        $this->student = $student;
    }


    public function index()
    {
//        $students = $this->student->all();
        //students é a coleção q to buscando e envindo p index

        $students = DB::table("students")
            ->join("studentsxteams", "students.id", '=', "studentsxteams.students_id")
            ->join("teams", "teams.id", '=', "studentsxteams.teams_id")
            ->where("students.status", "=", 1)
            ->select("students.id", "students.name as estudante", "teams.name")
            ->get();

        return view("students.index", compact("students"));
    }

    public function create()
    {
        $teams = $this->buscarTurmas();
        return view("students.create", compact("teams"));
    }

    public function store(Request $request)
    {
        $student = $this->student->create($request->all());

        $student_id = $student->id;
        $classroom_id = $request->classroom_id;

        $relationship = StudentTeam::create(["students_id" => $student_id, "teams_id"=>$classroom_id]);

        if (!$student || !$relationship) {
            return redirect("students")->with("feedback_error", "Candango não registrado");
        } else {
            return redirect("students")->with("feedback_ok", "Candango registrado");
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {

        $student = DB::table("students")
            ->join("studentsxteams", "students.id", '=', "studentsxteams.students_id")
            ->join("teams", "teams.id", '=', "studentsxteams.teams_id")
            ->where("students.id", $id)
            ->select("students.id", "students.name as estudante", "teams.name", "teams.id as idTurma")
            ->get();

//        $student = $this->student->findOrFail($id);
//        return $student;
        return view("students.edit", ["student" => $student[0]]);
    }

    public function update(Request $request, $id)
    {
        $student = $this->student->find($id)->fill($request->all());
        $student->update();

        if (!$student) {
            return redirect("students")->with("feedback_error", "Candango não atualizado");
        } else {
            return redirect("students")->with("feedback_ok", "Candango atualizado");
        }
    }

    public function destroy($id)
    {
        $student = $this->student->find($id);
        $student->status = 0;
        $student->save();


        if (!$student) {
            return redirect("students")->with("feedback_error", "Candango não deletado");
        } else {
            return redirect("students")->with("feedback_ok", "Candango deletado com sucesso");
        }
    }

    private function buscarTurmas()
    {
        return Team::all();

    }


}
