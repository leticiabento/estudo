<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('app');
});


Route::resource("students", "StudentsController", [
    "except" => ["destroy", "update"]
]);

Route::get("students/destroy/{id}", "StudentsController@destroy")->name("students.destroy");
Route::post("students/{id}", "StudentsController@update")->name("students.update");

Route::resource("teams", "TeamsController");