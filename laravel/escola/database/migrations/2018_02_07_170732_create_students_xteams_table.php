<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsXteamsTable extends Migration
{

    public function up()
    {
        Schema::create('studentsXteams', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('students_id')->unsigned();
            $table->foreign('students_id')->references('id')->on('students');

            $table->integer('teams_id')->unsigned();
            $table->foreign('teams_id')->references('id')->on('teams');

            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('studentsXteams');
    }
}
