<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Dashboard Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="{{asset('css/app.css')}}" rel="stylesheet">


    <![endif]-->
</head>

<body>

<div id="navbar" class="navbar-collapse " style="background: black">
    <ul class="nav navbar-nav navbar-right">
        <li><a href="{{route('home')}}">Dashboard</a></li>
        <li><a href="{{route('create')}}">Create Movie</a></li>
        <li><a href="{{route('createCinema')}}">Create Cinema</a></li>

    </ul>
    <form class="navbar-form navbar-right">
        <input class="form-control" placeholder="Search..." type="text">
    </form>
</div>

<div class="container-fluid">
    @yield("content")
</div>

<script src="{{asset('js/jquery.min.js')}}"></script>
<script src="{{asset('js/app.js')}}"></script>

</body>
</html>
