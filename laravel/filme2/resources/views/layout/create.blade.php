@extends("app")
@section("content")

    @include("layout._feedback")

    <div class="container padding">
        <form method="post" action="{{route("storeMovie")}}" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="form-group">
                <label for="titulo">Titulo</label>
                <input type="text" class="form-control" id="titulo" placeholder="Titulo" name="title">
            </div>

            <div class="form-group">
                <label for="duracao">Duração</label>
                <input type="text" class="form-control" id="duracao" placeholder="Duração" name="duration">
            </div>

            <div class="form-group">
                <label for="impropriedade">Impropriedade</label>
                <input type="number" class="form-control" id="impropriedade" placeholder="Impropriedade" name="improprietion">
            </div>

            <div class="form-group">
                <label for="pais">País</label>
                <input type="text" class="form-control" id="pais" placeholder="País" name="country">
            </div>

            <div class="form-group">
                <label for="diretor">Diretor</label>
                <select name="director_id" id="diretor" class="form-control">
                    <option value="">Selecione</option>
                    @foreach($directors as $director)
                        <option value="{{$director->id}}">{{$director->name}}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="type">Tipo</label>
                <select name="type_id" id="type" class="form-control">
                    <option value="">Selecione</option>
                    @foreach($types as $type)
                        <option value="{{$type->id}}">{{$type->description}}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="sort">Gênero</label>
                <select name="sort_id" id="sort" class="form-control">
                    <option value="">Selecione</option>
                    @foreach($sorts as $sort)
                        <option value="{{$sort->id}}">{{$sort->description}}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="cinema">Cinema</label>
                <select name="cinema_id" id="cinema" class="form-control">
                    <option value="">Selecione</option>
                    @foreach($cinemas as $cine)
                        <option value="{{$cine->id}}">{{$cine->fantasy_name}}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="sessao">Horário da sessão</label>
                <input type="text" class="form-control" id="sessao" placeholder="Horário da sessão" name="sessao">
            </div>


            <div class="form-group">
                <label for="imagem">Imagem</label>
                <input type="file" id="imagem" name="image">
                {{--<p class="help-block">Example block-level help text here.</p>--}}
            </div>

            <button type="submit" class="btn btn-success">Submit</button>
        </form>
    </div>
@endsection