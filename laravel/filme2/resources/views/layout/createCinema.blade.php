@extends("app")
@section("content")

    @include("layout._feedback")

    <div class="container padding">
        <form method="post" action="{{route("storeCinema")}}" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="form-group">
                <label for="fantasy_name">Nome Fantasia</label>
                <input type="text" class="form-control" id="fantasy_name" placeholder="Nome Fantasia" name="fantasy_name">
            </div>

            <div class="form-group">
                <label for="capacity">Capacidade</label>
                <input type="text" class="form-control" id="capacity" placeholder="Capacidade" name="capacity">
            </div>

            <div class="form-group">
                <label for="address">Address</label>
                <input type="text" class="form-control" id="address" placeholder="Address" name="address">
            </div>

            <div class="form-group">
                <label for="number">Número</label>
                <input type="text" class="form-control" id="number" placeholder="Número" name="number">
            </div>

            <div class="form-group">
                <label for="complement">Complemento</label>
                <input type="text" class="form-control" id="complement" placeholder="Complemento" name="complement">
            </div>

            <div class="form-group">
                <label for="state">Estado</label>
                <input type="text" class="form-control" id="state" placeholder="Estado" name="state">
            </div>

            <button type="submit" class="btn btn-success">Submit</button>
        </form>
    </div>
@endsection