<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('app');
//});

Route::get('/', "MoviesController@index")->name('home');
Route::get('/cadastro', "MoviesController@create")->name('create');
Route::post('/cadastro', "MoviesController@storeMovie")->name("storeMovie");


Route::get('/cadastro/cinema', "CinemasController@create")->name('createCinema');
Route::post('/cadastro/cinema', "CinemasController@storeCinema")->name('storeCinema');