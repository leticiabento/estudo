<?php

namespace App\Http\Controllers;

use App\Address;
use App\Cinema;
use Illuminate\Http\Request;

class CinemasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('layout.createCinema');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function storeCinema(Request $request)
    {
        $dataCinema = [
            "fantasy_name" => $request->fantasy_name,
            "capacity" => $request->capacity
        ];

        $cinema = Cinema::create($dataCinema);

        if (!$cinema) {
            return redirect('/cadastro/cinema')->with('feedback_error', "Erro ao cadastrar a imagem");
        }

        $dataAdress = [
            "address" => $request->address,
            "number" => $request->number,
            "complement" => $request->complement,
            "state" => $request->state,
            "cinema_id" => $cinema->id
        ];

        $address = Address::create($dataAdress);

        if (!$address) {
            return redirect('/cadastro/cinema')->with('feedback_error', "Erro ao cadastrar a imagem");
        }

        return redirect("/");
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
