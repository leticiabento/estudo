<?php

namespace App\Http\Controllers;

use App\Cinema;
use App\Director;
use App\Movie;
use App\MovieCinema;
use App\MovieSort;
use App\MovieType;
use App\Sort;
use App\Type;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MoviesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $movies = Movie::with('cinemas', 'sorts', 'types', 'director')->get();
        return $movies;
        return view('layout.index', compact("movies"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $directors = Director::get();
        $types = Type::all();
        $sorts = Sort::all();
        $cinemas = Cinema::all();

        return view('layout.create', compact("directors", "types", "sorts", "cinemas"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function storeMovie(Request $request)
    {
        $infos = $request->all();
        $imagem = $request->image->hashName();
        $type = $request->type_id;
        $sort = $request->sort_id;
        $cinema = $request->cinema_id;

        $infos['image'] = $imagem;


        if ($request->hasFile('image')) {
            $up = $request->image->store('imgs');

            if (!$up) {
                return redirect('/cadastro')->with('feedback_error', "Erro ao cadastrar a imagem");
            }

            $create = Movie::create($infos);

            if (!$create) {
                return redirect('/cadastro')->with('feedback_error', "Erro ao cadastrar a imagem");
            }

            $relationshipMovieType = [
                "type_id" => $type,
                "movie_id" => $create->id
            ];

            $relationshipMovieSort = [
                "sort_id" => $sort,
                "movie_id" => $create->id
            ];

            $relationshipMovieCinema = [
                "cinema_id" => $cinema,
                "movie_id" => $create->id,
                "sessao" => $request->sessao
            ];


            $createRelationshipMovieType = MovieType::create($relationshipMovieType);
            $createRelationshipMovieSort = MovieSort::create($relationshipMovieSort);
            $createRelationshipMovieCinema = MovieCinema::create($relationshipMovieCinema);

            if (!$createRelationshipMovieType && !$createRelationshipMovieSort && !$createRelationshipMovieCinema) {
                return redirect('/cadastro')->with('feedback_error', "Erro ao cadastrar a imagem");
            }

            return redirect('/');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
