<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sort extends Model
{
    protected $table = "sort";
    protected $fillable = ["description"];

    public function movies()
    {
        return $this->belongsToMany('App\Movie', 'moviessorts', 'sort_id', 'movie_id');
    }
}
