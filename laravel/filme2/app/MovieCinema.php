<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MovieCinema extends Model
{
    protected $table = "moviescinemas";
    protected $fillable = ["sessao", "movie_id", "cinema_id"];


}
