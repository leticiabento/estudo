<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cinema extends Model
{
    protected $table = "cinema";
    protected $fillable = ["fantasy_name", "capacity"];

    public function movies()
    {
        return $this->belongsToMany('App\Movie', 'moviescinemas', 'movie_id', 'cinema_id');
    }
}
