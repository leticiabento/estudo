<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    protected $table = "movie";
    protected $fillable = ["title", "duration", "improprietion", "country", "image", "director_id"];

    public function director()
    {
        return $this->belongsTo('App\Director', 'director_id');
    }

    public function sorts()
    {
        return $this->belongsToMany('App\Sort', 'moviessorts', 'movie_id', 'sort_id');
    }

    public function types()
    {
        return $this->belongsToMany('App\Type', 'moviestypes', 'movie_id', 'type_id');
    }

    public function cinemas()
    {
        return $this->belongsToMany('App\Cinema', 'moviescinemas', 'movie_id', 'cinema_id');
    }
}
