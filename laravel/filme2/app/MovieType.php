<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MovieType extends Model
{
    protected $table = "moviestypes";
    protected $fillable = ["type_id", "movie_id"];

    public function movie()
    {
        return $this->belongsTo('App\Movie');
    }

    public function type()
    {
        return $this->belongsTo('App\Type');
    }
}
