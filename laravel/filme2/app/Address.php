<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $table = "address";
    protected $fillable = ["address", "number", "complement", "state", "cinema_id"];

    public function cinema()
    {
        return $this->hasOne('App\Cinema');
    }
}
