<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    protected $table = "type";
    protected $fillable = ["description"];

    public function movies()
    {
        return $this->belongsToMany('App\Movie', 'moviestypes', 'type_id', 'movie_id');
    }
}
