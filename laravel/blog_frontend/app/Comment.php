<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = "comments";
    protected $fillable = ["name", "comment", "post_id"];

    public function posts()
    {
        return $this->hasMany('App\Post', 'post_id');
    }
}
