<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; //mudar p true
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|max:255',
            'image' => 'required|image',
            'category_id' => 'required',
            'name' => 'required',
            'description' => 'required|string|max:5000'
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Este campo é obrigatorio',
            'image.required' => 'Este campo é obrigatorio',
            'category_id.required' => 'Este campo é obrigatorio',
            'name.required' => 'Este campo é obrigatorio',
            'description.required' => 'Este campo é obrigatorio'
        ];
    }
}
