<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\PostsRequest;
use App\Post;
use App\Tag;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PostsController extends Controller
{
    private $post;

    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    public function index()
    {

        $posts = Post::with('user', 'category', 'comment')
            ->orderBy('id', 'DESC')
            ->simplePaginate(3);

        $order = $this->orders();
        $categories = $this->categories();

        $tags = $this->tags();

        return view('layout.index', compact("posts", "order", "categories", "tags"));
    }

    public function create()
    {
        $order = $this->orders();
        $categories = $this->categories();
        $tags = $this->tags();

        return view('layout.create', compact("categories", "order", "tags"));
    }


    public function show($id)
    {
        $post = $this->post->with("user", "comment")->find($id);
        $this->updateViews($id);
        $order = $this->orders();
        $categories = $this->categories();

//        $post['user']['totalPosts'] = Post::where('user_id', $post->id)->count();

        $tags = $this->tags();
        return view('layout.post', compact("post", "order", "categories", "tags"));
    }

    private function updateViews($id)
    {
        DB::table('posts')
            ->where('id', $id)
            ->increment('views');
    }

    private function orders()
    {
        $order = Post::orderBy('views', 'DESC')
            ->limit(3)
            ->get();


        return $order;
    }

    private function tags()
    {
        $tags = DB::table('tags')
            ->get();

        return $tags;
    }

    private function categories()
    {
        $array = [];
        $categories = Category::select('id', 'name')->get();

        foreach ($categories as $category) {
            $count = Post::where('category_id', $category->id)->count();

            array_push($array, [
                'id' => $category->id,
                'name' => $category->name,
                'count' => $count
            ]);
        }

        return $array;
    }

    public function store(PostsRequest $request)
    {

        $myImage = $request->image;
        $hashImage = $myImage->hashName();

        if ($request->hasFile('image')) {

            $up = $myImage->store('image');

            $post = [
                'title' => $request->title,
                'category_id' => $request->{'category_id'},
                'image' => $hashImage,
                'user_id' => Auth::user()->id,
                'description' => $request->description
            ];


            if ($up) {
                $createPost = $this->post->create($post);
                $post_id = $createPost->id;

                $tags = explode(", ", $request->name);
                $variables = [];

                foreach ($tags as $tag) {
                    $data = [
                        "name" => $tag
                    ];

                    $createNameTags = Tag::updateOrCreate(["name" => $tag], $data);
                    $tag_id = $createNameTags->id;

                    array_push($variables, [
                        "post_id" => $post_id,
                        "tag_id" => $tag_id
                    ]);


                }

                $createTags = $createPost->tags()->attach($variables);


                if (is_null($createTags)) {
                    return redirect("home")->with('feedback_ok', 'Cadastrado com sucesso');
                } else {
                    return redirect("home")->with('feedback_error', 'Erro ao cadastrar. Tente novamente!');

                }

            }
        }

    }

    public function search(Request $request)
    {
        $name = $request->name;

        $posts = Post::with('user', 'tags', 'category')
            ->where('title', 'like', "%$name%")
            ->simplePaginate(3);


        $order = $this->orders();
        $categories = $this->categories();

        $tags = $this->tags();

        return view('layout.search', compact("posts", "order", "categories", "tags"));
    }

}
