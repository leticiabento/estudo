<?php

namespace App\Http\Controllers;

use App\Category;
use App\Comment;
use App\Post;
use App\PostTag;
use App\Tag;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TagsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $posts = Tag::with('posts')
            ->where('id', $id)
            ->simplePaginate(3);

//        $data = [];
//
//        $posts = [];
//        $data = Tag::join('post_tags', 'tags.id', 'post_tags.tag_id')
//            ->join('posts', 'posts.id', 'post_tags.post_id')
//            ->select('posts.id', 'posts.title', 'posts.image', 'posts.description', 'posts.created_at', 'posts.user_id')
//            ->distinct('tags.name')
//            ->where('post_tags.tag_id', $id)
//            ->get();
//
//        foreach($data as $post) {
//            $post['user'] = array_get(
//                User::where('id', $post->user_id)
//                ->orderBy('created_at', 'desc')
//                ->get(), 0
//            );
//
//            $post['comments'] = Comment::where('post_id', $post->id)
//                ->orderBy('created_at', 'desc')
//                ->get();
//
//            $post['tags'] = Post::find($post->id)->tags;
//
//            array_push($posts, $post);
//
//        }
//
//
        $order = $this->orders();
        $categories = $this->categories();
        $tags = $this->tags();

        return view('layout.tags', compact( "posts", "order", "categories", "tags"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function orders()
    {

        $order = Post::orderBy('views', 'DESC')
            ->limit(3)
            ->get();


        return $order;
    }

    private function tags()
    {
        $tags = DB::table('tags')
            ->get();

        return $tags;
    }

    private function categories()
    {
        $array = [];
        $categories = Category::select('id', 'name')->get();

        foreach ($categories as $category) {
            $count = Post::where('category_id', $category->id)->count();

            array_push($array, [
                'id' => $category->id,
                'name' => $category->name,
                'count' => $count
            ]);
        }

        return $array;
    }
}
