<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $posts = Post::with('user', 'category', 'comment')
            ->where('category_id', $id)
            ->simplePaginate(3);

        $order = $this->orders();
        $categories = $this->categories();

        $tags = $this->tags();

        return view('layout.index', compact("posts", "order", "categories", "tags"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    private function orders()
    {
//        $order = DB::table('posts')
//            ->orderBy('views', 'DESC')
//            ->limit(3)
//            ->get();
//
//        return $order;
        $order = Post::orderBy('views', 'DESC')
            ->limit(3)
            ->get();


        return $order;
    }

    private function tags()
    {
        $tags = DB::table('tags')
            ->get();

        return $tags;
    }

    private function categories()
    {
        $array = [];
        $categories = Category::select('id', 'name')->get();

        foreach ($categories as $category) {
            $count = Post::where('category_id', $category->id)->count();

            array_push($array, [
                'id' => $category->id,
                'name' => $category->name,
                'count' => $count
            ]);
        }

        return $array;
    }
}
