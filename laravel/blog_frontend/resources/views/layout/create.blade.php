@extends("app")
@section("content")

    @include("layout._feedback")

    <form action="{{route('post.store')}}" method="POST" class="formCadastro" enctype="multipart/form-data">
        {{csrf_field()}}

        <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
            <label for="titulo">Título</label>
            <input type="text" name="title" id="titulo" class="form-control">

            @if ($errors->has('title'))
                <span class="help-block">
                    <strong>{{ $errors->first('title') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group {{ $errors->has('image') ? ' has-error' : '' }}">
            <label for="image">Imagem</label>
            <input type="file" name="image" id="image" class="form-control">
            @if ($errors->has('image'))
                <span class="help-block">
                    <strong>{{ $errors->first('image') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group {{ $errors->has('category_id') ? ' has-error' : '' }}">
            <label for="categoria">Categorias</label>
            <select name="category_id" id="categoria" class="form-control">
                <option value="" selected>Selecione</option>
                @foreach($categories as $cat)
                    <option value="{{$cat['id']}}">{{$cat['name']}}</option>
                @endforeach
            </select>

            @if ($errors->has('category_id'))
                <span class="help-block">
                    <strong>{{ $errors->first('category_id') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="tag">Tag</label>
            <input type="text" name="name" id="tag" class="form-control">

            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>


        <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
            <label for="description">Descrição</label>
            <textarea name="description" id="description" rows="10" class="form-control"></textarea>

            @if ($errors->has('description'))
                <span class="help-block">
                    <strong>{{ $errors->first('description') }}</strong>
                </span>
            @endif
        </div>

        <button type="submit" class="btn btn-lg btn-success">Postar</button>
    </form>
@endsection

@section('js')
    <script src="{{ asset('js/app.js') }}"></script>
@endsection