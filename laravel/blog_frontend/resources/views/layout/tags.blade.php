@extends("app")
@section("content")


    {{--    {{dd($posts[0]->posts)}}--}}

    @if($posts[0]->posts->count() == 0)
        <p>Nenhum post encontrado!</p>
    @endif

    @foreach($posts[0]->posts as $post)
        <article class="post-item">
            <div class="post-item-image">
                <a href="{{ route("post.show", $post->id) }}">
                    <img src="{{asset("app/image/".$post->image)}}" alt="">
                </a>
            </div>
            <div class="post-item-body">
                <div class="padding-10">
                    <h2><a href="{{ route("post.show", $post->id) }}">{{$post->title}}</a></h2>
                    <p>{{str_limit($post->description, 270, "...")}}</p>
                </div>

                <div class="post-meta padding-10 clearfix">
                    <div class="pull-left">
                        <ul class="post-meta-group">
                            <li><i class="fa fa-user"></i><a href="#"> {{$post->user->name}}</a></li>
                            <li><i class="fa fa-clock-o"></i>
                                <time> {{$post->created_at->format('F d, Y') }}</time>
                            </li>
                            <li><i class="fa fa-tags"></i><a href="#"> {{$post->category->name}}</a></li>
                            <li><i class="fa fa-comments"></i><a href="#"> {{count($post->comment)}} Comments</a></li>
                        </ul>
                    </div>
                    <div class="pull-right">
                        <a href="{{ route("post.show", $post->id) }}">Continue Reading &raquo;</a>
                    </div>
                </div>
            </div>
        </article>
    @endforeach


    <nav>
        {{$posts->links()}}
    </nav>
@endsection

@section("js")
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{asset("js/jquery.min.js")}}"></script>
    <script>
        $(function () {
            $(".pagination").addClass('pager');
            $(".pagination").removeClass('pagination');
            $(".pager li:first-child").addClass('previous');
            $(".pager li:last-child").addClass('next');
        })
    </script>

@endsection