<aside class="right-sidebar">
    <div class="search-widget">
        <form action="{{route('posts.search')}}" method="POST">
            {{csrf_field()}}

            <div class="input-group">
                <input type="text" name="name" class="form-control input-lg" placeholder="Search for...">
                <span class="input-group-btn">
                            <button class="btn btn-lg btn-default" type="submit">
                                <i class="fa fa-search"></i>
                            </button>
                          </span>
            </div><!-- /input-group -->
        </form>
    </div>

    <div class="widget">
        <div class="widget-heading">
            <h4>Categories</h4>
        </div>

        <div class="widget-body">


            <ul class="categories">
                @foreach($categories as $category)
                    <li>
                        <a href="{{route('category.show', $category['id'])}}"><i
                                    class="fa fa-angle-right"></i> {{$category['name']}}</a>
                        <span class="badge pull-right">{{$category['count']}}</span>
                    </li>
                @endforeach

            </ul>
        </div>
    </div>

    <div class="widget">

        <div class="widget-heading">
            <h4>Popular Posts</h4>
        </div>
        <div class="widget-body">
            <ul class="popular-posts">

                @foreach($order as $or)
                    <li>
                        <div class="post-image">
                            <a href="{{route("post.show", $or->id)}}">
                                <img src="{{asset("img/Post_Image_5_thumb.jpg")}}"/>
                            </a>
                        </div>
                        <div class="post-body">
                            <h6><a href="{{route("post.show", $or->id)}}">{{str_limit($or->title, 20)}}</a></h6>
                            <div class="post-meta">
                                <span>{{$or->created_at->diffForHumans()}}</span>
                            </div>
                        </div>
                    </li>
                @endforeach

            </ul>
        </div>
    </div>

    <div class="widget">
        <div class="widget-heading">
            <h4>Tags</h4>
        </div>
        <div class="widget-body">
            <ul class="tags">


                @foreach($tags as $tag)
                    <li><a href="{{route('tag.show', $tag->id)}}">{{ $tag->name }}</a></li>
                @endforeach


            </ul>
        </div>
    </div>
</aside>