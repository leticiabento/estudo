@if(session('feedback_ok'))
    <div class="alert alert-success">
        <p>{{session('feedback_ok')}}</p>
    </div>
@endif

@if(session('feedback_error'))
    <div class="alert alert-danger">
        <p>{{session('feedback_error')}}</p>
    </div>
@endif