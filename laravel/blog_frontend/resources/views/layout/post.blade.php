@extends("app")
@section("content")

    <article class="post-item post-detail">
        <div class="post-item-image">
            <a href="#">
                <img src="{{asset("app/image")}}/{{$post->image}}" alt="">
            </a>
        </div>

        <div class="post-item-body">
            <div class="padding-10">
                <h1>{{$post->title}}</h1>

                <div class="post-meta no-border">
                    <ul class="post-meta-group">
                        <li><i class="fa fa-user"></i><a href="#"> {{$post->user->name}}</a></li>
                        <li><i class="fa fa-clock-o"></i>
                            <time> {{$post->created_at->format('F d, Y') }}</time>
                        </li>
                        <li><i class="fa fa-tags"></i><a href="#"> {{$post->category->name}}</a></li>
                        <li><i class="fa fa-comments"></i><a href="#">{{count($post->comment)}} Comments</a></li>
                    </ul>
                </div>

                <p>{{$post->description}}</p>

            </div>
        </div>


    </article>

    <article class="post-author padding-10">
        <div class="media">
            <div class="media-left">
                <a href="#">
                    <img alt="Author 1" src="{{ asset("app")}}/{{ $post->user->image}}" class="media-object">
                </a>
            </div>
            <div class="media-body">
                <h4 class="media-heading"><a href="#">{{ $post->user->name }}</a></h4>
                <div class="post-author-count">
                    <a href="#">
                        <i class="fa fa-clone"></i>
                        {{$post->user->totalPosts}} posts
                    </a>
                </div>
                <p>{{ $post->user->description }}</p>
            </div>
        </div>
    </article>

    <article class="post-comments">
        <h3><i class="fa fa-comments"></i> {{count($post->comment)}} Comments</h3>

        <div class="comment-body padding-10">
            <ul class="comments-list">

                @foreach($post->comment as $comment)
                    <li class="comment-item">
                        <div class="comment-heading clearfix">
                            <div class="comment-author-meta">
                                <h4>{{$comment->name}}
                                    <small>{{$comment->created_at->format('F d, Y')}}</small>
                                </h4>
                            </div>
                        </div>
                        <div class="comment-content">
                            <p>{{$comment->comment}}</p>

                        </div>
                    </li>
                @endforeach

            </ul>

        </div>

        <div class="comment-footer padding-10">
            <h3>Leave a comment</h3>

            @include("layout._feedback")

            <form action="{{ route("comment.store" ) }}" method="POST">

                {{csrf_field()}}

                <input type="hidden" name="post_id" value="{{$post->id}}">

                <div class="form-group required">
                    <label for="name">Name</label>
                    <input type="text" name="name" id="name" class="form-control">
                </div>

                <div class="form-group required">
                    <label for="comment">Comment</label>
                    <textarea name="comment" id="comment" rows="6" class="form-control"></textarea>
                </div>

                <div class="clearfix">
                    <div class="pull-left">
                        <button type="submit" class="btn btn-lg btn-success">Submit</button>
                    </div>
                    <div class="pull-right">
                        <p class="text-muted">
                            <span class="required">*</span>
                            <em>Indicates required fields</em>
                        </p>
                    </div>
                </div>
            </form>
        </div>

    </article>
@endsection

@section('js')
    <script src="{{ asset('js/app.js') }}"></script>
@endsection