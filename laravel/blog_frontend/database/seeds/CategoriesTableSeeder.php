<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['name' => 'Web Development'],
            ['name' => 'Web Design'],
            ['name' => 'General'],
            ['name' => 'DIY'],
            ['name' => 'Facebook Development']
        ];

        DB::table('categories')->insert($data);
    }
}
