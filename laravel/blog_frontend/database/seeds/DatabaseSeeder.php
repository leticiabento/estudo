<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory('App\User', 2)->create();

        $this->call(CategoriesTableSeeder::class);

        factory('App\Post', 50)->create();

        factory('App\Tag', 30)->create();

        factory('App\PostTag', 20)->create();

        factory('App\Comment', 200)->create();


    }
}
