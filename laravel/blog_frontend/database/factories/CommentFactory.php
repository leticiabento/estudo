<?php

use Faker\Generator as Faker;

$factory->define(App\Comment::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'comment' => $faker->text(300),
        'post_id' => $faker->numberBetween(1, 50)
    ];
});
