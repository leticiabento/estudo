<?php

use Faker\Generator as Faker;

$factory->define(App\Post::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'image' => 'eb70448deebfcaf99afdb20d797130a5.jpg',
        'description' => $faker->text(1000),
        'views' => $faker->numberBetween(0, 100),
        'user_id' => $faker->numberBetween(1, 2),
        'category_id' => $faker->numberBetween(1, 5)
    ];
});
