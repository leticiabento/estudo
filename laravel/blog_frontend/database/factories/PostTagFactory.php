<?php

use Faker\Generator as Faker;

$factory->define(App\PostTag::class, function (Faker $faker) {
    return [
        "post_id" => $faker->numberBetween(1, 50),
        "tag_id" =>$faker->numberBetween(1, 30)
    ];
});
