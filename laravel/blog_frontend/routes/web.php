<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('layout.index');
//});
Route::get('/', "PostsController@index");
Route::get('post/create', "PostsController@create")->name('posts.create');
Route::post('post/search', "PostsController@search")->name('posts.search');
Route::get('post/{id}', "PostsController@show")->name('post.show');

Route::get('category/{id}', "CategoriesController@show")->name('category.show');
Route::get('tags/{id}', "TagsController@show")->name('tag.show');

Route::post('comment/store', "CommentsController@store")->name('comment.store');
Route::post('post/store', "PostsController@store")->name('post.store');

Auth::routes();

Route::get('/home', 'PostsController@create')->name('home');
