<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $table = "itens";
    protected $primaryKey = ['id', 'sale_id', 'product_id'];
    protected $fillable = ['quantity', 'total', 'sale_id', 'product_id'];

    public function sale()
    {
        return $this->belongsTo('App\Sale', 'sale_id');
    }

    public function product()
    {
        return $this->belongsTo('App\Product', 'product_id');
    }
}
