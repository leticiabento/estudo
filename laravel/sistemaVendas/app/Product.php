<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    protected $fillable = ['name', 'unit', 'price'];

    public function sales()
    {
        return $this->belongsToMany('App\Sale', 'itens', 'product_id', 'sale_id');
    }
}
