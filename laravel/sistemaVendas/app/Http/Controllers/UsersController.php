<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UsersController extends Controller
{

    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function index()
    {
        return view('layout.index');
    }


    public function create()
    {
        return view('layout.incluirClientes');
    }


    public function store(Request $request)
    {
        $user = $request->all();
        $user['remember_token'] = str_random(10);

        $users = $this->user->create($user);

        if ($users) {
            return redirect()->route('client.list');
        }

        return redirect('clientes/incluir')->with('feedback_error', 'Usuário não cadastrado. Tente novamente!');

    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $user = $this->user->find($id);
        return view("layout.alterarClientes", compact("user"));
    }


    public function update(Request $request, $id)
    {
        $user = $this->user->find($id)->fill($request->all());
        $user->update();

        if ($user) {
            return redirect('clientes/listar')->with('feedback_ok', 'Usuário alterado com sucesso.');
        }

        return redirect('clientes/editar/{id}')->with('feedback_error', 'Usuário não alterado. Tente novamente mais tarde!');
    }


    public function destroy($id)
    {
        $user = $this->user->find($id);
        $user->delete();

        if ($user) {
            return redirect('clientes/listar')->with('feedback_ok', 'Usuário apagado com sucesso');
        }

        return redirect('clientes/listar')->with('feedback_error', 'Usuário não apagado. Tente novamente!');


    }

    public function list()
    {
        $users = $this->user->all();
        return view('layout.listarClientes', compact("users"));
    }

    public function search()
    {
        return view('layout.pesquisarClientes');
    }

    public function find(Request $request)
    {
        $nome = $request->name;
        $cpf = $request->cpf;
        $users = DB::table('users')
            ->whereOr('name', 'like', "%$nome%")
            ->where('cpf', '=', $cpf)
            ->get();

//        $user = $this->user->whereOr('name', $nome)
//            ->whereOr('cpf', '=', $cpf)
//            ->get();

        if(empty($users)) {
            return redirect('clientes/listar')->with('feedback_error', 'Usuário não encontrado. Tente novamente!');
        }

        return view('layout.listarClientes', compact("users"));



    }
}
