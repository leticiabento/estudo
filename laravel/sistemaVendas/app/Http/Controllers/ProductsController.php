<?php

namespace App\Http\Controllers;


use App\Product;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    private $product;
    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function index()
    {
        //
    }

    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        $product = $request->all();
        $produto = $this->product->create($product);

        if($produto) {
            return redirect()->route('product.list');
        }

        return redirect('produtos/incluir')->with('feedback_error', 'Produto não cadastrado. Tente novamente!');
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $produto = $this->product->find($id);
        return view("layout.alterarProdutos", compact("produto"));
    }

    public function update(Request $request, $id)
    {
        $produto = $this->product->find($id)->fill($request->all());
        $produto->update();

        if($produto) {
            return redirect()->route('product.list');
        }

        return redirect('produtos/editar/{id}')->with('feedback_error', 'Produto não alterado. Tente novamente!');
    }


    public function destroy($id)
    {
        $produto = $this->product->find($id);
        $produto->delete();

        if($produto) {
            return redirect()->route('product.list');
        }

        return redirect('produtos/incluir')->with('feedback_error', 'Produto não cadastrado. Tente novamente!');
    }

    public function list()
    {
        $produtos = $this->product->all();
        return view("layout.listarProdutos", compact("produtos"));
    }

    public function include()
    {
        return view("layout.incluirProdutos");
    }
}
