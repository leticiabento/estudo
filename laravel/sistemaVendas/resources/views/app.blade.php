<!DOCTYPE HTML >
<html>
<head>
    <meta charset="UTF-8">
    <title>Sistema de Vendas</title>
</head>
<body>
    <h1>Sistema de Vendas</h1>
    <hr>
    <a href="{{ route('index') }}">Home</a>&nbsp;&nbsp;&nbsp;&nbsp;
    <a href="{{ route('client.search') }}">Listar Clientes</a>&nbsp;&nbsp;&nbsp;&nbsp;
    <a href="{{ route('client.include') }}">Incluir Cliente</a>&nbsp;&nbsp;&nbsp;&nbsp;
    <a href="{{ route('product.list') }}">Listar Produtos</a>&nbsp;&nbsp;&nbsp;&nbsp;
    <a href="{{ route('reports.sale') }}">Relatório de Vendas</a>&nbsp;&nbsp;&nbsp;&nbsp;
    <a href="grafico.html">Gráfico de Faturamento</a>&nbsp;&nbsp;&nbsp;&nbsp;
    <hr>

    @yield("content")
</body>
</html>
