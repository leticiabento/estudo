@extends('app')
@section("content")
    <form>
        <table border="0">
            <tbody>
            <tr>
                <td>Por:</td>
                <td><select name="por">
                        <option>Cliente</option>
                        <option>Produto</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Início:</td>
                <td><input type="text" name="inim" value="" size="2" /> / <input type="text" name="inia" value="" size="6" />
                </td>
            </tr>
            <tr>
                <td>Fim:</td>
                <td><input type="text" name="fimm" value="" size="2" /> / <input type="text" name="fima" value="" size="6" />
                </td>
            </tr>
            <tr>
                <td>Ordenação:</td>
                <td><select name="ord">
                        <option>crescente</option>
                        <option>decrescente</option>
                        <option>alfabetica</option>
                    </select>
                </td>
            </tr>
            </tbody>
        </table>
        <br>
        <input type="button" onClick="javascript:parent.location='relatorio_produto.html'" value="Gerar" name="gerar" />
        <br><br><a href="{{ route("reports.client") }}">por cliente</a>
    </form>
@endsection