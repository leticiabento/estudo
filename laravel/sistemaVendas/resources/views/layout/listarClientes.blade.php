@extends("app")
@section('content')

    @include("layout._feedback")

    <table border="0">
        <thead>
        <tr>
            <th>Nome</th>
            <th>CPF/CNPJ</th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
        </thead>

        <tbody>
        @foreach($users as $user)
            <tr>
                <td>{{ $user->name }}</td>
                <td>{{ $user->cpf }}</td>
                <td><a href="{{ route('client.destroy', $user->id) }}">Excluir</a></td>
                <td><a href="{{ route('client.edit', $user->id) }}">Alterar</a></td>
                <td><a href="vendas_cliente.html">Vendas</a></td>
                <td><a href="pedidos_cliente.html">Pedidos</a></td>
                <td><a href="incluir_pedido.html">Incluir Pedido</a></td>
            </tr>
        @endforeach



        </tbody>
    </table>
    <br>
    <a href="{{ route('client.include') }}">Incluir Cliente</a>
@endsection