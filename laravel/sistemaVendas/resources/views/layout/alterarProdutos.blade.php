@extends('app')
@section("content")

    @include("layout._feedback")

    <form action="{{ route("product.update", $produto->id) }}" method="post">

        {{csrf_field()}}

        <table border="0">
            <tbody>

            <tr>
                <td>Name:</td>
                <td><input type="text" name="name" value="{{$produto->name}}" size="50"/></td>
            </tr>
            <tr>
                <td>Unidade:</td>
                <td><select name="unit">
                        <option value="kg">Kg</option>
                        <option value="caixa">caixa</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Preço:</td>
                <td><input type="text" name="price" value="{{$produto->price}}" size="20"/></td>
            </tr>
            </tbody>
        </table>

        <button type="submit">Alterar</button>
        <a href="{{ route('product.list') }}">Cancelar</a>

    </form>
    <br>


@endsection