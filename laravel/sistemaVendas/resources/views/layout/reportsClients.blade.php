@extends("app")
@section("content")

    <table border="0">
        <thead>
        <tr>
            <th>Nome/Razão social</th>
            <th>CPF/CNPJ</th>
            <th>Descricao</th>
            <th>Unidade</th>
            <th>Ano/Mês</th>
            <th>Volume</th>
            <th>Faturamento</th>
        </tr>
        <tr><td colspan="7"><hr></td></tr>
        </thead>
        <tbody>
        <tr>
            <td>Metalurgica Aço Puro</td>
            <td>34.876.154/0001-65</td>
            <td>Parafuso</td>
            <td>Kg</td>
            <td>07/2015</td>
            <td>210</td>
            <td>R$ 1980,00</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>08/2015</td>
            <td>250</td>
            <td>R$ 2500,00</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>Total</td>
            <td></td>
            <td></td>
            <td>460</td>
            <td>R$ 4480,00</td>
        </tr>
        <tr><td></td><td></td><td colspan="5"><hr></td></tr>
        <tr>
            <td></td>
            <td></td>
            <td>Solda Elétrica</td>
            <td>caixa</td>
            <td>07/2015</td>
            <td>110</td>
            <td>R$ 16500,00</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>08/2015</td>
            <td>130</td>
            <td>R$ 19000,00</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>Total</td>
            <td></td>
            <td></td>
            <td>240</td>
            <td>R$ 35500,00</td>
        </tr>
        <tr><td colspan="7"><hr></td></tr>

        <tr>
            <td>ABC Confecções</td>
            <td>12.345.789/0001-45</td>
            <td>Parafuso</td>
            <td>Kg</td>
            <td>07/2015</td>
            <td>50</td>
            <td>R$ 500,00</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>08/2015</td>
            <td>40</td>
            <td>R$ 400,00</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>Total</td>
            <td></td>
            <td></td>
            <td>90</td>
            <td>R$ 900,00</td>
        </tr>
        <tr><td></td><td></td><td colspan="5"><hr></td></tr>
        <tr>
            <td></td>
            <td></td>
            <td>Solda Elétrica</td>
            <td>caixa</td>
            <td>07/2015</td>
            <td>0</td>
            <td>0</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>08/2015</td>
            <td>2</td>
            <td>R$ 300,00</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>Total</td>
            <td></td>
            <td></td>
            <td>2</td>
            <td>R$ 300,00</td>
        </tr>
        <tr><td colspan="7"><hr></td></tr>

        </tbody>
    </table>

@endsection