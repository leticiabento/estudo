@extends("app")
@section('content')

    @include("layout._feedback")

    <form action="{{ route('client.store') }}" method="POST">

        {{ csrf_field() }}
        <table border="0">
            <tbody>
            <tr>
                <td>Nome/Razão Social:</td>
                <td><input type="text" name="name" value="" size="50"/></td>
            </tr>

            <tr>
                <td>Email:</td>
                <td><input type="text" name="email" value="" size="50"/></td>
            </tr>

            <tr>
                <td>Senha:</td>
                <td><input type="password" name="password" value="" size="50"/></td>
            </tr>

            <tr>
                <td>Tipo:</td>
                <td>
                    <select name="type">
                        <option value="PF">PF</option>
                        <option value="PJ">PJ</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>CPF/CNPJ:</td>
                <td><input type="text" name="cpf" value="" size="20"/></td>
            </tr>
            <tr>
                <td>Endereço:</td>
                <td><input type="text" name="address" value="" size="60"/></td>
            </tr>
            <tr>
                <td>Número:</td>
                <td><input type="text" name="number" value="" size="8"/></td>
            </tr>
            <tr>
                <td>Complemento:</td>
                <td><input type="text" name="complement" value="" size="30"/></td>
            </tr>
            <tr>
                <td>Bairro:</td>
                <td><input type="text" name="neighborhood" value="" size="30"/></td>
            </tr>
            <tr>
                <td>UF:</td>
                <td>
                    <select name="uf">
                        <option value="RJ">RJ</option>
                        <option value="SP">SP</option>
                        <option value="MG">MG</option>
                        <option value="RS">RS</option>
                        <option value="BA">BA</option>
                        <option value="MT">MT</option>
                        <option value="MS">MS</option>
                        <option value="AM">AM</option>
                        <option value="ES">ES</option>
                        <option value="PR">PR</option>
                        <option value="SC">SC</option>
                        <option value="RN">RN</option>
                    </select>
                </td>
            </tr>
            </tbody>
        </table>

        <button type="submit" >Salvar</button>
        <a href="{{ route('client.search') }}">Cancelar</a>
    </form>
@endsection