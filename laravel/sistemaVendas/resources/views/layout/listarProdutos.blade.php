@extends('app')
@section("content")
    @include("layout._feedback")

    <table border="0">
        <thead>
        <tr>
            <th>Descrição</th>
            <th>Unidade</th>
            <th>Preço</th>
            <th></th>
            <th></th>
        </tr>
        </thead>
        <tbody>

        @foreach($produtos as $produto)
            <tr>
                <td>{{ $produto->name }}</td>
                <td>{{ $produto->unit }}</td>
                <td>R$ {{ $produto->price }}</td>
                <td><a href="{{ route('product.destroy', $produto->id) }}">Excluir</a></td>
                <td><a href="{{ route('product.edit', $produto->id) }}">Alterar</a></td>
            </tr>
        @endforeach

        </tbody>
    </table>
    <br>
    <a href="{{ route('product.include') }}">Incluir Produto</a>
@endsection