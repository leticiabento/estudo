@extends('app')
@section("content")

    @include("layout._feedback")

    <form action="{{route('client.find')}}" method="post">
        {{csrf_field()}}

        <table border="0">
            <tbody>
            <tr>
                <td>CPF/CNPJ:</td>
                <td><input type="text" name="cpf" value="" size="20"/>
                </td>
            </tr>
            <tr>
                <td>Nome/Razão Social:</td>
                <td><input type="text" name="name" value="" size="50"/>
                </td>
            </tr>
            </tbody>
        </table>

        <br>
        <button type="submit">Buscar</button>
    </form>
@endsection