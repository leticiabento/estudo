@extends('app')
@section("content")

    @include("layout._feedback")

    <form action="{{ route("product.store") }}" method="post">

        {{csrf_field()}}

        <table border="0">
            <tbody>

            <tr>
                <td>Name:</td>
                <td><input type="text" name="name" value="" size="50"/></td>
            </tr>
            <tr>
                <td>Unidade:</td>
                <td><select name="unit">
                        <option value="kg">Kg</option>
                        <option value="caixa">caixa</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Preço:</td>
                <td><input type="text" name="price" value="" size="20"/></td>
            </tr>
            </tbody>
        </table>

        <button type="submit">Salvar</button>
        <a href="{{ route('product.list') }}">Cancelar</a>

    </form>
    <br>


@endsection