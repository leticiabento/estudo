<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('app');
});

Route::get('index', 'UsersController@index')->name('index');
Route::get('clientes/pesquisar', 'UsersController@search')->name('client.search');
Route::post('clientes/buscar', 'UsersController@find')->name('client.find');
Route::get('clientes/listar', 'UsersController@list')->name('client.list');
Route::get('clientes/incluir', 'UsersController@create')->name('client.include');
Route::post('clientes/cadastrar', 'UsersController@store')->name('client.store');
Route::get('clientes/apagar/{id}', 'UsersController@destroy')->name('client.destroy');
Route::get('clientes/editar/{id}', 'UsersController@edit')->name('client.edit');
Route::post('clientes/update/{id}', 'UsersController@update')->name('client.update');

//Route::resource('clientes', 'UsersController');

Route::get('produtos/listar', 'ProductsController@list')->name('product.list');
Route::get('produtos/incluir', 'ProductsController@include')->name('product.include');
Route::post('produtos/cadastrar', 'ProductsController@store')->name('product.store');
Route::get('produtos/apagar/{id}', 'ProductsController@destroy')->name('product.destroy');
Route::get('produtos/editar/{id}', 'ProductsController@edit')->name('product.edit');
Route::post('produtos/update/{id}', 'ProductsController@update')->name('product.update');


Route::get('relatorios/venda', 'ReportsController@show')->name('reports.sale');
Route::get('relatorios/cliente', 'ReportsController@client')->name('reports.client');