<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();

Route::get('/', 'ProjectsController@index')->name('home');
Route::get('/cadastro', 'ProjectsController@create')->name('cadastro');
Route::get('/cadastroProjeto', 'HomeController@createProject')->name('cadastroProjeto');
Route::get('/meusProjetos', 'HomeController@meusProjetos')->name('meusProjetos');
Route::get('/detalhes/{id}', 'HomeController@show')->name('detalhes');
Route::get('/doar1/{id}', 'HomeController@doarUm')->name('doar1');

Route::post('/doar2', 'HomeController@doarDois')->name('doar2');
Route::get('/doar2', 'HomeController@doarDoisView')->name('doar2view');
Route::post('/doar3', 'HomeController@doarTres')->name('doar3');
Route::get('/confirme', 'HomeController@doarConfirm')->name('doarConfirm');



Route::post('/cadastro', 'ProjectsController@store')->name('cadastrar');
Route::post('/cadastroProjeto', 'ProjectsController@storeProject')->name('cadastrarProjeto');
