<?php

namespace App\Http\Controllers;

use App\Donation;
use App\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function createProject()
    {
        return view("layouts.cadastro_projeto");
    }

    public function meusProjetos()
    {
        $projects = Project::where('user_id', Auth::user()->id)->get();
        return view("layouts.meus_projetos", compact("projects"));
    }

    public function show($id)
    {

        $infos = Project::with('user')->where('id', $id)->get();
        return view("layouts.detalhe", compact("infos"));
    }

    public function doarUm($id)
    {
        return view("layouts.doar1", compact("id"));
    }

    public function doarDois(Request $request)
    {
        $infos = [
            "user_id" => $request->user_id,
            "valor" => $request->valor,
            "nome" => $request->nome,
            "numeroCartao" => $request->numeroCartao,
            "expiracao" => $request->mes . "/" . $request->ano,
            "cvn" => $request->cvn,
            "pais" => $request->pais,
            "cidade" => $request->cidade,
            "cep" => $request->cep,
            "endereco" => $request->endereco,
            "numero" => $request->numero,
            "complemento" => $request->complemento
        ];


        return view("layouts.doar2", compact("infos"));
    }

    public function doarDoisView()
    {
        return view("layouts.doar2");
    }

    public function doarTres(Request $request)
    {
        $infos = [
            "user_id" => $request->user_id,
            "valor" => $request->valor,
            "nome" => $request->nome,
            "numeroCartao" => $request->numeroCartao,
            "expiracao" => $request->expiracao,
            "cvn" => $request->cvn,
            "pais" => $request->pais,
            "cidade" => $request->cidade,
            "cep" => $request->cep,
            "endereco" => $request->endereco,
            "numero" => $request->numero,
            "complemento" => $request->complemento
        ];


        $donation = Donation::create($infos);

        if (!$donation) {
//            return redirect('/confirme')->with("feedback_error", "Erro ao doar");
        }

        return view('layouts.doar3')->with("nome", $request->nome);

    }

    public function doarConfirm()
    {
        return view('layouts.doar3');
    }

}
