<?php

namespace App\Http\Controllers;

use App\Project;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("layouts.index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("layouts.cadastro");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $imagem = $request->foto;
        $hashImagem = $imagem->hashName();


        if ($request->hasFile('foto')) {
            $up = $imagem->store('imgs');

            $infos = [
                "email" => $request->email,
                "password" => bcrypt($request->password),
                "name" => $request->name,
                "nomeConta" => $request->nomeConta,
                "foto" => $hashImagem,
                "remember_token" => str_random(10)
            ];

            if (!$up) {
                return redirect('/cadastro')->with("feedback_error", "Erro ao cadastrar a imagem");
            }

            $user = User::create($infos);

            if (!$user) {
                return redirect('/cadastro')->with("feedback_error", "Erro ao cadastrar o usuário");
            }

            return redirect('/cadastro')->with("feedback_ok", "Cadastrado com sucesso");
        }
    }


    public function storeProject(Request $request)
    {
        $img = $request->imagem;
        $video = $request->video;


        $hashImagem = $request->imagem->hashName();
        $hashVideo = $request->video->hashName();


        if ($request->hasFile('imagem') && $request->hasFile('video')) {
            $upImagem = $img->store('imgProjeto');
            $upVideo = $video->store('videoProjeto');

            if (!$upImagem) {
                return redirect('/cadastroProjeto')->with("feedback_error", "Erro ao cadastrar a imagem");
            }

            if (!$upVideo) {
                return redirect('/cadastroProjeto')->with("feedback_error", "Erro ao cadastrar o video");
            }

            $infos = [
                "nome" => $request->nome,
                "categoria" => $request->categoria,
                "pais" => $request->pais,
                "descricaoBreve" => $request->descricaoBreve,
                "imagem" => $hashImagem,
                "video" => $hashVideo,
                "dataLimite" => $request->dataLimite,
                "capitalLimite" => $request->capitalLimite,
                "descricao" => $request->descricao,
                "user_id" => Auth::user()
            ];

            $project = Project::create($infos);

            if (!$project) {
                return redirect('/cadastroProjeto')->with("feedback_error", "Erro ao cadastrar o projeto");
            }

            return redirect('/cadastroProjeto')->with("feedback_ok", "Projeto cadastrado c sucesso");

        }
        return $request;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
