<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $table = "projeto";
    protected $fillable = ["nome", "imagem", "categoria", "pais", "descricaoBreve", "video", "dataLimite", "capitalLimite", "descricao", "user_id"];

    public function donation()
    {
        return $this->belongsTo('App\Donation');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
