<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Donation extends Model
{
    protected $table = "doacao";
    protected $fillable = ["nome", "numeroCartao", "expiracao", "cvn", "cep", "endereco", "numero", "complemento", "valor", "projeto_id", "pais", "cidade"];

    public function projects()
    {
        return $this->hasMany('App\Project', 'projeto_id');
    }
}
