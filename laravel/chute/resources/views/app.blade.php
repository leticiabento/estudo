<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ChuteDeIniciante</title>
    <link rel="stylesheet" type="text/css" href="{{asset("css/bootstrap.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("css/styles.css")}}">
</head>
<body>
<header id="header">
    <div class="container">
        <h1 id="logo"><a href="{{route('home')}}" title="Chute de Iniciante">Chute de Iniciante</a></h1>
        <nav id="navegacaoPrimario">
            <ul>
                <li><a href="{{route('home')}}" title="Home">Home</a></li>
                <li><a href="categorias.html" title="Categorias">Categorias</a></li>
            </ul>
        </nav>
        <form id="formularioPesquisa" action="projetos_filtrados.html" method="get" accept-charset="utf-8">
            <div class="form-group">
                <input type="text" name="pesquisar" placeholder="Pesquisar Projetos">
                <button class="btn" type="submit"><i class="glyphicon glyphicon-search"></i></button>
            </div>
        </form>
        <nav id="navegacaoUsuario">
            <ul>
                {{--<li><a href="{{route('login')}}" title="Login">Login</a></li>--}}
                @auth
                    <div class="dropdown">
                        <li>
                            <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">
                                <div class="circle">
                                    <img src="{{asset("app/imgs")}}/{{Auth::user()->foto}}" alt="foto Perfil">
                                </div>
                                <span>{{Auth::user()->name}}</span>
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="{{route('meusProjetos')}}" title="Meus Projetos">Meus Projetos</a></li>
                                <li><a href="login.html" title="Logout">Logout</a></li>
                            </ul>
                        </li>
                    </div>
                @else
                    <li><a href="{{ route('login') }}">Login</a></li>
                    <li><a href="{{ route('register') }}">Register</a></li>
                @endauth
            </ul>
        </nav>
    </div>
</header>

@yield("content")

<script src="{{asset("js/jquery-2.2.4.js")}}"></script>
<script src="{{asset("js/bootstrap.js")}}"></script>

@stack("js")
@yield("js")

</body>
