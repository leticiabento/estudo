@extends("app")
@section("content")

    <main id="conteudo">
        <div class="container">
            <form id="formulario_doacao2" action="{{route('doar3')}}" method="POST">
                {{csrf_field()}}
                <div class="card">
                    <h3>Informações do Cartão</h3>

                    <input type="hidden" name="user_id" value="{{$infos['user_id']}}">
                    <input type="hidden" name="cvn" value="{{$infos['cvn']}}">


                    <div>
                        <small>Nome</small>
                        <span>{{$infos['nome']}}</span>
                        <input type="hidden" name="nome" value="{{$infos['nome']}}">
                    </div>
                    <div>
                        <small>Número do Cartão</small>
                        <span>{{$infos['numeroCartao']}}</span>
                        <input type="hidden" name="numeroCartao" value="{{$infos['numeroCartao']}}">

                    </div>
                    <div>
                        <small>Data de Expiração</small>
                        <span>{{$infos['expiracao']}}</span>
                        <input type="hidden" name="expiracao" value="{{$infos['expiracao']}}">

                    </div>

                    <h3>Endereço de Pagamento</h3>
                    <div>
                        <small>CEP</small>
                        <span>{{$infos['cep']}}</span>
                        <input type="hidden" name="cep" value="{{$infos['cep']}}">

                    </div>
                    <div>
                        <small>ENDEREÇO</small>
                        <span>{{$infos['pais']}}, {{$infos['cidade']}},  {{$infos['endereco']}}, {{$infos['numero']}}, {{$infos['complemento']}}</span>
                        <input type="hidden" name="pais" value="{{$infos['pais']}}">
                        <input type="hidden" name="cidade" value="{{$infos['cidade']}}">
                        <input type="hidden" name="endereco" value="{{$infos['endereco']}}">
                        <input type="hidden" name="numero" value="{{$infos['numero']}}">
                        <input type="hidden" name="complemento" value="{{$infos['complemento']}}">

                    </div>
                    <table class="table">
                        <tbody>
                        <tr>
                            <th>PREÇO TOTAL</th>
                            <td class="align-right">R${{$infos['valor']}}</td>
                            <input type="hidden" name="valor" value="{{$infos['valor']}}">
                        </tr>
                        </tbody>
                    </table>
                    <button class="btn btn-success pull-right" type="submit">CONFIRMAR</button>
                </div>

            </form>
        </div>
    </main>
@endsection