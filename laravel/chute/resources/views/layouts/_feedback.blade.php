@if(session('feedback_ok'))
    <div>
        <p>{{session('feedback_ok')}}</p>
    </div>
@endif

@if(session('feedback_error'))
    <div>
        <p>{{session('feedback_error')}}</p>
    </div>
@endif