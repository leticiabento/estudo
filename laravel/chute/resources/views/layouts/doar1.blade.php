@extends("app")
@section("content")

    <main id="conteudo">
        <div class="container">
            <form id="formulario_doacao" action="{{route('doar2')}}" method="POST" class="col-md-6">
                <h2>Escolha o valor a ser doado!</h2>

                {{csrf_field()}}
                <input type="hidden" name="user_id" value="{{$id}}">
                <div class="form-group">
                    <ul>
                        <li>
                            <label><input type="radio" name="valor" value="1">R$1,00</label>
                        </li>
                        <li>
                            <label><input type="radio" name="valor" value="5">R$5,00</label>
                        </li>
                        <li>
                            <label><input type="radio" name="valor" value="10">R$10,00</label>
                        </li>
                        <li>
                            <label><input type="radio" name="valor" value="15">R$15,00</label>
                        </li>
                        <li>
                            <label><input type="radio" name="valor" value="outros">Outros</label>
                            <div class="form-group">
                                <input class="form-control" type="text" name="valor" placeholder="0,00">
                            </div>
                        </li>
                    </ul>
                </div>

                <div class="cartao">
                    <h4>Informações do Cartão</h4>
                    <div class="form-group">
                        <label for="nome">Nome</label>
                        <input class="form-control" type="text" name="nome" id="nome">
                    </div>
                    <div class="form-group">
                        <label for="numerodocartao">Número do Cartão</label>
                        <input class="form-control" type="text" name="numeroCartao" id="numerodocartao">
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="expiracao">Expiração</label>
                                <div class="row">
                                    <div class="col-md-6">
                                        <select class="form-control" name="mes">
                                            <option value="08">08</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <select class="form-control" name="ano">
                                            <option value="2016">2016</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label for="cvn">CVN</label>
                                <input class="form-control" type="text" name="cvn">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="endereco-de-pagamento">
                    <h4>Endereço de Pagamento</h4>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="pais">País</label>
                                <select class="form-control" name="pais" id="pais">
                                    <option value="1">Brasil</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="cidade">Cidade</label>
                                <select class="form-control" name="cidade" id="cidade">
                                    <option value="1">São Paulo</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="cep">CEP</label>
                                <input class="form-control" type="text" name="cep" id="cep">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="endereco">Endereço</label>
                        <input class="form-control" type="text" name="endereco" id="endereco">
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="numero">Número</label>
                                <input class="form-control" type="text" name="numero" id="numero">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="complemento">Complemento</label>
                                <input class="form-control" type="text" name="complemento" id="complemento">
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-success btn-lg pull-right" type="submit">Doar</button>
                </div>

            </form>
        </div>
    </main>

@endsection