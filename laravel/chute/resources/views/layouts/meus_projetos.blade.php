@extends("app")
@section("content")
    <main id="conteudo">
        <div class="container">
            <section id="novidades">
                <a class="btn btn-success pull-right" href="{{route('cadastroProjeto')}}" title="Cadastrar um Projeto">Cadastrar
                    um Projeto</a>
                <h3>Meus Projetos</h3>
                <div class="row">
                    @foreach($projects as $project)
                        <div class="col-md-4">
                        <article class="projeto">
                        <div class="acoes">
                        <a class="btn btn-default btn-xs" href="cadastrar_projeto.html" title="Editar">Editar</a>
                        <a class="btn btn-danger btn-xs" href="#" title="Deletar">Deletar</a>
                        </div>
                        <a href="{{route('detalhes', $project->id)}}" title="Last Breath" class="projeto-imagem-link">
                        <figure>
                        <img src="{{asset('app/imgProjeto')}}/{{$project->imagem}}" alt="Projeto">
                        </figure>
                        </a>
                        <h4><a href="#" title="Last Breath">{{$project->name}}</a></h4>
                        <a href="#" class="nome usuario">{{Auth::user()->name}}</a>
                        <p>
                        {{$project->descricaoBreve}}
                        </p>
                        <span class="localizacao">{{$project->pais}}</span>
                        <a class="badge" href="#" title="Graphic Novels">Graphic Novels</a>
                        <div class="barra barra-porcentagem">
                        <div class="porcentagem"></div>
                        </div>
                        <div class="row">
                        <div class="col-md-3 doado">
                        <strong>142%</strong>
                        <span>alcançado</span>
                        </div>
                        <div class="col-md-3 dinheiro-ganho">
                        <strong>R$1780,00</strong>
                        <span>funded</span>
                        </div>
                        <div class="col-md-6 dias-faltantes">
                        <strong>11</strong>
                        <span>dias para acabar</span>
                        </div>
                        </div>
                        </article>
                        </div>
                    @endforeach


                </div>
            </section>
        </div>
    </main>
@endsection