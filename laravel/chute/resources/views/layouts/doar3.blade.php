@extends("app")
@section("content")
    <main id="conteudo">

        <div class="container">
            <div class="panel panel-success">
                <div class="panel-body">
                    <h2 >Doação feita com sucesso!</h2>
                    <p>
                        <strong>{{session("nome")}}</strong>, sua doação foi recebida com sucesso!
                        <a href="{{route('home')}}">Voltar para o início</a>
                    </p>
                </div>
            </div>
        </div>
    </main>
@endsection