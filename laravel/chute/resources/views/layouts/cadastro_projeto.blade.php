@extends("app")
@section("content")

    <main id="conteudo">

        <div class="container">

            @include("layouts._feedback")

            <form id="formulario_cadastrarprojeto" action="{{route("cadastrarProjeto")}}" method="POST" enctype="multipart/form-data">

                {{csrf_field()}}

                <div class="card">
                    <div class="form-group">
                        <label for="nome">Nome</label>
                        <input class="form-control" type="text" name="nome"/>
                    </div>
                    <div class="form-group">
                        <label for="TAGS">Tags</label>
                        <input class="form-control" type="text" name="categoria"/>
                    </div>
                    <div class="form-group">
                        <label for="pais">Pais</label>
                        <select class="form-control" type="text" name="pais">
                            <option value="Brasil">Brasil</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="resumo">Resumo do projeto</label>
                        <textarea class="form-control" name="descricaoBreve"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Imagem do Projeto</label>
                        <div class="area-foto">
                            <input type="file" name="imagem">
                            {{--<img src="imgs/area-imagem-projeto.svg" alt="Arraste aqui">--}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="video">Video</label>
                        <div class="area-video">
                            <input type="file" name="video">
                            {{--<img src="imgs/area-video.svg" alt="Área Vídeo">--}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="data-limite">Data Limite</label>
                        <input class="form-control" type="date" name="dataLimite"/>
                    </div>
                    <div class="form-group">
                        <label for="capital">Capital a ser Atingido</label>
                        <input class="form-control" type="text" name="capitalLimite"/>
                    </div>
                    <div class="form-group">
                        <label for="descricao">Descrição</label>
                        <textarea class="form-control descricao" name="descricao"></textarea>
                    </div>

                    <button class="btn btn-success pull-right" type="submit">CADASTRAR</button>

                </div>
            </form>
        </div>
    </main>

@endsection

@section("js")
    <script src="{{asset("assets/tinymce/tinymce.min.js")}}" charset="utf-8"></script>
    <script>
        tinymce.init({
            selector: '.descricao',
            height: 300,
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code'
            ],
            toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            // toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            content_css: [
                // '//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
                // '//www.tinymce.com/css/codepen.min.css'
            ]
        });
    </script>
    <script src="{{asset("js/jquery-2.2.4.js")}}"></script>
    <script src="{{asset("js/bootstrap.js")}}"></script>
@endsection