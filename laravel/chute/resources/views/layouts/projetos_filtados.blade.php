@extends("app")
@section("content")
    <main id="conteudo">
        <div class="container">
            <section id="novidades">
                <h2 class="page-header">Projetos com a palavra: {minha pesquisa}</h2>
                <div class="row">
                    <div class="col-md-4">
                        <article class="projeto">
                            <a href="detalhe.html" title="Last Breath" class="projeto-imagem-link">
                                <figure>
                                    <img src="imgs/projeto2.png" alt="Projeto">
                                </figure>
                            </a>
                            <h4><a href="detalhe.html" title="Last Breath">Last Breath</a></h4>
                            <a href="#" class="nome usuario">Jens K Style</a>
                            <p>
                                A 28 page comic zine collecting drawings done in only 15 minutes (each). Based on Jay Myer's 15 Minute Art Challenge.
                            </p>
                            <span class="localizacao">Portland</span>
                            <a class="badge" href="#" title="Graphic Novels">Graphic Novels</a>
                            <div class="barra barra-porcentagem">
                                <div class="porcentagem"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 doado">
                                    <strong>142%</strong>
                                    <span>alcançado</span>
                                </div>
                                <div class="col-md-3 dinheiro-ganho">
                                    <strong>R$1780,00</strong>
                                    <span>arrecadados</span>
                                </div>
                                <div class="col-md-6 dias-faltantes">
                                    <strong>11</strong>
                                    <span>dias para acabar</span>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="col-md-4">
                        <article class="projeto">
                            <a href="detalhe.html" title="Last Breath" class="projeto-imagem-link">
                                <figure>
                                    <img src="imgs/projeto2.png" alt="Projeto">
                                </figure>
                            </a>
                            <h4><a href="detalhe.html" title="Last Breath">Last Breath</a></h4>
                            <a href="#" class="nome usuario">Jens K Style</a>
                            <p>
                                A 28 page comic zine collecting drawings done in only 15 minutes (each). Based on Jay Myer's 15 Minute Art Challenge.
                            </p>
                            <span class="localizacao">Portland</span>
                            <a class="badge" href="#" title="Graphic Novels">Graphic Novels</a>
                            <div class="barra barra-porcentagem">
                                <div class="porcentagem"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 doado">
                                    <strong>142%</strong>
                                    <span>alcançado</span>
                                </div>
                                <div class="col-md-3 dinheiro-ganho">
                                    <strong>R$1780,00</strong>
                                    <span>arrecadados</span>
                                </div>
                                <div class="col-md-6 dias-faltantes">
                                    <strong>11</strong>
                                    <span>dias para acabar</span>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="col-md-4">
                        <article class="projeto">
                            <a href="detalhe.html" title="Last Breath" class="projeto-imagem-link">
                                <figure>
                                    <img src="imgs/projeto2.png" alt="Projeto">
                                </figure>
                            </a>
                            <h4><a href="detalhe.html" title="Last Breath">Last Breath</a></h4>
                            <a href="#" class="nome usuario">Jens K Style</a>
                            <p>
                                A 28 page comic zine collecting drawings done in only 15 minutes (each). Based on Jay Myer's 15 Minute Art Challenge.
                            </p>
                            <span class="localizacao">Portland</span>
                            <a class="badge" href="#" title="Graphic Novels">Graphic Novels</a>
                            <div class="barra barra-porcentagem">
                                <div class="porcentagem"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 doado">
                                    <strong>142%</strong>
                                    <span>alcançado</span>
                                </div>
                                <div class="col-md-3 dinheiro-ganho">
                                    <strong>R$1780,00</strong>
                                    <span>arrecadados</span>
                                </div>
                                <div class="col-md-6 dias-faltantes">
                                    <strong>11</strong>
                                    <span>dias para acabar</span>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="col-md-4">
                        <article class="projeto">
                            <a href="detalhe.html" title="Last Breath" class="projeto-imagem-link">
                                <figure>
                                    <img src="imgs/projeto2.png" alt="Projeto">
                                </figure>
                            </a>
                            <h4><a href="detalhe.html" title="Last Breath">Last Breath</a></h4>
                            <a href="#" class="nome usuario">Jens K Style</a>
                            <p>
                                A 28 page comic zine collecting drawings done in only 15 minutes (each). Based on Jay Myer's 15 Minute Art Challenge.
                            </p>
                            <span class="localizacao">Portland</span>
                            <a class="badge" href="#" title="Graphic Novels">Graphic Novels</a>
                            <div class="barra barra-porcentagem">
                                <div class="porcentagem"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 doado">
                                    <strong>142%</strong>
                                    <span>alcançado</span>
                                </div>
                                <div class="col-md-3 dinheiro-ganho">
                                    <strong>R$1780,00</strong>
                                    <span>arrecadados</span>
                                </div>
                                <div class="col-md-6 dias-faltantes">
                                    <strong>11</strong>
                                    <span>dias para acabar</span>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="col-md-4">
                        <article class="projeto">
                            <a href="detalhe.html" title="Last Breath" class="projeto-imagem-link">
                                <figure>
                                    <img src="imgs/projeto2.png" alt="Projeto">
                                </figure>
                            </a>
                            <h4><a href="detalhe.html" title="Last Breath">Last Breath</a></h4>
                            <a href="#" class="nome usuario">Jens K Style</a>
                            <p>
                                A 28 page comic zine collecting drawings done in only 15 minutes (each). Based on Jay Myer's 15 Minute Art Challenge.
                            </p>
                            <span class="localizacao">Portland</span>
                            <a class="badge" href="#" title="Graphic Novels">Graphic Novels</a>
                            <div class="barra barra-porcentagem">
                                <div class="porcentagem"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 doado">
                                    <strong>142%</strong>
                                    <span>alcançado</span>
                                </div>
                                <div class="col-md-3 dinheiro-ganho">
                                    <strong>R$1780,00</strong>
                                    <span>arrecadados</span>
                                </div>
                                <div class="col-md-6 dias-faltantes">
                                    <strong>11</strong>
                                    <span>dias para acabar</span>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="col-md-4">
                        <article class="projeto">
                            <a href="detalhe.html" title="Last Breath" class="projeto-imagem-link">
                                <figure>
                                    <img src="imgs/projeto2.png" alt="Projeto">
                                </figure>
                            </a>
                            <h4><a href="detalhe.html" title="Last Breath">Last Breath</a></h4>
                            <a href="#" class="nome usuario">Jens K Style</a>
                            <p>
                                A 28 page comic zine collecting drawings done in only 15 minutes (each). Based on Jay Myer's 15 Minute Art Challenge.
                            </p>
                            <span class="localizacao">Portland</span>
                            <a class="badge" href="#" title="Graphic Novels">Graphic Novels</a>
                            <div class="barra barra-porcentagem">
                                <div class="porcentagem"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 doado">
                                    <strong>142%</strong>
                                    <span>alcançado</span>
                                </div>
                                <div class="col-md-3 dinheiro-ganho">
                                    <strong>R$1780,00</strong>
                                    <span>arrecadados</span>
                                </div>
                                <div class="col-md-6 dias-faltantes">
                                    <strong>11</strong>
                                    <span>dias para acabar</span>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="col-md-4">
                        <article class="projeto">
                            <a href="detalhe.html" title="Last Breath" class="projeto-imagem-link">
                                <figure>
                                    <img src="imgs/projeto2.png" alt="Projeto">
                                </figure>
                            </a>
                            <h4><a href="detalhe.html" title="Last Breath">Last Breath</a></h4>
                            <a href="#" class="nome usuario">Jens K Style</a>
                            <p>
                                A 28 page comic zine collecting drawings done in only 15 minutes (each). Based on Jay Myer's 15 Minute Art Challenge.
                            </p>
                            <span class="localizacao">Portland</span>
                            <a class="badge" href="#" title="Graphic Novels">Graphic Novels</a>
                            <div class="barra barra-porcentagem">
                                <div class="porcentagem"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 doado">
                                    <strong>142%</strong>
                                    <span>alcançado</span>
                                </div>
                                <div class="col-md-3 dinheiro-ganho">
                                    <strong>R$1780,00</strong>
                                    <span>arrecadados</span>
                                </div>
                                <div class="col-md-6 dias-faltantes">
                                    <strong>11</strong>
                                    <span>dias para acabar</span>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="col-md-4">
                        <article class="projeto">
                            <a href="detalhe.html" title="Last Breath" class="projeto-imagem-link">
                                <figure>
                                    <img src="imgs/projeto2.png" alt="Projeto">
                                </figure>
                            </a>
                            <h4><a href="detalhe.html" title="Last Breath">Last Breath</a></h4>
                            <a href="#" class="nome usuario">Jens K Style</a>
                            <p>
                                A 28 page comic zine collecting drawings done in only 15 minutes (each). Based on Jay Myer's 15 Minute Art Challenge.
                            </p>
                            <span class="localizacao">Portland</span>
                            <a class="badge" href="#" title="Graphic Novels">Graphic Novels</a>
                            <div class="barra barra-porcentagem">
                                <div class="porcentagem"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 doado">
                                    <strong>142%</strong>
                                    <span>alcançado</span>
                                </div>
                                <div class="col-md-3 dinheiro-ganho">
                                    <strong>R$1780,00</strong>
                                    <span>arrecadados</span>
                                </div>
                                <div class="col-md-6 dias-faltantes">
                                    <strong>11</strong>
                                    <span>dias para acabar</span>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="col-md-4">
                        <article class="projeto">
                            <a href="detalhe.html" title="Last Breath" class="projeto-imagem-link">
                                <figure>
                                    <img src="imgs/projeto2.png" alt="Projeto">
                                </figure>
                            </a>
                            <h4><a href="detalhe.html" title="Last Breath">Last Breath</a></h4>
                            <a href="#" class="nome usuario">Jens K Style</a>
                            <p>
                                A 28 page comic zine collecting drawings done in only 15 minutes (each). Based on Jay Myer's 15 Minute Art Challenge.
                            </p>
                            <span class="localizacao">Portland</span>
                            <a class="badge" href="#" title="Graphic Novels">Graphic Novels</a>
                            <div class="barra barra-porcentagem">
                                <div class="porcentagem"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 doado">
                                    <strong>142%</strong>
                                    <span>alcançado</span>
                                </div>
                                <div class="col-md-3 dinheiro-ganho">
                                    <strong>R$1780,00</strong>
                                    <span>arrecadados</span>
                                </div>
                                <div class="col-md-6 dias-faltantes">
                                    <strong>11</strong>
                                    <span>dias para acabar</span>
                                </div>
                            </div>
                        </article>
                    </div>


                </div>
            </section>
        </div>
    </main>
@endsection