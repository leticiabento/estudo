@extends("app")
@section("content")
    <main id="conteudo">
        <div class="container">
            <section id="cadastro">
                <h1 id="logo"><a href="#" title="Chute de Iniciante">Chute de Iniciante</a></h1>

                @include("layouts._feedback")

                <form action="{{route('cadastrar')}}" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label>Email</label>
                        <input class="form-control" type="email" name="email" placeholder="exemplo@exemplo.com">
                    </div>
                    <div class="form-group">
                        <label>Senha</label>
                        <input class="form-control" type="password" name="password">
                    </div>
                    <div class="form-group">
                        <label>Confirmar Senha</label>
                        <input class="form-control" type="password" name="_senha">
                    </div>
                    <h3>Dados Pessoais</h3>
                    <div class="form-group">
                        <label>Nome</label>
                        <input class="form-control" type="text" name="name">
                    </div>
                    <div class="form-group">
                        <label>Nome da conta</label>
                        <input class="form-control" type="text" name="nomeConta">
                    </div>
                    <div class="form-group">
                        <label>Foto</label>
                        <input type="file" name="foto">
                        <img src="imgs/area-imagem.svg" alt="Arraste aqui">
                    </div>
                    <button class="btn btn-success pull-right btn-lg" type="submit">CADASTRAR</button>

                </form>
            </section>
        </div>
    </main>
@endsection