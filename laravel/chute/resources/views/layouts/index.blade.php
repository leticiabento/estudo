@extends("app")
@section("content")
    <main id="conteudo">
        <div class="container">
            <section id="destaque">
                <article class="projeto">
                    <a href="detalhe.html" title="Hey Baby: a comic memoir about becoming a m" class="projeto-imagem-link">
                        <figure>
                            <img src="imgs/image2.png" alt="image">
                        </figure>
                    </a>
                    <div class="texto">
                        <h3><a href="detalhe.html" title="Hey Baby: a comic memoir about becoming a m">Hey Baby: a comic memoir about becoming a m</a></h3>
                        <div class="perfil-usuario">
                            <figure>
                                <img src="imgs/profile1.png" alt="Profile">
                                <figcaption>por <a href="#" title="Breena Bard">Breena Bard</a></figcaption>
                            </figure>
                        </div>
                        <p>
                            Hey Baby is an 80 page comic memoir which tells one woman's story of pregnancy, birth and the earliest days of motherhood.
                        </p>
                        <span class="localizacao">Portland</span>
                        <span class="tags">
							<a class="badge" href="#" title="Graphic Novels">Graphic Novels</a>
							<a class="badge" href="#" title="Graphic Novels">DESIGN</a>
						</span>
                        <div class="row">
                            <div class="col-md-3 doado">
                                <strong>142%</strong>
                                <span>alcançado</span>
                            </div>
                            <div class="col-md-3 dinheiro-ganho">
                                <strong>R$1780,00</strong>
                                <span>arrecadados</span>
                            </div>
                            <div class="col-md-3 doadores">
                                <strong>62</strong>
                                <span>Colaboradores</span>
                            </div>
                            <div class="col-md-6 dias-faltantes">
                                <strong>11</strong>
                                <span>dias para acabar</span>
                            </div>
                        </div>
                    </div>
                </article>
            </section>
            <section id="novidades">
                <h3>Novidades</h3>
                <div class="row">
                    <div class="col-md-4">
                        <article class="projeto">
                            <a href="detalhe.html" title="Last Breath" class="projeto-imagem-link">
                                <figure>
                                    <img src="imgs/projeto2.png" alt="Projeto">
                                </figure>
                            </a>
                            <h4><a href="detalhe.html" title="Last Breath">Last Breath</a></h4>
                            <a href="#" class="nome usuario">Jens K Style</a>
                            <p>
                                A 28 page comic zine collecting drawings done in only 15 minutes (each). Based on Jay Myer's 15 Minute Art Challenge.
                            </p>
                            <span class="localizacao">Portland</span>
                            <a class="badge" href="#" title="Graphic Novels">Graphic Novels</a>
                            <a class="badge" href="#" title="Graphic Novels">DESIGN</a>
                            <div class="barra barra-porcentagem">
                                <div class="porcentagem"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 doado">
                                    <strong>142%</strong>
                                    <span>alcançado</span>
                                </div>
                                <div class="col-md-3 dinheiro-ganho">
                                    <strong>R$1780,00</strong>
                                    <span>arrecadados</span>
                                </div>
                                <div class="col-md-6 dias-faltantes">
                                    <strong>11</strong>
                                    <span>dias para acabar</span>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="col-md-4">
                        <article class="projeto">
                            <a href="detalhe.html" title="Last Breath" class="projeto-imagem-link">
                                <figure>
                                    <img src="imgs/projeto2.png" alt="Projeto">
                                </figure>
                            </a>
                            <h4><a href="detalhe.html" title="Last Breath">Last Breath</a></h4>
                            <a href="#" class="nome usuario">Jens K Style</a>
                            <p>
                                A 28 page comic zine collecting drawings done in only 15 minutes (each). Based on Jay Myer's 15 Minute Art Challenge.
                            </p>
                            <span class="localizacao">Portland</span>
                            <a class="badge" href="#" title="Graphic Novels">Graphic Novels</a>
                            <a class="badge" href="#" title="Graphic Novels">DESIGN</a>
                            <div class="barra barra-porcentagem">
                                <div class="porcentagem"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 doado">
                                    <strong>142%</strong>
                                    <span>alcançado</span>
                                </div>
                                <div class="col-md-3 dinheiro-ganho">
                                    <strong>R$1780,00</strong>
                                    <span>arrecadados</span>
                                </div>
                                <div class="col-md-6 dias-faltantes">
                                    <strong>11</strong>
                                    <span>dias para acabar</span>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="col-md-4">
                        <article class="projeto">
                            <a href="detalhe.html" title="Last Breath" class="projeto-imagem-link">
                                <figure>
                                    <img src="imgs/projeto2.png" alt="Projeto">
                                </figure>
                            </a>
                            <h4><a href="detalhe.html" title="Last Breath">Last Breath</a></h4>
                            <a href="#" class="nome usuario">Jens K Style</a>
                            <p>
                                A 28 page comic zine collecting drawings done in only 15 minutes (each). Based on Jay Myer's 15 Minute Art Challenge.
                            </p>
                            <span class="localizacao">Portland</span>
                            <a class="badge" href="#" title="Graphic Novels">Graphic Novels</a>
                            <a class="badge" href="#" title="Graphic Novels">DESIGN</a>
                            <div class="barra barra-porcentagem">
                                <div class="porcentagem"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 doado">
                                    <strong>142%</strong>
                                    <span>alcançado</span>
                                </div>
                                <div class="col-md-3 dinheiro-ganho">
                                    <strong>R$1780,00</strong>
                                    <span>arrecadados</span>
                                </div>
                                <div class="col-md-6 dias-faltantes">
                                    <strong>11</strong>
                                    <span>dias para acabar</span>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="col-md-4">
                        <article class="projeto">
                            <a href="detalhe.html" title="Last Breath" class="projeto-imagem-link">
                                <figure>
                                    <img src="imgs/projeto2.png" alt="Projeto">
                                </figure>
                            </a>
                            <h4><a href="detalhe.html" title="Last Breath">Last Breath</a></h4>
                            <a href="#" class="nome usuario">Jens K Style</a>
                            <p>
                                A 28 page comic zine collecting drawings done in only 15 minutes (each). Based on Jay Myer's 15 Minute Art Challenge.
                            </p>
                            <span class="localizacao">Portland</span>
                            <a class="badge" href="#" title="Graphic Novels">Graphic Novels</a>
                            <a class="badge" href="#" title="Graphic Novels">DESIGN</a>
                            <div class="barra barra-porcentagem">
                                <div class="porcentagem"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 doado">
                                    <strong>142%</strong>
                                    <span>alcançado</span>
                                </div>
                                <div class="col-md-3 dinheiro-ganho">
                                    <strong>R$1780,00</strong>
                                    <span>arrecadados</span>
                                </div>
                                <div class="col-md-6 dias-faltantes">
                                    <strong>11</strong>
                                    <span>dias para acabar</span>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="col-md-4">
                        <article class="projeto">
                            <a href="detalhe.html" title="Last Breath" class="projeto-imagem-link">
                                <figure>
                                    <img src="imgs/projeto2.png" alt="Projeto">
                                </figure>
                            </a>
                            <h4><a href="detalhe.html" title="Last Breath">Last Breath</a></h4>
                            <a href="#" class="nome usuario">Jens K Style</a>
                            <p>
                                A 28 page comic zine collecting drawings done in only 15 minutes (each). Based on Jay Myer's 15 Minute Art Challenge.
                            </p>
                            <span class="localizacao">Portland</span>
                            <a class="badge" href="#" title="Graphic Novels">Graphic Novels</a>
                            <a class="badge" href="#" title="Graphic Novels">DESIGN</a>
                            <div class="barra barra-porcentagem">
                                <div class="porcentagem"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 doado">
                                    <strong>142%</strong>
                                    <span>alcançado</span>
                                </div>
                                <div class="col-md-3 dinheiro-ganho">
                                    <strong>R$1780,00</strong>
                                    <span>arrecadados</span>
                                </div>
                                <div class="col-md-6 dias-faltantes">
                                    <strong>11</strong>
                                    <span>dias para acabar</span>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="col-md-4">
                        <article class="projeto">
                            <a href="detalhe.html" title="Last Breath" class="projeto-imagem-link">
                                <figure>
                                    <img src="imgs/projeto2.png" alt="Projeto">
                                </figure>
                            </a>
                            <h4><a href="detalhe.html" title="Last Breath">Last Breath</a></h4>
                            <a href="#" class="nome usuario">Jens K Style</a>
                            <p>
                                A 28 page comic zine collecting drawings done in only 15 minutes (each). Based on Jay Myer's 15 Minute Art Challenge.
                            </p>
                            <span class="localizacao">Portland</span>
                            <a class="badge" href="#" title="Graphic Novels">Graphic Novels</a>
                            <a class="badge" href="#" title="Graphic Novels">DESIGN</a>
                            <div class="barra barra-porcentagem">
                                <div class="porcentagem"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 doado">
                                    <strong>142%</strong>
                                    <span>alcançado</span>
                                </div>
                                <div class="col-md-3 dinheiro-ganho">
                                    <strong>R$1780,00</strong>
                                    <span>arrecadados</span>
                                </div>
                                <div class="col-md-6 dias-faltantes">
                                    <strong>11</strong>
                                    <span>dias para acabar</span>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="col-md-4">
                        <article class="projeto">
                            <a href="detalhe.html" title="Last Breath" class="projeto-imagem-link">
                                <figure>
                                    <img src="imgs/projeto2.png" alt="Projeto">
                                </figure>
                            </a>
                            <h4><a href="detalhe.html" title="Last Breath">Last Breath</a></h4>
                            <a href="#" class="nome usuario">Jens K Style</a>
                            <p>
                                A 28 page comic zine collecting drawings done in only 15 minutes (each). Based on Jay Myer's 15 Minute Art Challenge.
                            </p>
                            <span class="localizacao">Portland</span>
                            <a class="badge" href="#" title="Graphic Novels">Graphic Novels</a>
                            <a class="badge" href="#" title="Graphic Novels">DESIGN</a>
                            <div class="barra barra-porcentagem">
                                <div class="porcentagem"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 doado">
                                    <strong>142%</strong>
                                    <span>alcançado</span>
                                </div>
                                <div class="col-md-3 dinheiro-ganho">
                                    <strong>R$1780,00</strong>
                                    <span>arrecadados</span>
                                </div>
                                <div class="col-md-6 dias-faltantes">
                                    <strong>11</strong>
                                    <span>dias para acabar</span>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="col-md-4">
                        <article class="projeto">
                            <a href="detalhe.html" title="Last Breath" class="projeto-imagem-link">
                                <figure>
                                    <img src="imgs/projeto2.png" alt="Projeto">
                                </figure>
                            </a>
                            <h4><a href="detalhe.html" title="Last Breath">Last Breath</a></h4>
                            <a href="#" class="nome usuario">Jens K Style</a>
                            <p>
                                A 28 page comic zine collecting drawings done in only 15 minutes (each). Based on Jay Myer's 15 Minute Art Challenge.
                            </p>
                            <span class="localizacao">Portland</span>
                            <a class="badge" href="#" title="Graphic Novels">Graphic Novels</a>
                            <a class="badge" href="#" title="Graphic Novels">DESIGN</a>
                            <div class="barra barra-porcentagem">
                                <div class="porcentagem"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 doado">
                                    <strong>142%</strong>
                                    <span>alcançado</span>
                                </div>
                                <div class="col-md-3 dinheiro-ganho">
                                    <strong>R$1780,00</strong>
                                    <span>arrecadados</span>
                                </div>
                                <div class="col-md-6 dias-faltantes">
                                    <strong>11</strong>
                                    <span>dias para acabar</span>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="col-md-4">
                        <article class="projeto">
                            <a href="detalhe.html" title="Last Breath" class="projeto-imagem-link">
                                <figure>
                                    <img src="imgs/projeto2.png" alt="Projeto">
                                </figure>
                            </a>
                            <h4><a href="detalhe.html" title="Last Breath">Last Breath</a></h4>
                            <a href="#" class="nome usuario">Jens K Style</a>
                            <p>
                                A 28 page comic zine collecting drawings done in only 15 minutes (each). Based on Jay Myer's 15 Minute Art Challenge.
                            </p>
                            <span class="localizacao">Portland</span>
                            <a class="badge" href="#" title="Graphic Novels">Graphic Novels</a>
                            <a class="badge" href="#" title="Graphic Novels">DESIGN</a>
                            <div class="barra barra-porcentagem">
                                <div class="porcentagem"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 doado">
                                    <strong>142%</strong>
                                    <span>alcançado</span>
                                </div>
                                <div class="col-md-3 dinheiro-ganho">
                                    <strong>R$1780,00</strong>
                                    <span>arrecadados</span>
                                </div>
                                <div class="col-md-6 dias-faltantes">
                                    <strong>11</strong>
                                    <span>dias para acabar</span>
                                </div>
                            </div>
                        </article>
                    </div>


                </div>
            </section>
        </div>
    </main>
@endsection