@extends("app")
@section("content")
    <main id="conteudo">
        <div class="container">

            <section id="detalhe">
                <h2>{{$infos[0]->nome}}</h2>
                <span>por <a href="#" title="A Tecnologia irá nos salvar">{{$infos[0]->user->name}}</a></span>
                <div class="row">
                    <div class="col-md-9">
                        <video autobuffer autoloop loop controls class="video-full">
                            <source src="{{asset("app/videoProjeto")}}/{{$infos[0]->video}}">
                            {{--<object type="video/ogg" data="/media/video.oga" width="320" height="240">--}}
                                {{--<param name="src" value="/media/video.oga">--}}
                                {{--<param name="autoplay" value="false">--}}
                                {{--<param name="autoStart" value="0">--}}
                                {{--<p><a href="/media/video.oga">Download this video file.</a></p>--}}
                            {{--</object>--}}
                        </video>
                    </div>
                    <div class="col-md-3 informacoes">

                        <div class="doadores">
                            <strong>62</strong>
                            <span>Colaboradores</span>
                        </div>

                        <div class="dinheiro-ganho">
                            <strong>R$1780,00</strong>
                            <span>arrecadados de R${{$infos[0]->capitalLimite}}</span>
                        </div>

                        <div class="dias-faltantes">
                            <strong>11</strong>
                            <span>dias para acabar</span>
                        </div>

                        <a class="btn btn-lg btn-success" href="{{route('doar1', $infos[0]->id)}}" title="Doar para este Projeto">Doar para este Projeto</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-9">
                        <div class="sobre-projeto">
                            <h3 class="page-header	">Sobre o projeto</h3>
                            {!! $infos[0]->descricao !!}

                        </div>
                    </div>
                    <div class="col-md-3">

                    </div>
                </div>

            </section>
        </div>
    </main>
@endsection