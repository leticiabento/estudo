var segundo = 0, minuto = 0;
var carta;
var rodada = 0;
var card;

$(document).ready(function () {
    $(".reveal-modal").css('visibility', 'visible');
    $(".reveal-modal").css('display', 'block');
});

$("#btnStart").click(function() {
    $(".reveal-modal").css('visibility', 'hidden');
    $(".reveal-modal").css('display', 'none');


    if($(".card").hasClass('face-down')) {
        $(".card").removeClass('face-down');

        setTimeout(function() {
            cartas();
            tempo();
        }, 1000);
    }


});

function tempo() {
    setInterval(function() {
        segundo++;
        if(segundo > 59) {
            segundo = 0;
            minuto++;

            if(minuto < 10) {
                minuto = '0' + minuto;
            }
        }

        if(segundo < 10) {
            segundo = '0' + segundo;
        }

        $("#timer").text(minuto + ':' + segundo);
    }, 1000);
}

function cartas() {
    $(".card").addClass('face-down');
    rodada = true;

    $(".card").click(function () {
        rodada++;
        $(this).removeClass('face-down');

        card = $(this).attr('class');
        console.log(card !== card);

        // if(!card) {
        //     console.log(rodada);
        //
        //     if(rodada === 3) {
        //         $(".card").addClass('face-down');
        //         rodada = 0;
        //         console.log(rodada);
        //
        //     }
        // }

    })

}