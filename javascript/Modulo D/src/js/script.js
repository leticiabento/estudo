$(function () {
    var index = 1;
    var elementos = $(".slide-elemento");
    var tempo = 0;
    var bolinha = 1;
    var id;
    pagers();

    setInterval(function () {
        tempo++;

        if (tempo === 5) {
            slideShow();
        }

    }, 1000);

    $("#anterior").click(function () {
        slideShow('prev');
    });

    $("#proximo").click(function () {
        slideShow();
    });

    $(".bola").click(function () {
        id = parseInt($(this).index()) + 1;
        slideShow(null, id);
    });

    function slideShow(status, id) {
        tempo = 1;
        (status === 'prev') ? index-- : index++;

        if (index > elementos.length) {
            index = 1
        }

        if (index === 0) {
            index = 3;
        }

        if (id) {
            index = id;
        }

        $(".bola").removeClass('borda');

        for (var i = 0; i < elementos.length; i++) {
            var slideAtual = 'slide' + index;

            if (elementos[i].getAttribute('id') === slideAtual) {
                elementos[i].style.display = 'block';
                $(".bola").eq(i).addClass('borda');
            } else {
                elementos[i].style.display = 'none';
            }
        }
    }

    function pagers() {
        for (var i = 0; i < elementos.length; i++) {
            var bolinhas = $("<button />");
            bolinhas.attr({
                "class": "bola"
            });

            $(".bolinhas").append(bolinhas);

            bolinha++;
        }

        $(".bola").eq(0).addClass('borda');
    }

    $("#leituraA").click(function() {
        $(this).remove();
        $("section.aboutLink").css('height', 'auto');
        // $("#leituraAbout").css('height', 'auto');
        $("#leituraAbout.hidden-text").animate({
            'opacity': '1',
            'height' : '273px'
        })
    });

    $("#leitura41").click(function() {
        $(this).remove();
        $("#leituraCompetition41.hidden-text").animate({
            'opacity': '1',
            'height' : '230px'
        })
    });

    $("#leitura40").click(function() {
        $(this).remove();
        $("#leituraCompetition40.hidden-text").animate({
            'opacity': '1',
            'height' : '150px'
        })
    });


    $(".competitions li").on('click', function () {
        // console.log($(this).children().eq(1));

        if($(this).children().eq(1).css('display') === 'block') {
            $(this).children().eq(1).css("display", "none");
        } else {
            $(this).children().css("display", "block");
        }
    });


    $(window).scroll(function () {
        var page = pageYOffset;

        if (page > 100) {
            $(".btn-subir").css("display", "block");
        } else {
            $(".btn-subir").css("display", "none");
        }

    })


    $(document).on("click", "a", function (el) {
        el.preventDefault();
        var target = $(this).attr('href');
        var offset = $(target).offset().top;

        $("html, body").animate({
            scrollTop: offset
        }, "slow")

    });

    $(".btn-subir").click(function () {

        $("html, body").animate({
            scrollTop: 0
        }, "slow");
    });

    document.addEventListener("scroll", parallax, false);

    function parallax() {

        var posWindow = Math.floor(pageYOffset / 15);
        // console.log(posWindow);

        $(".aboutLink").css('background-position-y', posWindow + "%");
        $(".competition41Link").css('background-position-y', -posWindow + "%");
        $(".competition40Link").css('background-position-y', posWindow + "%");
    }

    $("input[list='competidores']").on('keydown', function () {
        $(".competitions > li").css('display', 'none');
        $(".resultLink .c").css("color", "#FFF");
        $(".resultLink .c").text('nenhum competidor encontrado');
        var nomes = $(".competitions li ul li");
        var valor = $(this).val();

        for (var i = 0; i < nomes.length; i++) {

            var nome = $(nomes[i]);


            if (nome.text().indexOf(valor) != -1) {
                $($(nome).parent().parent()).css('display', 'block');
                $(".resultLink .c").css("color", "#182744");
            }




        }

    })
})
;